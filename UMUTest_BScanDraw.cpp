//---------------------------------------------------------------------------

// #pragma hdrstop

#include "UMUTest_BScanDraw.h"


// #pragma package(smart_init)

/*
       cChannelsTable* Tbe;
       cDeviceConfig* Cfg;
       cJointTestingReport* Rep;
       std::vector < cBScanLines* > BScanLines;
*/

cBScanDraw::cBScanDraw(cChannelsTable* Table, cDeviceConfig* Config, cJointTestingReport* Report,cDeviceCalibration* Calibration)
{
   Tbl = Table;
   Cfg = Config;
   Rep = Report;
   Calib = Calibration;
   //Modifyed by KirillB (bug)
   Th = 0;
   //GroupShift �� ���������������!
   memset(GroupShift,0,sizeof(GroupShift));
   CursorSysCrd = 0;
   CursorBScanLinesIdx = 0;
   CursorBScanLinesSubIdx = 0;
   bCursorEnabled = false;

}

cBScanDraw::~cBScanDraw(void)
{
    //
}

void cBScanDraw::ClearBScanLines()
{
	BScanLinesList.clear();
}

void cBScanDraw::AddBScanLine(cBScanLines* BScanLines)
{
   BScanLinesList.push_back(BScanLines);
}

void cBScanDraw::Prepare(void)
{
	for (int idx = 0; idx < MaxLinkData; idx++) LinkData[idx].Used = false;

	//int t=BScanLinesList.size();
	//Added by KirillB
	memset(LinkData,0,MaxLinkData*sizeof(LinkDataItem));

	int Shift_CID;
	for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
	{
        for (int x = 0; x < 6; x++)
            for (int y = 0; y < 6; y++)
				if (BScanLinesList[BScanLines_idx]->BtnVisible[x][y])
                {
                    Shift_CID = BScanLinesList[BScanLines_idx]->BtnId[x][y] - AutoconStartCID;
					LinkData[Shift_CID].Used = true;
                    LinkData[Shift_CID].Enabled = BScanLinesList[BScanLines_idx]->BtnBtnState[x][y];
                    LinkData[Shift_CID].Rt = BScanLinesList[BScanLines_idx]->LineRect[y];
					LinkData[Shift_CID].Buffer = BScanLinesList[BScanLines_idx]->Buffer;
					LinkData[Shift_CID].Color =  BScanLinesList[BScanLines_idx]->BtnChColor[x][y];
					LinkData[Shift_CID].drawType = BScanLinesList[BScanLines_idx]->LineDrawType[y];
					Tbl->ItemByCID(BScanLinesList[BScanLines_idx]->BtnId[x][y], &LinkData[Shift_CID].ChDesc);
                }
	}
}

bool cBScanDraw::GetSysCoord(int X, int Y, int* SysCoord, Shift_CID_List* Shift_CID, int* Shift_CID_Cnt, BScan_Color_List *BScanColor)
{
    int SysCrd;
    int BScanLine;
	int Shift_CID_;


	for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        BScanLinesList[BScanLines_idx]->DrawCursor = False;

	for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        if (BScanLinesList[BScanLines_idx]->Used)
        {
            if (BScanLinesList[BScanLines_idx]->GetSysCoord(X, Y, Rep->Header.MaxSysCoord, &SysCrd, &BScanLine))
            {
                *Shift_CID_Cnt = 0;
                for (int x = 0; x < BScanLinesList[BScanLines_idx]->BtnColumnCount * 2; x++)
                {
                    Shift_CID_ = BScanLinesList[BScanLines_idx]->BtnId[x][BScanLine]; // �������� ���� �� �������� Shift_CID
                    if (Shift_CID_ != 0)
                    {
                        Shift_CID_ = Shift_CID_ - AutoconStartCID;
                        if (LinkData[Shift_CID_].Enabled)
                        {
                           int idx = *Shift_CID_Cnt;
                           (*Shift_CID)[(*Shift_CID_Cnt)] = Shift_CID_;
                           (*BScanColor)[(*Shift_CID_Cnt)] = BScanLinesList[BScanLines_idx]->BtnChColor[x][BScanLine];
                           (*Shift_CID_Cnt)++;
                        }
                    }
                }
                *SysCoord = SysCrd;
                return true;
            }
        }
    return false;
}

void cBScanDraw::SetCursorXY(bool bEnable, int X, int Y, TColor col)
{
    int SysCrd;
    int BScanLine;

    for (unsigned int BScanLines_idx = 0; BScanLines_idx < BScanLinesList.size(); BScanLines_idx++)
        if (BScanLinesList[BScanLines_idx]->Used)
        {
            if (BScanLinesList[BScanLines_idx]->GetSysCoord(X, Y, Rep->Header.MaxSysCoord, &SysCrd, &BScanLine))
            {
                SetCursorSC( bEnable,SysCrd, BScanLines_idx, BScanLine, col);
                return;
            }
        }
}

void cBScanDraw::SetCursorSC(bool bEnable, int SysCoord, int BScanLines_idx,int BScanLines_sub_idx, TColor col)
{
    CursorSysCrd = SysCoord;
    if(BScanLines_idx != -1)
        CursorBScanLinesIdx = BScanLines_idx;
    if(BScanLines_sub_idx != -1)
        CursorBScanLinesSubIdx = BScanLines_sub_idx;
    bCursorEnabled = bEnable;

    Draw(false);
    return;
};

int HLSMAX = 240;
int RGBMAX = 255;
int UNDEFINED;
int R, G, B; //  �����
float Magic1, Magic2;

float HueToRGB(float n1, float n2, float hue)
{
  if (hue < 0) hue = hue + HLSMAX;
  if (hue > HLSMAX) hue = hue - HLSMAX;
  if (hue < (HLSMAX/6))
  {
     return ( n1 + (((n2-n1)*hue+(HLSMAX/12))/(HLSMAX/6)) );
  }
  else if (hue < (HLSMAX/2)) return n2;
       else if (hue < ((HLSMAX*2)/3)) return (n1 + (((n2-n1)*(((HLSMAX*2)/3)-hue)+(HLSMAX/12))/(HLSMAX/6)));
            else return n1;
}

TColor HLStoRGB(int H, int L, int S)
{
    UNDEFINED = (HLSMAX * 2) / 3;

    if (S == 0)
    {
       B = (L*RGBMAX)/HLSMAX ;
       R = B;
       G = B;
    }
    else
    {
        if (L <= (HLSMAX/2)) Magic2 = (L*(HLSMAX + S) + (HLSMAX/2))/HLSMAX;
                             else Magic2 = L + S - ((L*S) + (HLSMAX/2))/HLSMAX;
        Magic1 = 2*L-Magic2;
        R = (HueToRGB(Magic1,Magic2,H+(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
        G = (HueToRGB(Magic1,Magic2,H)*RGBMAX + (HLSMAX/2)) / HLSMAX ;
        B = (HueToRGB(Magic1,Magic2,H-(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
    }
    if (R<0) R=0; if (R>RGBMAX) R = RGBMAX;
    if (G<0) G=0; if (G>RGBMAX) G = RGBMAX;
    if (B<0) B=0; if (B>RGBMAX) B = RGBMAX;
    return TColor(R + (G << 8) + (B << 16));
}

//---------------------------------------------------------------------------

void cBScanDraw::DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl)
{
    if (!Sgl) return;

	int DelayHeight = LinkData[Channel].ChDesc.cdBScanGate.gEnd - LinkData[Channel].ChDesc.cdBScanGate.gStart;
	int Height = LinkData[Channel].Rt.Height();
    int aidx = 11;
    bool Flg = false;

    //sScanChannelDescription ChDesc;
	/*if(Cfg->getFirstSChannelbyID(Channel + AutoconStartCID, &ChDesc) == -1)
	{
		LOGERROREX(LOGCHANNEL_CHECK,"Wrong channel id: %d!",Channel + AutoconStartCID);
		return;
	}*/

    //GroupShift �� ������������
    //SysCoord = SysCoord + GroupShift[ChDesc.StrokeGroupIdx];

    //Update channels gates
    int stGate = 0;
    int edGate = 0;

    //Updating channel gates - if channel draws as LINEDRAW_SMALL_LINE
    if(LinkData[Channel].drawType == LINEDRAW_SMALL_LINE)
    {
        for(int i = 0; i < 9; i++)
        {
            if(channels_gates_temp[i][0] == LinkData[Channel].ChDesc.id)
            {
                stGate = channels_gates_temp[i][1];
                edGate = channels_gates_temp[i][2];
            }
        }
    }

    //��� �� �� �������� �� ������� ������� ���������
    HRGN ClipRgn;
    if( LinkData[Channel].Used )
    {
        ClipRgn = ::CreateRectRgn(LinkData[Channel].Rt.left,LinkData[Channel].Rt.top,
                                    LinkData[Channel].Rt.right,LinkData[Channel].Rt.bottom);
        ::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,ClipRgn);
    }

	if ((LinkData[Channel].Used) && (LinkData[Channel].Enabled))
        for (int idx = 0; idx < Sgl->Count; idx++)
            if ((Sgl->Signals[idx].Delay >= LinkData[Channel].ChDesc.cdBScanGate.gStart) &&
                        (Sgl->Signals[idx].Delay <= LinkData[Channel].ChDesc.cdBScanGate.gEnd))
            {
                //�������� - ���� ������ ������������� ��� LINEDRAW_SMALL_LINE
                    //�� �� �������� ���� �� �� � ������
                if(LinkData[Channel].drawType == LINEDRAW_SMALL_LINE)
                {
                    if((Sgl->Signals[idx].Delay < stGate) ||    //���� ������ �� � ������
                        (Sgl->Signals[idx].Delay > edGate))
                        continue;
                }

                //�������� - ������ ���� ������
				Flg = false;
                for (int aidx = 0; aidx < 24; aidx++)
                    if (Sgl->Signals[idx].Ampl[aidx] >= Th)
                    {
                        Flg = true;
                        break;
                    }

                //���� ���� ������ - ������
                if (Flg)
                {
                    x = LinkData[Channel].Rt.Left + (LinkData[Channel].Rt.Right - LinkData[Channel].Rt.Left) * SysCoord / Rep->Header.MaxSysCoord;
					//y = LinkData[Channel].Rt.Bottom - float(Sgl->Signals[idx].Delay * Height)/255.f;// * float(Sgl->Signals[idx].Delay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);
                    y = LinkData[Channel].Rt.Bottom - float(Height) * float(Sgl->Signals[idx].Delay - LinkData[Channel].ChDesc.cdBScanGate.gStart) / float(DelayHeight);

                    int pointHalfHeight = ceil(128.f / float(DelayHeight));

					LinkData[Channel].Buffer->Canvas->Brush->Color = LinkData[Channel].Color;

					if(LinkData[Channel].drawType == LINEDRAW_STD)
					{
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x-1, y-pointHalfHeight, x+1, y+pointHalfHeight));
					}
					else if( LinkData[Channel].drawType == LINEDRAW_SMALL_LINE )
					{
						int offset = 3;
						LinkData[Channel].Buffer->Canvas->FillRect(Rect(x - 1, LinkData[Channel].Rt.Top + offset, x + 1, LinkData[Channel].Rt.Bottom - offset));
                    }
                }
            }

    if( LinkData[Channel].Used )
    {
        ::SelectClipRgn(LinkData[Channel].Buffer->Canvas->Handle,NULL);
        DeleteObject(ClipRgn);
    }
}

int cBScanDraw::SysCoordToScreen(int SysCoord,CID Channel)
{
    return LinkData[Channel].Rt.Left + (LinkData[Channel].Rt.Right - LinkData[Channel].Rt.Left) * SysCoord / Rep->Header.MaxSysCoord;
};

void cBScanDraw::Draw(bool NewItemFlag)
{
	PsScanSignalsOneCrdOneChannel sgl;
    int SysCoord = 0;
    CID ChannelId = 0;
    bool Result;

    //Update channels gates (����� �� �������� GetStGate � getFirstSChannelbyID �� ������ ���������� - ������ ��� ���)
    sScanChannelDescription scDesc;
    for (int i = 0; i < MaxLinkData; i++)
		if (LinkData[i].drawType == LINEDRAW_SMALL_LINE)
        {
            if(Cfg->getFirstSChannelbyID(LinkData[i].ChDesc.id, &scDesc))
            {
                channels_gates_temp[scDesc.BScanGroup][0] = LinkData[i].ChDesc.id;
                channels_gates_temp[scDesc.BScanGroup][1] =
                            Calib->GetStGate(dsNone, LinkData[i].ChDesc.id, 0);
                channels_gates_temp[scDesc.BScanGroup][2] =
                            Calib->GetEdGate(dsNone, LinkData[i].ChDesc.id, 0);
            }
        }

    if (!NewItemFlag)
    {
        for (int i = 0; i < MaxLinkData; i++)
			if (LinkData[i].Buffer)
			{
                LinkData[i].Buffer->Canvas->Brush->Color = clWhite;
                LinkData[i].Buffer->Canvas->FillRect(LinkData[i].Rt);
            }

		sgl = Rep->GetFirstScanSignals(SysCoord, ChannelId, &Result);
        if (Result) DrawSignals(SysCoord, ChannelId, sgl);
        while (true)
        {
            sgl = Rep->GetNextScanSignals(&SysCoord, &ChannelId, &Result);
            if (!Result) break;
            DrawSignals(SysCoord, ChannelId, sgl);
        }
    }
    else
    {
        int SaveIdx = Rep->NewItemListIdx;
        for (int i = 0; i < SaveIdx; i++)
        {
            sgl = Rep->GetScanSignals(Rep->NewItemList[i].Crd, Rep->NewItemList[i].Ch);
            DrawSignals(Rep->NewItemList[i].Crd, Rep->NewItemList[i].Ch, sgl);
        }
        Rep->ClearNewItemList(SaveIdx);
    }

    if(bCursorEnabled)
    {
        TRect rect = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx];
        int top = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx].top;
        int bottom = BScanLinesList[CursorBScanLinesIdx]->LineRect[CursorBScanLinesSubIdx].bottom;
        TBitmap* buffer = BScanLinesList[CursorBScanLinesIdx]->Buffer;

        TPoint triangle[4];
        const int triRadius = 4;

        int sysPosX = rect.Left + (rect.Right - rect.Left) * CursorSysCrd / Rep->Header.MaxSysCoord;

        buffer->Canvas->MoveTo(sysPosX, top);
        buffer->Canvas->LineTo(sysPosX, bottom);

        buffer->Canvas->Brush->Color = clBlack;
        buffer->Canvas->Brush->Style = bsSolid;

        triangle[0].x = sysPosX - triRadius;
        triangle[0].y = top;
        triangle[1].x = sysPosX + triRadius;
        triangle[1].y = top;
        triangle[2].x = sysPosX;
        triangle[2].y = top + triRadius*2;
        triangle[3].x = sysPosX - triRadius;
        triangle[3].y = top;
        buffer->Canvas->Polygon(triangle,3);

        triangle[0].x = sysPosX - triRadius;
        triangle[0].y = bottom;
        triangle[1].x = sysPosX + triRadius;
        triangle[1].y = bottom;
        triangle[2].x = sysPosX;
        triangle[2].y = bottom - triRadius*2;
        triangle[3].x = sysPosX - triRadius;
        triangle[3].y = bottom;
        buffer->Canvas->Polygon(triangle,3);
    }

	for (int i = 0; i < MaxLinkData; i++)
        if (LinkData[i].Buffer)
        {
            LinkData[i].Buffer->Canvas->MoveTo((LinkData[i].Rt.left + LinkData[i].Rt.right) / 2, LinkData[i].Rt.Top);
            LinkData[i].Buffer->Canvas->LineTo((LinkData[i].Rt.left + LinkData[i].Rt.right) / 2, LinkData[i].Rt.Bottom);
			LinkData[i].Buffer->Canvas->Brush->Style = bsClear;

			LinkData[i].Buffer->Canvas->Rectangle(LinkData[i].Rt);
        }
}

