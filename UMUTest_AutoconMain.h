//---------------------------------------------------------------------------

#ifndef UMUTest_AutoconMainH
#define UMUTest_AutoconMainH

//---------------------------------------------------------------------------
#include "UMUsTest.inc"
#include "Device.h"
#include "prot_umu_lan.h"
#include "EventManager.h"
#include "EventManager_Win.h"
#include "DeviceConfig_Autocon.h"
#include "DeviceConfig_Avk31.h"
#include "DeviceConfig_BUMTEST.h"
#include "DeviceConfig_DB_Lan.h"
#include "DeviceConfig_DB_Can.h"
#include "DeviceConfig_HeadScan_Test.h"
#include "DeviceConfig_HeadScan.h"
#include "DeviceConfig_Av15_usbcan.h"
#include "DeviceConfig_ManualSet.h"
#include "DeviceConfig_Avk31E.h"
#include "UMUTest_DataStorage.h"
#include "threadClassList_Win.h"
#include "DeviceCalibration.h"
#include "CriticalSection_Win.h"
#include "Platforms.h"
#include "TickCount.h"
#include "idatatransfer.h"
#include "UMUTest_BScanLines.h"
#include "UMUTest_BScanDraw.h"
//#include "cavtk2.h"
#include "prot_umu_lan.h"
//#include "AController.h"
#include "UMUTest_TestThreadUnit.h"
//#include "DataContainer.h"

#include <Vcl.Dialogs.hpp>


//Added by KirillB
#include "UMUTest_Utils.h"

#define PORT_out 43000
#define PORT_in  43001

//---------------------------------------------------------------------------

class cDeviceThread;

struct RegCrdParams
{
    int ChannelGroupIndex;
    int RegLength;
    int StartCoord;
    bool Used;
};

class cTestThread;  // ����������� ��. � TestThreadUnit.h

class cAutoconMain
{
    public:

    int dip12maxdiff;
    int dip34maxdiff;
	BOOL CtrlWork;
	BOOL UMU_Connection_OK;
	BOOL QryThreadFlag;
//	int WorkCycle;
//  int AlignCycle;
    RegCrdParams RegCrdParamsList[15];

	//Added by KirillB
	CHTMLLogger logger;
	volatile int XSysCrd_Stored[2];
	volatile int XSysDir_Stored[2];
	volatile int XSysCrd_Transformed[2];

//	cIDataTransfer *DT;
	cDevice *DEV;
//    cDataContainer DataCnt*;
	cChannelsTable *Table;
//    cDeviceConfig_Avk31 *Config;
    cDeviceConfig *Config;
    cDeviceConfig *Config2;
	cDeviceCalibration *Calibration;
	cEventManagerWin *DeviceEventManage;
	cEventManagerWin *UMUEventManage;
	cThreadClassList_Win *threadList;
//    cJointTestingReport *Rep;
	cCriticalSectionWin *CS;
	cDeviceThread *devt;
//    cavtk *ctrl; // -
//	cAController *ac;
//    cTestThread *cavtk_test;
    int ControlMode; // 1 - �����, 2 - ����, 3 - ���������;
//    int ReportFileType; // 1 - �����, 2 - ����, 3 - ���������; 4 - �������� ������ �����;
    bool SkipRep;

    cAutoconMain(int ConfigIndex);
    ~cAutoconMain(void);
    bool OpenConnection(int ModeIdx, CallBackProcPtr cbf);
    void SetMode(int Mode_);
//    void SetReportFileType(int Type);
    int GetMode(void);
    void CleanupEventMgr();
    void Create(ConfigIndex);
    void Release();
};


// --- DeviceThread -----------------------------------------------------------

//Added by KirillB
#define DEVTHREAD_LIST_SIZE 2048
#define TextLog_def

union uPtrListPointer
{
    void* pVoid;
    PtDEV_AScanMeasure pAScanMeasure;
    PtDEV_AScanHead pAScanHead;
    PtDEV_BScan2Head pBScan2Head;
    PtDEV_AlarmHead pAlarmHead;
    PtDEV_MetalSensorData pMetalSensorData;
    PtDEV_PathStepData pPathStepData;
    PtDEV_A15ScanerPathStepData pA15ScanerPathStepData;
    PtUMU_AScanData pAScanData;
    PtUMU_BScanData pBScanData;
    PtUMU_AlarmItem pAlarmItem;
    PtDEV_SignalSpacing pSignalSpacing;
    PtDEV_Defect53_1 pDefect53_1;
    PtDEV_Packet pBScanPacket;
    PtDEV_BottomSignalAmpl pBottomSignalAmpl;
    unsigned int dDeviceSpeed;

};

struct sPtrListItem
{
    sPtrListItem()
    {
        Ptr1.pVoid = NULL;
        Ptr2.pVoid = NULL;
        Type = NULL;
    };

    explicit sPtrListItem(uPtrListPointer Ptr1, uPtrListPointer Ptr2, EventDataType Type)
    {
        this->Ptr1 = Ptr1;
        this->Ptr2 = Ptr2;
        this->Type = Type;
    };

    explicit sPtrListItem(sPtrListItem& other)
    {
        this->Ptr1 = other.Ptr1;
        this->Ptr2 = other.Ptr2;
        this->Type = other.Type;

        other.Ptr1.pVoid = NULL;
        other.Ptr2.pVoid = NULL;
        other.Type = NULL;
    };

    void operator = (sPtrListItem& other)
    {
        free();

        this->Ptr1 = other.Ptr1;
        this->Ptr2 = other.Ptr2;
        this->Type = other.Type;

        other.Ptr1.pVoid = NULL;
        other.Ptr2.pVoid = NULL;
        other.Type = NULL;
    };

    void free()
    {
        _delPtrs(Ptr1,Ptr2);

        Type = NULL;
    };

    ~sPtrListItem()
    {
        free();
    };

    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType Type;

private:
    void _delPtrs(uPtrListPointer& ptr1,uPtrListPointer& ptr2)
    {
        switch(Type)
        {
        case edAScanMeas:
            if(ptr1.pAScanMeasure)
                delete ptr1.pAScanMeasure;
            break;
        case edAScanData:
        case edTVGData:
            if(ptr1.pAScanHead)
                delete ptr1.pAScanHead;
            if(ptr2.pAScanData)
                delete ptr2.pAScanData;
            break;
        case edAlarmData:
            if(ptr1.pAlarmHead)
                delete ptr1.pAlarmHead;
           // if(ptr2.pAlarmItem)
           //     delete ptr2.pAlarmItem;
            break;
        case edBScan2Data:
        case edMScan2Data:
            if(ptr1.pBScan2Head)
                delete ptr1.pBScan2Head;
          //  if(ptr2.pBScanData)
          //      delete ptr2.pBScanData;
            break;
        case edMSensor:
            if(ptr1.pMetalSensorData)
                delete ptr1.pMetalSensorData;
            if(ptr2.pMetalSensorData)
                delete ptr2.pMetalSensorData;
            break;

        case edSignalSpacing:
            if(ptr1.pSignalSpacing)
                delete ptr1.pSignalSpacing;
            break;

        case edDefect53_1:
            if(ptr1.pDefect53_1)
                delete ptr1.pDefect53_1;
            break;

        case edSignalPacket:
            if(ptr1.pBScanPacket)
                delete ptr1.pBScanPacket;
            break;

        case edBottomSignalAmpl:
            if(ptr1.pBottomSignalAmpl)
                delete ptr1.pBottomSignalAmpl;
            break;

        case edPathStepData:
            if(ptr1.pPathStepData)
                delete ptr1.pPathStepData;
            break;

        case edA15ScanerPathStepData:
            if(ptr1.pA15ScanerPathStepData)
                delete ptr1.pA15ScanerPathStepData;
            break;

       case edDeviceSpeed:
            break;


        default:
            if(ptr1.pVoid)
                delete ptr1.pVoid;
            if(ptr2.pVoid)
                delete ptr2.pVoid;
        };
        ptr1.pVoid = NULL;
        ptr2.pVoid = NULL;
    };
    sPtrListItem(const sPtrListItem&);
    //void operator = (const sPtrListItem& other);
};

class cDeviceThread : public TThread
{
//    TForm2 * frm;
//    TDrawThread * dth;
    DWORD DataID;
    void __fastcall Execute(void);
    cAutoconMain *main;
    //void Inc_Ptr_List_Put_Idx(void);

    #ifdef TextLog_def
    TStringList* TextLog;
    #endif

public:
    bool EndWorkFlag;
    int OutTick;
    int InTick;

    ringbuf<sPtrListItem> Ptr_List;

    __fastcall cDeviceThread(cAutoconMain *main_);
    __fastcall ~cDeviceThread(void);

};

extern TStringList* TextLog;

#endif
