﻿object MainForm: TMainForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  BorderWidth = 3
  Caption = 'Demo LanUMU'
  ClientHeight = 931
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShortCut = FormShortCut
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 431
    Top = 0
    Width = 562
    Height = 931
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    ActivePage = tsAKTest
    Align = alClient
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MultiLine = True
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 0
    OnChange = PageControlChange
    object AScanSheet: TTabSheet
      Caption = #1054#1094#1077#1085#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object ControlsPanel: TPanel
        Left = 337
        Top = 41
        Width = 217
        Height = 844
        Align = alRight
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object NumPadPanel: TPanel
          Left = 0
          Top = 644
          Width = 217
          Height = 200
          Align = alBottom
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = False
          object SpeedButton10: TSpeedButton
            Tag = 7
            Left = 4
            Top = 3
            Width = 49
            Height = 46
            Caption = '7'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton11: TSpeedButton
            Tag = 8
            Left = 57
            Top = 3
            Width = 49
            Height = 46
            Caption = '8'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton20: TSpeedButton
            Tag = 9
            Left = 110
            Top = 3
            Width = 49
            Height = 46
            Caption = '9'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton21: TSpeedButton
            Tag = 13
            Left = 163
            Top = 65
            Width = 49
            Height = 60
            Caption = 'DEL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton26: TSpeedButton
            Tag = 4
            Left = 4
            Top = 52
            Width = 49
            Height = 46
            Caption = '4'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton27: TSpeedButton
            Tag = 5
            Left = 57
            Top = 52
            Width = 49
            Height = 46
            Caption = '5'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton28: TSpeedButton
            Tag = 6
            Left = 110
            Top = 52
            Width = 49
            Height = 46
            Caption = '6'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton30: TSpeedButton
            Tag = 1
            Left = 4
            Top = 100
            Width = 49
            Height = 46
            Caption = '1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton31: TSpeedButton
            Tag = 2
            Left = 57
            Top = 100
            Width = 49
            Height = 46
            Caption = '2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton32: TSpeedButton
            Tag = 3
            Left = 110
            Top = 100
            Width = 49
            Height = 46
            Caption = '3'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton33: TSpeedButton
            Tag = 12
            Left = 163
            Top = 128
            Width = 49
            Height = 66
            Caption = 'OK'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton34: TSpeedButton
            Left = 4
            Top = 148
            Width = 49
            Height = 46
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton35: TSpeedButton
            Tag = 10
            Left = 57
            Top = 148
            Width = 49
            Height = 46
            Caption = '-'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton36: TSpeedButton
            Tag = 11
            Left = 110
            Top = 148
            Width = 49
            Height = 46
            Caption = '.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
          object SpeedButton29: TSpeedButton
            Tag = 14
            Left = 163
            Top = 3
            Width = 49
            Height = 60
            Caption = 'CLR'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            OnClick = NumPadClick
          end
        end
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 217
          Height = 644
          HorzScrollBar.Visible = False
          VertScrollBar.Position = 524
          VertScrollBar.Smooth = True
          VertScrollBar.Style = ssFlat
          VertScrollBar.Tracking = True
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object Panel7: TPanel
            Left = 0
            Top = 1014
            Width = 196
            Height = 95
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 0
            object PrismDelayPanel: TPanel
              Tag = 9
              Left = 8
              Top = 30
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton05: TSpinButton
              Tag = 9
              Left = 144
              Top = 25
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel8: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' 2'#1058#1087' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 634
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 10
            object TVGPanel: TPanel
              Tag = 8
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton04: TSpinButton
              Tag = 8
              Left = 142
              Top = 24
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel10: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1042#1056#1063' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object AScanKuPanel: TPanel
            Left = 0
            Top = 539
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            object SensPanel: TPanel
              Tag = 7
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton03: TSpinButton
              Tag = 7
              Left = 144
              Top = 24
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel12: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1050#1091' ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel13: TPanel
            Left = 0
            Top = 919
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 2
            object EdGatePanel: TPanel
              Tag = 12
              Left = 6
              Top = 30
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton02: TSpinButton
              Tag = 12
              Left = 144
              Top = 25
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel15: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1050#1086#1085#1077#1094' c'#1090#1088#1086#1073#1072' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel16: TPanel
            Left = 0
            Top = 824
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 3
            object StGatePanel: TPanel
              Tag = 11
              Left = 8
              Top = 31
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton01: TSpinButton
              Tag = 11
              Left = 144
              Top = 26
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel18: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1053#1072#1095#1072#1083#1086' c'#1090#1088#1086#1073#1072' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object LabelPanel: TPanel
            Left = 0
            Top = -161
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 4
            Visible = False
            object GeneratorPanel: TPanel
              Tag = 2
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton06: TSpinButton
              Tag = 2
              Left = 142
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel21: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1043#1077#1085#1077#1088#1072#1090#1086#1088' '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object BScanRecPanel: TPanel
            Left = 0
            Top = -40
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 5
            Visible = False
            object ReceiverPanel: TPanel
              Tag = 3
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel14: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1055#1088#1080#1077#1084#1085#1080#1082' '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton3: TSpinButton
              Tag = 3
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object EndPanel: TPanel
            Left = 0
            Top = 3109
            Width = 196
            Height = 9
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 6
          end
          object Panel24: TPanel
            Left = 0
            Top = 2820
            Width = 196
            Height = 95
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 7
            object SpeedButton1: TSpeedButton
              Tag = 19
              Left = 142
              Top = 24
              Width = 49
              Height = 64
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              ParentFont = False
              OnClick = sbSideLineClick
            end
            object RullerModePanel: TPanel
              Tag = 19
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = #1084#1082#1089
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel26: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1064#1082#1072#1083#1072' '#1075#1083#1091#1073#1080#1085#1099
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object TuningPanel: TPanel
            Left = 0
            Top = 1541
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 8
            Visible = False
            object CallibrationButton: TSpeedButton
              Tag = 14
              Left = 142
              Top = 24
              Width = 49
              Height = 64
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object ValuePanel00: TPanel
              Tag = 14
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -24
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel29: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1053#1072#1089#1090#1088#1086#1080#1090#1100
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel17: TPanel
            Left = 0
            Top = 3118
            Width = 196
            Height = 85
            Align = alTop
            ParentBackground = False
            TabOrder = 9
            Visible = False
            object Button15: TButton
              Tag = 7
              Left = 33
              Top = 11
              Width = 134
              Height = 62
              Caption = #1042#1099#1093#1086#1076
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Consolas'
              Font.Style = []
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
            end
          end
          object Panel19: TPanel
            Left = 0
            Top = -282
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 11
            Visible = False
            object sbSideLine: TSpeedButton
              Tag = 1
              Left = 142
              Top = 33
              Width = 49
              Height = 81
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object ValuePanel08: TPanel
              Tag = 1
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -23
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel20: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1057#1090#1086#1088#1086#1085#1072' / '#1051#1080#1085#1080#1103
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel25: TPanel
            Left = 0
            Top = 2362
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 12
            Visible = False
            object BScanGateLevelPanel: TPanel
              Tag = 17
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel28: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' BScan '#1087#1086#1088#1086#1075' ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton5: TSpinButton
              Tag = 17
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel32: TPanel
            Left = 0
            Top = 2725
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 13
            Visible = False
            object SpeedButton8: TSpeedButton
              Tag = 18
              Left = 142
              Top = 24
              Width = 49
              Height = 64
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object NoiseRedPanel: TPanel
              Tag = 18
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel34: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1064#1091#1084#1086#1087#1086#1076#1072#1074#1083#1077#1085#1080#1077' '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel35: TPanel
            Left = 0
            Top = 1325
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 14
            Visible = False
            object SpeedButton9: TSpeedButton
              Tag = 13
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object AnglePanel: TPanel
              Tag = 13
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel37: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1059#1075#1086#1083' '#1074#1074#1086#1076#1072' ['#1075#1088#1072#1076'] '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel38: TPanel
            Left = 0
            Top = 81
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 15
            Visible = False
            object DurationPanel: TPanel
              Tag = 4
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel40: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1044#1083#1080#1090'. '#1090#1072#1082#1090#1072' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton9: TSpinButton
              Tag = 4
              Left = 144
              Top = 33
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel41: TPanel
            Left = 0
            Top = 202
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 16
            Visible = False
            object DelayScalePanel: TPanel
              Tag = 5
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel43: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1052#1072#1089#1096#1090#1072#1073' '#1040' '#1088#1072#1079#1074#1077#1088#1090#1082#1080
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton10: TSpinButton
              Tag = 5
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel44: TPanel
            Left = 0
            Top = 323
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 17
            object ZondAmplPanel: TPanel
              Tag = 6
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel46: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1040#1084#1087#1083#1080#1090#1091#1076#1072' '#1047#1048' '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton11: TSpinButton
              Tag = 6
              Left = 144
              Top = 34
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel33: TPanel
            Left = 0
            Top = 2241
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 18
            Visible = False
            object BScanMaxDelayPanel: TPanel
              Tag = 16
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton1: TSpinButton
              Tag = 16
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel39: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = '1 BScan '#1082#1086#1085#1077#1094' c'#1090#1088#1086#1073#1072
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel42: TPanel
            Left = 0
            Top = 2120
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 19
            Visible = False
            object BScanMinDelayPanel: TPanel
              Tag = 15
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton2: TSpinButton
              Tag = 15
              Left = 144
              Top = 33
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel47: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' BScan '#1085#1072#1095#1072#1083#1086' c'#1090#1088#1086#1073#1072
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel22: TPanel
            Left = 0
            Top = 1204
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 20
            Visible = False
            object EvaluationGateLevelPanel: TPanel
              Tag = 10
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel30: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1055#1086#1088#1086#1075' '#1040#1057#1044' ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton4: TSpinButton
              Tag = 10
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object AScanAttPanel: TPanel
            Left = 0
            Top = 444
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 21
            object AttPanel: TPanel
              Tag = 77
              Left = 6
              Top = 29
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton6: TSpinButton
              Tag = 77
              Left = 144
              Top = 24
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel31: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1040#1058#1058' ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = 1999
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 22
            Visible = False
            object BScanGatePanel: TPanel
              Tag = 78
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel5: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1055#1086#1088#1086#1075' B-Scan ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton7: TSpinButton
              Tag = 78
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = 2483
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 23
            Visible = False
            object BScanViewBtn: TSpeedButton
              Tag = 38
              Left = 142
              Top = 32
              Width = 49
              Height = 81
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object BScanViewPanel: TPanel
              Tag = 38
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel52: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1042'-Scan '
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
          end
          object Panel55: TPanel
            Left = 0
            Top = -403
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 24
            Visible = False
            object Stroke: TPanel
              Tag = 1
              Left = 6
              Top = 39
              Width = 179
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -23
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel57: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1057#1091#1084#1084#1072' '#1040'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton8: TSpinButton
              Tag = 256
              Left = 142
              Top = 34
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              Visible = False
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object Panel56: TPanel
            Left = 0
            Top = -524
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 25
            Visible = False
            object StrokeCount: TPanel
              Tag = -1
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -23
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel59: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 32
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1040#1050' '#1085#1072#1095#1072#1083#1086' '#1089#1090#1088#1086#1073#1072
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton12: TSpinButton
              Tag = -1
              Left = 142
              Top = 34
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object BackBtnPanel: TPanel
            Left = 0
            Top = 2915
            Width = 196
            Height = 73
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 26
            object SpeedButton2: TSpeedButton
              Tag = 19
              Left = 26
              Top = 3
              Width = 145
              Height = 64
              Caption = #1042#1099#1093#1086#1076
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -29
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              ParentFont = False
              OnClick = SpeedButton2Click
            end
          end
          object GateIdxPanel: TPanel
            Left = 0
            Top = 729
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 27
            object GateIdxTextPanel: TPanel
              Tag = 16
              Left = 6
              Top = 31
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object SpinButton13: TSpinButton
              Tag = 22
              Left = 142
              Top = 26
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 1
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel49: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1057#1090#1088#1086#1073
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
            end
          end
          object WriteBScanPanel: TPanel
            Left = 0
            Top = 2988
            Width = 196
            Height = 121
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 28
            Visible = False
            object WriteBScanBtn: TSpeedButton
              Tag = 40
              Left = 26
              Top = 34
              Width = 145
              Height = 73
              AllowAllUp = True
              GroupIndex = 10
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -29
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              ParentFont = False
              OnClick = sbSideLineClick
            end
            object WriteBScanLabel: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = #1047#1072#1087#1080#1089#1100' B-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
            end
          end
          object Panel6: TPanel
            Left = 0
            Top = 2604
            Width = 196
            Height = 121
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 29
            object MarkPanel: TPanel
              Tag = 10
              Left = 6
              Top = 55
              Width = 130
              Height = 55
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel23: TPanel
              Left = 1
              Top = 28
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              object Panel76: TPanel
                Left = 0
                Top = 0
                Width = 194
                Height = 24
                Align = alTop
                Alignment = taLeftJustify
                BevelOuter = bvNone
                Caption = ' '#1052#1077#1090#1082#1072' ['#1084#1082#1089'] '
                Ctl3D = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = []
                ParentBackground = False
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
              end
            end
            object SpinButton14: TSpinButton
              Tag = 99
              Left = 144
              Top = 50
              Width = 49
              Height = 64
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
            object Panel74: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 27
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              object MarkSpeedButton: TSpeedButton
                Left = 0
                Top = 0
                Width = 177
                Height = 27
                Align = alLeft
                AllowAllUp = True
                GroupIndex = 44
                Caption = #1042#1067#1050#1051
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -16
                Font.Name = 'Verdana'
                Font.Style = []
                ParentFont = False
                OnClick = MarkSpeedButtonClick
                ExplicitLeft = 8
                ExplicitHeight = 24
              end
            end
          end
          object Panel73: TPanel
            Left = 0
            Top = 1109
            Width = 196
            Height = 95
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentBackground = False
            ParentFont = False
            TabOrder = 30
            object AutoPrismDelay: TSpeedButton
              Tag = 18
              Left = 27
              Top = 31
              Width = 134
              Height = 50
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              ParentFont = False
              OnClick = AutoPrismDelayClick
            end
            object Panel75: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1040#1074#1090#1086#1084#1072#1090'. '#1085#1072#1089#1090#1088'. 2'#1058#1087
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
            end
          end
          object NotchPanel3: TPanel
            Left = 0
            Top = 1878
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 31
            object LevelNotchPanel: TPanel
              Tag = 78
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel79: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1059#1088#1086#1074#1077#1085#1100' '#1087#1086#1083#1086#1095#1082#1080' ['#1076#1041']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton15: TSpinButton
              Tag = 15
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object NotchPanel2: TPanel
            Left = 0
            Top = 1757
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 32
            object LenNotchPanel: TPanel
              Tag = 78
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel82: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1044#1083#1080#1090'-'#1090#1100' '#1087#1086#1083#1086#1095#1082#1080' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton16: TSpinButton
              Tag = 14
              Left = 144
              Top = 32
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object NotchPanel1: TPanel
            Left = 0
            Top = 1636
            Width = 196
            Height = 121
            Align = alTop
            ParentBackground = False
            TabOrder = 33
            object StNotchPanel: TPanel
              Tag = 78
              Left = 6
              Top = 39
              Width = 130
              Height = 65
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = '-'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -32
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnClick = EditModePanelClick
            end
            object Panel85: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1053#1072#1095#1072#1083#1086' '#1087#1086#1083#1086#1095#1082#1080' ['#1084#1082#1089']'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
            end
            object SpinButton17: TSpinButton
              Tag = 13
              Left = 142
              Top = 33
              Width = 49
              Height = 81
              DownGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              TabOrder = 2
              UpGlyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8AA7FFFEAAA7FFFE6A678FFE29F6DFFE19661FFDB8D52FFD68342FFD276
                34FFCD6D22FFC96119FFC35817FFBF4D15FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFE8A87DFFE8A87BFFEFC8ACFFEEC7A7FFECC0A0FFEABA98FFE7B48FFFE4AE
                86FFE1A77DFFDFA274FFCC703AFFB64013FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFE2A173FFE8AF86FFEDC1A2FFE8B691FFE3A97FFFE0A273FFDFA0
                71FFE0A578FFCF733BFFB84414FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFDD945FFFE3A374FFE9BA98FFE3AA80FFE0A376FFE1A8
                7BFFD0763CFFBC4915FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFD68346FFDD9660FFE4B089FFE2AA80FFD079
                3EFFBC4F16FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFCD712BFFD6874AFFD37E42FFBF52
                17FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFC56119FFC55818FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnDownClick = SpinButton06DownClick
              OnUpClick = SpinButton06UpClick
            end
          end
          object TuningPanel2: TPanel
            Left = 0
            Top = 1446
            Width = 196
            Height = 95
            Align = alTop
            ParentBackground = False
            TabOrder = 34
            Visible = False
            object SpeedButton3: TSpeedButton
              Tag = 42
              Left = 8
              Top = 25
              Width = 87
              Height = 64
              Caption = #1057#1073#1088#1086#1089
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object SpeedButton4: TSpeedButton
              Tag = 41
              Left = 101
              Top = 25
              Width = 87
              Height = 64
              Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
              Glyph.Data = {
                36040000424D3604000000000000360000002800000010000000100000000100
                2000000000000004000000000000000000000000000000000000EFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB1B1B1FFACACACFFA7A7A7FFA3A3A3FF9E9E9EFF9C9C
                9CFF9A9A9AFF989898FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFB8B8B8FFB4B4B4FFB0B0B0FFACACACFFA8A8A8FFA5A5
                A5FFA3A3A3FFA1A1A1FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC0C0C0FFBDBDBDFFB9B9B9FFB6B6B6FFB4B4B4FFB1B1
                B1FFAFAFAFFFADADADFFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFC7C7C7FFC5C5C5FFC2C2C2FFC0C0C0FFBEBEBEFFBCBC
                BCFFBABABAFFB8B8B8FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3
                F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FFEFF3F3FF}
              OnClick = sbSideLineClick
            end
            object Panel90: TPanel
              Left = 1
              Top = 1
              Width = 194
              Height = 24
              Align = alTop
              Alignment = taLeftJustify
              BevelOuter = bvNone
              Caption = ' '#1053#1072#1089#1090#1088#1086#1080#1090#1100' '#1074#1072#1088#1080#1072#1085#1090' 2'
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -16
              Font.Name = 'Verdana'
              Font.Style = []
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
            end
          end
        end
      end
      object AScanDataPanel: TPanel
        Left = 0
        Top = 41
        Width = 337
        Height = 844
        Align = alClient
        BevelOuter = bvNone
        Caption = #1044#1072#1085#1085#1099#1081' '#1082#1072#1085#1072#1083' '#1085#1077' '#1090#1088#1077#1073#1091#1077#1090' '#1085#1072#1089#1090#1088#1086#1081#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -35
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 183
          Width = 337
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          Color = clSilver
          ParentColor = False
          ResizeStyle = rsUpdate
          Visible = False
          ExplicitLeft = 16
          ExplicitTop = 184
          ExplicitWidth = 861
        end
        object ParamPanel: TPanel
          Left = 0
          Top = 0
          Width = 337
          Height = 90
          Align = alTop
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object HPanel: TPanel
            Left = 0
            Top = 0
            Width = 200
            Height = 90
            Align = alLeft
            BevelOuter = bvNone
            BorderStyle = bsSingle
            Caption = 'H 182'
            Color = clBtnHighlight
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = 70
            Font.Name = 'Verdana'
            Font.Style = [fsBold]
            ParentBackground = False
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
          end
          object Panel64: TPanel
            Left = 200
            Top = 0
            Width = 420
            Height = 90
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object ChannelTitlePanel_: TPanel
              Left = 0
              Top = 47
              Width = 420
              Height = 43
              Align = alBottom
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = 25
              Font.Name = 'Verdana'
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              object ChannelTitlePanel: TPanel
                Left = 0
                Top = 0
                Width = 418
                Height = 41
                Align = alClient
                BevelOuter = bvNone
                Ctl3D = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = 25
                Font.Name = 'Verdana'
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
              end
              object Panel51: TPanel
                Left = 0
                Top = 0
                Width = 418
                Height = 41
                Align = alClient
                TabOrder = 1
                Visible = False
                object AlarmAlg: TPanel
                  Left = 217
                  Top = 1
                  Width = 200
                  Height = 39
                  Align = alClient
                  Alignment = taLeftJustify
                  BevelOuter = bvNone
                  Ctl3D = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 25
                  Font.Name = 'Verdana'
                  Font.Style = []
                  ParentCtl3D = False
                  ParentFont = False
                  TabOrder = 0
                end
                object BScanAlg: TPanel
                  Left = 1
                  Top = 1
                  Width = 216
                  Height = 39
                  Align = alLeft
                  Alignment = taLeftJustify
                  BevelOuter = bvNone
                  Ctl3D = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = 25
                  Font.Name = 'Verdana'
                  Font.Style = []
                  ParentCtl3D = False
                  ParentFont = False
                  TabOrder = 1
                end
              end
            end
            object Panel66: TPanel
              Left = 0
              Top = 0
              Width = 420
              Height = 47
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object LPanel: TPanel
                Left = 0
                Top = 0
                Width = 210
                Height = 47
                Align = alLeft
                BevelOuter = bvNone
                BorderStyle = bsSingle
                Caption = 'L 50'
                Color = clBtnHighlight
                Ctl3D = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = 55
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentBackground = False
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
              end
              object NPanel: TPanel
                Left = 210
                Top = 0
                Width = 210
                Height = 47
                Align = alLeft
                BevelOuter = bvNone
                BorderStyle = bsSingle
                Caption = 'N 14'
                Color = clBtnHighlight
                Ctl3D = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = 55
                Font.Name = 'Verdana'
                Font.Style = [fsBold]
                ParentBackground = False
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
              end
            end
          end
          object Panel65: TPanel
            Left = 620
            Top = 0
            Width = 5
            Height = 90
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            object InfoPanel: TPanel
              Left = 0
              Top = 0
              Width = 5
              Height = 90
              Align = alClient
              BevelOuter = bvNone
              BorderStyle = bsSingle
              Caption = 'H 182'
              Color = clBtnHighlight
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = 40
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentBackground = False
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
            end
          end
        end
        object AScanChart: TChart
          Left = 0
          Top = 90
          Width = 337
          Height = 93
          AllowPanning = pmNone
          BackWall.Size = 1
          BottomWall.Size = 1
          Gradient.EndColor = clGray
          Gradient.MidColor = clWhite
          Gradient.StartColor = clSilver
          LeftWall.Dark3D = False
          LeftWall.Size = 1
          LeftWall.Visible = False
          Legend.Visible = False
          MarginBottom = -13
          MarginLeft = 0
          MarginRight = 0
          MarginTop = 0
          MarginUnits = muPixels
          RightWall.Size = 1
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Axis.Visible = False
          BottomAxis.LabelsFormat.Font.Height = -29
          BottomAxis.LabelsFormat.Font.Name = 'Consolas'
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          BottomAxis.PositionPercent = 15.000000000000000000
          BottomAxis.PositionUnits = muPixels
          ClipPoints = False
          DepthAxis.Automatic = False
          DepthAxis.AutomaticMaximum = False
          DepthAxis.AutomaticMinimum = False
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.Maximum = 0.530000000000000400
          DepthAxis.Minimum = -0.470000000000000000
          DepthTopAxis.Automatic = False
          DepthTopAxis.AutomaticMaximum = False
          DepthTopAxis.AutomaticMinimum = False
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.Maximum = 0.530000000000000400
          DepthTopAxis.Minimum = -0.470000000000000000
          LeftAxis.Automatic = False
          LeftAxis.AutomaticMaximum = False
          LeftAxis.AutomaticMinimum = False
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.LabelsSize = 1
          LeftAxis.Maximum = 275.000000000000000000
          LeftAxis.Minimum = -10.000000000000000000
          LeftAxis.Title.Visible = False
          LeftAxis.Visible = False
          Panning.MouseWheel = pmwNone
          RightAxis.Automatic = False
          RightAxis.AutomaticMaximum = False
          RightAxis.AutomaticMinimum = False
          RightAxis.LabelsFormat.TextAlignment = taCenter
          Shadow.Visible = False
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          View3DOptions.Orthogonal = False
          View3DWalls = False
          Zoom.Allow = False
          Zoom.Pen.Mode = pmNotXor
          Align = alClient
          BevelOuter = bvNone
          BevelWidth = 9
          Color = clWhite
          TabOrder = 1
          OnClick = AScanChartClick
          OnMouseDown = AScanChartMouseDown
          DefaultCanvas = 'TGDIPlusCanvas'
          PrintMargins = (
            15
            11
            15
            11)
          ColorPaletteIndex = 13
          object AScanSeries_: TAreaSeries
            Active = False
            Marks.Visible = False
            AreaChartBrush.Color = clGray
            AreaChartBrush.BackColor = clDefault
            AreaLinesPen.Visible = False
            DrawArea = True
            LinePen.Width = 2
            Pointer.Brush.Gradient.EndColor = 10708548
            Pointer.Gradient.EndColor = 10708548
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            UseYOrigin = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YOrigin = -1.000000000000000000
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object MaxPosSeries: TChartShape
            Marks.Visible = False
            SeriesColor = clYellow
            Brush.Color = clYellow
            Pen.Width = 3
            Style = chasTriangle
            X0 = 30.000000000000000000
            X1 = 33.000000000000000000
            Y0 = 3.000000000000000000
            Y1 = -8.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000000000000000003E4000000000000008400000000000804040000000
              00000020C0}
          end
          object TVGSeries_: TAreaSeries
            Active = False
            Marks.Visible = False
            AreaChartBrush.Color = clGray
            AreaChartBrush.BackColor = clDefault
            AreaLinesPen.Visible = False
            Dark3D = False
            DrawArea = True
            LinePen.Visible = False
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            Transparency = 75
            UseYOrigin = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YOrigin = -1.000000000000000000
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object PeakSeries: TChartShape
            Marks.Visible = False
            SeriesColor = clOlive
            Alignment = taLeftJustify
            Brush.Color = clOlive
            Font.Height = -24
            Font.Name = 'Consolas'
            Pen.Width = 3
            Style = chasInvertPyramid
            Transparency = 50
            X0 = 24.700000000000000000
            X1 = 55.300000000000100000
            Y0 = 73.076917812500100000
            Y1 = 198.153835625000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B33840BD4AB038EC4452407466666666A64B40B64AB0
              38ECC46840}
          end
          object BScanGateMainSeries: TChartShape
            Active = False
            Marks.Visible = False
            Marks.Symbol.Frame.SmallSpace = 1
            Marks.Symbol.Pen.SmallSpace = 1
            SeriesColor = clAqua
            Brush.Color = clRed
            Pen.SmallSpace = 1
            Pen.Visible = False
            Style = chasRectangle
            X0 = 20.000000000000000000
            X1 = 60.000000000000000000
            Y0 = 100.000000000000000000
            Y1 = 98.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              0102000000000000000000344000000000000059400000000000004E40000000
              0000805840}
          end
          object BScanGateLeftSeries: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clAqua
            Brush.Color = clAqua
            Pen.Visible = False
            Style = chasRectangle
            X0 = 19.600000000000000000
            X1 = 20.000000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000009A999999999933400000000000805B400000000000003440000000
              0000805640}
          end
          object BScanGateRightSeries: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clAqua
            Brush.Color = clAqua
            Pen.Visible = False
            Style = chasRectangle
            X0 = 60.000000000000000000
            X1 = 60.400000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000000000000000004E400000000000805B403333333333334E40000000
              0000805640}
          end
          object GateSeriesMain: TChartShape
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.Color = clBlue
            Pen.Visible = False
            Style = chasRectangle
            X0 = 19.600000000000000000
            X1 = 20.000000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000009A999999999933400000000000805B400000000000003440000000
              0000805640}
          end
          object GateSeriesMain_: TChartShape
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.Color = clBlue
            Pen.Visible = False
            Style = chasRectangle
            X0 = 19.600000000000000000
            X1 = 20.000000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000009A999999999933400000000000805B400000000000003440000000
              0000805640}
          end
          object GateSeriesLeft: TChartShape
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.Color = clBlue
            Pen.Visible = False
            Style = chasRectangle
            X0 = 19.600000000000000000
            X1 = 20.000000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000009A999999999933400000000000805B400000000000003440000000
              0000805640}
          end
          object GateSeriesRight: TChartShape
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.Color = clBlue
            Pen.Visible = False
            Style = chasRectangle
            X0 = 60.000000000000000000
            X1 = 60.400000000000000000
            Y0 = 110.000000000000000000
            Y1 = 90.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000000000000000004E400000000000805B403333333333334E40000000
              0000805640}
          end
          object Series1: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clWhite
            Font.Color = clWhite
            Font.Height = -16
            Font.Style = [fsBold]
            Style = chasVertLine
            VertAlign = vaTop
            X0 = 24.700000000000000000
            X1 = 55.300000000000100000
            Y0 = 7.786953920000010000
            Y1 = 141.573907840000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B33840A96F973FD7251F407466666666A64B40FA76F9
              735DB26140}
          end
          object Series2: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clWhite
            Style = chasVertLine
            X0 = 24.700000000000000000
            X1 = 55.300000000000100000
            Y0 = 46.107259126400000000
            Y1 = 227.214518252800000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B33840F30AC4AABA0D47407466666666A64B40790562
              55DD666C40}
          end
          object BScanDebugSeries: TPointSeries
            Marks.Bevel = bvRaised
            Marks.Brush.BackColor = clWhite
            Marks.Brush.Gradient.Balance = 40
            Marks.Brush.Gradient.Direction = gdTopBottom
            Marks.Brush.Gradient.EndColor = clBlack
            Marks.Brush.Gradient.MidColor = 8388672
            Marks.Brush.Gradient.StartColor = clWhite
            Marks.Font.Height = -25
            Marks.Frame.Color = clBlue
            Marks.Frame.SmallSpace = 1
            Marks.Shadow.Visible = False
            Marks.ShapeStyle = fosRectangle
            Marks.Visible = True
            Marks.Margins.Left = 15
            Marks.Margins.Top = 8
            Marks.Margins.Right = 14
            Marks.Arrow.Color = clBlue
            Marks.Arrow.Width = 2
            Marks.Arrow.SmallSpace = 1
            Marks.BackColor = clAqua
            Marks.Callout.HorizSize = 5
            Marks.Callout.Style = psCross
            Marks.Callout.VertSize = 5
            Marks.Callout.Arrow.Color = clBlue
            Marks.Callout.Arrow.Width = 2
            Marks.Callout.Arrow.SmallSpace = 1
            Marks.Callout.Length = 22
            Marks.Symbol.Frame.SmallSpace = 1
            Marks.Symbol.Pen.SmallSpace = 1
            Marks.Color = clAqua
            ClickableLine = False
            Pointer.Brush.Gradient.EndColor = 1330417
            Pointer.Gradient.EndColor = 1330417
            Pointer.InflateMargins = True
            Pointer.Pen.Width = 2
            Pointer.Style = psRectangle
            Pointer.Visible = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01190000009A999999999933406CA9CD49A9953B40CDCCCCCCCC4C35403B9336
              5527762F4000000000000037403B93365527762F403333333333B338404B17AC
              825A293E406666666666663A403A4E1B1F33734B409999999999193C40328C60
              8899194840CCCCCCCCCCCC3D404B17AC825A293E40FFFFFFFFFF7F3F40864886
              1C3BB1154099999999999940407BA30C0BF4422A4033333333337341405532EF
              27B9911140CDCCCCCCCC4C42402208EB5A6666354067666666662643407C725E
              ADD04B4640010000000000444063E712B30F3C44409B99999999D94440F8B30E
              FD7DA040403533333333B34540321697F4811F2440CFCCCCCCCC8C464021F47D
              82955A2D406966666666664740CEC18E5ADCC81B4003000000004048401977DE
              21F00322409D9999999919494009693288D44A2B403733333333F34940E774B4
              053FF01D40D1CCCCCCCCCC4A4000000000000020406B66666666A64B40FA1E3F
              65466EF43F0500000000804C40DF597160E0873A409F99999999594D409DBF64
              3E2BB543403933333333334E40C6CE25661F783840}
          end
          object AScanSeries: TLineSeries
            ColorEachLine = False
            Marks.Visible = False
            SeriesColor = -1
            Brush.BackColor = clDefault
            Dark3D = False
            LinePen.Width = 5
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object TVGSeries: TLineSeries
            ColorEachLine = False
            Marks.Visible = False
            SeriesColor = 236
            Brush.BackColor = clDefault
            Dark3D = False
            LinePen.Width = 3
            Pointer.Brush.Gradient.EndColor = 236
            Pointer.Gradient.EndColor = 236
            Pointer.HorizSize = 2
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.VertSize = 2
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object MarkShape: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clRed
            Brush.Color = clRed
            Pen.Visible = False
            Style = chasRectangle
            X0 = 24.700000000000000000
            X1 = 55.300000000000100000
            Y0 = 48.569228084375000000
            Y1 = 122.138456168750000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B338407C2D4377DC4848407466666666A64B407C2D43
              77DC885E40}
          end
          object BS1: TLineSeries
            Active = False
            Marks.Visible = False
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS2: TLineSeries
            Active = False
            Marks.Visible = False
            Marks.Symbol.Frame.Width = 5
            Marks.Symbol.Pen.Width = 5
            Marks.Symbol.Transparency = 47
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.Brush.Gradient.EndColor = 11048782
            Pointer.Gradient.EndColor = 11048782
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS3: TLineSeries
            Active = False
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS5: TLineSeries
            Active = False
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS6: TLineSeries
            Active = False
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS7: TLineSeries
            Active = False
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object BS8: TLineSeries
            Active = False
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object Series5: TChartShape
            Active = False
            Marks.Visible = False
            SeriesColor = clWhite
            Style = chasCube
            X0 = 24.700000000000000000
            X1 = 55.300000000000100000
            Y0 = 121.586339739805000000
            Y1 = 335.172679479609000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              01020000003333333333B33840B0B31D9786655E407466666666A64B40C6D98E
              4BC3F27440}
          end
          object BS4: TLineSeries
            Marks.Visible = False
            SeriesColor = 11048782
            Brush.BackColor = clDefault
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object Series3: TPointSeries
            Marks.Visible = False
            ClickableLine = False
            Pointer.Brush.Color = clRed
            Pointer.Brush.Gradient.EndColor = 7028779
            Pointer.Gradient.EndColor = 7028779
            Pointer.HorizSize = 8
            Pointer.InflateMargins = True
            Pointer.Style = psDiamond
            Pointer.VertSize = 8
            Pointer.Visible = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
        end
        object BScanDataPanel: TPanel
          Left = 0
          Top = 654
          Width = 337
          Height = 190
          Align = alBottom
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          object ControlPanel: TPanel
            Left = 0
            Top = 0
            Width = 337
            Height = 35
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Visible = False
            object Button34: TButton
              Left = 7
              Top = 4
              Width = 75
              Height = 25
              Caption = 'Set Axiss'
              TabOrder = 0
              OnClick = Button34Click
            end
            object CheckBox1: TCheckBox
              Left = 88
              Top = 9
              Width = 97
              Height = 17
              Caption = 'Points'
              TabOrder = 1
              OnClick = CheckBox1Click
            end
            object CheckBox6: TCheckBox
              Left = 200
              Top = 10
              Width = 97
              Height = 17
              Caption = 'BScan'
              Checked = True
              State = cbChecked
              TabOrder = 2
              OnClick = CheckBox1Click
            end
          end
          object BScanDebugPanel: TPanel
            Left = 0
            Top = 35
            Width = 337
            Height = 155
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Visible = False
            object AmplDebugPanel: TPanel
              Left = -3
              Top = 1
              Width = 252
              Height = 153
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object BScanChart: TChart
                Left = 0
                Top = 0
                Width = 252
                Height = 131
                LeftWall.Visible = False
                Legend.Visible = False
                Title.Text.Strings = (
                  'TChart')
                Title.Visible = False
                BottomAxis.Automatic = False
                BottomAxis.AutomaticMaximum = False
                BottomAxis.AutomaticMinimum = False
                BottomAxis.LabelsFormat.TextAlignment = taCenter
                BottomAxis.Maximum = 24.000000000000000000
                DepthAxis.Automatic = False
                DepthAxis.AutomaticMaximum = False
                DepthAxis.AutomaticMinimum = False
                DepthAxis.LabelsFormat.TextAlignment = taCenter
                DepthAxis.Maximum = 0.829999999999996900
                DepthAxis.Minimum = -0.170000000000000700
                DepthTopAxis.Automatic = False
                DepthTopAxis.AutomaticMaximum = False
                DepthTopAxis.AutomaticMinimum = False
                DepthTopAxis.LabelsFormat.TextAlignment = taCenter
                DepthTopAxis.Maximum = 0.829999999999996900
                DepthTopAxis.Minimum = -0.170000000000000700
                LeftAxis.Automatic = False
                LeftAxis.AutomaticMaximum = False
                LeftAxis.AutomaticMinimum = False
                LeftAxis.Grid.Visible = False
                LeftAxis.LabelsFormat.TextAlignment = taCenter
                LeftAxis.Maximum = 255.000000000000000000
                LeftAxis.Minimum = -0.000000000000006217
                RightAxis.Automatic = False
                RightAxis.AutomaticMaximum = False
                RightAxis.AutomaticMinimum = False
                RightAxis.LabelsFormat.TextAlignment = taCenter
                RightAxis.Visible = False
                TopAxis.LabelsFormat.TextAlignment = taCenter
                TopAxis.Visible = False
                View3D = False
                Zoom.Pen.Mode = pmNotXor
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                DefaultCanvas = 'TGDIPlusCanvas'
                ColorPaletteIndex = 13
                object BScanSeries: TLineSeries
                  Marks.Visible = False
                  SeriesColor = -1
                  Brush.BackColor = clDefault
                  LinePen.Width = 2
                  Pointer.InflateMargins = True
                  Pointer.Style = psRectangle
                  Pointer.Visible = False
                  XValues.Name = 'X'
                  XValues.Order = loAscending
                  YValues.Name = 'Y'
                  YValues.Order = loNone
                end
              end
              object Panel54: TPanel
                Left = 0
                Top = 131
                Width = 252
                Height = 22
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object Label1: TLabel
                  Left = 0
                  Top = 0
                  Width = 87
                  Height = 22
                  Align = alLeft
                  Alignment = taCenter
                  AutoSize = False
                  Caption = ' '#1053#1086#1084#1077#1088' '#1089#1080#1075#1085#1072#1083#1072':  '
                  Layout = tlCenter
                  ExplicitLeft = -6
                  ExplicitTop = -4
                end
                object SelEchoIdx: TComboBox
                  Left = 87
                  Top = 0
                  Width = 165
                  Height = 21
                  Align = alRight
                  Style = csDropDownList
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  Items.Strings = (
                    '0'
                    '1'
                    '2'
                    '3'
                    '4'
                    '5'
                    '6'
                    '7')
                end
              end
            end
            object BScanChart2: TChart
              Left = 1
              Top = 1
              Width = 2
              Height = 153
              LeftWall.Visible = False
              Legend.Visible = False
              Title.Text.Strings = (
                'TChart')
              Title.Visible = False
              BottomAxis.Automatic = False
              BottomAxis.AutomaticMaximum = False
              BottomAxis.AutomaticMinimum = False
              BottomAxis.LabelsFormat.TextAlignment = taCenter
              DepthAxis.Automatic = False
              DepthAxis.AutomaticMaximum = False
              DepthAxis.AutomaticMinimum = False
              DepthAxis.LabelsFormat.TextAlignment = taCenter
              DepthAxis.Maximum = -0.040000000000000000
              DepthAxis.Minimum = -1.040000000000000000
              DepthTopAxis.Automatic = False
              DepthTopAxis.AutomaticMaximum = False
              DepthTopAxis.AutomaticMinimum = False
              DepthTopAxis.LabelsFormat.TextAlignment = taCenter
              DepthTopAxis.Maximum = -0.040000000000000260
              DepthTopAxis.Minimum = -1.040000000000000000
              LeftAxis.Automatic = False
              LeftAxis.AutomaticMaximum = False
              LeftAxis.AutomaticMinimum = False
              LeftAxis.Grid.Visible = False
              LeftAxis.LabelsFormat.TextAlignment = taCenter
              LeftAxis.Maximum = 255.000000000000000000
              RightAxis.Automatic = False
              RightAxis.AutomaticMaximum = False
              RightAxis.AutomaticMinimum = False
              RightAxis.LabelsFormat.TextAlignment = taCenter
              RightAxis.Visible = False
              TopAxis.LabelsFormat.TextAlignment = taCenter
              TopAxis.Visible = False
              View3D = False
              Zoom.Pen.Mode = pmNotXor
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              Visible = False
              DefaultCanvas = 'TGDIPlusCanvas'
              ColorPaletteIndex = 13
              object BSSeries1: TLineSeries
                Marks.Visible = False
                SeriesColor = clRed
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.Brush.Gradient.EndColor = 10708548
                Pointer.Gradient.EndColor = 10708548
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries2: TLineSeries
                Marks.Visible = False
                SeriesColor = clBlue
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries3: TLineSeries
                Marks.Visible = False
                SeriesColor = clLime
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries4: TLineSeries
                Marks.Visible = False
                SeriesColor = 33023
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries5: TLineSeries
                Marks.Visible = False
                SeriesColor = 16777088
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries6: TLineSeries
                Marks.Visible = False
                SeriesColor = 15680478
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries7: TLineSeries
                Marks.Visible = False
                SeriesColor = clSilver
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object BSSeries8: TLineSeries
                Marks.Visible = False
                SeriesColor = clBlack
                Brush.BackColor = clDefault
                ClickableLine = False
                Dark3D = False
                LinePen.Color = clRed
                LinePen.Width = 3
                OutLine.Width = 2
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = False
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
                Data = {0000000000}
              end
              object Series4: TPointSeries
                Marks.Visible = False
                ClickableLine = False
                Pointer.Brush.Gradient.EndColor = 10708548
                Pointer.Gradient.EndColor = 10708548
                Pointer.InflateMargins = True
                Pointer.Style = psRectangle
                Pointer.Visible = True
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Y'
                YValues.Order = loNone
              end
            end
            object DebugMemo: TMemo
              Left = 249
              Top = 1
              Width = 87
              Height = 153
              Align = alRight
              TabOrder = 2
            end
          end
        end
        object CBLogPanel: TPanel
          Left = 0
          Top = 188
          Width = 337
          Height = 247
          Align = alBottom
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Visible = False
          object Panel72: TPanel
            Left = 0
            Top = 0
            Width = 337
            Height = 29
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object ClearCBLogButton: TButton
              Left = 2
              Top = 1
              Width = 76
              Height = 25
              Caption = 'Clear'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnClick = ClearCBLogButtonClick
            end
            object CBLogSwitch: TButton
              Left = 84
              Top = 1
              Width = 76
              Height = 25
              Caption = 'Off'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnClick = CBLogSwitcClick
            end
            object Bigger: TButton
              Left = 166
              Top = 1
              Width = 76
              Height = 25
              Caption = 'Bigger'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              OnClick = BiggerClick
            end
            object Smaller: TButton
              Left = 248
              Top = 1
              Width = 76
              Height = 25
              Caption = 'Smaller'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              OnClick = SmallerClick
            end
          end
          object CBLog: TMemo
            Left = 0
            Top = 29
            Width = 337
            Height = 218
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object BScanPanel: TPanel
          Left = 0
          Top = 435
          Width = 337
          Height = 219
          Align = alBottom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnResize = BScanPanelResize
          object BScanPB: TPaintBox
            Left = 1
            Top = 1
            Width = 335
            Height = 217
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ExplicitLeft = 2
            ExplicitTop = 2
            ExplicitWidth = 683
          end
        end
      end
      object WorkStatePanel: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object DebugPanel_: TPanel
          Left = 83
          Top = 0
          Width = 471
          Height = 41
          Align = alClient
          Alignment = taLeftJustify
          BevelOuter = bvNone
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 16
          Font.Name = 'Verdana'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnDblClick = DebugPanel_DblClick
          object DebugPanel: TLabel
            Left = 0
            Top = 0
            Width = 471
            Height = 41
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = 16
            Font.Name = 'Verdana'
            Font.Style = []
            ParentFont = False
            WordWrap = True
            ExplicitWidth = 5
            ExplicitHeight = 16
          end
        end
        object Panel36: TPanel
          Left = 43
          Top = 0
          Width = 40
          Height = 41
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 43
          Height = 41
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          Visible = False
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1057#1082#1072#1085#1077#1088' '#1075#1086#1083#1086#1074#1082#1080
      ImageIndex = 9
      TabVisible = False
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 121
        Align = alTop
        TabOrder = 0
      end
      object AmpChart_0: TChart
        Left = 0
        Top = 121
        Width = 554
        Height = 250
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 255.000000000000000000
        LeftAxis.Minimum = 5.000000000000000000
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 1
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object AmpShape_0: TChartShape
          Marks.Visible = False
          SeriesColor = 722175
          Brush.Color = 722175
          Pen.Width = 4
          Style = chasVertLine
          X0 = 21.000000000000000000
          X1 = 21.000000000000000000
          Y1 = 255.000000000000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            0102000000000000000000354000000000000000000000000000003540000000
            0000E06F40}
        end
        object AmpSeries_Pos_0: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 3513587
          LinePen.SmallSpace = 1
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            01190000000000000000000840CDCCCCCC7C8681400000000000000E40CDCCCC
            CC3CF882400000000000001240CDCCCCCC3CF882400000000000001540000000
            0010FB834000000000000018409A999999C99185400000000000001B409A9999
            99A94A86400000000000001E403433333363E187400000000000802040676666
            66562B8840000000000000224067666666169D89400000000000802340676666
            66C687884000000000000025406766666636E488400000000000802640676666
            66562B884000000000000028409A999999318E87400000000000802940CDCCCC
            CCCC6288400000000000002B409A999999A1EA87400000000000802C40CDCCCC
            CC2C3886400000000000002E40CDCCCCCCBCDB85400000000000802F40333333
            334BFA8640000000000080304099999999593585400000000000403140333333
            33C3B68540000000000000324033333333DB9D86400000000000C03240999999
            9951D5864000000000008033409999999951D586400000000000403440CCCCCC
            CCF40986400000000000003540FFFFFFFF5F108540}
        end
      end
      object AmpChart_P45: TChart
        Left = 0
        Top = 371
        Width = 554
        Height = 250
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 255.000000000000000000
        LeftAxis.Minimum = 5.000000000000000000
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 2
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object AmpShape_P45: TChartShape
          Marks.Visible = False
          SeriesColor = 722175
          Brush.Color = 722175
          Style = chasVertLine
          X0 = 3.000000000000000000
          X1 = 21.000000000000000000
          Y0 = 281.187500000000000000
          Y1 = 743.375000000000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            0102000000000000000000084000000000009371400000000000003540000000
            00003B8740}
        end
        object AmpSeries_Pos_P45: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object AmpChart_M45: TChart
        Left = 0
        Top = 621
        Width = 554
        Height = 250
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 255.000000000000000000
        LeftAxis.Minimum = 5.000000000000000000
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 3
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object AmpShape_M45: TChartShape
          Marks.Visible = False
          SeriesColor = 722175
          Brush.Color = 722175
          Style = chasVertLine
          X0 = 3.000000000000000000
          X1 = 21.000000000000000000
          Y0 = 281.187500000000000000
          Y1 = 743.375000000000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            0102000000000000000000084000000000009371400000000000003540000000
            00003B8740}
        end
        object AmpSeries_Pos_M45: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1072
      ImageIndex = 12
      TabVisible = False
      object Chart2: TChart
        Left = 0
        Top = 57
        Width = 285
        Height = 828
        LeftWall.Visible = False
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.Automatic = False
        DepthAxis.AutomaticMaximum = False
        DepthAxis.AutomaticMinimum = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.Maximum = 0.829999999999996900
        DepthAxis.Minimum = -0.170000000000000700
        DepthTopAxis.Automatic = False
        DepthTopAxis.AutomaticMaximum = False
        DepthTopAxis.AutomaticMinimum = False
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.Maximum = 0.829999999999996900
        DepthTopAxis.Minimum = -0.170000000000000700
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.Automatic = False
        RightAxis.AutomaticMaximum = False
        RightAxis.AutomaticMinimum = False
        RightAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.Visible = False
        TopAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.Visible = False
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries3: TLineSeries
          Marks.Visible = False
          Brush.BackColor = clDefault
          ClickableLine = False
          Dark3D = False
          LinePen.Color = clRed
          LinePen.Width = 3
          OutLine.Width = 2
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            001900000000000000007874400000000000D06B400000000000C06240000000
            000090654000000000006068400000000000205C400000000000804640000000
            0000C047400000000000004E400000000000F064400000000000405F40000000
            0000E05F4000000000006053400000000000E050400000000000404040000000
            000000044000000000000024400000000000A059400000000000A05E40000000
            0000006940000000000030614000000000002062400000000000804640000000
            0000002E400000000000002940}
        end
      end
      object Memo2: TMemo
        Left = 285
        Top = 57
        Width = 269
        Height = 828
        Align = alRight
        TabOrder = 1
      end
      object Panel67: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 57
        Align = alTop
        TabOrder = 2
        object Button19: TButton
          Left = 8
          Top = 5
          Width = 75
          Height = 43
          Caption = 'Clear'
          TabOrder = 0
          OnClick = Button19Click
        end
        object Button20: TButton
          Left = 89
          Top = 5
          Width = 75
          Height = 43
          Caption = 'Save'
          TabOrder = 1
          OnClick = Button20Click
        end
      end
    end
    object TabSheet15: TTabSheet
      Caption = #1044#1072#1090#1095#1080#1082' '#1087#1091#1090#1080
      ImageIndex = 3
      TabVisible = False
      object Chart1: TChart
        Left = 0
        Top = 0
        Width = 554
        Height = 885
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series6: TLineSeries
          Marks.Visible = False
          Brush.BackColor = clDefault
          Pointer.Brush.Gradient.EndColor = 10708548
          Pointer.Gradient.EndColor = 10708548
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object Настройка: TTabSheet
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      ImageIndex = 4
      TabVisible = False
      object Panel53: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 60
        Align = alTop
        TabOrder = 0
        object Button7: TButton
          Left = 16
          Top = 15
          Width = 75
          Height = 25
          Caption = 'Get'
          TabOrder = 0
          OnClick = Button7Click
        end
      end
      object CalMemo: TMemo
        Left = 0
        Top = 60
        Width = 554
        Height = 825
        Align = alClient
        TabOrder = 1
      end
    end
    object TabSheet18: TTabSheet
      Caption = #1044#1072#1090#1095#1080#1082#1080' '#1087#1091#1090#1080
      ImageIndex = 5
      object Splitter3: TSplitter
        Left = 0
        Top = 417
        Width = 554
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ExplicitWidth = 716
      end
      object Chart3: TChart
        Left = 0
        Top = 41
        Width = 554
        Height = 376
        Legend.LegendStyle = lsSeries
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Main_PE: TLineSeries
          Marks.Visible = False
          SeriesColor = clBlue
          Brush.BackColor = clDefault
          LinePen.Color = clBlue
          Pointer.Brush.Color = clBlue
          Pointer.Brush.Gradient.EndColor = clBlue
          Pointer.Gradient.EndColor = clBlue
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clBlue
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object HandScan_PE: TLineSeries
          Marks.Visible = False
          SeriesColor = clRed
          Brush.BackColor = clDefault
          LinePen.Color = clRed
          Pointer.Brush.Color = clRed
          Pointer.Brush.Gradient.EndColor = clRed
          Pointer.Gradient.EndColor = clRed
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clRed
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object HeadScaner_PE: TLineSeries
          Marks.Visible = False
          SeriesColor = clLime
          Brush.BackColor = clDefault
          LinePen.Color = clLime
          Pointer.Brush.Color = clLime
          Pointer.Brush.Gradient.EndColor = clLime
          Pointer.Gradient.EndColor = clLime
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clLime
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object Series7: TChartShape
          Marks.Visible = False
          SeriesColor = clWhite
          Style = chasVertLine
          X0 = 3.000000000000000000
          X1 = 21.000000000000000000
          Y0 = 365.287570312500000000
          Y1 = 1025.575140625000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            01020000000000000000000840CFF753E399D476400000000000003540E7FBA9
            F14C069040}
        end
      end
      object Panel58: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 1
        object Button10: TButton
          Left = 84
          Top = 6
          Width = 75
          Height = 25
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100
          TabOrder = 0
          OnClick = Button10Click
        end
        object ComboBox1: TComboBox
          Left = 170
          Top = 8
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 1
          Text = 'DisCoord'
          Visible = False
          Items.Strings = (
            'SysCoord'
            'DisCoord')
        end
        object CoordGraphCheckBox: TCheckBox
          Left = 5
          Top = 10
          Width = 52
          Height = 17
          Caption = #1042#1082#1083
          TabOrder = 2
        end
        object Button12: TButton
          Left = 324
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Test Reset Coord (1)'
          TabOrder = 3
          OnClick = Button12Click
        end
        object Button1811: TButton
          Left = 455
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Test Reset Coord (2)'
          TabOrder = 4
          OnClick = Button1811Click
        end
      end
      object Chart20: TChart
        Left = 0
        Top = 423
        Width = 554
        Height = 462
        Legend.LegendStyle = lsSeries
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 2
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries1: TLineSeries
          Marks.Visible = False
          SeriesColor = clBlue
          Brush.BackColor = clDefault
          LinePen.Color = clBlue
          Pointer.Brush.Color = clBlue
          Pointer.Brush.Gradient.EndColor = clBlue
          Pointer.Gradient.EndColor = clBlue
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clBlue
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object LineSeries2: TLineSeries
          Active = False
          Marks.Visible = False
          SeriesColor = clRed
          Brush.BackColor = clDefault
          LinePen.Color = clRed
          Pointer.Brush.Color = clRed
          Pointer.Brush.Gradient.EndColor = clRed
          Pointer.Gradient.EndColor = clRed
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clRed
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object LineSeries4: TLineSeries
          Active = False
          Marks.Visible = False
          SeriesColor = clLime
          Brush.BackColor = clDefault
          LinePen.Color = clLime
          Pointer.Brush.Color = clLime
          Pointer.Brush.Gradient.EndColor = clLime
          Pointer.Gradient.EndColor = clLime
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clLime
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object ChartShape1: TChartShape
          Active = False
          Marks.Visible = False
          SeriesColor = clWhite
          Style = chasVertLine
          X0 = 3.000000000000000000
          X1 = 21.000000000000000000
          Y0 = 365.287570312500000000
          Y1 = 1025.575140625000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            01020000000000000000000840CFF753E399D476400000000000003540E7FBA9
            F14C069040}
        end
      end
    end
    object AC_TabSheet: TTabSheet
      Caption = #1040#1050
      ImageIndex = 6
      TabVisible = False
      OnResize = AC_TabSheetResize
      object Panel80: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 0
        object ACChartClearButton: TButton
          Left = 52
          Top = 6
          Width = 75
          Height = 25
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100
          TabOrder = 0
          OnClick = ACChartClearButtonClick
        end
        object ACCheckBox: TCheckBox
          Left = 5
          Top = 10
          Width = 39
          Height = 17
          Caption = #1042#1082#1083
          TabOrder = 1
        end
      end
      object Chart5: TChart
        Left = 0
        Top = 41
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 1
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries1: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart4: TChart
        Left = 0
        Top = 130
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 2
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries2: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart6: TChart
        Left = 0
        Top = 219
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 3
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries3: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart7: TChart
        Left = 0
        Top = 308
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 4
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries4: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart8: TChart
        Left = 0
        Top = 397
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 5
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries5: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart9: TChart
        Left = 0
        Top = 486
        Width = 554
        Height = 89
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 6
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries6: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart10: TChart
        Left = 0
        Top = 575
        Width = 554
        Height = 82
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 7
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries7: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart11: TChart
        Left = 0
        Top = 657
        Width = 554
        Height = 96
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 8
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries8: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart12: TChart
        Left = 0
        Top = 753
        Width = 554
        Height = 96
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Visible = False
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alTop
        TabOrder = 9
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object FastLineSeries9: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object tsAKTest: TTabSheet
      Tag = 100
      Caption = 'AK '#1058#1077#1089#1090
      DoubleBuffered = False
      ImageIndex = 7
      ParentDoubleBuffered = False
      OnResize = tsAKTestResize
      object PaintBox1: TPaintBox
        Left = 105
        Top = 0
        Width = 449
        Height = 885
        Align = alClient
        ExplicitLeft = 0
        ExplicitTop = 51
        ExplicitWidth = 661
        ExplicitHeight = 757
      end
      object Panel81: TPanel
        Left = 0
        Top = 0
        Width = 105
        Height = 885
        Align = alLeft
        TabOrder = 0
        object Label7: TLabel
          Left = 17
          Top = 84
          Width = 19
          Height = 13
          Caption = #1040#1058#1058
          WordWrap = True
        end
        object Label9: TLabel
          Left = 3
          Top = 174
          Width = 83
          Height = 13
          Caption = #1055#1086#1088#1086#1075'              %'
        end
        object Label10: TLabel
          Left = 2
          Top = 201
          Width = 100
          Height = 68
          AutoSize = False
          Caption = 
            #1063#1072#1089#1090#1086#1090#1072' '#1087#1077#1088#1077#1085#1072#1089#1090#1088#1086#1081#1082#1080'                                           ' +
            '                    '#1090#1099#1089'.                       '#1094#1099#1082#1083#1086#1074
          WordWrap = True
        end
        object Button117: TButton
          Left = 2
          Top = 109
          Width = 96
          Height = 37
          Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
          TabOrder = 0
          OnClick = Button117Click
        end
        object Edit9: TEdit
          Left = 43
          Top = 81
          Width = 36
          Height = 21
          TabOrder = 1
          Text = '60'
        end
        object Button18: TButton
          Left = 2
          Top = 8
          Width = 96
          Height = 37
          Caption = #1042#1050#1051
          TabOrder = 2
          OnClick = Button18Click
        end
        object Button21: TButton
          Left = 4
          Top = 275
          Width = 95
          Height = 37
          Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
          TabOrder = 3
          OnClick = Button21Click
        end
        object UpDown16: TUpDown
          Left = 55
          Top = 171
          Width = 16
          Height = 21
          Associate = Edit10
          Position = 50
          TabOrder = 4
        end
        object Edit10: TEdit
          Left = 34
          Top = 171
          Width = 21
          Height = 21
          TabOrder = 5
          Text = '50'
        end
        object Panel83: TPanel
          Left = 3
          Top = 59
          Width = 94
          Height = 6
          TabOrder = 6
        end
        object Button137: TButton
          Left = 2
          Top = 329
          Width = 95
          Height = 37
          Caption = #1042#1067#1050#1051
          TabOrder = 7
          OnClick = Button137Click
        end
        object Panel87: TPanel
          Left = 3
          Top = 154
          Width = 94
          Height = 6
          TabOrder = 8
        end
        object Panel84: TPanel
          Left = 4
          Top = 317
          Width = 94
          Height = 6
          TabOrder = 9
        end
        object Edit11: TEdit
          Left = 2
          Top = 243
          Width = 31
          Height = 21
          TabOrder = 10
          Text = '30'
        end
        object UpDown17: TUpDown
          Left = 33
          Top = 243
          Width = 16
          Height = 21
          Associate = Edit11
          Position = 30
          TabOrder = 11
        end
      end
    end
    object TabSheet23: TTabSheet
      Caption = #1057#1086#1073#1099#1090#1080#1103
      ImageIndex = 8
      object Memo4: TMemo
        Left = 0
        Top = 0
        Width = 554
        Height = 885
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
    object TabSheet24: TTabSheet
      Caption = 'TabSheet24'
      ImageIndex = 9
      TabVisible = False
      object GainArrMemo: TMemo
        Left = 0
        Top = 41
        Width = 554
        Height = 844
        Align = alClient
        TabOrder = 0
      end
      object Panel86: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 1
        object Button17: TButton
          Left = 6
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Create List'
          TabOrder = 0
          OnClick = Button17Click
        end
      end
    end
    object TabSheet25: TTabSheet
      Tag = 101
      Caption = 'B-Scan '#1058#1077#1089#1090
      ImageIndex = 10
      OnResize = TabSheet25Resize
      OnShow = TabSheet25Show
      object PaintBox2: TPaintBox
        Left = 0
        Top = 41
        Width = 554
        Height = 594
        Align = alClient
        ExplicitLeft = 2
        ExplicitTop = 39
        ExplicitWidth = 800
        ExplicitHeight = 706
      end
      object Panel88: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 0
        object Label11: TLabel
          Left = 16
          Top = 12
          Width = 114
          Height = 13
          Caption = #1064#1072#1075' '#1044#1055'                     '#1084#1084
        end
        object Label12: TLabel
          Left = 152
          Top = 12
          Width = 136
          Height = 13
          Caption = #1057#1082#1086#1088#1086#1089#1090#1100'                      '#1082#1084'/'#1095
        end
        object Edit12: TEdit
          Left = 63
          Top = 9
          Width = 48
          Height = 21
          TabOrder = 0
          Text = '2'
        end
        object Edit13: TEdit
          Left = 211
          Top = 9
          Width = 48
          Height = 21
          ReadOnly = True
          TabOrder = 1
          Text = '0'
        end
        object Button22: TButton
          Left = 360
          Top = 8
          Width = 75
          Height = 25
          Caption = #1057#1073#1088#1086#1089
          TabOrder = 2
          OnClick = Button22Click
        end
      end
      object Chart13: TChart
        Left = 0
        Top = 635
        Width = 554
        Height = 250
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alBottom
        TabOrder = 1
        DefaultCanvas = 'TGDIPlusCanvas'
        PrintMargins = (
          15
          34
          15
          34)
        ColorPaletteIndex = 13
        object Series8: TBarSeries
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
        end
      end
    end
    object TabSheet26: TTabSheet
      Tag = 102
      Caption = #1050#1086#1088#1088#1077#1083#1103#1094#1080#1103' '#1040'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
      ImageIndex = 11
      object ShiftMemo: TMemo
        Left = 0
        Top = 0
        Width = 97
        Height = 885
        Align = alLeft
        TabOrder = 0
      end
      object Panel89: TPanel
        Left = 97
        Top = 0
        Width = 457
        Height = 885
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Chart17: TChart
          Left = 0
          Top = 570
          Width = 457
          Height = 190
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Automatic = False
          BottomAxis.AutomaticMaximum = False
          BottomAxis.AutomaticMinimum = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          BottomAxis.Maximum = 200.000000000000000000
          BottomAxis.Minimum = -200.000000000000000000
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Automatic = False
          LeftAxis.AutomaticMaximum = False
          LeftAxis.AutomaticMinimum = False
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Maximum = 255.000000000000000000
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alTop
          TabOrder = 0
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object Series10: TPointSeries
            Marks.Visible = False
            ClickableLine = False
            Pointer.Brush.Gradient.EndColor = 10708548
            Pointer.Gradient.EndColor = 10708548
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object Series11: TChartShape
            Marks.Visible = False
            SeriesColor = clWhite
            Style = chasVertLine
            Y0 = 255.000000000000000000
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
            Data = {
              010200000000000000000000000000000000E06F400000000000000000000000
              0000000000}
          end
        end
        object Chart16: TChart
          Left = 0
          Top = 380
          Width = 457
          Height = 190
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alTop
          TabOrder = 1
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object FastLineSeries11: TFastLineSeries
            Marks.Visible = False
            LinePen.Color = 10708548
            LinePen.Width = 3
            LinePen.SmallSpace = 1
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
        end
        object Chart15: TChart
          Left = 0
          Top = 190
          Width = 457
          Height = 190
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alTop
          TabOrder = 2
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object FastLineSeries10: TFastLineSeries
            Marks.Visible = False
            LinePen.Color = 10708548
            LinePen.Width = 3
            LinePen.SmallSpace = 1
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
        end
        object Chart14: TChart
          Left = 0
          Top = 0
          Width = 457
          Height = 190
          Legend.Visible = False
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alTop
          TabOrder = 3
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object Series9: TFastLineSeries
            Marks.Visible = False
            LinePen.Color = 10708548
            LinePen.Width = 3
            LinePen.SmallSpace = 1
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
        end
      end
    end
    object TabSheet27: TTabSheet
      Tag = 1112
      Caption = #1052#1057#1082#1072#1085#1077#1088
      ImageIndex = 12
      object Chart18: TChart
        Left = 0
        Top = 41
        Width = 554
        Height = 654
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 20.000000000000000000
        BottomAxis.Minimum = -20.000000000000000000
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 20.000000000000000000
        LeftAxis.Minimum = -20.000000000000000000
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series12: TPointSeries
          Marks.Visible = False
          ClickableLine = False
          Pointer.Brush.Gradient.EndColor = 10708548
          Pointer.Gradient.EndColor = 10708548
          Pointer.HorizSize = 14
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.VertSize = 14
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Panel91: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 1
        object MScanerResetButton: TButton
          Left = 10
          Top = 6
          Width = 75
          Height = 25
          Caption = #1057#1073#1088#1086#1089
          TabOrder = 0
          OnClick = MScanerResetButtonClick
        end
      end
      object Chart19: TChart
        Left = 0
        Top = 695
        Width = 554
        Height = 190
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alBottom
        TabOrder = 2
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object MousScanerAScan: TFastLineSeries
          Marks.Visible = False
          LinePen.Color = 10708548
          LinePen.Width = 3
          LinePen.SmallSpace = 1
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object TabSheet28: TTabSheet
      Tag = -876
      Caption = 'Bad B Scan Delay'
      ImageIndex = 13
      object Memo5: TMemo
        Left = 0
        Top = 729
        Width = 554
        Height = 156
        Align = alClient
        TabOrder = 0
      end
      object Panel48: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 729
        Align = alTop
        BevelOuter = bvNone
        BiDiMode = bdLeftToRight
        DoubleBuffered = True
        ParentBiDiMode = False
        ParentDoubleBuffered = False
        TabOrder = 1
        OnResize = Panel48Resize
        object BScanTestData: TPaintBox
          Left = 0
          Top = 0
          Width = 554
          Height = 729
          Align = alClient
          ExplicitWidth = 770
          ExplicitHeight = 619
        end
      end
    end
    object FiltrationTabSheet: TTabSheet
      Tag = 111
      Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103
      ImageIndex = 14
      object FiltrationTabMemo: TMemo
        Left = 0
        Top = 42
        Width = 554
        Height = 1797
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Panel92: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 42
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label13: TLabel
          Left = 89
          Top = 10
          Width = 134
          Height = 13
          Caption = #1056#1072#1079#1084#1077#1088' '#1089#1090#1088#1091#1082#1090#1091#1088#1099' Filtraion'
        end
        object FiltrSizeLabel: TLabel
          Left = 249
          Top = 10
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Button23: TButton
          Left = 2
          Top = 3
          Width = 75
          Height = 25
          Caption = #1042#1082#1083
          TabOrder = 0
          OnClick = Button23Click
        end
      end
    end
    object PE_Test: TTabSheet
      Caption = #1044#1072#1090#1095#1080#1082' '#1087#1091#1090#1080' '#1089' 2-'#1093' '#1041#1059#1052#1086#1074
      ImageIndex = 15
      object Panel93: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 87
        Align = alTop
        TabOrder = 0
        object Label14: TLabel
          Left = 48
          Top = 45
          Width = 31
          Height = 13
          Caption = 'UMU 0'
        end
        object UMU0_PEValue: TLabel
          Left = 88
          Top = 45
          Width = 22
          Height = 13
          Caption = '   -   '
        end
        object Label16: TLabel
          Left = 48
          Top = 64
          Width = 31
          Height = 13
          Caption = 'UMU 1'
        end
        object UMU1_PEValue: TLabel
          Left = 88
          Top = 64
          Width = 22
          Height = 13
          Caption = '   -   '
        end
        object PE_Delta: TLabel
          Left = 144
          Top = 55
          Width = 22
          Height = 13
          Caption = '   -   '
        end
        object Label17: TLabel
          Left = 122
          Top = 55
          Width = 7
          Height = 13
          Caption = #916
        end
        object Button24: TButton
          Left = 73
          Top = 6
          Width = 75
          Height = 25
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100
          TabOrder = 0
          OnClick = Button24Click
        end
        object ComboBox5: TComboBox
          Left = 170
          Top = 8
          Width = 145
          Height = 21
          Style = csDropDownList
          ItemIndex = 1
          TabOrder = 1
          Text = 'DisCoord'
          Visible = False
          Items.Strings = (
            'SysCoord'
            'DisCoord')
        end
        object CheckBox4: TCheckBox
          Left = 2
          Top = 10
          Width = 52
          Height = 17
          Caption = #1042#1082#1083
          TabOrder = 2
        end
        object Button25: TButton
          Left = 324
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Test Reset Coord (1)'
          TabOrder = 3
          Visible = False
          OnClick = Button12Click
        end
        object Button26: TButton
          Left = 455
          Top = 6
          Width = 120
          Height = 25
          Caption = 'Test Reset Coord (2)'
          TabOrder = 4
          Visible = False
          OnClick = Button1811Click
        end
      end
      object Chart21: TChart
        Left = 0
        Top = 87
        Width = 554
        Height = 561
        Legend.LegendStyle = lsSeries
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alClient
        TabOrder = 1
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries5: TLineSeries
          ColorEachLine = False
          Marks.Visible = False
          SeriesColor = clBlue
          Brush.BackColor = clDefault
          Dark3D = False
          DrawStyle = dsCurve
          LinePen.Color = clBlue
          Pointer.Brush.Color = clBlue
          Pointer.Brush.Gradient.EndColor = clBlue
          Pointer.Gradient.EndColor = clBlue
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clBlue
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
        object LineSeries6: TLineSeries
          Marks.Visible = False
          SeriesColor = clRed
          Brush.BackColor = clDefault
          LinePen.Color = clRed
          Pointer.Brush.Color = clRed
          Pointer.Brush.Gradient.EndColor = clRed
          Pointer.Gradient.EndColor = clRed
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clRed
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object Chart22: TChart
        Left = 0
        Top = 648
        Width = 554
        Height = 237
        Legend.LegendStyle = lsSeries
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        Align = alBottom
        TabOrder = 2
        Visible = False
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object LineSeries7: TLineSeries
          ColorEachLine = False
          Marks.Visible = False
          SeriesColor = clBlack
          Brush.BackColor = clDefault
          Dark3D = False
          DrawStyle = dsCurve
          LinePen.Color = clBlue
          Pointer.Brush.Color = clBlue
          Pointer.Brush.Gradient.EndColor = clBlack
          Pointer.Gradient.EndColor = clBlack
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Color = clBlue
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
    end
    object TabSheet29: TTabSheet
      Caption = 'TabSheet29'
      ImageIndex = 16
      object Panel94: TPanel
        Left = 0
        Top = 0
        Width = 554
        Height = 41
        Align = alTop
        TabOrder = 0
        object Button27: TButton
          Left = 9
          Top = 6
          Width = 75
          Height = 25
          Caption = 'Button27'
          TabOrder = 0
          OnClick = Button27Click
        end
      end
      object Panel95: TPanel
        Left = 0
        Top = 41
        Width = 554
        Height = 844
        Align = alClient
        TabOrder = 1
        object Chart23: TChart
          Left = 1
          Top = 1
          Width = 367
          Height = 842
          Legend.LegendStyle = lsSeries
          Legend.Visible = False
          Title.Font.Height = -1
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Automatic = False
          BottomAxis.AutomaticMaximum = False
          BottomAxis.AutomaticMinimum = False
          BottomAxis.LabelsFormat.Font.Height = -21
          BottomAxis.LabelsFormat.TextAlignment = taCenter
          BottomAxis.Maximum = 18.000000000000000000
          BottomAxis.Minimum = -12.000000000000000000
          DepthAxis.LabelsFormat.TextAlignment = taCenter
          DepthTopAxis.LabelsFormat.TextAlignment = taCenter
          LeftAxis.Increment = 2.000000000000000000
          LeftAxis.LabelsFormat.Font.Height = -21
          LeftAxis.LabelsFormat.TextAlignment = taCenter
          RightAxis.LabelsFormat.TextAlignment = taCenter
          TopAxis.LabelsFormat.TextAlignment = taCenter
          View3D = False
          Zoom.Pen.Mode = pmNotXor
          Align = alClient
          TabOrder = 0
          DefaultCanvas = 'TGDIPlusCanvas'
          ColorPaletteIndex = 13
          object LineSeries8: TLineSeries
            ColorEachLine = False
            Marks.Visible = False
            SeriesColor = clBlue
            Brush.BackColor = clDefault
            Dark3D = False
            DrawStyle = dsCurve
            LinePen.Color = clBlue
            LinePen.Width = 4
            LinePen.SmallSpace = 1
            LinePen.Visible = False
            Pointer.Brush.Color = clBlue
            Pointer.Brush.Gradient.EndColor = clBlue
            Pointer.Gradient.EndColor = clBlue
            Pointer.InflateMargins = True
            Pointer.Pen.Color = clBlue
            Pointer.Style = psRectangle
            Pointer.Visible = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
          object LineSeries9: TLineSeries
            Marks.Visible = False
            SeriesColor = clRed
            Brush.BackColor = clDefault
            LinePen.Color = clRed
            Pointer.Brush.Color = clRed
            Pointer.Brush.Gradient.EndColor = clRed
            Pointer.Gradient.EndColor = clRed
            Pointer.HorizSize = 2
            Pointer.InflateMargins = True
            Pointer.Pen.Color = clRed
            Pointer.Style = psRectangle
            Pointer.VertSize = 2
            Pointer.Visible = True
            XValues.Name = 'X'
            XValues.Order = loAscending
            YValues.Name = 'Y'
            YValues.Order = loNone
          end
        end
        object Memo6: TMemo
          Left = 368
          Top = 1
          Width = 185
          Height = 842
          Align = alRight
          TabOrder = 1
        end
      end
    end
    object TabSheet32: TTabSheet
      Caption = #1057#1082#1072#1085#1077#1088' '#1040#1074#1080#1082#1086#1085'-15'
      ImageIndex = 17
      object Label15: TLabel
        Left = 24
        Top = 53
        Width = 37
        Height = 13
        Caption = 'Label15'
      end
      object Label18: TLabel
        Left = 24
        Top = 24
        Width = 37
        Height = 13
        Caption = 'Label18'
      end
      object Label19: TLabel
        Left = 24
        Top = 86
        Width = 37
        Height = 13
        Caption = 'Label19'
      end
      object Chart24: TChart
        Left = 24
        Top = 117
        Width = 545
        Height = 250
        Legend.Visible = False
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.LabelsFormat.TextAlignment = taCenter
        BottomAxis.Maximum = 200.000000000000000000
        BottomAxis.Minimum = -5.000000000000000000
        DepthAxis.LabelsFormat.TextAlignment = taCenter
        DepthTopAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelsFormat.TextAlignment = taCenter
        LeftAxis.Maximum = 40.000000000000000000
        LeftAxis.Minimum = -30.000000000000000000
        RightAxis.LabelsFormat.TextAlignment = taCenter
        TopAxis.LabelsFormat.TextAlignment = taCenter
        View3D = False
        Zoom.Pen.Mode = pmNotXor
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        ColorPaletteIndex = 13
        object Series13: TChartShape
          Marks.Visible = False
          SeriesColor = clWhite
          Alignment = taLeftJustify
          Font.Color = clBlue
          Font.Height = -13
          Font.Style = [fsBold]
          Text.Strings = (
            '1')
          Style = chasCube
          X1 = 10.000000000000000000
          Y1 = 10.000000000000000000
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {
            0102000000000000000000000000000000000000000000000000002440000000
            0000002440}
        end
      end
    end
  end
  object ChPanel_: TPanel
    Left = 202
    Top = 0
    Width = 229
    Height = 931
    Align = alLeft
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object ChannelsPanel: TPanel
      Left = 0
      Top = 88
      Width = 229
      Height = 828
      Align = alClient
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Panel71: TPanel
        Left = 0
        Top = 0
        Width = 225
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        Caption = '   '#1050#1072#1085#1072#1083#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object cbShowCID: TCheckBox
          Left = 4
          Top = 4
          Width = 55
          Height = 17
          Caption = 'CID'
          TabOrder = 0
          OnClick = cbShowCIDClick
        end
      end
      object ChList: TListBox
        Left = 39
        Top = 25
        Width = 186
        Height = 799
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 0
        OnClick = Button127Click
      end
      object ChList2: TListBox
        Left = 0
        Top = 25
        Width = 39
        Height = 799
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 2
        Visible = False
        OnClick = Button127Click
      end
    end
    object Panel45: TPanel
      Left = 0
      Top = 41
      Width = 229
      Height = 47
      Align = alTop
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Label29: TLabel
        Left = 0
        Top = 0
        Width = 229
        Height = 16
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = #1043#1088#1091#1087#1087#1072' '#1082#1072#1085#1072#1083#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlBottom
        ExplicitWidth = 185
      end
      object GroupList: TComboBox
        Left = 28
        Top = 22
        Width = 121
        Height = 21
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        TabStop = False
        OnClick = GroupListClick
      end
    end
    object Panel68: TPanel
      Left = 0
      Top = 0
      Width = 229
      Height = 41
      Align = alTop
      TabOrder = 2
      object Button5: TButton
        Left = 56
        Top = 8
        Width = 75
        Height = 25
        Caption = #1057#1082#1088#1099#1090#1100
        TabOrder = 0
        OnClick = Button5Click
      end
      object Button8: TButton
        Left = 0
        Top = 9
        Width = 12
        Height = 25
        Caption = '>'
        TabOrder = 1
        Visible = False
        OnClick = Button8Click
      end
    end
    object BScanDebugMemo2: TMemo
      Left = 0
      Top = 916
      Width = 229
      Height = 15
      Align = alBottom
      TabOrder = 3
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 202
    Height = 931
    Align = alLeft
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Panel50: TPanel
      Left = 0
      Top = 41
      Width = 202
      Height = 258
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object RailTypeTuningButton: TButton
        Left = 14
        Top = 62
        Width = 148
        Height = 26
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1058#1056
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = RailTypeTuningButtonClick
      end
      object TuningButton: TButton
        Left = 14
        Top = 32
        Width = 110
        Height = 29
        Caption = '[-] '#1053#1072#1089#1090#1088#1086#1081#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = TuningButtonClick
      end
      object PathSimButton: TButton
        Left = 14
        Top = 113
        Width = 99
        Height = 28
        Caption = '[-] '#1048#1084#1080#1090#1072#1090#1086#1088' '#1044#1055
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = PathSimButtonClick
      end
      object BoltJntButton: TButton
        Left = 14
        Top = 87
        Width = 110
        Height = 26
        Caption = '[-] '#1041#1086#1083#1090#1086#1074#1086#1081' '#1057#1090#1099#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = BoltJntButtonClick
      end
      object cbBScanDebug: TCheckBox
        Left = 21
        Top = 219
        Width = 143
        Height = 17
        Caption = #1057#1051#1058' / '#1043#1077#1085' / '#1055#1088#1080'-'#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        WordWrap = True
        OnClick = cbBScanDebugClick
      end
      object cbViewBScan: TCheckBox
        Left = 21
        Top = 147
        Width = 143
        Height = 19
        Caption = #1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        WordWrap = True
        OnClick = cbViewBScanClick
      end
      object CheckBox7: TCheckBox
        Left = 21
        Top = 203
        Width = 143
        Height = 17
        Caption = #1054#1090#1083#1072#1076#1082#1072' '#1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        WordWrap = True
        OnClick = CheckBox7Click
      end
      object CheckBox8: TCheckBox
        Left = 21
        Top = 235
        Width = 143
        Height = 17
        Caption = 'Device log'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        WordWrap = True
        OnClick = CheckBox8Click
      end
      object TrackBar1: TTrackBar
        Left = 14
        Top = 167
        Width = 150
        Height = 30
        Max = 35
        Min = 5
        Position = 22
        TabOrder = 8
      end
      object btnSaveAscan: TButton
        Left = 168
        Top = 29
        Width = 147
        Height = 39
        Caption = '[-] '#1047#1072#1087#1080#1089#1100' '#1040'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        Visible = False
        OnClick = btnSaveAscanClick
      end
      object PauseButton: TButton
        Left = 14
        Top = 2
        Width = 148
        Height = 29
        Caption = '[-] '#1055#1072#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        OnClick = PauseButtonClick
      end
      object AddKuSpinEdit: TSpinEdit
        Left = 125
        Top = 91
        Width = 37
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 11
        Value = 5
      end
      object CheckBox3: TCheckBox
        Left = 130
        Top = 37
        Width = 49
        Height = 20
        Caption = '!TT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
        WordWrap = True
        OnClick = CheckBox3Click
      end
      object PathSimButton2: TButton
        Left = 119
        Top = 113
        Width = 79
        Height = 28
        Caption = '[-] '#1048#1084'. '#1044#1055
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        OnClick = PathSimButton2Click
      end
    end
    object Panel60: TPanel
      Left = 0
      Top = 0
      Width = 202
      Height = 41
      Align = alTop
      TabOrder = 1
      object Button4: TButton
        Left = 56
        Top = 8
        Width = 75
        Height = 25
        Caption = #1057#1082#1088#1099#1090#1100
        TabOrder = 0
        OnClick = Button4Click
      end
      object Button6: TButton
        Left = 0
        Top = 8
        Width = 11
        Height = 25
        Caption = '>'
        TabOrder = 1
        Visible = False
        OnClick = Button6Click
      end
    end
    object ScrollBox2: TScrollBox
      Left = 0
      Top = 299
      Width = 202
      Height = 632
      HorzScrollBar.Visible = False
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 2
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 185
        Height = 736
        ActivePage = TabSheet12
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        MultiLine = True
        ParentFont = False
        TabOrder = 0
        OnChange = PageControl2Change
        object TabSheet6: TTabSheet
          Caption = #1044#1072#1090#1095#1080#1082#1080' '#1087#1091#1090#1080
          object Panel69: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 352
            Align = alTop
            TabOrder = 0
            object Label32: TLabel
              Left = 79
              Top = 0
              Width = 82
              Height = 13
              Caption = #1044#1055'      '#1048#1084#1080#1090#1072#1090#1086#1088
            end
            object Label33: TLabel
              Left = 8
              Top = 17
              Width = 60
              Height = 15
              AutoSize = False
              Caption = #1058#1077#1083#1077#1078#1082#1072
              WordWrap = True
            end
            object Label38: TLabel
              Left = 8
              Top = 38
              Width = 59
              Height = 17
              AutoSize = False
              Caption = #1057#1082#1072#1085#1077#1088' dL'
              WordWrap = True
            end
            object Label39: TLabel
              Left = 8
              Top = 58
              Width = 59
              Height = 18
              AutoSize = False
              Caption = #1057#1082#1072#1085#1077#1088' dH'
              WordWrap = True
            end
            object RadioButton1: TRadioButton
              Left = 18
              Top = 266
              Width = 113
              Height = 17
              Caption = #1047#1085#1072#1095#1077#1085#1080#1077
              Checked = True
              TabOrder = 4
              TabStop = True
            end
            object sgPathCounters: TStringGrid
              Left = 62
              Top = 15
              Width = 103
              Height = 61
              ColCount = 2
              DefaultColWidth = 49
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 3
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object PageControl1: TPageControl
              Left = 16
              Top = 98
              Width = 142
              Height = 136
              ActivePage = TabSheet1
              MultiLine = True
              TabOrder = 1
              object TabSheet1: TTabSheet
                Caption = #1048#1084#1080#1090#1072#1090#1086#1088' '#1089#1082#1072#1085#1077#1088#1072
                object Label35: TLabel
                  Left = 52
                  Top = 3
                  Width = 6
                  Height = 13
                  Caption = 'T'
                end
                object Label36: TLabel
                  Left = 52
                  Top = 27
                  Width = 7
                  Height = 13
                  Caption = 'A'
                end
                object Label37: TLabel
                  Left = 52
                  Top = 51
                  Width = 6
                  Height = 13
                  Caption = 'B'
                end
                object PSOn: TButton
                  Left = 3
                  Top = 4
                  Width = 42
                  Height = 25
                  Caption = #1042#1082#1083
                  TabOrder = 0
                  OnClick = PSOnClick
                end
                object PSOff: TButton
                  Left = 3
                  Top = 33
                  Width = 42
                  Height = 25
                  Caption = #1042#1099#1082#1083
                  TabOrder = 1
                  OnClick = PSOffClick
                end
                object UpDown4: TUpDown
                  Left = 107
                  Top = 3
                  Width = 16
                  Height = 21
                  Associate = SpinEdit2
                  Position = 25
                  TabOrder = 2
                end
                object SpinEdit2: TEdit
                  Left = 66
                  Top = 3
                  Width = 41
                  Height = 21
                  TabOrder = 3
                  Text = '25'
                end
                object UpDown5: TUpDown
                  Left = 107
                  Top = 25
                  Width = 16
                  Height = 21
                  Associate = SpinEdit3
                  Position = 25
                  TabOrder = 4
                end
                object SpinEdit3: TEdit
                  Left = 66
                  Top = 25
                  Width = 41
                  Height = 21
                  TabOrder = 5
                  Text = '25'
                end
                object UpDown6: TUpDown
                  Left = 106
                  Top = 47
                  Width = 16
                  Height = 21
                  Associate = SpinEdit4
                  Position = 25
                  TabOrder = 6
                end
                object SpinEdit4: TEdit
                  Left = 65
                  Top = 47
                  Width = 41
                  Height = 21
                  TabOrder = 7
                  Text = '25'
                end
              end
              object TabSheet3: TTabSheet
                Caption = #1048#1084#1080#1090#1072#1090#1086#1088' '#1088#1072#1089#1096'.'
                ImageIndex = 1
                object Label34: TLabel
                  Left = 54
                  Top = 39
                  Width = 6
                  Height = 13
                  Caption = 'T'
                end
                object Button3: TButton
                  Left = 3
                  Top = 6
                  Width = 42
                  Height = 25
                  Caption = #1042#1082#1083
                  TabOrder = 0
                  OnClick = PSOnClick
                end
                object Button13: TButton
                  Left = 3
                  Top = 32
                  Width = 42
                  Height = 25
                  Caption = #1042#1099#1082#1083
                  TabOrder = 1
                  OnClick = PSOffClick
                end
                object cbPathSimIndex1: TComboBox
                  Left = 72
                  Top = 8
                  Width = 41
                  Height = 21
                  Style = csDropDownList
                  ItemIndex = 0
                  TabOrder = 2
                  Text = '0'
                  Items.Strings = (
                    '0'
                    '1'
                    '2')
                end
                object UpDown3: TUpDown
                  Left = 109
                  Top = 35
                  Width = 16
                  Height = 21
                  Associate = SpinEdit1
                  Position = 25
                  TabOrder = 3
                end
                object SpinEdit1: TEdit
                  Left = 68
                  Top = 35
                  Width = 41
                  Height = 21
                  TabOrder = 4
                  Text = '25'
                end
              end
              object TabSheet4: TTabSheet
                Caption = #1048#1084#1080#1090#1072#1090#1086#1088
                ImageIndex = 2
                object Button14: TButton
                  Left = 26
                  Top = -1
                  Width = 42
                  Height = 25
                  Caption = #1042#1082#1083
                  TabOrder = 0
                  OnClick = PSOnClick
                end
                object Button16: TButton
                  Left = 25
                  Top = 30
                  Width = 42
                  Height = 25
                  Caption = #1042#1099#1082#1083
                  TabOrder = 1
                  OnClick = PSOffClick
                end
                object cbPathSimIndex2: TComboBox
                  Left = 74
                  Top = 19
                  Width = 41
                  Height = 21
                  Style = csDropDownList
                  ItemIndex = 0
                  TabOrder = 2
                  Text = '0'
                  Items.Strings = (
                    '0'
                    '1'
                    '2')
                end
              end
            end
            object ComboBox2: TComboBox
              Left = 115
              Top = 244
              Width = 41
              Height = 21
              Style = csDropDownList
              BiDiMode = bdLeftToRight
              ItemIndex = 0
              ParentBiDiMode = False
              TabOrder = 2
              Text = '0'
              Items.Strings = (
                '0'
                '1'
                '2')
            end
            object ComboBox3: TComboBox
              Left = 17
              Top = 244
              Width = 92
              Height = 21
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ItemIndex = 0
              ParentFont = False
              TabOrder = 3
              Text = #1044#1072#1090#1095#1080#1082' '#1087#1091#1090#1080
              Items.Strings = (
                #1044#1072#1090#1095#1080#1082' '#1087#1091#1090#1080
                #1048#1084#1080#1090#1072#1090#1086#1088)
            end
            object RadioButton2: TRadioButton
              Left = 18
              Top = 284
              Width = 113
              Height = 17
              Caption = #1054#1090#1089#1085#1086#1074#1085#1086#1081' '#1044#1055
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
            end
            object AmpSeries_Pos_P_45: TButton
              Left = 52
              Top = 302
              Width = 86
              Height = 25
              Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
              OnClick = AmpSeries_Pos_P_45Click
            end
            object UpDown2: TUpDown
              Left = 131
              Top = 266
              Width = 16
              Height = 21
              Associate = SpinEdit5
              Position = 25
              TabOrder = 7
            end
            object SpinEdit5: TEdit
              Left = 90
              Top = 266
              Width = 41
              Height = 21
              TabOrder = 8
              Text = '25'
            end
            object cbPathEncoderGraph: TCheckBox
              Left = 40
              Top = 328
              Width = 77
              Height = 17
              Caption = #1043#1088#1072#1092#1080#1082' '#1044#1055
              TabOrder = 9
            end
          end
          object Memo3: TMemo
            Left = 0
            Top = 352
            Width = 177
            Height = 153
            Align = alTop
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
        object TabSheet7: TTabSheet
          Caption = '2'#1058#1087
          ImageIndex = 1
          object Label27: TLabel
            Left = 0
            Top = 0
            Width = 177
            Height = 16
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' 2'#1058#1087' '#1076#1083#1103' '#1074#1089#1077#1093' '#1082#1072#1085#1072#1083#1086#1074
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Layout = tlBottom
            ExplicitTop = 16
            ExplicitWidth = 166
          end
          object Label28: TLabel
            Left = 110
            Top = 49
            Width = 17
            Height = 13
            Caption = #1084#1082#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object PrismDelayEdit: TEdit
            Left = 42
            Top = 45
            Width = 63
            Height = 21
            Ctl3D = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            Text = '100'
          end
          object Button2: TButton
            Left = 11
            Top = 80
            Width = 147
            Height = 39
            Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = Button2Click
          end
        end
        object TabSheet8: TTabSheet
          Caption = #1044#1072#1090#1095#1080#1082' '#1052#1077#1090#1072#1083#1083#1072
          ImageIndex = 2
          object MetallSensorInfo: TMemo
            Left = 0
            Top = 0
            Width = 177
            Height = 456
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
        end
        object TabSheet9: TTabSheet
          Caption = #1054#1090#1083#1072#1076#1082#1072' '#1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
          ImageIndex = 3
          object BScanGateLabel: TLabel
            Left = 13
            Top = 447
            Width = 171
            Height = 18
            Alignment = taCenter
            AutoSize = False
            Caption = #1057#1090#1088#1086#1073' '#1042'-'#1088#1072#1079#1074#1077#1077#1088#1090#1082#1080
            Layout = tlCenter
          end
          object BadBScanDelay: TLabel
            Left = 13
            Top = 471
            Width = 171
            Height = 18
            Alignment = taCenter
            AutoSize = False
            Caption = #1057#1080#1075#1085#1072#1083#1099'  '#1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080' - '#1054#1050
            Layout = tlCenter
          end
          object Panel27: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 185
            Align = alTop
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object CheckListBox1: TCheckListBox
              Left = 35
              Top = 71
              Width = 42
              Height = 113
              OnClickCheck = CheckListBox1Click
              ItemHeight = 13
              Items.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7')
              TabOrder = 0
              OnClick = CheckListBox1Click
            end
            object Button1323: TButton
              Left = 83
              Top = 102
              Width = 75
              Height = 25
              Caption = #1042#1067#1050#1051' '#1074#1089#1077
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnClick = Button1323Click
            end
            object Button9: TButton
              Left = 83
              Top = 77
              Width = 75
              Height = 25
              Caption = #1042#1050#1051' '#1074#1089#1077
              TabOrder = 2
              OnClick = Button9Click
            end
            object BScan12DB: TButton
              Left = 83
              Top = 152
              Width = 75
              Height = 25
              Caption = '-12 '#1076#1041
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              OnClick = BScan12DBClick
            end
            object BScan6DB: TButton
              Left = 83
              Top = 127
              Width = 75
              Height = 25
              Caption = '-6 '#1076#1041
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              OnClick = BScan6DBClick
            end
            object CheckBox5: TCheckBox
              Left = 10
              Top = -1
              Width = 159
              Height = 17
              Caption = #1053#1086#1084#1077#1088
              Checked = True
              State = cbChecked
              TabOrder = 5
              OnClick = CheckBox5Click
            end
            object CheckBox9: TCheckBox
              Left = 10
              Top = 16
              Width = 159
              Height = 17
              Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072' - '#1076#1041
              TabOrder = 6
              OnClick = CheckBox9Click
            end
            object CheckBox10: TCheckBox
              Left = 10
              Top = 33
              Width = 159
              Height = 17
              Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072' - '#1086#1090#1089#1095#1077#1090#1099' '#1040#1062#1055' '
              TabOrder = 7
              OnClick = CheckBox10Click
            end
            object CheckBox11: TCheckBox
              Left = 10
              Top = 50
              Width = 159
              Height = 17
              Caption = #1047#1072#1076#1077#1088#1078#1082#1072' - 0.1 '#1084#1082#1089
              TabOrder = 8
              OnClick = CheckBox11Click
            end
          end
          object BScanDebugMemo: TMemo
            Left = 0
            Top = 185
            Width = 177
            Height = 48
            Align = alTop
            TabOrder = 1
          end
          object BScanData: TStringGrid
            Left = 27
            Top = 256
            Width = 148
            Height = 186
            ColCount = 4
            DefaultColWidth = 32
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 9
            FixedRows = 0
            TabOrder = 2
          end
        end
        object TabSheet10: TTabSheet
          Caption = #1044#1072#1085#1085#1099#1077' '#1085#1072#1089#1090#1088#1086#1081#1082#1080' '#1085#1072' '#1090#1080#1087' '#1088#1077#1083#1100#1089#1072
          ImageIndex = 4
          object RailTypeTuningInfo: TMemo
            Left = 0
            Top = 30
            Width = 177
            Height = 426
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -9
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object Panel97: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 30
            Align = alTop
            TabOrder = 1
            object cbTrackingRailType: TCheckBox
              Left = 12
              Top = 4
              Width = 115
              Height = 17
              Caption = #1054#1090#1089#1083#1077#1078#1080#1074#1072#1085#1080#1077' '#1044#1057
              TabOrder = 0
              OnClick = cbTrackingRailTypeClick
            end
          end
        end
        object TabSheet11: TTabSheet
          Caption = #1056#1077#1078#1080#1084' '#1040#1057#1044
          ImageIndex = 5
          object sbFirstGateMode: TSpeedButton
            Left = 67
            Top = 29
            Width = 51
            Height = 25
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = sbFirstGateModeClick
          end
          object sbSecondGateMode: TSpeedButton
            Left = 67
            Top = 61
            Width = 51
            Height = 25
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = sbSecondGateModeClick
          end
          object Label2: TLabel
            Left = 33
            Top = 26
            Width = 34
            Height = 26
            Alignment = taCenter
            Caption = #1057#1090#1088#1086#1073' '#8470'1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
          object Label26: TLabel
            Left = 30
            Top = 60
            Width = 34
            Height = 26
            Alignment = taCenter
            Caption = #1057#1090#1088#1086#1073' '#8470'2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
        end
        object TabSheet12: TTabSheet
          Caption = #1040#1050
          ImageIndex = 6
          object Bevel1: TBevel
            Left = -6
            Top = 94
            Width = 182
            Height = 97
          end
          object Bevel2: TBevel
            Left = 3
            Top = 197
            Width = 166
            Height = 59
          end
          object Label40: TLabel
            Left = 9
            Top = 233
            Width = 156
            Height = 13
            Caption = #1055#1086#1088#1086#1075' = '#1047#1085#1072#1095#1077#1085#1080#1077' +              %'
          end
          object Label41: TLabel
            Left = 3
            Top = 176
            Width = 19
            Height = 13
            Caption = #1040#1058#1058
            WordWrap = True
          end
          object AC_On_Button: TButton
            Left = 0
            Top = 40
            Width = 96
            Height = 25
            Caption = #1042#1050#1051' ('#1058#1045#1057#1058')'
            TabOrder = 0
            OnClick = AC_On_ButtonClick
          end
          object AC_Off_Button: TButton
            Left = 40
            Top = 122
            Width = 96
            Height = 25
            Caption = #1042#1067#1050#1051
            TabOrder = 1
            OnClick = AC_Off_ButtonClick
          end
          object Calibrate_AC: TButton
            Left = 51
            Top = 205
            Width = 75
            Height = 25
            Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
            Enabled = False
            TabOrder = 2
            OnClick = Calibrate_ACClick
          end
          object cbViewAC: TCheckBox
            Left = 8
            Top = 89
            Width = 154
            Height = 17
            Caption = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100' '#1089#1091#1084#1084#1091
            TabOrder = 3
            OnClick = cbViewACClick
          end
          object SetStartACDelay: TButton
            Left = 104
            Top = 170
            Width = 68
            Height = 25
            Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
            TabOrder = 4
            OnClick = SetStartACDelayClick
          end
          object cbViewACState: TCheckBox
            Left = 9
            Top = 23
            Width = 154
            Height = 17
            Caption = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
            TabOrder = 5
            OnClick = cbViewACClick
          end
          object cbViewACTh: TCheckBox
            Left = 8
            Top = 104
            Width = 154
            Height = 17
            Caption = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100' '#1087#1086#1088#1086#1075
            TabOrder = 6
            OnClick = cbViewACClick
          end
          object Button1: TButton
            Left = 40
            Top = 0
            Width = 96
            Height = 25
            Caption = #1042#1050#1051
            TabOrder = 7
            OnClick = Button1Click
          end
          object RrsButton7: TButton
            Left = 0
            Top = 65
            Width = 96
            Height = 25
            Caption = #1057#1073#1088#1086#1089
            TabOrder = 8
            OnClick = RrsButton7Click
          end
          object seACStartDelay: TEdit
            Left = 35
            Top = 174
            Width = 36
            Height = 21
            TabOrder = 9
            Text = '35'
          end
          object UpDown1: TUpDown
            Left = 71
            Top = 174
            Width = 16
            Height = 21
            Associate = seACStartDelay
            Position = 35
            TabOrder = 10
          end
          object seACThAddVal: TEdit
            Left = 115
            Top = 230
            Width = 21
            Height = 21
            TabOrder = 11
            Text = '10'
          end
          object UpDown7: TUpDown
            Left = 136
            Top = 230
            Width = 16
            Height = 21
            Associate = seACThAddVal
            Position = 10
            TabOrder = 12
          end
          object cbNoiseLog: TCheckBox
            Left = 102
            Top = 53
            Width = 67
            Height = 17
            Caption = 'Log '#1092#1072#1081#1083
            TabOrder = 13
            OnClick = cbNoiseLogClick
          end
          object GenOffButton: TButton
            Left = 51
            Top = 262
            Width = 75
            Height = 25
            Caption = 'Gen Off'
            TabOrder = 14
            OnClick = GenOffButtonClick
          end
          object GenOnButton: TButton
            Left = 51
            Top = 293
            Width = 75
            Height = 25
            Caption = 'Gen On'
            TabOrder = 15
            OnClick = GenOnButtonClick
          end
          object SpinEdit7: TSpinEdit
            Left = 17
            Top = 360
            Width = 121
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 16
            Value = 1000
            OnChange = SpinEdit7Change
          end
        end
        object TabSheet13: TTabSheet
          Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103' '#1042'-'#1088#1072#1079#1074#1077#1088#1090#1082#1080
          ImageIndex = 7
          object BScanFiltrOff_: TButton
            Left = 37
            Top = 38
            Width = 92
            Height = 25
            Caption = #1042#1099#1082#1083
            TabOrder = 0
            OnClick = BScanFiltrOff_Click
          end
          object BScanFiltrOn_: TButton
            Left = 37
            Top = 7
            Width = 92
            Height = 25
            Caption = #1042#1082#1083
            TabOrder = 1
            OnClick = BScanFiltrOn_Click
          end
          object Memo1: TMemo
            Left = 0
            Top = 69
            Width = 164
            Height = 241
            TabOrder = 2
          end
        end
        object TabSheet14: TTabSheet
          Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103
          ImageIndex = 8
          object Label31: TLabel
            Left = 16
            Top = 6
            Width = 27
            Height = 13
            Caption = #1056#1077#1083#1077' '
          end
          object HSDeviceConfig: TButton
            Left = 41
            Top = 132
            Width = 102
            Height = 62
            Caption = ' '#1040#1074#1080#1082#1086#1085'-31  '#1057#1082#1072#1085#1077#1088' '#1075#1086#1083#1086#1074#1082#1080
            TabOrder = 0
            Visible = False
            WordWrap = True
            OnClick = HSDeviceConfigClick
          end
          object A31DeviceConfig: TButton
            Left = 13
            Top = 30
            Width = 147
            Height = 58
            Caption = 
              '                  '#1057#1084#1077#1085#1072'                  '#1057#1087#1083#1086#1096#1085#1086#1081' '#1082#1086#1085#1090#1088#1086#1083#1100' '#1057#1082#1072#1085#1077 +
              #1088' '#1075#1086#1083#1086#1074#1082#1080
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            WordWrap = True
            OnClick = A31DeviceConfigClick
          end
          object Test: TButton
            Left = 52
            Top = 101
            Width = 75
            Height = 25
            Caption = 'Test'
            TabOrder = 2
            OnClick = TestClick
          end
          object cbSwitching: TComboBox
            Left = 52
            Top = 3
            Width = 106
            Height = 21
            Style = csDropDownList
            ItemIndex = 0
            TabOrder = 3
            Text = #1057#1087#1083#1086#1096#1085#1086#1081' '#1082#1086#1085#1090#1088#1086#1083#1100
            OnChange = cbSwitchingChange
            Items.Strings = (
              #1057#1087#1083#1086#1096#1085#1086#1081' '#1082#1086#1085#1090#1088#1086#1083#1100
              #1057#1082#1072#1085#1077#1088)
          end
        end
        object TabSheet16: TTabSheet
          Caption = #1040#1050#1050
          ImageIndex = 9
          object StatusLabel: TLabel
            Left = 28
            Top = 16
            Width = 120
            Height = 13
            Alignment = taCenter
            AutoSize = False
            Caption = '-'
          end
          object AkkButton_: TButton
            Left = 51
            Top = 48
            Width = 75
            Height = 25
            Caption = 'AkkButton_'
            TabOrder = 0
            OnClick = AkkButton_Click
          end
        end
        object TabSheet17: TTabSheet
          Caption = #1040#1074#1090#1086#1085#1072#1089#1090#1088#1086#1081#1082#1072' '#1050#1091
          ImageIndex = 10
          object AutoCal_Enable: TButton
            Left = 51
            Top = 25
            Width = 75
            Height = 25
            Caption = #1042#1050#1051
            TabOrder = 0
            OnClick = AutoCal_EnableClick
          end
          object AutoCal_Disable: TButton
            Left = 51
            Top = 56
            Width = 75
            Height = 25
            Caption = #1042#1067#1050#1051
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = AutoCal_DisableClick
          end
        end
        object TabSheet19: TTabSheet
          Caption = #1059#1089#1090#1072#1085#1086#1074#1083#1077#1085#1085#1072#1103' '#1090#1072#1073#1083#1080#1094#1072' '#1090#1072#1082#1090#1086#1074
          ImageIndex = 11
          OnShow = TabSheet19Show
          object StrokeTableMemo: TMemo
            Left = 0
            Top = 19
            Width = 177
            Height = 437
            Align = alClient
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssHorizontal
            TabOrder = 0
          end
          object Panel61: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 19
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object RadioButton3: TRadioButton
              Left = 6
              Top = 0
              Width = 78
              Height = 17
              Caption = #1053#1086#1084#1077#1088#1072' '#1089' 0'
              TabOrder = 0
              OnClick = RadioButton3Click
            end
            object RadioButton4: TRadioButton
              Left = 88
              Top = 0
              Width = 78
              Height = 17
              Caption = #1053#1086#1084#1077#1088#1072' '#1089' 1'
              Checked = True
              TabOrder = 1
              TabStop = True
              OnClick = RadioButton3Click
            end
          end
        end
        object TabSheet20: TTabSheet
          Caption = #1047#1072#1076#1072#1085#1080#1077' '#1075#1077#1085#1077#1088#1072#1090#1086#1088#1072'/'#1087#1088#1080#1077#1084#1085#1080#1082#1072
          ImageIndex = 12
          object Label3: TLabel
            Left = 86
            Top = 61
            Width = 64
            Height = 13
            Caption = 'Gen         Rec'
          end
          object Label4: TLabel
            Left = 2
            Top = 82
            Width = 80
            Height = 91
            Caption = 
              'Left,  Line 0 (1)                        Left,  Line 1 (2)'#9'     ' +
              '                Right, Line 0 (1)'#9'                        Right,' +
              ' Line 1 (2)'
            WordWrap = True
          end
          object Edit1: TEdit
            Left = 86
            Top = 80
            Width = 22
            Height = 21
            TabOrder = 0
            Text = '0'
          end
          object UpDown8: TUpDown
            Left = 108
            Top = 80
            Width = 16
            Height = 21
            Associate = Edit1
            Max = 15
            TabOrder = 1
            OnChangingEx = UpDown8ChangingEx
          end
          object Edit2: TEdit
            Left = 131
            Top = 80
            Width = 22
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object UpDown9: TUpDown
            Left = 153
            Top = 80
            Width = 16
            Height = 21
            Associate = Edit2
            Max = 15
            TabOrder = 3
            OnChangingEx = UpDown9ChangingEx
          end
          object Edit3: TEdit
            Left = 86
            Top = 107
            Width = 22
            Height = 21
            TabOrder = 4
            Text = '0'
          end
          object UpDown10: TUpDown
            Left = 108
            Top = 107
            Width = 16
            Height = 21
            Associate = Edit3
            Max = 15
            TabOrder = 5
            OnChangingEx = UpDown10ChangingEx
          end
          object Edit4: TEdit
            Left = 131
            Top = 107
            Width = 22
            Height = 21
            TabOrder = 6
            Text = '0'
          end
          object UpDown11: TUpDown
            Left = 153
            Top = 107
            Width = 16
            Height = 21
            Associate = Edit4
            Max = 15
            TabOrder = 7
            OnChangingEx = UpDown11ChangingEx
          end
          object Edit5: TEdit
            Left = 86
            Top = 133
            Width = 22
            Height = 21
            TabOrder = 8
            Text = '0'
          end
          object UpDown12: TUpDown
            Left = 108
            Top = 133
            Width = 16
            Height = 21
            Associate = Edit5
            Max = 15
            TabOrder = 9
            OnChangingEx = UpDown12ChangingEx
          end
          object Edit6: TEdit
            Left = 131
            Top = 133
            Width = 22
            Height = 21
            TabOrder = 10
            Text = '0'
          end
          object UpDown13: TUpDown
            Left = 153
            Top = 133
            Width = 16
            Height = 21
            Associate = Edit6
            Max = 15
            TabOrder = 11
            OnChangingEx = UpDown13ChangingEx
          end
          object Edit7: TEdit
            Left = 86
            Top = 159
            Width = 22
            Height = 21
            TabOrder = 12
            Text = '0'
          end
          object UpDown14: TUpDown
            Left = 108
            Top = 159
            Width = 16
            Height = 21
            Associate = Edit7
            Max = 15
            TabOrder = 13
            OnChangingEx = UpDown14ChangingEx
          end
          object Edit8: TEdit
            Left = 131
            Top = 159
            Width = 22
            Height = 21
            TabOrder = 14
            Text = '0'
          end
          object UpDown15: TUpDown
            Left = 153
            Top = 159
            Width = 16
            Height = 21
            Associate = Edit8
            Max = 15
            TabOrder = 15
            OnChangingEx = UpDown15ChangingEx
          end
          object Panel62: TPanel
            Left = 6
            Top = 8
            Width = 161
            Height = 41
            TabOrder = 16
            object Label5: TLabel
              Left = 1
              Top = 1
              Width = 159
              Height = 39
              Align = alClient
              Alignment = taCenter
              Caption = #1056#1072#1073#1086#1090#1072#1077#1090' '#1090#1086#1083#1100#1082#1086' '#1076#1083#1103' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1080' TEST 2 !!!'
              Layout = tlCenter
              WordWrap = True
              ExplicitWidth = 124
              ExplicitHeight = 26
            end
          end
        end
        object TabSheet21: TTabSheet
          Caption = #1057#1087#1080#1089#1086#1082' '#1073#1083#1086#1082#1086#1074' / '#1057#1090#1072#1090#1091#1089' '#1073#1083#1086#1082#1086#1074
          ImageIndex = 13
          object BlockListMemo: TMemo
            Left = 0
            Top = 28
            Width = 177
            Height = 304
            Align = alClient
            TabOrder = 0
          end
          object Panel63: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 28
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object Button11: TButton
              Left = 48
              Top = 1
              Width = 75
              Height = 25
              Caption = #1055#1086#1083#1091#1095#1080#1090#1100
              TabOrder = 0
              OnClick = Button11Click
            end
          end
          object Panel70: TPanel
            Left = 0
            Top = 332
            Width = 177
            Height = 124
            Align = alBottom
            TabOrder = 2
          end
        end
        object Разное: TTabSheet
          Caption = #1056#1072#1079#1085#1086#1077
          ImageIndex = 14
          object ScrollBox3: TScrollBox
            Left = 0
            Top = 0
            Width = 177
            Height = 456
            Align = alClient
            TabOrder = 0
            object Label6: TLabel
              Left = 3
              Top = 56
              Width = 100
              Height = 13
              Caption = #1057#1077#1089#1080#1080' B-'#1088#1072#1079#1074#1077#1088#1090#1082#1080':'
            end
            object TestButton3: TButton
              Left = 8
              Top = 262
              Width = 138
              Height = 22
              Caption = 'GetScanZeroProbeAmpl'
              TabOrder = 0
              OnClick = TestButton3Click
            end
            object BScanSesionMemo: TMemo
              Left = 3
              Top = 74
              Width = 146
              Height = 161
              TabOrder = 1
            end
            object Button12_: TButton
              Left = 28
              Top = 8
              Width = 113
              Height = 22
              Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100' '#1042#1057#1025
              TabOrder = 2
              OnClick = TestButtonClick
            end
            object Button13__: TButton
              Left = 28
              Top = 31
              Width = 113
              Height = 22
              Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1042#1057#1025
              TabOrder = 3
              OnClick = Button13__Click
            end
            object Panel77: TPanel
              Left = 11
              Top = 484
              Width = 135
              Height = 4
              TabOrder = 4
            end
            object WorkButton: TButton
              Left = 43
              Top = 300
              Width = 75
              Height = 25
              Caption = 'Work'
              TabOrder = 5
              OnClick = WorkButtonClick
            end
            object UnWorkButton: TButton
              Left = 43
              Top = 331
              Width = 75
              Height = 25
              Caption = 'UnWork'
              TabOrder = 6
              OnClick = UnWorkButtonClick
            end
            object Button_17: TButton
              Left = 43
              Top = 428
              Width = 75
              Height = 25
              Caption = 'Button_17'
              TabOrder = 7
              OnClick = Button_17Click
            end
            object ComboBox4: TComboBox
              Left = 43
              Top = 379
              Width = 75
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 8
              Text = #1057#1093#1077#1084#1072' 1'
              Items.Strings = (
                #1057#1093#1077#1084#1072' 1'
                #1057#1093#1077#1084#1072' 2'
                #1057#1093#1077#1084#1072' 3')
            end
            object Panel78: TPanel
              Left = 39
              Top = 408
              Width = 86
              Height = 16
              Caption = '-'
              TabOrder = 9
            end
          end
        end
        object tsGates: TTabSheet
          Caption = #1057#1090#1088#1086#1073#1099
          ImageIndex = 15
          object sgGates: TStringGrid
            Left = 12
            Top = 17
            Width = 148
            Height = 186
            ColCount = 3
            DefaultColWidth = 32
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 9
            FixedRows = 0
            TabOrder = 0
          end
        end
        object TabSheet22: TTabSheet
          Caption = #1042#1086#1079#1074#1088#1072#1097#1077#1085#1080#1077' '#1050#1091
          ImageIndex = 16
          object Label8: TLabel
            Left = 7
            Top = 42
            Width = 128
            Height = 13
            Caption = #1042#1088#1077#1084#1103'                           '#1089#1077#1082
          end
          object CheckBox2: TCheckBox
            Left = 40
            Top = 16
            Width = 97
            Height = 17
            Caption = #1042#1082#1083
            TabOrder = 0
            OnClick = CheckBox2Click
          end
          object SpinEdit6: TSpinEdit
            Left = 40
            Top = 39
            Width = 73
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 1
            Value = 10
            OnChange = SpinEdit6Change
          end
        end
        object TabSheet30: TTabSheet
          Caption = #1054#1090#1083#1072#1076#1082#1072' '#1076#1074#1072' '#1041#1059#1052#1072
          ImageIndex = 17
          object UMU0_Memo: TMemo
            Left = 3
            Top = 80
            Width = 90
            Height = 402
            TabOrder = 0
          end
          object UMU1_Memo: TMemo
            Left = 99
            Top = 80
            Width = 90
            Height = 402
            TabOrder = 1
          end
          object Panel96: TPanel
            Left = 0
            Top = 0
            Width = 177
            Height = 65
            Align = alTop
            TabOrder = 2
            object PathEncoderDebug: TStringGrid
              Left = 1
              Top = 1
              Width = 175
              Height = 63
              Align = alClient
              ColCount = 2
              DefaultColWidth = 84
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 3
              FixedRows = 0
              ScrollBars = ssNone
              TabOrder = 0
            end
          end
        end
        object TabSheet31: TTabSheet
          Caption = #1050#1091' '#1086#1090' '#1089#1082#1086#1088#1086#1089#1090#1080
          ImageIndex = 18
          object SpeedState: TLabel
            Left = 3
            Top = 104
            Width = 171
            Height = 41
            AutoSize = False
            Caption = #1057#1082#1086#1088#1086#1089#1090#1100':'
            WordWrap = True
          end
          object Button28: TButton
            Left = 52
            Top = 32
            Width = 75
            Height = 25
            Caption = #1042#1082#1083
            TabOrder = 0
            OnClick = Button28Click
          end
          object Button29: TButton
            Left = 52
            Top = 63
            Width = 75
            Height = 25
            Caption = #1042#1099#1082#1083
            TabOrder = 1
            OnClick = Button29Click
          end
          object PathStepEdit: TEdit
            Left = 37
            Top = 5
            Width = 108
            Height = 21
            TabOrder = 2
            Text = '2,22'
          end
        end
      end
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 256
    Top = 256
  end
  object ClickTimer: TTimer
    OnTimer = ClickTimerTimer
    Left = 256
    Top = 688
  end
  object UpdateLog: TTimer
    Interval = 500
    OnTimer = UpdateLogTimer
    Left = 256
    Top = 144
  end
  object ChangeModeTimer_: TTimer
    Interval = 300
    Left = 256
    Top = 200
  end
  object Timer2: TTimer
    Interval = 100
    OnTimer = Timer2Timer
    Left = 256
    Top = 368
  end
  object DrawThreadTimer: TTimer
    Interval = 50
    OnTimer = DrawThreadTimerTimer
    Left = 256
    Top = 312
  end
  object Timer3: TTimer
    Interval = 250
    OnTimer = Timer3Timer
    Left = 256
    Top = 488
  end
  object HandScanTimeTimer: TTimer
    Interval = 5000
    Left = 256
    Top = 616
  end
  object BScanHandTimer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = BScanHandTimerTimer
    Left = 256
    Top = 432
  end
  object Timer5: TTimer
    Interval = 200
    OnTimer = Timer5Timer
    Left = 256
    Top = 544
  end
  object UMUEventTimer: TTimer
    Enabled = False
    OnTimer = UMUEventTimerTimer
    Left = 352
    Top = 144
  end
end
