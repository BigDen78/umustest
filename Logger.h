//---------------------------------------------------------------------------

#ifndef UMUTest_LoggerH_
#define UMUTest_LoggerH_

#include <stdlib.h>
#include <string>
#include <memory>
#include <map>
#include <vector>
#include <list>
#include <queue>
#include <time.h>
#include <stdarg.h>
#include <cstdarg>


#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOGDI
#include <windows.h>
#else
#include <stdlib.h>
#include <pthread.h>
#endif

//RGB macro for setting color ad DWORD (uint32)
//#define RGB(r,g,b)          ((unsigned int)(((unsigned char)(r)|((unsigned short)((unsigned char)(g))<<8))|(((unsigned int)(unsigned char)(b))<<16)))

//�� ��� ���, ���� �� �������� �������� ������� ��������,
	//��������� ����� ������� � ������ (�� ��������� �� �����) ����� ���� �����
	//��� �� ���� �� �������� �� ������� � ����������� �� Borland C++


//---------------�������� (����� ���� ������� ���)---------
template<class T>
class Singleton
{
public:
	static T& Instance()
	{
		if(_self.get()==0)
			_self.reset(new T);
		return *_self.get();
	}

protected:
	Singleton() {};
	virtual ~Singleton() {_self.release();}
private:
	static std::unique_ptr<T> _self;
};

template <class T>
std::unique_ptr<T> Singleton<T>::_self;
//------------------------------------------------------------------------------

//-----------------����������� ������ (������ ��������)-------------------------

#ifdef WIN32
  typedef CRITICAL_SECTION MutexType;
  #define __CurrThreadId GetCurrentThreadId

#else
  typedef pthread_mutex_t MutexType;
  #define __CurrThreadId pthread_self
#endif

class Mutex
{
public:
	inline Mutex();
	inline ~Mutex();

	inline void Lock();
	inline void Unlock();
	inline bool TryLock();
private:
	MutexType __mutex;

	//Copy protection
	Mutex(const Mutex&);
	void operator=(const Mutex&);
};

#ifdef WIN32
// ���������� ����� Windows API
Mutex::Mutex()             { InitializeCriticalSection(&__mutex); }
Mutex::~Mutex()            { DeleteCriticalSection(&__mutex); }
void Mutex::Lock()         { EnterCriticalSection(&__mutex); }
void Mutex::Unlock()       { LeaveCriticalSection(&__mutex); }
bool Mutex::TryLock()	   { return TryEnterCriticalSection(&__mutex); }

#else // WIN32
// UNIX ������ ����� pthread
Mutex::Mutex()             { pthread_mutex_init(&__mutex, NULL); }
Mutex::~Mutex()            { pthread_mutex_destroy(&__mutex); }
void Mutex::Lock()         { pthread_mutex_lock(&__mutex); }
void Mutex::Unlock()       { pthread_mutex_unlock(&__mutex); }
bool Mutex::TryLock()	   { return pthread_mutex_trylock(&__mutex)!=0; }
#endif // WIN32

//������� ��� ������������� � ������� (Lock � ������������, Unlock � �����������)
template<class LockType>
class AutoLock
{
public:
	AutoLock(LockType& lock_) : lock(lock_) {lock.Lock();};
	~AutoLock() {lock.Unlock();};

private:
	LockType& lock;
};

template<class LockType>
class AutoLockWinCS
{
public:
	AutoLockWinCS(LockType& lock_) : lock(lock_) {lock.Enter();};
	~AutoLockWinCS() {lock.Release();};

private:
	LockType& lock;
};

#define AUTO_LOCK(m) AutoLock<Mutex> __AutoLock(m);
#define AUTO_LOCK_WCS(m) AutoLockWinCS<cCriticalSectionWin> __AutoLockWinCS(m);

//------------------------------------------------------------------------------


//--------------!!  LOGGER STARTS HERE  !!--------------------------------------


//�� ����� ������, �� �������� �� �������� ����������� ��������
//� ANSI ��������
#ifdef _UNICODE
typedef std::wstring _tstring;
#else
typedef std::string _tstring;
#endif

//----------------���������� �������--------------------------------------------


#if defined(LOGGER_OUT_FILE_FUNC_LINE)
	#define _PLACE_LOGDATA(x) _T("[%s, %s, %d] ")_T(x),_T(__FILE__),_T(__FUNCTION__),__LINE__
#elif defined(LOGGER_OUT_FUNC_LINE)
	#define _PLACE_LOGDATA(x) _T("[%s, %d] ")_T(x),_T(__FUNCTION__),__LINE__
#elif defined(LOGGER_OUT_FUNC)
	#define _PLACE_LOGDATA(x) _T("[%s] ")_T(x),_T(__FUNCTION__)
#else
	#define _PLACE_LOGDATA(x) x
#endif


inline unsigned int LogpersHashFunc(const TCHAR* data)
{
	int len = _tcslen(data);
	UINT hash = 0;

	for(int i=0;i<len;i++)
	{
		hash = (hash + data[i])*data[i];
	}
	return hash;
}

//Macro for calling logger with filepath, function name and codeline in log string
//Simple
//example: LOGINFO("SomeText: %d",SomeVar);
#define LOGINFO(x,...) Logger::Info(Logger::CHANNEL_ID_MAIN, _PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGWARNING(x,...) Logger::Warning(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGERROR(x,...) Logger::Error(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGCRITICAL(x,...) Logger::CriticalError(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x),__VA_ARGS__)

//With no arguments (��������� ��������� (�� ���� ���������� �������) �� �������� ����� "..." ��� ����������)
//example: LOGINFO_NF("SomeText");
#define LOGINFO_NF(x) Logger::Info(Logger::CHANNEL_ID_MAIN, _PLACE_LOGDATA(x))
#define LOGWARNING_NF(x) Logger::Warning(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x))
#define LOGERROR_NF(x) Logger::Error(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x))
#define LOGCRITICAL_NF(x) Logger::CriticalError(Logger::CHANNEL_ID_MAIN,_PLACE_LOGDATA(x))

//With color
//example: LOGINFO_COL(RGB(255,0,0),"SomeText: %d",SomeVar);
#define LOGINFO_COL(col,x,...) Logger::Message(Logger::CHANNEL_ID_MAIN,Logger::TYPE_INFO,col,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGWARNING_COL(col,x,...) Logger::Warning(Logger::CHANNEL_ID_MAIN,Logger::TYPE_WARNING,col,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGERROR_COL(col,x,...) Logger::Error(Logger::CHANNEL_ID_MAIN,Logger::TYPE_ERROR,col,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGCRITICAL_COL(col,x,...) Logger::CriticalError(Logger::CHANNEL_ID_MAIN,Logger::TYPE_CRITICAL,col,_PLACE_LOGDATA(x),__VA_ARGS__)
//With channel
//example: LOGINFOEX(nChannel,"SomeText: %d",SomeVar);
#define LOGINFOEX(y,x,...) Logger::Info(y, _PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGWARNINGEX(y,x,...) Logger::Warning(y,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGERROREX(y,x,...) Logger::Error(y,_PLACE_LOGDATA(x),__VA_ARGS__)
#define LOGCRITICALEX(y,x,...) Logger::CriticalError(y,_PLACE_LOGDATA(x),__VA_ARGS__)

//Persistent log
#define LOGPERS_HASH(x) LogpersHashFunc(x)
#define LOGPERS(y,x,...) Logger::PersistentInfo(LOGPERS_HASH(_T(y)),Logger::CHANNEL_ID_MAIN, _T(x),__VA_ARGS__)
#define LOGPERS_NF(y,x) Logger::PersistentInfo(LOGPERS_HASH(_T(y)),Logger::CHANNEL_ID_MAIN, _T(x))

//------------------------------------------------------------------------------

 namespace Logger
 {
	//���� ��������� (�������� ���������������� - LOG_MSG_USER+N)
	const int LOG_MSG_UNKNOWN = 0;
	const int LOG_MSG_INFO = 1;
	const int LOG_MSG_WARNING = 2;
	const int LOG_MSG_ERROR = 3;
	const int LOG_MSG_CRITICAL = 4;
	const int LOG_MSG_USER = 10;

	//�������������� ������� (�������� ���������������� - 1,2,3,...)
	//Up to 32 channels
	const unsigned int CHANNEL_ID_ALL = 0xFFFFFFFF;
	const unsigned int CHANNEL_ID_MAIN= 0x1;
	const unsigned int CHANNEL_ID1   = 0x1;
	const unsigned int CHANNEL_ID2   = 0x2;
	const unsigned int CHANNEL_ID3   = 0x4;
	const unsigned int CHANNEL_ID4   = 0x8;
	const unsigned int CHANNEL_ID5   = 0x10;
	const unsigned int CHANNEL_ID6   = 0x20;
	const unsigned int CHANNEL_ID7   = 0x40;
	const unsigned int CHANNEL_ID8   = 0x80;
	const unsigned int CHANNEL_ID9   = 0x100;
	const unsigned int CHANNEL_ID10  = 0x200;
	const unsigned int CHANNEL_ID11  = 0x400;
	const unsigned int CHANNEL_ID12  = 0x800;
	const unsigned int CHANNEL_ID13  = 0x1000;
	const unsigned int CHANNEL_ID14  = 0x2000;
	const unsigned int CHANNEL_ID15  = 0x4000;
	const unsigned int CHANNEL_ID16  = 0x8000;



	struct sLoggerMessage
	{
		sLoggerMessage()
		{
			type=LOG_MSG_UNKNOWN;
			channel=0;
			persistId=0;
			threadId=0;
			color=0xFFFFFFFF;
			msgTime=0;
		};

		_tstring data; //string mlog message
		UINT color;  //message color (if listener supports color)
		int type;            //message type
		UINT channel;         //message channel
		UINT threadId;			//Thread id
		UINT persistId;			//Persistence hash
		time_t msgTime;      //time

		_tstring toString(); //converts message to string with time and message type
	};

	enum eListenerCollectType
	{
		LCT_CALL_ON_MESSAGE = 0,  //Call processMessage() on every message (neesd syncronize with multiple threads (depends on context))
		LCT_PUSH_MESSAGE,			// Syncronized push and pop
		LCT_BOTH,
	};

	//Interface for logger listener
	class CLoggerListener
	{
	public:
		CLoggerListener();
		CLoggerListener(UINT channelMask);
		virtual ~CLoggerListener();

		void setChannelByIndex(int channelIndex, bool bEnable);
		void setChannelByMask(UINT channelMask, bool bEnable);
		void setChannelMask(UINT channelMask);

		//On adding listener - uploads older messages
		//overwrite for set
		virtual bool needUploadOlderMessages() {return true;};

		//collect type
		//overwrite for set
		virtual eListenerCollectType getCollectType() {return LCT_PUSH_MESSAGE;};

		UINT getChannelsMask() {return channelsMask;};

		//procesing input messages (out to screen, file, etc)
		//need syncronize: example - LOGINFO(thread2) => LoggerCore => Listener => Draws in MainThread
		//overwrite it if inerhit from CLoggerListener with collectType = LCT_CALL_ON_MESSAGE or LCT_BOTH
		virtual void onNewMessage(sLoggerMessage& message) {};
		virtual void onPersistMessage(sLoggerMessage& message) {};

		void processMessage(sLoggerMessage& message);

		//2nd variant of collecting messages - syncronized push and pop
		//collectType = LCT_PUSH_MESSAGE or LCT_BOTH
		//�� ������� ������������� �� CLoggerListener
		virtual void push(sLoggerMessage& message);
		virtual bool pop(sLoggerMessage& message);

		bool isNewPersistendData();
		//�� ����� ������ ������ - ������ ������ ���� ������ ���� ����������� ������ ��� �� ����.
		//������, ���� ������ ������ ���� ��� �� ������ ������� (��������, 100 - 300 ��) - ��� �� �������� �� ��������
		bool readPersistentData(sLoggerMessage** ppData, UINT* pSize, bool bSort = false);
	private:
		//multi-thread part (not safe for use)
		std::queue<sLoggerMessage> dataList;
		Mutex arrayLock;
		volatile bool hasPersistentVariables;
		std::map<UINT,sLoggerMessage> persistVariables;
	protected:
		//single-thread part
		UINT channelsMask;
		UINT MaxMessageCount;
		UINT MessageLostCount;

	};

	//contains listeners for one channel
	typedef std::vector<CLoggerListener*> ListenersArray;
    //contains array of listeners for any used channel
	//�.�. ����� ������ ����� ���� ����� - ���������� map
	typedef std::map<int, ListenersArray> ListenersMap;

	//�ontains all channel info and all messages (or msg buffer) - Singleton
	class CLoggerCore : public Singleton<CLoggerCore>
	{
	public:
		CLoggerCore();
		~CLoggerCore();

		void addListener(CLoggerListener* pListener);
		void removeListener(CLoggerListener* pListener);


		void addMessage(UINT channelMask, int type, unsigned int color, const TCHAR* str);
		void addMessageVA(UINT channelMask, int type, unsigned int color, const TCHAR* str, va_list& args);
		void addMessageFmt(UINT channelMask, int type, unsigned int color, const TCHAR* str, ...);

		void addPersistentMessage( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str);
		void addPersistentMessageVA( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str, va_list& args);
		void addPersistentMessageFmt( UINT hash, UINT channelMask, int type, unsigned int color, const TCHAR* str, ...);

		UINT getInfoCount() {return MsgInfoCnt;};
		UINT getErrorCount() {return MsgErrorCnt;};
		UINT getWarningCount() {return MsgWarningCnt;};
		UINT getUnknownCount() {return MsgUnknownCnt;};
		UINT getOthersCount() {return MsgOtherCnt;};
	protected:
		//ListenersArray& _getChannelListeners(int channelMask);
		int _findListener(CLoggerListener* pListener);//,int channelMask);
	protected:
        UINT messageLimit;
		std::list<sLoggerMessage> messages;
		//map <channel id, assigned listeners>
		//ListenersMap listeners;

		ListenersArray channelListeners[32];
		ListenersArray allListeners;

		Mutex msgLock;

		int MsgWarningCnt;
		int MsgErrorCnt;
		int MsgInfoCnt;
		int MsgUnknownCnt;
		int MsgOtherCnt; ///
	};

	inline CLoggerCore& GetLogger()
	{
		return (CLoggerCore&)CLoggerCore::Instance();
	};

	void Info(UINT channelMask,const TCHAR* str, ...);
	void Warning(UINT channelMask, const TCHAR* str, ...);
	void Error(UINT channelMask, const TCHAR* str, ...);
	void CriticalError(UINT channelMask, const TCHAR* str, ...);
	void Message(UINT channelMask, int type,unsigned int color, const TCHAR* str, ...);

	void PersistentInfo(UINT hash, UINT channelMask,const TCHAR* str, ...);
 };


//---------------------------------------------------------------------------
#endif
