object LogForm: TLogForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSizeToolWin
  Caption = 'Log Form'
  ClientHeight = 464
  ClientWidth = 1147
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1147
    Height = 464
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    Caption = ' '
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 6
      Top = 68
      Height = 393
      Visible = False
      ExplicitLeft = 455
      ExplicitTop = 38
      ExplicitHeight = 292
    end
    object Panel6: TPanel
      Left = 3
      Top = 3
      Width = 1141
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel48: TPanel
        Left = 71
        Top = 0
        Width = 1070
        Height = 65
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LogAScan: TSpeedButton
          Left = 456
          Top = 2
          Width = 54
          Height = 27
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'AScan'
          ParentShowHint = False
          ShowHint = False
        end
        object LogBScan: TSpeedButton
          Left = 566
          Top = 2
          Width = 50
          Height = 27
          AllowAllUp = True
          GroupIndex = 4
          Caption = 'BScan'
        end
        object LogIn: TSpeedButton
          Left = 190
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 22
          Down = True
          Caption = 'In'
          ParentShowHint = False
          ShowHint = False
        end
        object LogOut: TSpeedButton
          Left = 359
          Top = 2
          Width = 42
          Height = 27
          AllowAllUp = True
          GroupIndex = 33
          Caption = 'Out All'
        end
        object LogState: TSpeedButton
          Left = 13
          Top = 1
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 55
          Down = True
          Caption = 'On'
          ParentShowHint = False
          ShowHint = False
        end
        object LogAScanMeasure: TSpeedButton
          Left = 962
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 2
          Caption = 'AScan Measure'
          ParentShowHint = False
          ShowHint = False
        end
        object LogASD: TSpeedButton
          Left = 512
          Top = 32
          Width = 51
          Height = 27
          AllowAllUp = True
          GroupIndex = 7
          Caption = 'ASD'
        end
        object LogBScan2: TSpeedButton
          Left = 456
          Top = 32
          Width = 54
          Height = 27
          AllowAllUp = True
          GroupIndex = 5
          Caption = 'BScan2'
        end
        object LogNoBody: TSpeedButton
          Left = 273
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 225
          Caption = 'No Body'
          ParentShowHint = False
          ShowHint = False
        end
        object LogSerialNumber: TSpeedButton
          Left = 512
          Top = 2
          Width = 51
          Height = 27
          AllowAllUp = True
          GroupIndex = 8
          Caption = 'SN'
        end
        object LogFWver: TSpeedButton
          Left = 790
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 9
          Caption = 'FWver'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object UMUManage: TSpeedButton
          Left = 790
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 90
          Caption = 'UMU Manage'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LoggerMsgBtn: TSpeedButton
          Left = 876
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 9066
          Caption = 'Logger'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LogMetalSensor: TSpeedButton
          Left = 962
          Top = 2
          Width = 80
          Height = 26
          AllowAllUp = True
          GroupIndex = 3441
          Caption = 'Metal Sensor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LogAC_Summ: TSpeedButton
          Left = 876
          Top = 32
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 341
          Caption = 'AC (Summ)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LogAC_: TSpeedButton
          Left = 566
          Top = 32
          Width = 50
          Height = 27
          AllowAllUp = True
          GroupIndex = -341
          Caption = 'AC'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LogPathEncoder: TSpeedButton
          Left = 621
          Top = 2
          Width = 80
          Height = 27
          AllowAllUp = True
          GroupIndex = 3
          Caption = 'Path Encoder'
        end
        object LogSpeed: TSpeedButton
          Left = 620
          Top = 32
          Width = 54
          Height = 27
          AllowAllUp = True
          GroupIndex = 1
          Caption = 'Speed'
          ParentShowHint = False
          ShowHint = False
        end
        object LogOut2: TSpeedButton
          Left = 402
          Top = 2
          Width = 42
          Height = 27
          AllowAllUp = True
          GroupIndex = 33
          Down = True
          Caption = 'Out 2'
        end
        object Panel1: TPanel
          Left = 619
          Top = 30
          Width = 168
          Height = 1
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
        end
        object Button12: TButton
          Left = 96
          Top = 1
          Width = 80
          Height = 27
          Caption = 'Clear'
          TabOrder = 0
          OnClick = Button12Click
        end
        object Panel27: TPanel
          Left = 182
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 1
        end
        object Panel36: TPanel
          Left = 5
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 2
        end
        object Panel45: TPanel
          Left = 446
          Top = 2
          Width = 4
          Height = 59
          BevelOuter = bvLowered
          TabOrder = 3
        end
        object CheckBoxOneMessage: TCheckBox
          Left = 15
          Top = 29
          Width = 85
          Height = 17
          Caption = 'One Message'
          TabOrder = 4
        end
        object FilterCheckBox: TCheckBox
          Left = 190
          Top = 37
          Width = 219
          Height = 17
          Caption = 'Filtr  S:                 L:               T:'
          TabOrder = 5
        end
        object SideEdit: TEdit
          Left = 243
          Top = 36
          Width = 34
          Height = 21
          TabOrder = 6
          Text = '0'
        end
        object LineEdit: TEdit
          Left = 303
          Top = 36
          Width = 34
          Height = 21
          TabOrder = 7
          Text = '0'
        end
        object TaktEdit: TEdit
          Left = 357
          Top = 36
          Width = 34
          Height = 21
          TabOrder = 8
          Text = '0'
        end
        object Decoding: TCheckBox
          Left = 15
          Top = 46
          Width = 98
          Height = 17
          Caption = #1056#1072#1089#1096#1080#1092#1088#1086#1074#1082#1072
          TabOrder = 9
        end
        object cbTime: TCheckBox
          Left = 102
          Top = 29
          Width = 85
          Height = 17
          Caption = 'Time'
          Checked = True
          State = cbChecked
          TabOrder = 10
        end
        object PEMode: TCheckBox
          Left = 707
          Top = 6
          Width = 70
          Height = 17
          Caption = 'PE Dir != 0'
          TabOrder = 11
        end
        object Panel3: TPanel
          Left = 619
          Top = 0
          Width = 168
          Height = 1
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 13
        end
        object Panel4: TPanel
          Left = 619
          Top = 1
          Width = 1
          Height = 29
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
        end
        object Panel5: TPanel
          Left = 786
          Top = 1
          Width = 1
          Height = 29
          BevelOuter = bvNone
          BorderStyle = bsSingle
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 15
        end
        object cbShowAll: TCheckBox
          Left = 103
          Top = 46
          Width = 85
          Height = 17
          Caption = 'Show All'
          TabOrder = 16
        end
      end
      object Panel49: TPanel
        Left = 0
        Top = 0
        Width = 71
        Height = 65
        Align = alLeft
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object CheckBox1: TCheckBox
          Left = 0
          Top = 6
          Width = 73
          Height = 17
          Caption = ' Show Log'
          TabOrder = 0
          OnClick = CheckBox1Click
        end
        object Button1: TButton
          Left = 13
          Top = 30
          Width = 51
          Height = 25
          Caption = 'Copy'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = Button1Click
        end
      end
    end
    object PersistDataListBox: TListBox
      Left = 3
      Top = 68
      Width = 3
      Height = 393
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
    end
    object Memo1: TMemo
      Left = 9
      Top = 68
      Width = 1118
      Height = 393
      Align = alClient
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 12
      Font.Name = 'Verdana'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 2
      WordWrap = False
    end
    object ScrollBar1: TScrollBar
      Left = 1127
      Top = 68
      Width = 17
      Height = 393
      Align = alRight
      Kind = sbVertical
      PageSize = 0
      TabOrder = 3
      OnChange = ScrollBar1Change
    end
  end
  object UpdateLogTimer: TTimer
    Interval = 500
    OnTimer = UpdateLogTimerTimer
    Left = 280
    Top = 192
  end
end
