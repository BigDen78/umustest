﻿// ---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "UMUTest_MainUnit.h"
#include "math.h"
#include "UMUsTest.inc"

#define UMUDataDebug   // вывод отладочной иформации по В-развертки
#define NewBScanProt

// ---------------------------------------------------------------------------
//#pragma package(smart_init)

#pragma resource "*.dfm"
TMainForm *MainForm;

bool EndWork = false;
double fps = 0;
bool NoiseRed;
int RullerMode; // 0 - мкс; 1 - мм; 2 - отсчеты
int OldRullerMode; // 0 - мкс; 1 - мм; 2 - отсчеты
bool BScanView; // 0 - мкс; 1 - мм; 2 - отсчеты
int AmplId_to_DB[16] = { -12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18 };
int static ThValList[16] = { 8, 10, 12, 16, 20, 25, 32, 40, 50, 63, 80, 101, 127, 160, 201, 254};
int DisCoord;

TDrawThread * dt;

//---------------------------------------------------------------------------

static int HLSMAX = 240;
static int RGBMAX = 255;
static int UNDEFINED;
static int R, G, B; //  цвета
static float Magic1, Magic2;

UnicodeString UMUSideToStr(eUMUSide side)
{
	if (side == usLeft) return "Левая"; else return "Правая";
}

UnicodeString UMULineToStr(int Line)
{
    if (Line == 0) return "БР1"; else return "БР2";
}

float HueToRGB(float n1, float n2, float hue)
{
  if (hue < 0) hue = hue + HLSMAX;
  if (hue > HLSMAX) hue = hue - HLSMAX;
  if (hue < (HLSMAX/6))
  {
	 return ( n1 + (((n2-n1)*hue+(HLSMAX/12))/(HLSMAX/6)) );
  }
  else if (hue < (HLSMAX/2)) return n2;
       else if (hue < ((HLSMAX*2)/3)) return (n1 + (((n2-n1)*(((HLSMAX*2)/3)-hue)+(HLSMAX/12))/(HLSMAX/6)));
            else return n1;
}

TColor HLStoRGB(int H, int L, int S)
{
    UNDEFINED = (HLSMAX * 2) / 3;

    if (S == 0)
	{
       B = (L*RGBMAX)/HLSMAX ;
       R = B;
       G = B;
    }
    else
    {
        if (L <= (HLSMAX/2)) Magic2 = (L*(HLSMAX + S) + (HLSMAX/2))/HLSMAX;
                             else Magic2 = L + S - ((L*S) + (HLSMAX/2))/HLSMAX;
        Magic1 = 2*L-Magic2;
        R = (HueToRGB(Magic1,Magic2,H+(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
        G = (HueToRGB(Magic1,Magic2,H)*RGBMAX + (HLSMAX/2)) / HLSMAX ;
        B = (HueToRGB(Magic1,Magic2,H-(HLSMAX/3))*RGBMAX + (HLSMAX/2))/HLSMAX ;
    }
    if (R<0) R=0; if (R>RGBMAX) R = RGBMAX;
    if (G<0) G=0; if (G>RGBMAX) G = RGBMAX;
    if (B<0) B=0; if (B>RGBMAX) B = RGBMAX;
    return (TColor)(R + (G << 8) + (B << 16));
}

// --- TDrawThread -----------------------------------------------------------

__fastcall TDrawThread::TDrawThread(cCriticalSectionWin * cs) : TThread(false)
{
    EndWorkFlag = False;
    SaveParamH = 0;
    SaveParamKd = 0;
    XSysCrd_ = 0;

	//cs = new TCriticalSection();
	this->cs = cs;
}

__fastcall TDrawThread::~TDrawThread(void)
{
	//delete cs;
}

void __fastcall TDrawThread::Execute(void)
{
 //   while (!EndWorkFlag)
    {
//		if (GetTickCount_() - Time > 50 )
		{
//			AutoconMain->devt->PtrListRWLock.Enter();
//			Synchronize(ValueToScreen);
//            ValueToScreen();
//			AutoconMain->devt->PtrListRWLock.Release();

//			Time = GetTickCount_();

/*			AutoconMain->devt->PtrListRWLock.Enter();

            for (int idx = 0; idx < AutoconMain->devt->Count_ptr; idx++)
			{
				if(AutoconMain->devt->Ptr_List_1[idx])
				   delete AutoconMain->devt->Ptr_List_1[idx];
				if(AutoconMain->devt->Ptr_List_2[idx])
				   delete AutoconMain->devt->Ptr_List_2[idx];
			}
			AutoconMain->devt->Count_ptr = 0;

			AutoconMain->devt->PtrListRWLock.Release();
*/
//            Sleep(10);
		}
		//Added by KirillB
//		Sleep(1);
    }
}

// int DgbCnt = 0;

int CID_to_IDX(CID Channel) {

    switch (Channel) {

        case 0xB2: {  return 0; break; }
        case 0xB4: {  return 1; break; }
        case 0xB6: {  return 2; break; }

    }
}

void __fastcall TDrawThread::ValueToScreen(void)
{
    frmrestore restore;
    int SummCount;
    TMainForm * frm  = MainForm;
	if (!frm->Visible) return;

    AUTO_LOCK_WCS(*(AutoconMain->CS));

    PtDEV_AScanMeasure DEV_AScanMeasure_ptr;
	PtDEV_AScanHead    DEV_AScanHead_ptr;
    PtDEV_BScan2Head   DEV_BScan2Head_ptr = NULL;
    PtDEV_PathStepData DEV_PathStepData_ptr = NULL;
	PtDEV_AlarmHead    DEV_AlarmHead_ptr;
    PtDEV_MetalSensorData MetalSensorData_ptr;

	PtUMU_AScanData    AScanData_ptr;
	PtUMU_AScanData    TVGData_ptr;
	PtUMU_AScanMeasure AScanMeasure_ptr;
//	PtUMU_BScanData    PBScanData_ptr;

    ringbuf<sPtrListItem>& Ptr_List = AutoconMain->devt->Ptr_List; //Getting link to ringbuffer (for ease of access)

    bool bHasAScanData = false;
    bool bHasTVGData = false;
    bool bHasAScanMeasData = false;

//    frm->cbViewBScan->Caption = IntToStr(AutoconMain->Table->IndexByCIDCount);

    frm->BScanAlg->Caption = "M/B-Scan: ?";
    frm->AlarmAlg->Caption = "АСД: ?";

    frm->BScanData->Tag = 0;

    frm->BScanData->Cells[0][0] = "№";
    frm->BScanData->Cells[1][0] = "Delay";
    frm->BScanData->Cells[2][0] = "Ampl";
    frm->BScanData->ColWidths[1] = 52;
    frm->BScanData->ColWidths[2] = 52;
    frm->BScanData->ColWidths[3] = 25;


    frm->UMU0_Memo->Lines->Clear();
    frm->UMU0_Memo->Lines->Add(" - " + IntToStr((int)AutoconMain->DEV->UMUBScan2Data_ptrs[0].size()) + " - ");
    for (int Idx = 0; Idx < Min(21, AutoconMain->DEV->UMUBScan2Data_ptrs[0].size()); Idx++)
        if (AutoconMain->DEV->UMUBScan2Data_ptrs[0][Idx]) frm->UMU0_Memo->Lines->Add(IntToStr(AutoconMain->DEV->UMUBScan2Data_ptrs[0][Idx]->XDisCrd[0]));
                                                             else frm->UMU0_Memo->Lines->Add("NULL");

    frm->UMU1_Memo->Lines->Clear();
    frm->UMU1_Memo->Lines->Add(" - " + IntToStr((int)AutoconMain->DEV->UMUBScan2Data_ptrs[1].size()) + " - ");
    for (int Idx = 0; Idx < Min(21, AutoconMain->DEV->UMUBScan2Data_ptrs[1].size()); Idx++)
        if (AutoconMain->DEV->UMUBScan2Data_ptrs[1][Idx]) frm->UMU1_Memo->Lines->Add(IntToStr(AutoconMain->DEV->UMUBScan2Data_ptrs[1][Idx]->XDisCrd[0]));
                                                             else frm->UMU1_Memo->Lines->Add("NULL");
    // Вывод значений стробов

    frm->sgGates->Cells[1][1] = IntToStr(AutoconMain->Calibration->GetStGate(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 0));
    frm->sgGates->Cells[2][1] = IntToStr(AutoconMain->Calibration->GetEdGate(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 0));
    frm->sgGates->Cells[1][2] = IntToStr(AutoconMain->Calibration->GetStGate(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 1));
    frm->sgGates->Cells[2][2] = IntToStr(AutoconMain->Calibration->GetEdGate(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 1));
    if (AutoconMain->DEV->GetMarkState()) {
        frm->sgGates->Cells[1][3] = IntToStr(AutoconMain->DEV->GetStMark());
        frm->sgGates->Cells[2][3] = IntToStr(AutoconMain->DEV->GetEdMark());
    } else {
        frm->sgGates->Cells[1][3] = "-";
        frm->sgGates->Cells[2][3] = "-";
    };
    frm->sgGates->Cells[1][4] = FloatToStr(frm->LastParamM);
    frm->sgGates->Cells[2][4] = "-";

    for (int SignalIdx = 0; SignalIdx < 8; SignalIdx++)
    {
        frm->BScanData->Cells[0][frm->BScanData->Tag + 1] = "";
        frm->BScanData->Cells[1][frm->BScanData->Tag + 1] = "";
        frm->BScanData->Cells[2][frm->BScanData->Tag + 1] = "";
        frm->BScanData->Tag++;
    }

//    frm->Memo1->Clear();
//    frm->Memo1->Lines->Add(Format("R %d", ARRAYOFCONST((AutoconMain->DEV->_fTailCoord))));


    frm->BScanDebugSeries->Clear();

 //   if (frm->PageControl->ActivePageIndex == 0) // Оценка
    {
//        if (frm->CheckBox3->Checked) return;


    //edPathStepData-------------------------------------------------------------------
        for(int PtrListIdx = 0; PtrListIdx < Ptr_List.size(); PtrListIdx++) {

            sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];

            switch( Ptr_Item.Type )
            {
                case edPathStepData:
                {
                   if (Ptr_Item.Ptr1.pPathStepData->UMUIdx == 0) {

                       frm->sgPathCounters->Cells[(Byte)Ptr_Item.Ptr1.pPathStepData->Simulator[0]][0] = IntToStr(Ptr_Item.Ptr1.pPathStepData->XSysCrd[0]);
                       frm->sgPathCounters->Cells[(Byte)Ptr_Item.Ptr1.pPathStepData->Simulator[1]][1] = IntToStr(Ptr_Item.Ptr1.pPathStepData->XSysCrd[1]);
                       frm->sgPathCounters->Cells[(Byte)Ptr_Item.Ptr1.pPathStepData->Simulator[2]][2] = IntToStr(Ptr_Item.Ptr1.pPathStepData->XSysCrd[2]);
                   }

                   if (frm->PageControl->ActivePage == frm->PE_Test)  {

                       switch (Ptr_Item.Ptr1.pPathStepData->UMUIdx) {

                          case 0: {
                                       frm->UMU0_PEValue->Caption = IntToStr(frm->UMU0_last_XSysCrd);
                                       frm->UMU0_last_XSysCrd = Ptr_Item.Ptr1.pPathStepData->XSysCrd[0];
                                       if (frm->CheckBox4->Checked) frm->LineSeries5->Add(frm->UMU0_last_XSysCrd);
                                       break;
                                  }
                          case 1: {
                                       frm->UMU1_PEValue->Caption = IntToStr(frm->UMU1_last_XSysCrd);
                                       frm->UMU1_last_XSysCrd = Ptr_Item.Ptr1.pPathStepData->XSysCrd[0];
                                       if (frm->CheckBox4->Checked) frm->LineSeries6->Add(frm->UMU1_last_XSysCrd);
                                       break;
                                  }
                       }


                      // frm->LineSeries7->Add(frm->UMU1_last_XSysCrd - frm->UMU0_last_XSysCrd);
                       frm->PE_Delta->Caption = IntToStr(frm->UMU1_last_XSysCrd - frm->UMU0_last_XSysCrd);

                   }
                }

                case edA15ScanerPathStepData:
                {
                   frm->Label18->Caption = IntToStr(Ptr_Item.Ptr1.pA15ScanerPathStepData->XCrd);
                   frm->Label15->Caption = IntToStr(Ptr_Item.Ptr1.pA15ScanerPathStepData->YCrd);
                   frm->Label19->Caption = IntToStr(Ptr_Item.Ptr1.pA15ScanerPathStepData->Angle);
                   int X_mm = (float)97 * (float)(Ptr_Item.Ptr1.pA15ScanerPathStepData->XCrd - 87) / (float)(220-87);
                   frm->Series13->X0 = X_mm - 5;
                   frm->Series13->X1 = X_mm + 5;
                   frm->Series13->Y0 = - 5 + Ptr_Item.Ptr1.pA15ScanerPathStepData->YCrd;
                   frm->Series13->Y1 = + 5 + Ptr_Item.Ptr1.pA15ScanerPathStepData->YCrd;
                   frm->Series13->Text->Clear();
                   frm->Series13->Text->Add(IntToStr(X_mm));
                }
            }
        }


        for(int PtrListIdx = Ptr_List.size() - 1; PtrListIdx >= 0; PtrListIdx--)
        {
         /* if(PtrListIdx >= Ptr_List.size())
            {
                int t = 0;
                t++;
            } */
            sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];


            if ((Ptr_Item.Type == edAScanData) || (Ptr_Item.Type == edTVGData)) {

                if ((OldRullerMode != RullerMode) || (frm->NeedUpdate))
                {
                    switch(RullerMode)
                    {
                        case 0:
                        case 1: {
                                    frm->AScanChart->BottomAxis->SetMinMax(0, frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                    frm->BScanChart2->BottomAxis->SetMinMax(0, frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                    break;
                                }
                        case 2: {
                                    frm->AScanChart->BottomAxis->SetMinMax(0, 232);
                                    frm->BScanChart2->BottomAxis->SetMinMax(0, 232);
                                    break;
                                }
                    }
                    OldRullerMode = RullerMode;
                    frm->NeedUpdate = false;
                }
            }

            switch( Ptr_Item.Type )
            {
    //edAScanData-------------------------------------------------------------------

                case edAScanData:
                {
                    //----Check----
                    if(bHasAScanData)
                        break;
                    if ((Ptr_Item.Ptr1.pAScanHead->Side != AutoconMain->DEV->GetSide()) ||
                        (Ptr_Item.Ptr1.pAScanHead->Channel != AutoconMain->DEV->GetChannel()))
                        break;
                    bHasAScanData = true;
                    //---End Check---

                    frm->AScanSeries->BeginUpdate();
                    frm->GateSeriesMain->BeginUpdate();
                    frm->GateSeriesMain_->BeginUpdate();
                    frm->GateSeriesLeft->BeginUpdate();
                    frm->GateSeriesRight->BeginUpdate();
                    frm->BScanGateMainSeries->BeginUpdate();
                    frm->BScanGateLeftSeries->BeginUpdate();
                    frm->BScanGateRightSeries->BeginUpdate();

                    DEV_AScanHead_ptr = Ptr_Item.Ptr1.pAScanHead;
                    AScanData_ptr = Ptr_Item.Ptr2.pAScanData;

                    if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
                        (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
                    {

                        {
                            if ((frm->SaveMax1 != frm->AScanChart->BottomAxis->Maximum) || (frm->AScanSeries->XValues->Count == 0))
                            {
                                frm->SaveMax1 = frm->AScanChart->BottomAxis->Maximum;
                                frm->AScanSeries->Clear();
                                for (int i = 0; i < 232; i++)
                                    frm->AScanSeries->AddXY(frm->PixelToChartValue(i), frm->Filter(AScanData_ptr->Data[i]));
                            }
                            else
                                for (int i = 0; i < std::min(232, frm->AScanSeries->XValues->Count); i++)
                                    frm->AScanSeries->YValue[i] = frm->Filter(AScanData_ptr->Data[i]);
                        }

//                        SummCount = 0;
//                        for (int i = AutoconMain->DEV->DelayToPixel(frm->Series5->X0); i < std::min(232, frm->AScanSeries->XValues->Count); i++)
//                            SummCount = SummCount + AScanData_ptr->Data[i];
//                        frm->StrokeCount->Caption = IntToStr(SummCount);

//                        int Summ = 0;
//                        for (int i = 0; i < 232; i++)
//                            if (AutoconMain->DEV->PixelToDelayFloat(i) >= frm->StartACGate)
//                                Summ = Summ + AScanData_ptr->Data[i];

//                        frm->Stroke->Caption = IntToStr(Summ);


                        if ((frm->btnSaveAscan->Tag == 1) && (frm->msSaveAscan))
                        {
                            frm->msSaveAscan->WriteBuffer(&AScanData_ptr->Data, 232);
                        }

                        int GateLevet = AutoconMain->Config->getEvaluationGateLevel();

                        frm->GateSeriesMain->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()/*, true*/));
                        frm->GateSeriesMain->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()/*, true*/));

                        frm->GateSeriesMain_->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()/*, true*/));
                        frm->GateSeriesMain_->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()/*, true*/));


                        if (AutoconMain->DEV->GetAlarmMode(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), AutoconMain->DEV->GetGateIndex()) == amTwoEcho)
                        {
                            frm->GateSeriesMain->Y0 = GateLevet - 1;
                            frm->GateSeriesMain->Y1 = GateLevet - 3;
                            frm->GateSeriesMain_->Y0 = GateLevet + 1;
                            frm->GateSeriesMain_->Y1 = GateLevet + 3;
                            frm->GateSeriesMain_->Visible = true;
                        }
                        else
                        {
                            frm->GateSeriesMain->Y0 = GateLevet - 1;
                            frm->GateSeriesMain->Y1 = GateLevet + 1;
                            frm->GateSeriesMain_->Visible = false;
                        }


                        frm->GateSeriesLeft->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()/*, true*/)) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->GateSeriesLeft->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()/*, true*/));
                        frm->GateSeriesLeft->Y0 = GateLevet - 5;
                        frm->GateSeriesLeft->Y1 = GateLevet + 5;

                        frm->GateSeriesRight->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()/*, true*/));
                        frm->GateSeriesRight->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()/*, true*/)) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->GateSeriesRight->Y0 = GateLevet - 5;
                        frm->GateSeriesRight->Y1 = GateLevet + 5;

                        // Строб Б-развертки
                        sChannelDescription ChanDesc;
                        AutoconMain->Table->ItemByCID(DEV_AScanHead_ptr->Channel, &ChanDesc);
                        frm->BScanGateMainSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                        frm->BScanGateMainSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd);

                        GateLevet = AutoconMain->Config->getBScanGateLevel();

                        frm->BScanGateMainSeries->Y0 = GateLevet - 1;
                        frm->BScanGateMainSeries->Y1 = GateLevet + 1;

                        frm->BScanGateLeftSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->BScanGateLeftSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                        frm->BScanGateLeftSeries->Y0 = GateLevet - 5;
                        frm->BScanGateLeftSeries->Y1 = GateLevet + 5;

                        frm->BScanGateRightSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd);
                        frm->BScanGateRightSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->BScanGateRightSeries->Y0 = GateLevet - 5;
                        frm->BScanGateRightSeries->Y1 = GateLevet + 5;
                    }


                    int GateLevet = AutoconMain->Config->getBScanGateLevel();

                    frm->AScanSeries->EndUpdate();
                    frm->GateSeriesMain->EndUpdate();
                    frm->GateSeriesMain_->EndUpdate();
                    frm->GateSeriesLeft->EndUpdate();
                    frm->GateSeriesRight->EndUpdate();
                    frm->BScanGateMainSeries->EndUpdate();
                    frm->BScanGateLeftSeries->EndUpdate();
                    frm->BScanGateRightSeries->EndUpdate();

                    if (AutoconMain->DEV->GetMarkState()) {
                        frm->MarkShape->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStMark());
                        frm->MarkShape->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdMark());
                        frm->MarkShape->Y0 = GateLevet - 5 + 45;
                        frm->MarkShape->Y1 = GateLevet + 5 + 45;
                        frm->MarkShape->Visible = true;
                    } else frm->MarkShape->Visible = false;

                    // Корреляция А-развертки
                    if (frm->PageControl->ActivePage->Tag == 102) {

                        long res;
                        long max_ = 0;
                        int maxpos_;

                        frm->Series9->Clear();
                        for (int dx = - 123; dx <= + 123; dx ++) {
                           res = 0;
                           for (int i = 0; i < 232; i++)
                                if ((i + dx >= 0) && (i + dx < 232)) res = res + AScanData_ptr->Data[i] * frm->SaveAData[i + dx];

                           if (res > max_) {
                                max_ = res;
                                maxpos_ = dx;
                           }

                           frm->Series9->AddXY(dx, res);
                        }

                        frm->ShiftMemo->Lines->Insert(0, IntToStr(maxpos_));
                        if ((maxpos_ >= - 10) && (maxpos_ <= 10)) {

                            frm->Series11->X0 = frm->Series11->X0 + maxpos_;
                            frm->Series11->X1 = frm->Series11->X1 + maxpos_;
                        }

                        frm->FastLineSeries10->Clear();
                        for (int i = 0; i < 232; i++) frm->FastLineSeries10->AddXY(i, AScanData_ptr->Data[i]);

                        frm->FastLineSeries11->Clear();
                        for (int i = 0; i < 232; i++) frm->FastLineSeries11->AddXY(i, frm->SaveAData[i]);

                        memcpy(&(frm->SaveAData[0]), &(AScanData_ptr->Data[0]), 232);
                    }

                    // Мышка сканер
                    if (frm->PageControl->ActivePage->Tag == 1112) {

                        unsigned char MaxVal = 0;
                        frm->MousScanerAScan->Clear();
                        for (int i = 0; i < 232; i++) {
                          frm->MousScanerAScan->AddXY(i, AScanData_ptr->Data[i]);
                          if (AScanData_ptr->Data[i] > MaxVal) MaxVal = AScanData_ptr->Data[i];
                        }

                        float Scale = 30;
                        int x = (int)((float)(Mouse->CursorPos.x - frm->StartScanerCursorPos.x) / Scale);
                        int y = -1*(int)((float)(Mouse->CursorPos.y - frm->StartScanerCursorPos.y) / Scale);

                        frm->Series12->AddXY(x,y,"", MaxVal);

                    }
                    break;
                }
    //edTVGData---------------------------------------------------------------------
                case edTVGData:
                    //----Check----
                    if(bHasTVGData)
                        break;
                    if ((Ptr_Item.Ptr1.pAScanHead->Side != AutoconMain->DEV->GetSide()) ||
                        (Ptr_Item.Ptr1.pAScanHead->Channel != AutoconMain->DEV->GetChannel()))
                        break;
                    bHasTVGData = true;
                    //---End Check---

                    frm->TVGSeries->BeginUpdate();

                    DEV_AScanHead_ptr = Ptr_Item.Ptr1.pAScanHead;
                    TVGData_ptr = Ptr_Item.Ptr2.pAScanData;

                    //if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    //    (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
                    //{
                        if ((frm->SaveMax2 != frm->AScanChart->BottomAxis->Maximum) ||
                            (frm->TVGSeries->XValues->Count == 0))
                        {
                            frm->SaveMax2 = frm->AScanChart->BottomAxis->Maximum;
                            frm->TVGSeries->Clear();
                            for (int i = 0; i < 232; i++)
                                frm->TVGSeries->AddXY(frm->PixelToChartValue(i),
                                                      255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->getGainBase()) / (AutoconMain->Config->getGainMax() - AutoconMain->Config->getGainBase()) );
                        }
                        else
                        {
                            for (int i = 0; i < std::min(232, frm->TVGSeries->XValues->Count); i++)
                                frm->TVGSeries->YValue[i] = 255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->getGainBase()) / (AutoconMain->Config->getGainMax() - AutoconMain->Config->getGainBase());
                        }
                    //}

                    frm->TVGSeries->EndUpdate();

                    break;
    //edAScanMeas-------------------------------------------------------------------
                case edAScanMeas:

                    //----Check----
                    if(bHasAScanMeasData)
                        break;
                    frm->MaxPosSeries->Visible = false;
                    if ((Ptr_Item.Ptr1.pAScanMeasure->Side != AutoconMain->DEV->GetSide()) ||
                        (Ptr_Item.Ptr1.pAScanMeasure->Channel != AutoconMain->DEV->GetChannel())) break;
                    bHasAScanMeasData = true;
                    //---End Check---

                    frm->MaxPosSeries->BeginUpdate();

                    DEV_AScanMeasure_ptr = Ptr_Item.Ptr1.pAScanMeasure;

                    frm->LastParamM = DEV_AScanMeasure_ptr->ParamM;

                    frm->MaxPosSeries->Visible = false;
                    //if ((DEV_AScanMeasure_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    //    (DEV_AScanMeasure_ptr->Channel == AutoconMain->DEV->GetChannel()))
                    //{
                   //     if (DEV_AScanMeasure_ptr->ParamA >= AutoconMain->Config->EvaluationGateLevel)
                        {
                            frm->MaxPosSeries->Visible = true;
                            frm->MaxPosSeries->X0 = frm->DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                            frm->MaxPosSeries->X1 = frm->DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                            frm->MaxPosSeries->Y0 = -5;
                            frm->MaxPosSeries->Y1 = 6;
                        }

                        // Таблица ------------------------------------------------------------------------------

                        if ((AutoconMain->DEV->GetCalibrationMode() == cmPrismDelay) && (AutoconMain->DEV->GetMode() == dmCalibration))
                                 frm->HPanel->Caption = Format("R %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamR)));
                            else frm->HPanel->Caption = Format("H %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamH)));
                        frm->LPanel->Caption = Format("L %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamL)));
                        frm->NPanel->Caption = Format("N %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamN)));



                        SaveParamH = DEV_AScanMeasure_ptr->ParamH;
                        SaveParamKd = DEV_AScanMeasure_ptr->ParamN - AutoconMain->DEV->GetSens();

                        frm->ChannelTitlePanel->Caption = AutoconMain->DEV->WorkChannelDescription.Title.c_str();
                        if (!frm->DebugOut)
                            frm->DebugPanel->Caption = Format("UMU: №-%d(1,2,3), Side-%d:%s(0,1), GenLine-%d:%s(0,1), ResLine-%d:%s(0,1), Stroke-%d, Gen-%d, Res-%d, Длит.такта-%d мкс, Отображ. %d мкс; Device: Size-%d, CID-%x, GateIdx-%d(1,2), Группа-%d",
                                                ARRAYOFCONST((AutoconMain->DEV->CurChannelUMUIndex + 1,
                                                                          AutoconMain->DEV->CurChannelUMUSide,
                                                                          UMUSideToStr(AutoconMain->DEV->CurChannelUMUSide),
                                                                                   AutoconMain->DEV->CurChannelUMUGenLine + 1,
                                                                                   UMULineToStr(AutoconMain->DEV->CurChannelUMUGenLine),
                                                                                   AutoconMain->DEV->CurChannelUMUResLine + 1,
                                                                                   UMULineToStr(AutoconMain->DEV->CurChannelUMUResLine),
                                                                                            AutoconMain->DEV->CurChannelUMUStroke + 1,
                                                                                                       AutoconMain->DEV->CurChannelUMUGen + 1,
                                                                                                               AutoconMain->DEV->CurChannelUMURes + 1,
                                                                                                                       AutoconMain->DEV->CurChannelUMUStrokeDuration,
                                                                                                                                                    AutoconMain->DEV->CurChannelUMUAScanDuration, (int)AutoconMain->DEV->GetSide(),
                                                                                                                                                                    AutoconMain->DEV->GetChannel(), AutoconMain->DEV->GetGateIndex(),
                                                                                                                                                                            AutoconMain->DEV->GetChannelGroup())));

                        if (AutoconMain->DEV->GetMode() != dmCalibration)
                            frm->InfoPanel->Caption = Format ("АТТ: %d", ARRAYOFCONST((AutoconMain->DEV->GetGain())));
                            else frm->InfoPanel->Caption = Format ("Ку %d (%d)", ARRAYOFCONST((AutoconMain->DEV->GetSens(), AutoconMain->DEV->GetRecommendedSens() )));
                    //}

                    frm->MaxPosSeries->EndUpdate();

                    break;
    //edAlarmData-------------------------------------------------------------------
                case edAlarmData:    // >---< >-+-<

//                    if (AutoconMain->DEV->AlarmData_from_UMU) frm->AlarmAlg->Caption = "АСД: БУМ";
//                                                         else frm->AlarmAlg->Caption = "АСД: Алекс";

                    DEV_AlarmHead_ptr = Ptr_Item.Ptr1.pAlarmHead;
                    eDeviceSide s;

                    for (unsigned int Idx = 0; Idx < DEV_AlarmHead_ptr->Items.size(); Idx++)
                    {
                        for (unsigned int J = 0; J < frm->ChList->Count; J++) {

                            int temp_id;
                            UnicodeString tmp = "";

                            temp_id = (int)frm->ChList->Items->Objects[J];

                            if (temp_id > 9999) {
                                s = dsNone;
                                temp_id = temp_id - 10000;

                                } else {
                                        if (temp_id > 0) s = dsLeft;
                                          else s = dsRight;
                                        temp_id = abs(temp_id);
                                        temp_id = temp_id - 1;
                                    }

                            if ((DEV_AlarmHead_ptr->Items[Idx].Side  == s) &&
                                (DEV_AlarmHead_ptr->Items[Idx].Channel == temp_id)) {

                                if (DEV_AlarmHead_ptr->Items[Idx].State[1]) tmp = "+"; else tmp = "-";
                                if (DEV_AlarmHead_ptr->Items[Idx].State[2]) tmp = tmp + " +"; else tmp = tmp + " -";
                                UnicodeString tmp2 = frm->ChList->Items->Strings[J];
                                tmp2.Delete(1, 6);
                                tmp2 = "[" + tmp + "] " + tmp2;
                                frm->ChList->Items->Strings[J] = tmp2;
                            }
                        }

                        if ((DEV_AlarmHead_ptr->Items[Idx].Side == AutoconMain->DEV->GetSide()) &&
                            (DEV_AlarmHead_ptr->Items[Idx].Channel == AutoconMain->DEV->GetChannel()))
                        {

                                if (AutoconMain->DEV->GetAlarmMode(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), AutoconMain->DEV->GetGateIndex()) == amOff)
                                {
                                    frm->GateSeriesMain->Color = clSilver;
                                    frm->GateSeriesMain_->Color = clSilver;
                                    frm->GateSeriesLeft->Color = clSilver;
                                    frm->GateSeriesRight->Color = clSilver;
                                }
                                else
                                {
                                    if (DEV_AlarmHead_ptr->Items[Idx].State[AutoconMain->DEV->GetGateIndex()])
                                    {
                                        frm->GateSeriesMain->Color = clRed;
                                        frm->GateSeriesMain_->Color = clRed;
                                        frm->GateSeriesLeft->Color = clRed;
                                        frm->GateSeriesRight->Color = clRed;
                                    }
                                    else
                                    {
                                        frm->GateSeriesMain->Color = clBlue;
                                        frm->GateSeriesMain_->Color = clBlue;
                                        frm->GateSeriesLeft->Color = clBlue;
                                        frm->GateSeriesRight->Color = clBlue;
                                    }
                                }
                        }
                    }

                    break;

    //edMSensor-------------------------------------------------------------------
                case edMSensor:         {

                    MetalSensorData_ptr = Ptr_Item.Ptr1.pMetalSensorData;

                    MetalSensorState[MetalSensorData_ptr->Side][MetalSensorData_ptr->SensorId] = /*(*/MetalSensorData_ptr->State /* == 2)*/;

                    UnicodeString str = "";
                    frm->MetallSensorInfo->Clear();

                    int side_idx;
                    int s_idx;

                    for (side_idx = dsLeft; side_idx <= dsRight; side_idx++) {
                        for (s_idx = 0; s_idx < 2; s_idx++) {

                            str = "Сторона: " + IntToStr(side_idx);
                            str = str + " Датчик №" + IntToStr(s_idx) + " Состояние: " + IntToStr((int)MetalSensorState[side_idx][s_idx]);
                            frm->MetallSensorInfo->Lines->Add(str);
                        }
                    }
                    break;

                }
                case edBottomSignalAmpl:  {
//                    ShowMessage("!");
                  break;

                }

                case edDeviceSpeed:  {

                    frm->SpeedState->Caption = Format("Скорость: %3.1f; км/ч; Зона: %d", ARRAYOFCONST((AutoconMain->DEV->last_speed, AutoconMain->DEV->last_speedZone)));
                  break;
                 }
    //------------------------------------------------------------------------------
            }; //end switch( Ptr_Item.Type )

            if ((Ptr_Item.Type != edBScan2Data) && (Ptr_Item.Type != edMScan2Data) && (Ptr_Item.Type != edPathStepData))
                Ptr_Item.free();
        } //end cycle


///////////////////////////////////////////////////////////////////////////////

        bool ShowData = false;
        int SaveMaxAmpl;
        int SaveMaxDelay;
        int SaveDelay;
        int SaveStDelay;
        int SaveEndDelay;

        DEV_BScan2Head_ptr = NULL;
        for(unsigned int PtrListIdx = 0; PtrListIdx < Ptr_List.size(); PtrListIdx++)
        {
//            if (AutoconMain->DEV->MBScanData_from_UMU) frm->BScanAlg->Caption = "M/B-Scan: БУМ";
//                                                  else frm->BScanAlg->Caption = "M/B-Scan: Алекс";

            sPtrListItem& Ptr_Item = Ptr_List[PtrListIdx];
            if ((Ptr_Item.Type == edBScan2Data) || (Ptr_Item.Type == edMScan2Data))
            {

                if (frm->ACCheckBox->Checked) frm->Chart4->Tag= frm->Chart4->Tag + 1;

                DEV_BScan2Head_ptr = Ptr_Item.Ptr1.pBScan2Head;
//                PBScanData_ptr = Ptr_Item.Ptr2.pBScanData;

                //Hand BScan
                if ((DEV_BScan2Head_ptr) && ((Ptr_Item.Type == edBScan2Data) /*|| (Ptr_Item.Type == edMScan2Data)*/)) {
                     // frm->bsl->SetViewCrdZone(DEV_BScan2Head_ptr->XSysCrd[0] - frm->TrackBar1->Position * 100, DEV_BScan2Head_ptr->XSysCrd[0]);


                   if (frm->CoordGraphCheckBox->Checked)
                     if (AutoconMain->DEV->GetCurrentBScanSessionID() == DEV_BScan2Head_ptr->BScanSessionID) {


                           if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp)  {

                               if (frm->ComboBox1->ItemIndex == 1)
                               {
                                   if (DEV_BScan2Head_ptr->PathEncodersIndex == 0) frm->Main_PE->AddY(DEV_BScan2Head_ptr->XDisCrd[frm->MainPathEncoderIndex]);
                                   if (DEV_BScan2Head_ptr->PathEncodersIndex == 1) frm->HeadScaner_PE->AddY(DEV_BScan2Head_ptr->XDisCrd[DEV_BScan2Head_ptr->PathEncodersIndex]);
                               }
                               else
                               {
                                   if (DEV_BScan2Head_ptr->PathEncodersIndex == 0) frm->LineSeries1->AddY(DEV_BScan2Head_ptr->XSysCrd[frm->MainPathEncoderIndex]);
                                   //if (DEV_BScan2Head_ptr->PathEncodersIndex == 1) frm->HeadScaner_PE->AddY(DEV_BScan2Head_ptr->XSysCrd[DEV_BScan2Head_ptr->PathEncodersIndex]);
                               }
                           }
                           else
                           {
                               frm->HandScan_PE->AddY(DEV_BScan2Head_ptr->XSysCrd[frm->MainPathEncoderIndex]);
                           }
                       }

                   if (frm->cbPathEncoderGraph->Checked) frm->Series6->Add(DEV_BScan2Head_ptr->XSysCrd[frm->MainPathEncoderIndex]);

                   if (DEV_BScan2Head_ptr->Simulator[frm->MainPathEncoderIndex] == (bool)frm->MainPathEncoderType)
                        XSysCrd_ = XSysCrd_ + abs(DEV_BScan2Head_ptr->Dir[frm->MainPathEncoderIndex]);

                   frm->bsl->SetViewCrdZone(XSysCrd_ - frm->TrackBar1->Position * 10, XSysCrd_);


                   frm->sgPathCounters->Cells[(Byte)DEV_BScan2Head_ptr->Simulator[0]][0] = IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]);
                   frm->sgPathCounters->Cells[(Byte)DEV_BScan2Head_ptr->Simulator[1]][1] = IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[1]);
                   frm->sgPathCounters->Cells[(Byte)DEV_BScan2Head_ptr->Simulator[2]][2] = IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[2]);

//                   frm->LogCrd_->Add(IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]) + " " + IntToStr((int)(DEV_BScan2Head_ptr->ChannelType == ctCompInsp)));

                   UnicodeString tmp = "";
//                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! // Отладка скачка кординаты

/* BAD
                   if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp) tmp = "-Comp"; else tmp = "-Hand";
                   if (tmp != frm->LastChType)
                   {
                       frm->Memo3->Lines->Add("Last: " + IntToStr(frm->LastXSysCrd) + tmp +
                                              " New: " + IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]) + frm->LastChType);
                   }
                   frm->LastXSysCrd = (int)DEV_BScan2Head_ptr->XSysCrd[0];
                   frm->LastChType = tmp;
*/
/* GOOD
                   UnicodeString tmp = "";
                   if (((DEV_BScan2Head_ptr->ChannelType == ctCompInsp) && (std::abs((int)DEV_BScan2Head_ptr->XSysCrd[0]) < 100000)) ||
                       ((DEV_BScan2Head_ptr->ChannelType == ctHandScan) && (std::abs((int)DEV_BScan2Head_ptr->XSysCrd[0]) > 100000)))
                   {


                       if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp) tmp = "-Comp"; else tmp = "-Hand";
                       frm->Memo3->Lines->Add("Last: " + IntToStr(frm->LastXSysCrd) + tmp +
                                              " New: " + IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]) + frm->LastChType);
                   }
                   frm->LastXSysCrd = (int)DEV_BScan2Head_ptr->XSysCrd[0];
                   frm->LastChType = tmp;
*/
/*
//                   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! // Отладка скачка кординаты
                   if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp) {

                       if (std::abs(frm->LastXSysCrd - std::abs((int)DEV_BScan2Head_ptr->XSysCrd[0]) > 10)  {

                            frm->Memo3->Lines->Add(IntToStr(frm->LastXSysCrd) + " " + IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]));
                            frm->LogCrd_->SaveToFile("crd.log");
                       }
                       frm->LastXSysCrd = (int)DEV_BScan2Head_ptr->XSysCrd[0];
                   }
   */
/* Запись значений координат с указанием типа данных - ручник / сплошник
                  if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp) tmp = "-Comp"; else tmp = "-Hand";
                   frm->LogCrd_->Add(IntToStr((int)DEV_BScan2Head_ptr->XSysCrd[0]) + tmp);
*/
                   tmp = " no data";

                    if (DEV_BScan2Head_ptr->Items.size() != 0)
                        switch (DEV_BScan2Head_ptr->Items[0].Side)
                        {
                           case 0: tmp = " dsNone"; break;  // Нет стороны (для однониточных приборов)
                           case 1: tmp = " dsLeft"; break;  // Левая сторона
                           case 2: tmp = " dsRight"; break; // Правая сторона
                        }

                    if (frm->CBLogSwitch->Tag == 1)
                        if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp)
                        {
                            frm->LogLines_->Insert(0, Format("ДП: %d, ИМ: %d, СЕСИЯ: %d, CompInsp, Side: %s", ARRAYOFCONST(((int)DEV_BScan2Head_ptr->XSysCrd[0], (Byte)DEV_BScan2Head_ptr->Simulator[0], DEV_BScan2Head_ptr->BScanSessionID, tmp))));
                        }
                        else
                        {
                            frm->LogLines_->Insert(0, Format("ДП: %d, ИМ: %d, СЕСИЯ: %d, ctHandScan, Side: %s", ARRAYOFCONST(((int)DEV_BScan2Head_ptr->XSysCrd[0], (Byte)DEV_BScan2Head_ptr->Simulator[0], DEV_BScan2Head_ptr->BScanSessionID, tmp))));
                        }
                }


//                if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp)
                if (AutoconMain->DEV->GetCurrentBScanSessionID() == DEV_BScan2Head_ptr->BScanSessionID)
                {
                    frm->bsl->SetMark(XSysCrd_, clGreen);
                }
                else
                {
                    frm->bsl->SetMark(XSysCrd_, clRed);
                }

                //for (unsigned int ItemIdx = 0; ItemIdx < frm->ChList2->Items->Count; ItemIdx++)
                //    frm->ChList2->Items->Objects[ItemIdx] = (TObject*)1;

                bool SaveNoiseFlag = false;
                if (frm->cbNoiseLog->Checked)
                    if ((GetTickCount() - frm->NoiseTick >= 1000 /*60000*/ ) || (frm->NoiseTick == - 1)) {

                        frm->NoiseTick = GetTickCount();
                        SaveNoiseFlag = true;
                        frm->LogCrd_->Add(Format("%10d", ARRAYOFCONST(((int)(((float)((int)GetTickCount() - frm->NoiseStartTick)) / (float)6000)))) + "#");

                    }

                if ((frm->PageControl->ActivePage->Tag == 100) && (frm->ACBuffer != NULL)) {

                    frm->ACBuffer->Canvas->Brush->Color = clWhite;
                    frm->ACBuffer->Canvas->FillRect(Rect(0, 0, frm->ACBuffer->Width, frm->ACBuffer->Height));
                    frm->ACBuffer->Canvas->Pen->Color = clBlack;
                    frm->ACBuffer->Canvas->MoveTo(frm->ACBuffer->Width / 2, 0);
                    frm->ACBuffer->Canvas->LineTo(frm->ACBuffer->Width / 2, frm->ACBuffer->Height);
                }

                for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->ACItems.size(); ItemIdx++)

                    for (unsigned int J = 0; J < frm->ChList->Count; J++) { // --- Акустический контакт

                        int temp_id;
                        eDeviceSide s;
                        UnicodeString tmp = "";

                        temp_id = (int)frm->ChList->Items->Objects[J];

                        if (temp_id > 9999) {
                            s = dsNone;
                            temp_id = temp_id - 10000;

                            } else {
                                    if (temp_id > 0) s = dsLeft;
                                      else s = dsRight;
                                    temp_id = abs(temp_id);
                                    temp_id = temp_id - 1;
                                }
/*
                        if ((DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x01) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x06) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x0B) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x17) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x18) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x19) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x1A) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x07) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel != 0x0C))

                             ShowMessage("fFF!");
  */
                        if ((DEV_BScan2Head_ptr->ACItems[ItemIdx].Side  == s) &&
                            (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel == temp_id)) {


                            frm->ChList2->Items->Objects[ItemIdx] = (TObject*)GetTickCount();
                            if (frm->cbViewACState->Checked)
                            {
                                if ((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACValue != 0)
                                {
                                    frm->ChList2->Items->Strings[J] = "[+]";
                                }
                                else
                                {
                                    frm->ChList2->Items->Strings[J] = "[-]";
                                };
                            } else frm->ChList2->Items->Strings[J] = "";

                            if (frm->cbViewAC->Tag == 1) {

                                frm->ACMinMas[J] = std::min(frm->ACMinMas[J], (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                frm->ACMaxMas[J] = std::max(frm->ACMaxMas[J], (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);

                                if (frm->ACCheckBox->Checked) {


                                    if (J == 1) frm->FastLineSeries1->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 2) frm->FastLineSeries2->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 3) frm->FastLineSeries3->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 4) frm->FastLineSeries4->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 5) frm->FastLineSeries5->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 6) frm->FastLineSeries6->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 7) frm->FastLineSeries7->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 8) frm->FastLineSeries8->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    if (J == 9) frm->FastLineSeries9->AddXY(frm->Chart4->Tag, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm);
                                    frm->Chart4->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart5->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart6->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart7->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart8->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart9->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart10->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart11->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                    frm->Chart12->BottomAxis->SetMinMax(frm->Chart4->Tag - 1000, frm->Chart4->Tag);
                                }

                                // Акустический контакт
                                if ((frm->PageControl->ActivePage->Tag == 100) && (frm->ACBuffer != NULL) && (J != 0)) {

                                    int X;
                                    int Y;
                                    int ValTop = std::max(frm->ACMaxMas[J] + 100, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh + 100);
                                    int ValBtm = 0; // std::min(frm->ACMinMas[J] - 100, (int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh - 100);

                                    if (s != dsRight) Y = J - 1;
                                                 else Y = J - (AutoconMain->Config->GetScanChannelsCount_() / 2) - 1;

                                    if (Y < 5) {

                                        if (s != dsRight) X = frm->ACBuffer->Width / 8;
                                                     else X = 5 * frm->ACBuffer->Width / 8;
                                    }
                                    else {

                                        if (s != dsRight) X = 3 * frm->ACBuffer->Width / 8;
                                                     else X = 7 * frm->ACBuffer->Width / 8;
                                        Y = Y - 5;
                                    }

                                    Y = 15 + Y * (abs(frm->ACBuffer->Canvas->Font->Height) + 130);


                                    UnicodeString tmp;

                                    tmp = frm->ChList->Items->Strings[J];
                                    tmp.Delete(1, 6);

                                    if ((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACValue != 0)
                                    {
                                        tmp = "[+] " + tmp;
                                    }
                                    else
                                    {
                                        tmp = "[-] " + tmp;
                                    };

                                    TRect Rt = Rect(X - 25, Y + 27, X + 25, Y + 120);
                                    if (ValTop != ValBtm) Rt.Top = Rt.Bottom - Rt.Height() * ((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm - ValBtm) / (ValTop - ValBtm);
                                    if ((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACValue != 0)
                                    {
                                       frm->ACBuffer->Canvas->Brush->Color = clLime;
                                    }
                                    else
                                    {
                                       frm->ACBuffer->Canvas->Brush->Color = clRed;
                                    };
                                    frm->ACBuffer->Canvas->FillRect(Rt);
                                    frm->ACBuffer->Canvas->Brush->Style = bsClear;
                                    frm->ACBuffer->Canvas->TextOutW(Rt.Right + 5, Rt.Top, IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm));

                                    Rt = Rect(X - 25, Y + 27, X + 25, Y + 120);
                                    if (ValTop != ValBtm) Rt.Top = Rt.Bottom - Rt.Height() * ((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh - ValBtm) / (ValTop - ValBtm);
                                    frm->ACBuffer->Canvas->Pen->Width = 3;
                                    frm->ACBuffer->Canvas->MoveTo(Rt.Left - 10, Rt.Top);
                                    frm->ACBuffer->Canvas->LineTo(Rt.Right + 10, Rt.Top);
                                    frm->ACBuffer->Canvas->TextOutW(Rt.Left - 5 - frm->ACBuffer->Canvas->TextWidth(IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh)), Rt.Top, IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh));
                                    frm->ACBuffer->Canvas->Pen->Width = 1;

                                    Rt = Rect(X - 25, Y + 27, X + 25, Y + 120);
                                    frm->ACBuffer->Canvas->Rectangle(Rt);
                                    frm->ACBuffer->Canvas->TextOutW(Rt.Left + 5, Rt.Top - std::abs(frm->ACBuffer->Canvas->Font->Height) - 3, IntToStr(ValTop /*frm->ACMaxMas[J]*/));
                                    frm->ACBuffer->Canvas->TextOutW(Rt.Left + 5, Rt.Bottom + 3, IntToStr(ValBtm));
                                    frm->ACBuffer->Canvas->Font->Height = -14;
                                    frm->ACBuffer->Canvas->TextOutW(Rt.CenterPoint().X - frm->ACBuffer->Canvas->TextWidth(tmp) / 2, Rt.Top - 30, tmp);
                                    frm->ACBuffer->Canvas->Font->Height = -11;

                                }

                            }

                            if (SaveNoiseFlag) {

                                UnicodeString tmp;
                                tmp = frm->LogCrd_->Strings[frm->LogCrd_->Count - 1];
                                tmp = tmp + Format("%10d", ARRAYOFCONST(((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm))) + "#";
                                frm->LogCrd_->Strings[frm->LogCrd_->Count - 1] = tmp;
                            }


                            if (frm->cbViewAC->Checked)
                                frm->ChList2->Items->Strings[J] = frm->ChList2->Items->Strings[J] + IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm)  + " (" + IntToStr(frm->ACMinMas[J]) + "/"  + IntToStr(frm->ACMaxMas[J]) + ") ";

                            if (frm->cbViewACTh->Checked)
                                frm->ChList2->Items->Strings[J] = frm->ChList2->Items->Strings[J] + IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh) + " ";

//                            frm->ChList2->Items->Objects[J] = (TObject*)((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACValue);
                        }
                    }

                if ((frm->PageControl->ActivePage->Tag == 100) && (frm->ACBuffer != NULL))
                  frm->PaintBox1->Canvas->Draw(0, 0, frm->ACBuffer);



                // Отладка В-развертки !!!!!!!!!!!!!!
                if ((frm->PageControl->ActivePage->Tag == 101) && (frm->BSBuffer != NULL)) {


                    unsigned long tmp = GetTickCount_();
                    if (tmp - frm->PathStepGis_LastUpdate2 > 1000) {


                        int EnterStep;
                        float speed = 0;
                        if (TryStrToInt(frm->Edit12->Text, EnterStep))
                            speed = 3.6 * (DEV_BScan2Head_ptr->XDisCrd[0] - frm->PathStepGis_LastVal2) * StrToInt(frm->Edit12->Text) / 1000;

                        frm->Edit13->Text = Format("%3.1f", ARRAYOFCONST((speed)));
                        frm->PathStepGis_LastVal2 = DEV_BScan2Head_ptr->XDisCrd[0];
                        frm->PathStepGis_LastUpdate2 = tmp;
                    }

                    if (frm->PathStepGis_LastVal != -1 ) {

                        int val = DEV_BScan2Head_ptr->XDisCrd[0] - frm->PathStepGis_LastVal;
                        if ((val >= 0) && (val < 30)) frm->PathStepGis[val]++;
                    }
                    frm->PathStepGis_LastVal = DEV_BScan2Head_ptr->XDisCrd[0];

                    if (tmp - frm->PathStepGis_LastUpdate > 100) {

                        frm->BSBuffer->Canvas->Brush->Color = clWhite;
                        frm->BSBuffer->Canvas->FillRect(Rect(0, 0, frm->BSBuffer->Width, frm->BSBuffer->Height));
                        frm->BSBuffer->Canvas->Pen->Color = clBlack;
                        frm->BSBuffer->Canvas->MoveTo(frm->BSBuffer->Width / 2, 0);
                        frm->BSBuffer->Canvas->LineTo(frm->BSBuffer->Width / 2, frm->BSBuffer->Height);

                        int LastY = 0;

                        for (unsigned int J = 1; J < frm->ChList->Count; J++) {

                            int temp_id;
                            eDeviceSide s;
                            UnicodeString tmp = "";

                            temp_id = (int)frm->ChList->Items->Objects[J];

                            if (temp_id > 9999) {
                                s = dsNone;
                                temp_id = temp_id - 10000;

                                } else {
                                        if (temp_id > 0) s = dsLeft;
                                          else s = dsRight;
                                        temp_id = abs(temp_id);
                                        temp_id = temp_id - 1;
                                    }

                            int SignalCount = 0;
                            for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->Items.size(); ItemIdx++)
                              if ((DEV_BScan2Head_ptr->Items[ItemIdx].Side  == s) &&
                                  (DEV_BScan2Head_ptr->Items[ItemIdx].Channel == temp_id)) SignalCount = DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size();

                                if ((frm->PageControl->ActivePage->Tag == 101) && (frm->BSBuffer != NULL) && (J != 0)) {

                                    int X;
                                    int Y;
                                    int ValTop = 8;
                                    int ValBtm = 0;

                                    if (s != dsRight) Y = J - 1;
                                                 else Y = J - (AutoconMain->Config->GetScanChannelsCount_() / 2) - 1;

                                    if (Y < 5) {

                                        if (s != dsRight) X = frm->BSBuffer->Width / 8;
                                                     else X = 5 * frm->BSBuffer->Width / 8;
                                    }
                                    else {

                                        if (s != dsRight) X = 3 * frm->BSBuffer->Width / 8;
                                                     else X = 7 * frm->BSBuffer->Width / 8;
                                        Y = Y - 5;
                                    }

                                    Y = 15 + Y * (abs(frm->BSBuffer->Canvas->Font->Height) + 60);
                                    if (LastY < Y) LastY = Y;

                                    UnicodeString tmp;

                                    tmp = frm->ChList->Items->Strings[J];
                                    tmp.Delete(1, 6);

                                    TRect Rt = Rect(X - 25, Y + 27, X + 25, Y + 50);
                                    if (ValTop != ValBtm) Rt.Top = Rt.Bottom - Rt.Height() * SignalCount / (ValTop - ValBtm);
                                    frm->BSBuffer->Canvas->Brush->Color = clRed;

                                    frm->BSBuffer->Canvas->FillRect(Rt);

                                    Rt = Rect(X - 25, Y + 27, X + 25, Y + 50);
                                    if (ValTop != ValBtm) Rt.Top = Rt.Bottom - Rt.Height() * (SignalCount - ValBtm) / (ValTop - ValBtm);
    //                                frm->BSBuffer->Canvas->Pen->Width = 3;
                                    frm->BSBuffer->Canvas->Pen->Color = clGray;
                                    frm->BSBuffer->Canvas->MoveTo(Rt.Left - 10, Rt.Top);
                                    frm->BSBuffer->Canvas->LineTo(Rt.Right + 10, Rt.Top);
    //                                frm->BSBuffer->Canvas->TextOutW(Rt.Left - 5 - frm->BSBuffer->Canvas->TextWidth(IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh)), Rt.Top, IntToStr((int)DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh));
                                    frm->BSBuffer->Canvas->Pen->Color = clBlack;
                                    frm->BSBuffer->Canvas->Pen->Width = 1;

                                    Rt = Rect(X - 25, Y + 27, X + 25, Y + 50);
                                    frm->BSBuffer->Canvas->Brush->Style = bsClear;
                                    frm->BSBuffer->Canvas->Rectangle(Rt);
    //                                frm->BSBuffer->Canvas->TextOutW(Rt.Left + 5, Rt.Top - std::abs(frm->BSBuffer->Canvas->Font->Height) - 3, IntToStr(ValTop));
    //                                frm->BSBuffer->Canvas->TextOutW(Rt.Left + 5, Rt.Bottom + 3, IntToStr(ValBtm));
                                    frm->BSBuffer->Canvas->Font->Height = -14;
                                    frm->BSBuffer->Canvas->TextOutW(Rt.CenterPoint().X - frm->BSBuffer->Canvas->TextWidth(tmp) / 2, Rt.Top - 30, tmp);
                                    frm->BSBuffer->Canvas->Font->Height = -11;
                                    tmp = IntToStr(SignalCount);
                                    frm->BSBuffer->Canvas->TextOutW(Rt.CenterPoint().X - frm->BSBuffer->Canvas->TextWidth(tmp) / 2, Rt.CenterPoint().Y - frm->BSBuffer->Canvas->TextHeight(tmp) / 2, tmp);
                                }
                        }

                        frm->PathStepGis_LastUpdate = tmp;
                        frm->Series8->BeginUpdate();
                        frm->Series8->Clear();
                        for (unsigned int J = 0; J < 29; J++) {

                          frm->Series8->Add(frm->PathStepGis[J]);
                        }
                        frm->Series8->EndUpdate();
                        frm->PaintBox2->Canvas->Draw(0, 0, frm->BSBuffer);
                    }
                }

                // Отладка Фильтрации
                if (frm->PageControl->ActivePage->Tag == 111) {

                    unsigned long tmp = GetTickCount_();
                    if (tmp - frm->PathStepGis_LastUpdate3 > 1000) {

                        frm->PathStepGis_LastUpdate3 = tmp;
                        int Summ = 0;
                        int Summ_ = 0;
                        for (unsigned char first = 0; first < 2; ++first)
                            for (unsigned char idx = 0; idx < CHANNELS_TOTAL; ++idx) {

                                FiltrationData& data = AutoconMain->DEV->_filtrationData[first][idx];
                                Summ_ = Summ_ + data.masks.size();
                                Summ = Summ + data.debugGetSize();
                            }

                        frm->FiltrSizeLabel->Caption = "Size: " + IntToStr(Summ) + " Count: " + IntToStr(Summ_);
                        frm->FiltrationTabMemo->Lines->Clear();
                        for (unsigned int J = 1; J < frm->ChList->Count; J++) {

                            int temp_id;
                            eDeviceSide s;
                            UnicodeString tmp = "";
                            UnicodeString tmp_ = "";

                            temp_id = (int)frm->ChList->Items->Objects[J];

                            if (temp_id > 9999) {
                                s = dsNone;
                                temp_id = temp_id - 10000;

                                } else {
                                        if (temp_id > 0) s = dsLeft;
                                          else s = dsRight;
                                        temp_id = abs(temp_id);
                                        temp_id = temp_id - 1;
                                    }


                               tmp = frm->ChList->Items->Strings[J];
                               tmp.Delete(1, 6);

                               if ((s >= 1) && (s <= 2) && (temp_id >= 0) && (temp_id < CHANNELS_TOTAL)) {

                                   FiltrationData& data = AutoconMain->DEV->_filtrationData[s == dsRight][temp_id];

                                   frm->FiltrationTabMemo->Lines->Add(Format("%s - Count: %d, Size: %d", ARRAYOFCONST((tmp, data.masks.size(), data.debugGetSize()))));
                                   tmp_ = "";
                                   for (int J = 0; J < data.masks.size(); J++) {

                                       if (data.masks[J].Busy) {

                                           tmp_ = tmp_ + Format("№%d; Cnt: %d; -6dB Cnt: %d ", ARRAYOFCONST((J, data.masks[J].Signals_.size(), 0 /*data.masks[J].SignalsM6dB.size()*/)));

                                       }
                                   }
                                   frm->FiltrationTabMemo->Lines->Add(tmp_);
                               }
                               frm->FiltrationTabMemo->Lines->Add("");
                        }
                    }

                }

                // -----------------------------------------------------------------------------
                                                                     // --- АС

                for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->ACItems.size(); ItemIdx++)

                    if ((DEV_BScan2Head_ptr->ACItems[ItemIdx].Side == AutoconMain->DEV->GetSide()) &&
                        (DEV_BScan2Head_ptr->ACItems[ItemIdx].Channel == AutoconMain->DEV->GetChannel()))

                        // Вывод В-развертки
                        if (frm->CheckBox6->Checked) {

                          if (DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm >= DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh)
                            frm->bsl->SetPoint3(XSysCrd_, DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm, clGreen); else
                            frm->bsl->SetPoint3(XSysCrd_, DEV_BScan2Head_ptr->ACItems[ItemIdx].ACSumm, clRed);

                          frm->bsl->SetPoint3(XSysCrd_, DEV_BScan2Head_ptr->ACItems[ItemIdx].ACTh, clBlack);
                        }

                // -----------------------------------------------------------------------------

                                                                     // --- В - развертка

                if ((frm->BSDebugBuffer != NULL) && (frm->PageControl->ActivePage->Tag == -876)) {

                    frm->BSDebugBuffer->Canvas->Brush->Color = clWhite;
                    frm->BSDebugBuffer->Canvas->FillRect(Rect(0, 0, frm->BScanTestData->Width, frm->BScanTestData->Height));
                    frm->BSDebugBuffer->Canvas->Pen->Width = 1;

                    int LineHeight = 25;
                    int LineY = 0;
                    int StartX = 175;
                    int LineNumber = -1;
                    eDeviceSide s;

                    frm->BadBScanDelay->Caption = "";

                    for (unsigned int J = 0; J < frm->ChList->Count; J++) {

                        int temp_id = (int)frm->ChList->Items->Objects[J];

                        if (temp_id == 123456) continue;

                        if (temp_id > 9999) {

                              s = dsNone;
                              temp_id = temp_id - 10000;

                            } else {

                                    LineNumber++;
                                    if (temp_id > 0) s = dsLeft;
                                      else s = dsRight;
                                    temp_id = abs(temp_id);
                                    temp_id = temp_id - 1;
                                }


                        if (AutoconMain->Table->isHandScan(temp_id)) continue;

                        sChannelDescription ChanDesc;
                        AutoconMain->Table->ItemByCID(temp_id, &ChanDesc);
                        int BScanDelayMultiply = AutoconMain->DEV->GetChannelBScanDelayMultiply(temp_id);

                        float XFactor = BScanDelayMultiply * (frm->BSDebugBuffer->Width - StartX) / 255;
                        LineY = LineHeight / 5 + LineNumber * LineHeight;

                        frm->BSDebugBuffer->Canvas->Brush->Color = clWhite;
                        frm->BSDebugBuffer->Canvas->Font->Color = clBlack;
                        frm->BSDebugBuffer->Canvas->Font->Height = LineHeight / 1.3;
                        frm->BSDebugBuffer->Canvas->TextOutW(5, LineY - frm->BSDebugBuffer->Canvas->Font->Height / 2 , Format("Side: %d Ch: %x", ARRAYOFCONST((s, temp_id))));

                        frm->BSDebugBuffer->Canvas->MoveTo(StartX + ChanDesc.cdBScanGate.gStart * XFactor, LineY);
                        frm->BSDebugBuffer->Canvas->LineTo(StartX + ChanDesc.cdBScanGate.gEnd * XFactor, LineY);

                        frm->BSDebugBuffer->Canvas->MoveTo(StartX + ChanDesc.cdBScanGate.gStart * XFactor, LineY - LineHeight / 2.2);
                        frm->BSDebugBuffer->Canvas->LineTo(StartX + ChanDesc.cdBScanGate.gStart * XFactor, LineY + LineHeight / 2.2);

                        frm->BSDebugBuffer->Canvas->MoveTo(StartX + ChanDesc.cdBScanGate.gEnd * XFactor, LineY - LineHeight / 2.2);
                        frm->BSDebugBuffer->Canvas->LineTo(StartX + ChanDesc.cdBScanGate.gEnd * XFactor, LineY + LineHeight / 2.2);

                        frm->BSDebugBuffer->Canvas->Font->Color = clWhite;
                        frm->BSDebugBuffer->Canvas->Font->Height = LineHeight / 1.4;


                        for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->Items.size(); ItemIdx++)

                            if ((DEV_BScan2Head_ptr->Items[ItemIdx].Side == s) &&
                                (DEV_BScan2Head_ptr->Items[ItemIdx].Channel == temp_id))

                                {
                                    // Проверка выхода задержек за стробы В-развертки

                                    for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size(); SignalIdx++)
                                    {
                                        int delay = (int)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Delay / BScanDelayMultiply);
                                        if ((delay > ChanDesc.cdBScanGate.gEnd ) || (delay < ChanDesc.cdBScanGate.gStart)) {

                                            frm->BSDebugBuffer->Canvas->Brush->Color = clRed;
                                            frm->BadBScanDelay->Caption = Format("BScan Delay Error - delay %d", ARRAYOFCONST((delay)));
                                            frm->Memo5->Lines->Add(Format("BScan Delay Error - side: %d Ch: %x: delay: %d", ARRAYOFCONST((DEV_BScan2Head_ptr->Items[ItemIdx].Side, DEV_BScan2Head_ptr->Items[ItemIdx].Channel, delay))));
                                        }  else {
                                          //frm->Memo5->Lines->Add(Format("BScan Delay OK - side: %d Ch: %x: delay: %d", ARRAYOFCONST((DEV_BScan2Head_ptr->Items[ItemIdx].Side, DEV_BScan2Head_ptr->Items[ItemIdx].Channel, delay))));
                                          frm->BSDebugBuffer->Canvas->Brush->Color = clGreen;
                                        }

                                        frm->BSDebugBuffer->Canvas->FillRect(Rect(StartX + delay * XFactor - LineHeight / 3, LineY - LineHeight / 3, StartX + delay * XFactor + LineHeight / 3, LineY + LineHeight / 3));
                                        frm->BSDebugBuffer->Canvas->Brush->Style = bsClear;
                                        frm->BSDebugBuffer->Canvas->TextOutW(StartX + delay * XFactor - LineHeight / 3, LineY - LineHeight / 3, IntToStr(SignalIdx));

                                    }
                                }
                    }
                    frm->BScanTestData->Canvas->Draw(0, 0, frm->BSDebugBuffer);
                }

                for (unsigned int ItemIdx = 0; ItemIdx < DEV_BScan2Head_ptr->Items.size(); ItemIdx++)
                {

                    if ((DEV_BScan2Head_ptr->Items[ItemIdx].Side == AutoconMain->DEV->GetSide()) &&
                        (DEV_BScan2Head_ptr->Items[ItemIdx].Channel == AutoconMain->DEV->GetChannel()))
                    {

                        #ifdef NewBScanProt // - Вывод данных для последней версии ПО БУМ от Олега
                        int BScanDelayMultiply = AutoconMain->DEV->GetChannelBScanDelayMultiply(DEV_BScan2Head_ptr->Items[ItemIdx].Channel);

                        frm->BScanData->Tag = 0;
                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size(); SignalIdx++)
                        {
                            frm->BScanData->Cells[0][frm->BScanData->Tag + 1] = IntToStr(SignalIdx + 1);
                            frm->BScanData->Cells[1][frm->BScanData->Tag + 1] = IntToStr(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Delay / BScanDelayMultiply);
                            frm->BScanData->Cells[2][frm->BScanData->Tag + 1] = IntToStr(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxAmpl]);
                            frm->BScanData->Tag++;
                        }

                        // Вывод В-развертки
                        if (frm->CheckBox6->Checked)


                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size(); SignalIdx++)
                        {

                            if (Ptr_Item.Type != edMScan2Data)
                                {


/*                                    frm->bsl->SetPoint(XSysCrd_,
                                                       (float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[StartDelay_int]) + (float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[StartDelay_frac]) / (float)10,
                                                       (float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[EndDelay_int]) + (float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[EndDelay_frac]) / (float)10,
                                                       (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[MaxAmpl]); */
                                    frm->bsl->SetPoint2(XSysCrd_,
                                                       DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Delay / BScanDelayMultiply,
                                                       DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxAmpl]);
                                }
                        }




                        #ifdef UMUDataDebug  /*
                        if (ShowData)
                        {

                            frm->DebugMemo->Clear();
                            frm->DebugMemo->Lines->Add("-BSCAN:-");
                            frm->DebugMemo->Lines->Add("D:" + IntToStr(SaveDelay));

                            int Ampl = - 1;
                            #ifdef BScanSize_12Byte
                            int AmplIdx = 6;
                            if (AmplIdx&1) Ampl = / * ThValList[* /((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx / 2] >> 4) / *]* /; else
                                           Ampl = / * ThValList[* /((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx / 2] & 0x0F)/ *]* /;
                            #endif
                            #ifndef BScanSize_12Byte
                            int AmplIdx = 6;
                            Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx];
                            #endif

                            frm->DebugMemo->Lines->Add("A:" + IntToStr(Ampl));
                            frm->DebugMemo->Lines->Add("");
                            frm->DebugMemo->Lines->Add("-CALC:-");
                            frm->DebugMemo->Lines->Add("I:" + IntToStr(SaveMaxDelay));
                            frm->DebugMemo->Lines->Add("A:" + IntToStr(SaveMaxAmpl));
                            frm->DebugMemo->Lines->Add("Шир: > " + IntToStr(SaveStDelay));
                            frm->DebugMemo->Lines->Add("Шир: < " + IntToStr(SaveEndDelay));
                            frm->DebugMemo->Lines->Add("Th: " + IntToStr(TableDB[AutoconMain->Config->BScanGateLevel] & 0x0F));

                        }
                                */
                        // Вывод одного выбранного сигнала В-развертки
                        frm->BScanSeries->Clear();
                        float Ampl;
                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size(); SignalIdx++)
                            if (frm->SelEchoIdx->ItemIndex == SignalIdx)
                            {
                                for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                                {
                                    #ifdef BScanSize_12Byte
                                   // if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                   //                Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                    if (AmplIdx&1) Ampl = ((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4); else
                                                   Ampl = ((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F);
                                    #endif
                                    #ifndef BScanSize_12Byte
                                    float Ampl = DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[AmplIdx];
                                    #endif
                                    frm->BScanSeries->AddXY(AmplIdx, Ampl);
                                }
                            }

                        // Вывод В-развертки - как А-развертки
                        frm->BScanDebugSeries->Clear();
                        if (Ptr_Item.Type == edBScan2Data) frm->BScanDebugSeries->Marks->Color = clAqua;
                            else frm->BScanDebugSeries->Marks->Color = clFuchsia;
                        frm->BSSeries1->Clear();
                        frm->BSSeries2->Clear();
                        frm->BSSeries3->Clear();
                        frm->BSSeries4->Clear();
                        frm->BSSeries5->Clear();
                        frm->BSSeries6->Clear();
                        frm->BSSeries7->Clear();
                        frm->BSSeries8->Clear();
                        frm->Series4->Clear();

                        frm->BS1->Clear();
                        frm->BS2->Clear();
                        frm->BS3->Clear();
                        frm->BS4->Clear();
                        frm->BS5->Clear();
                        frm->BS6->Clear();
                        frm->BS7->Clear();
                        frm->BS8->Clear();

                        frm->BScanDebugMemo->Lines->Clear();
//                        frm->BScanDebugMemo2->Lines->Clear();
                        frm->Series3->Clear();

                        float y;
                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Signals.size(); SignalIdx++)
                        {
                                TLineSeries *tmp1;
                                TLineSeries *tmp2;
                                switch (SignalIdx)
                                {
                                    case 0: { tmp1 = frm->BSSeries1; break; }
                                    case 1: { tmp1 = frm->BSSeries2; break; }
                                    case 2: { tmp1 = frm->BSSeries3; break; }
                                    case 3: { tmp1 = frm->BSSeries4; break; }
                                    case 4: { tmp1 = frm->BSSeries5; break; }
                                    case 5: { tmp1 = frm->BSSeries6; break; }
                                    case 6: { tmp1 = frm->BSSeries7; break; }
                                    case 7: { tmp1 = frm->BSSeries8; break; }
                                }

                                switch (SignalIdx)
                                {
                                    case 0: { tmp2 = frm->BS1; break; }
                                    case 1: { tmp2 = frm->BS2; break; }
                                    case 2: { tmp2 = frm->BS3; break; }
                                    case 3: { tmp2 = frm->BS4; break; }
                                    case 4: { tmp2 = frm->BS5; break; }
                                    case 5: { tmp2 = frm->BS6; break; }
                                    case 6: { tmp2 = frm->BS7; break; }
                                    case 7: { tmp2 = frm->BS8; break; }

                                }

                         	tIncomeData data;
                            static const int AmplIdtoDB[16] = {-12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18};

                            if (AutoconMain->DEV->GetBScanDataType() == bstWithForm)  {

                                data.y0 = DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartAmpl];
                                data.x0 = 10 * ((float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartDelay_int]) +
                                                (float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartDelay_frac]) / (float)10);

                                data.y1 = DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxAmpl];
                                data.x1 = 10 * ((float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxDelay_int]) +
                                                (float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxDelay_frac]) / (float)10); // MaxDelay_frac

                                data.y2 = DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxAmpl] / 4;
                                data.x2 = 10 * ((float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[EndDelay_int]) +
                                                (float)(DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[EndDelay_frac]) / (float)10); // MaxDelay_frac
                            }
                            else {
                                data.y1 = int(32 * pow(10,AmplIdtoDB[DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[AmplDBIdx_int]] / 20.0) );
                                data.x1 = DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Delay / BScanDelayMultiply;
                            }
                            tmp1->Clear();
                            tmp2->Clear();
                            double pY0XLCoord;
                            double pY0XRCoord;

                         /*   restore.gefCurve(&data, &pY0XLCoord, &pY0XRCoord);

                            for (int i = pY0XLCoord; i <= pY0XRCoord; i++)
                            {
                               tmp1->AddXY(frm->DelayToChartValue((float)i/10),restore.getValue((float)i));
                               tmp2->AddXY(frm->DelayToChartValue((float)i/10),restore.getValue((float)i));
                            }
                           */
                             if (((SignalIdx == 0) && (frm->CheckListBox1->Checked[0])) ||
                                 ((SignalIdx == 1) && (frm->CheckListBox1->Checked[1])) ||
                                 ((SignalIdx == 2) && (frm->CheckListBox1->Checked[2])) ||
                                 ((SignalIdx == 3) && (frm->CheckListBox1->Checked[3])) ||
                                 ((SignalIdx == 4) && (frm->CheckListBox1->Checked[4])) ||
                                 ((SignalIdx == 5) && (frm->CheckListBox1->Checked[5])) ||
                                 ((SignalIdx == 6) && (frm->CheckListBox1->Checked[6])) ||
                                 ((SignalIdx == 7) && (frm->CheckListBox1->Checked[7])))
                             {
                                 frm->Series3->AddXY(frm->DelayToChartValue(data.x0/(float)10), data.y0);
                                 frm->Series3->AddXY(frm->DelayToChartValue(data.x2/(float)10), data.y2);

                                 frm->BScanDebugMemo->Lines->Add(Format("%d-D:%d;S:%d.%d/%d;M:%d.%d/%d;E:%d.%d",
                                    ARRAYOFCONST((SignalIdx,
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Delay,
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartDelay_int],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartDelay_frac],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[StartAmpl],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxDelay_int],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxDelay_frac],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[MaxAmpl],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[EndDelay_int],
                                                 DEV_BScan2Head_ptr->Items[ItemIdx].Signals[SignalIdx].Ampl[EndDelay_frac]))));
                             }

                            if (AutoconMain->DEV->GetBScanDataType() == bstWithForm)  {

                             //frm->BScanDebugSeries->AddXY(frm->DelayToChartValue(data.x1/(float)10), data.y1, IntToStr(SignalIdx));
                             UnicodeString tmp = "";
                             if (frm->CheckBox5->Checked) tmp = "№" + IntToStr(SignalIdx);
                             if (frm->CheckBox9->Checked) tmp = IntToStr(AmplId_to_DB[TableDB[data.y1] & 0x0F]) + " дБ";
                             if (frm->CheckBox10->Checked) tmp = IntToStr(data.y1);
                             if (frm->CheckBox11->Checked) tmp = Format("%3.1f", ARRAYOFCONST((data.x1/10)));

                             if ((frm->CheckBox5->Checked) ||
                                 (frm->CheckBox9->Checked) ||
                                 (frm->CheckBox10->Checked) ||
                                 (frm->CheckBox11->Checked)) frm->BScanDebugSeries->AddXY(frm->DelayToChartValue(data.x1/(float)10), data.y1, tmp);
//                                else frm->BScanDebugSeries->AddXY(frm->DelayToChartValue(data.x1/(float)10), data.y1);

                             frm->Series4->AddXY(frm->DelayToChartValue(data.x0/(float)10), data.y0);
                             frm->Series4->AddXY(frm->DelayToChartValue(data.x1/(float)10), data.y1);
                             frm->Series4->AddXY(frm->DelayToChartValue(data.x2/(float)10), data.y2);
                           }
                           else {

                                frm->BScanDebugSeries->AddXY(frm->DelayToChartValue(data.x1), data.y1, IntToStr(SignalIdx));
                           }
                        }
                        #endif


                        #endif

                        // -----------------------------------------------------------------------------------

                        #ifndef NewBScanProt // - Вывод данных для версии ПО БУМ основанных на разработки Дерюгина

                        frm->Series4->Clear();
                        int BScanDelayMultiply = AutoconMain->DEV->GetChannelBScanDelayMultiply(DEV_BScan2Head_ptr->Items[ItemIdx].Channel);

                        // Вывод В-развертки
                        if (frm->CheckBox6->Checked)

                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                        {

                            // Поиск центра по максимальной амплитуде
                            int Ampl;
                            int MaxDelayIndex = - 1;
                            int MaxAmpl_ = - 1;
                            for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif
                                #ifndef BScanSize_12Byte
                                Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif
                                if (MaxAmpl_ < Ampl) {
                                    MaxAmpl_ = Ampl;
                                    MaxDelayIndex = AmplIdx;
                                }
                            }

                            // BScanSize_12Byte
                         //   if (MaxAmpl < TableDB[AutoconMain->Config->BScanGateLevel]) MaxAmpl = - 1;

                            if (MaxDelayIndex != - 1) {

                                int StDelay = - 1;
                                int EndDelay = - 1;
                                int MaxAmpl = - 1;

                                // Поиск левой границы
                                for (int AmplIdx = MaxDelayIndex; AmplIdx >= 0; AmplIdx--)
                                {
                                    #ifdef BScanSize_12Byte
                                    if (AmplIdx&1) Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)/*]*/; else
                                                   Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)/*]*/;
                                    if (Ampl < (TableDB[AutoconMain->Config->BScanGateLevel] & 0x0F)) break;
                                    #endif
                                    #ifndef BScanSize_12Byte
                                    Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                    if (Ampl < AutoconMain->Config->GetBScanGateLevel()) break;
                                    #endif
                                    StDelay = AmplIdx;
                                }

                                // Поиск правой границы
                                MaxAmpl = - 1;
                                for (int AmplIdx = MaxDelayIndex; AmplIdx < 24; AmplIdx++)
                                {
                                    #ifdef BScanSize_12Byte
                                    if (AmplIdx&1) Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)/*]*/; else
                                                   Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)/*]*/;
                                    MaxAmpl = std::max(MaxAmpl , Ampl);
                                    if (Ampl < (TableDB[AutoconMain->Config->BScanGateLevel] & 0x0F)) break;
                                    #endif
                                    #ifndef BScanSize_12Byte
                                    Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                    MaxAmpl = std::max(MaxAmpl , Ampl);
                                    if (Ampl < AutoconMain->Config->GetBScanGateLevel()) break;
                                    #endif

                                    EndDelay = AmplIdx;
                                }

                                if (frm->SelEchoIdx->ItemIndex == SignalIdx)
                                {
                                    ShowData = true;
                                    SaveMaxAmpl = MaxAmpl;
                                    SaveMaxDelay = MaxDelayIndex;
                                    SaveDelay = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay / AutoconMain->DEV->GetChannelBScanDelayMultiply(AutoconMain->DEV->GetChannel());
                                    SaveStDelay = StDelay;
                                    SaveEndDelay = EndDelay;
                                }

                                if (Ptr_Item.Type != edMScan2Data)
                                    if ((StDelay != - 1) && (EndDelay != - 1)) {
                                        //int Wth = 2;
                                        int Dly = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay / BScanDelayMultiply;
//                                        int BScanCrd = DEV_BScan2Head_ptr->XSysCrd[0];
                                        int BScanCrd = XSysCrd_;
                                       // frm->SetPoint(BScanCrd, Dly - Wth, Dly + Wth, MaxAmpl); // точка на В-развертке

                                        #ifdef BScanSize_12Byte
                                        frm->bsl->SetPoint(BScanCrd, Dly - (MaxDelayIndex - StDelay) * 0.333, Dly + (EndDelay - MaxDelayIndex) * 0.333, ThValList[MaxAmpl]);
                                        #endif
                                        #ifndef BScanSize_12Byte
                                        frm->bsl->SetPoint(BScanCrd, Dly - (MaxDelayIndex - StDelay) * 0.333, Dly + (EndDelay - MaxDelayIndex) * 0.333, MaxAmpl);
                                        #endif

                                        if (DEV_BScan2Head_ptr->ChannelType == ctCompInsp)
                                            frm->bsl->SetMark(BScanCrd, clRed);
                                            else frm->bsl->SetMark(BScanCrd, clGreen);
                                    }
                            }
                        }


                        #ifdef UMUDataDebug

                        if (ShowData)
                        {

                            frm->DebugMemo->Clear();
                            frm->DebugMemo->Lines->Add("-BSCAN:-");
                            frm->DebugMemo->Lines->Add("D:" + IntToStr(SaveDelay));

                            int Ampl = - 1;
                            #ifdef BScanSize_12Byte
                            int AmplIdx = 6;
                            if (AmplIdx&1) Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx / 2] >> 4) /*]*/; else
                                           Ampl = /*ThValList[*/((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx / 2] & 0x0F)/*]*/;
                            #endif
                            #ifndef BScanSize_12Byte
                            int AmplIdx = 6;
                            Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[frm->SelEchoIdx->ItemIndex].Ampl[AmplIdx];
                            #endif

                            frm->DebugMemo->Lines->Add("A:" + IntToStr(Ampl));
                            frm->DebugMemo->Lines->Add("");
                            frm->DebugMemo->Lines->Add("-CALC:-");
                            frm->DebugMemo->Lines->Add("I:" + IntToStr(SaveMaxDelay));
                            frm->DebugMemo->Lines->Add("A:" + IntToStr(SaveMaxAmpl));
                            frm->DebugMemo->Lines->Add("Шир: > " + IntToStr(SaveStDelay));
                            frm->DebugMemo->Lines->Add("Шир: < " + IntToStr(SaveEndDelay));
                            frm->DebugMemo->Lines->Add("Th: " + IntToStr(TableDB[AutoconMain->Config->GetBScanGateLevel()] & 0x0F));

                        }

                        // Вывод одного выбранного сигнала В-развертки
                        frm->BScanSeries->Clear();
                        float Ampl;
                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                            if (frm->SelEchoIdx->ItemIndex == SignalIdx)
                            {
                                for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                                {
                                    #ifdef BScanSize_12Byte
                                   // if (AmplIdx&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                   //                Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                    if (AmplIdx&1) Ampl = ((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4); else
                                                   Ampl = ((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F);
                                    #endif
                                    #ifndef BScanSize_12Byte
                                    float Ampl = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                    #endif
                                    frm->BScanSeries->AddXY(AmplIdx, Ampl);
                                }
                            }

                        // Вывод В-развертки - как А-развертки
                        frm->BScanDebugSeries->Clear();
                        frm->BSSeries1->Clear();
                        frm->BSSeries2->Clear();
                        frm->BSSeries3->Clear();
                        frm->BSSeries4->Clear();
                        frm->BSSeries5->Clear();
                        frm->BSSeries6->Clear();
                        frm->BSSeries7->Clear();
                        frm->BSSeries8->Clear();

                        float y;
                        for (int SignalIdx = 0; SignalIdx < DEV_BScan2Head_ptr->Items[ItemIdx].Count; SignalIdx++)
                        {
                            for (int AmplIdx = 0; AmplIdx < 24; AmplIdx++)
                            {
                                #ifdef BScanSize_12Byte
                                if (AmplIdx&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                #endif

                                #ifndef BScanSize_12Byte
                                float y = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx];
                                #endif

                                float x = frm->DelayToChartValue((float)((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay / BScanDelayMultiply) + 0.333 * (float)AmplIdx - (float)2);

                                switch (SignalIdx)
                                {
                                    case 0: { frm->BSSeries1->AddXY(x, y); break; }
                                    case 1: { frm->BSSeries2->AddXY(x, y); break; }
                                    case 2: { frm->BSSeries3->AddXY(x, y); break; }
                                    case 3: { frm->BSSeries4->AddXY(x, y); break; }
                                    case 4: { frm->BSSeries5->AddXY(x, y); break; }
                                    case 5: { frm->BSSeries6->AddXY(x, y); break; }
                                    case 6: { frm->BSSeries7->AddXY(x, y); break; }
                                    case 7: { frm->BSSeries8->AddXY(x, y); break; }
                                }
                            }

                            int ddd = frm->DelayToChartValue((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Delay / BScanDelayMultiply);
                            if (ddd <= frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen())) {

                                #ifdef BScanSize_12Byte
                                int AmplIdx = 6;
                                if (AmplIdx&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] >> 4)]; else
                                               y = ThValList[((*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[AmplIdx / 2] & 0x0F)];
                                frm->BScanDebugSeries->AddXY(ddd, y, IntToStr(SignalIdx));
                                #endif

                                #ifndef BScanSize_12Byte
                                int m_ampl_ = (*DEV_BScan2Head_ptr->Items[ItemIdx].BScanDataPtr)[SignalIdx].Ampl[MaxAmpl];
                                // frm->BScanDebugSeries->AddXY(ddd, m_ampl_, IntToStr(SignalIdx));
                                frm->BScanDebugSeries->AddXY(ddd, m_ampl_, AmplId_to_DB[TableDB[m_ampl_] & 0x0F]);
                                #endif

                            }
                        }

                        #endif
                        #endif

                    };
                };


                if (frm->PageControl->ActivePageIndex == 1) {

                  for (size_t k = 0; k < DEV_BScan2Head_ptr->Items.size(); k++) {

                      int MaxAmp = - 1;
                      for (size_t j = 0; j < DEV_BScan2Head_ptr->Items[k].Signals.size(); j++)  // Просматриваем сигналы
                         if (DEV_BScan2Head_ptr->Items[k].Signals[j].Ampl[MaxAmpl] > MaxAmp) MaxAmp = DEV_BScan2Head_ptr->Items[k].Signals[j].Ampl[MaxAmpl];

                      int crd = DEV_BScan2Head_ptr->XSysCrd[2];
                      switch (CID_to_IDX(DEV_BScan2Head_ptr->Items[k].Channel)) {

                        case 1: { frm->AmpSeries_Pos_0->YValue[0] = MaxAmp; break; }
                        case 2: { frm->AmpSeries_Pos_P45->YValue[crd] = MaxAmp; break; }
                        case 3: { frm->AmpSeries_Pos_M45->YValue[crd] = MaxAmp; break; }

                      }


                      frm->AmpShape_0->X0 = crd;
                      frm->AmpShape_0->X1 = crd;
                      frm->AmpShape_P45->X0 = crd;
                      frm->AmpShape_P45->X1 = crd;
                      frm->AmpShape_M45->X0 = crd;
                      frm->AmpShape_M45->X1 = crd;

//                       HeadData[DEV_BScan2Head_ptr->XSysCrd[3]][DEV_BScan2Head_ptr->XSysCrd[2]][CID_to_IDX(DEV_BScan2Head_ptr->Items[k].Channel)] = MaxAmp

                  }

            }
            }
            if (Ptr_Item.Type == edPathStepData)
            {
                if (frm->PageControl->ActivePageIndex == 1) {

                     DEV_PathStepData_ptr = Ptr_Item.Ptr1.pPathStepData;

                      int crd = DEV_PathStepData_ptr->XSysCrd[1];

                      frm->AmpShape_0->X0 = crd;
                      frm->AmpShape_0->X1 = crd;
                      frm->AmpShape_P45->X0 = crd;
                      frm->AmpShape_P45->X1 = crd;
                      frm->AmpShape_M45->X0 = crd;
                      frm->AmpShape_M45->X1 = crd;


                  }
//                DEV_BScan2Head_ptr->Items
//                    DEV_BScan2Head_ptr->

                }

            Ptr_Item.free();
        } //end cycle
   //     if (BScanCrd != - 1)
        if (DEV_BScan2Head_ptr) {
            frm->bsl->Draw(0, 0, frm->BScanPB->Canvas);
        }


  ///////////////////////////////////////////////////////////////////////////////////////


        //--------- Указатель максимума для режима настройки --------
        tDEV_AScanMax tmp = AutoconMain->DEV->GetAScanMax();
        frm->PeakSeries->BeginUpdate();
        if ((AutoconMain->DEV->GetDeviceMode() == dmCalibration) && (tmp.Ampl >= AutoconMain->Config->getEvaluationGateLevel()))
        {
             frm->PeakSeries->Visible = true;
             frm->PeakSeries->X0 = frm->DelayToChartValue(tmp.Delay) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
             frm->PeakSeries->X1 = frm->DelayToChartValue(tmp.Delay) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
             frm->PeakSeries->Y0 = tmp.Ampl - 5;
             frm->PeakSeries->Y1 = tmp.Ampl + 6;
        }
        else frm->PeakSeries->Visible = false;
        frm->PeakSeries->EndUpdate();

        //-----------------------------------
    } //End if

    //------------Skip something?-------------------
    if (!frm->SkipFlag)
	{
/*		#ifndef BSCAN_ALL_IN_ONE
		if ((frm->PageControl->ActivePageIndex >= 3) && (frm->PageControl->ActivePageIndex <= 10))
		#endif
		#ifdef BSCAN_ALL_IN_ONE */
		if ((frm->PageControl->ActivePageIndex == 13) || (frm->PageControl->ActivePageIndex == 15))
//		#endif
		{
			#ifndef SCANDRAW
			//Added by KirillB
			frm->UpdatePBoxData(true);
			frm->DrawPBoxData();
			#endif
		}
	}
	frm->SkipFlag = !frm->SkipFlag;
    //---------End skip----------------------------

    //--------Deleting our pointers----------------

    while(!Ptr_List.empty())
    {
        Ptr_List.front().free();
        Ptr_List.pop_front();
    }

    //---------------------------------------------

//    frm->AlarmList->TopIndex = frm->ChList->TopIndex;
}

/*void __fastcall TDrawThread::ValueToScreen(void)
{
	TMainForm * frm  = MainForm;

	if (!frm->Visible) return;

    PtDEV_AScanMeasure DEV_AScanMeasure_ptr;
	PtDEV_AScanHead    DEV_AScanHead_ptr;
    PtDEV_BScan2Head   DEV_BScan2Head_ptr;
	PtDEV_AlarmHead    DEV_AlarmHead_ptr;
//	PtUMU_AlarmItem    DEV_AlarmItem_ptr;
	PtUMU_AScanData    AScanData_ptr;
	PtUMU_AScanData    TVGData_ptr;
	PtUMU_AScanMeasure AScanMeasure_ptr;
	PtUMU_BScanData    PBScanData_ptr;

    int StartIdx; // = AutoconMain->devt->Get_Idx();
	int EndIdx; // = AutoconMain->devt->Put_Idx();

    int AScanDataIdx = - 1;
    int TVGDataIdx = - 1;
    int AScanMeasIdx = - 1;
    int AIdx = 0;
    int AlarmDataIdx[128];
    int BIdx = 0;
    int BScan2DataIdx[128];
//  int MScan2DataIdx = - 1;

    StartIdx = AutoconMain->devt->Get_Idx();

    if (AutoconMain->devt->Test_Ptr_List_Get_Idx())
       while (true)
       {
            if (AutoconMain->devt->Get_Id_List() == edAScanData)
            {
                DEV_AScanHead_ptr = (PtDEV_AScanHead)AutoconMain->devt->Get_Ptr_List_1();
                if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel())) AScanDataIdx = AutoconMain->devt->Get_Idx();
            }
            else
            if (AutoconMain->devt->Get_Id_List() == edTVGData)
            {
                DEV_AScanHead_ptr = (PtDEV_AScanHead)AutoconMain->devt->Get_Ptr_List_1();
                if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel())) TVGDataIdx = AutoconMain->devt->Get_Idx();
            }
			else
            if (AutoconMain->devt->Get_Id_List() == edAScanMeas)
            {
                DEV_AScanMeasure_ptr = (PtDEV_AScanMeasure)AutoconMain->devt->Get_Ptr_List_1();
                frm->MaxPosSeries->Visible = false;
                if ((DEV_AScanMeasure_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AScanMeasure_ptr->Channel == AutoconMain->DEV->GetChannel())) AScanMeasIdx = AutoconMain->devt->Get_Idx();
            }

            else
            if (AutoconMain->devt->Get_Id_List() == edAlarmData)
            {
                if (AIdx < 128) AlarmDataIdx[AIdx] = AutoconMain->devt->Get_Idx();
                AIdx++;
            }

            else
            if (AutoconMain->devt->Get_Id_List() == edBScan2Data)
            {
                if (BIdx < 128) BScan2DataIdx[BIdx] = AutoconMain->devt->Get_Idx();
                BIdx++;
            }

           // else
           // if (AutoconMain->devt->Get_Id_List() == edMScan2Data) MScan2DataIdx = AutoconMain->devt->Get_Idx();

            if (!AutoconMain->devt->Inc_Ptr_List_Get_Idx()) break;
       }
       EndIdx = AutoconMain->devt->Get_Idx();

	if (frm->PageControl->ActivePageIndex == 1) // Оценка
	{

	   if (frm->CheckBox3->Checked) return;

	   frm->AScanSeries->BeginUpdate();
	   frm->TVGSeries->BeginUpdate();
	   frm->GateSeriesMain->BeginUpdate();
	   frm->GateSeriesLeft->BeginUpdate();
	   frm->GateSeriesRight->BeginUpdate();
	   frm->BScanGateMainSeries->BeginUpdate();
       frm->BScanGateLeftSeries->BeginUpdate();
	   frm->BScanGateRightSeries->BeginUpdate();
	   frm->PeakSeries->BeginUpdate();

        /// ------------------------------------------------------------------

        int idx;
            if (AScanDataIdx != - 1)
            {
                idx = AScanDataIdx;
                {

					DEV_AScanHead_ptr = (PtDEV_AScanHead)AutoconMain->devt->Ptr_List_1[idx];
					AScanData_ptr = (PtUMU_AScanData)AutoconMain->devt->Ptr_List_2[idx];

					if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
						(DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
					{

                        if (OldRullerMode != RullerMode)
                        {
                            switch(RullerMode)
                            {
                                case 0:
                                case 1: {
                                            frm->AScanChart->BottomAxis->SetMinMax(0, frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                            frm->BScanChart2->BottomAxis->SetMinMax(0, frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                                            break;
                                        }
                                case 2: {
                                            frm->AScanChart->BottomAxis->SetMinMax(0, 232);
                                            frm->BScanChart2->BottomAxis->SetMinMax(0, 232);
                                            break;
                                        }
                            }
                            OldRullerMode = RullerMode;
                        }

                        {
                            if ((frm->SaveMax1 != frm->AScanChart->BottomAxis->Maximum) || (frm->AScanSeries->XValues->Count == 0))
                            {
                                frm->SaveMax1 = frm->AScanChart->BottomAxis->Maximum;
                                frm->AScanSeries->Clear();
                                for (int i = 0; i < 232; i++)
                                    frm->AScanSeries->AddXY(frm->PixelToChartValue(i), frm->Filter(AScanData_ptr->Data[i]));
                            }
                            else
                                for (int i = 0; i < std::min(232, frm->AScanSeries->XValues->Count); i++)
                                    frm->AScanSeries->YValue[i] = frm->Filter(AScanData_ptr->Data[i]);
                        }

                        frm->GateSeriesMain->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()));
                        frm->GateSeriesMain->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()));

                        int GateLevet = AutoconMain->Config->EvaluationGateLevel;

                        frm->GateSeriesMain->Y0 = GateLevet - 1;
                        frm->GateSeriesMain->Y1 = GateLevet + 1;

                        frm->GateSeriesLeft->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()))
                            - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->GateSeriesLeft->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetStGate(AutoconMain->DEV->GetGateIndex()));
                        frm->GateSeriesLeft->Y0 = GateLevet - 5;
                        frm->GateSeriesLeft->Y1 = GateLevet + 5;

                        frm->GateSeriesRight->X0 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex()));
                        frm->GateSeriesRight->X1 = frm->DelayToChartValue(AutoconMain->DEV->GetEdGate(AutoconMain->DEV->GetGateIndex())) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->GateSeriesRight->Y0 = GateLevet - 5;
                        frm->GateSeriesRight->Y1 = GateLevet + 5;

                        // Строб Б-развертки
                        sChannelDescription ChanDesc;
                        AutoconMain->Table->ItemByCID(DEV_AScanHead_ptr->Channel, &ChanDesc);
                        frm->BScanGateMainSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                        frm->BScanGateMainSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd);

						GateLevet = AutoconMain->Config->BScanGateLevel;

                        frm->BScanGateMainSeries->Y0 = GateLevet - 1;
                        frm->BScanGateMainSeries->Y1 = GateLevet + 1;

                        frm->BScanGateLeftSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->BScanGateLeftSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gStart);
                        frm->BScanGateLeftSeries->Y0 = GateLevet - 5;
                        frm->BScanGateLeftSeries->Y1 = GateLevet + 5;

                        frm->BScanGateRightSeries->X0 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd);
                        frm->BScanGateRightSeries->X1 = frm->DelayToChartValue(ChanDesc.cdBScanGate.gEnd) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 128;
                        frm->BScanGateRightSeries->Y0 = GateLevet - 5;
                        frm->BScanGateRightSeries->Y1 = GateLevet + 5;
                    }
                }
            }

            if (TVGDataIdx != - 1)
            {
                idx = TVGDataIdx;
                DEV_AScanHead_ptr = (PtDEV_AScanHead)AutoconMain->devt->Ptr_List_1[idx];
				TVGData_ptr = (PtUMU_AScanData)AutoconMain->devt->Ptr_List_2[idx];

                if ((DEV_AScanHead_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AScanHead_ptr->Channel == AutoconMain->DEV->GetChannel()))
				{
                    if ((frm->SaveMax2 != frm->AScanChart->BottomAxis->Maximum) ||
                        (frm->TVGSeries->XValues->Count == 0))
                    {
                        frm->SaveMax2 = frm->AScanChart->BottomAxis->Maximum;
                        frm->TVGSeries->Clear();
                        for (int i = 0; i < 232; i++)
                            frm->TVGSeries->AddXY(frm->PixelToChartValue(i),
                                                  255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->GainBase) / (AutoconMain->Config->GainMax - AutoconMain->Config->GainBase) );
                    }
                    else
					{
                        for (int i = 0; i < std::min(232, frm->TVGSeries->XValues->Count); i++)
                            frm->TVGSeries->YValue[i] = 255 * 0.2 + (255 * 0.75) * (TVGData_ptr->Data[i] - AutoconMain->Config->GainBase) / (AutoconMain->Config->GainMax - AutoconMain->Config->GainBase);
                    }
				}
            }
            //else
            //if ((AutoconMain->devt->Id_List[idx] == edAScanMeas))
            if (AScanMeasIdx != - 1)
            {
                idx = AScanMeasIdx;
				DEV_AScanMeasure_ptr = (PtDEV_AScanMeasure)AutoconMain->devt->Ptr_List_1[idx];

                frm->MaxPosSeries->Visible = false;
                if ((DEV_AScanMeasure_ptr->Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AScanMeasure_ptr->Channel == AutoconMain->DEV->GetChannel()))
				{
                    if (DEV_AScanMeasure_ptr->ParamA >= AutoconMain->Config->EvaluationGateLevel)
                    {
                        frm->MaxPosSeries->Visible = true;
                        frm->MaxPosSeries->X0 = frm->DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                        frm->MaxPosSeries->X1 = frm->DelayToChartValue(DEV_AScanMeasure_ptr->ParamM) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
                        frm->MaxPosSeries->Y0 = -5;
                        frm->MaxPosSeries->Y1 = 6;
					}

                    // Таблица ------------------------------------------------------------------------------

                    if ((AutoconMain->DEV->GetCalibrationMode() == cmPrismDelay) && (AutoconMain->DEV->GetMode() == dmCalibration))
                             frm->HPanel->Caption = Format("R %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamR)));
                        else frm->HPanel->Caption = Format("H %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamH)));
					frm->LPanel->Caption = Format("L %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamL)));
                    frm->NPanel->Caption = Format("N %d", ARRAYOFCONST((DEV_AScanMeasure_ptr->ParamN)));
                    frm->ChannelTitlePanel->Caption = AutoconMain->DEV->WorkChannelDescription.Title;
                    if (!frm->DebugOut)
						frm->DebugPanel->Caption = Format("UMU: №-%d(1,2,3), Side-%d:%s(0,1), Line-%d:%s(0,1), Stroke-%d, Gen-%d, Res-%d, Длит.такта-%d мкс, Отображ. %d мкс; Device: CID-%x, GateIdx-%d(1,2), Группа-%d",
                                            ARRAYOFCONST((AutoconMain->DEV->CurChannelUMUIndex + 1,
                                                                      AutoconMain->DEV->CurChannelUMUSide,
                                                                      UMUSideToStr(AutoconMain->DEV->CurChannelUMUSide),
																			   AutoconMain->DEV->CurChannelUMULine,
                                                                               UMULineToStr(AutoconMain->DEV->CurChannelUMULine),
                                                                                        AutoconMain->DEV->CurChannelUMUStroke,
                                                                                                   AutoconMain->DEV->CurChannelUMUGen,
                                                                                                           AutoconMain->DEV->CurChannelUMURes,
                                                                                                                   AutoconMain->DEV->CurChannelUMUStrokeDuration,
                                                                                                                                                AutoconMain->DEV->CurChannelUMUScanDuration,
                                                                                                                                                                AutoconMain->DEV->GetChannel(), AutoconMain->DEV->GetGateIndex(),
                                                                                                                                                                        AutoconMain->DEV->GetChannelGroup())));

                    if (AutoconMain->DEV->GetMode() != dmCalibration)
                        frm->InfoPanel->Caption = Format ("АТТ: %d", ARRAYOFCONST((AutoconMain->DEV->GetGain())));
                        else frm->InfoPanel->Caption = Format ("Ку %d (%d)", ARRAYOFCONST((AutoconMain->DEV->GetSens(), AutoconMain->DEV->GetRecommendedSens() )));
                }
            }

         for (int idx_ = 0; idx_ < AIdx; idx_++)
         {
            idx = AlarmDataIdx[idx_];

            DEV_AlarmHead_ptr = (PtDEV_AlarmHead)AutoconMain->devt->Ptr_List_1[idx];

            for (unsigned int Idx = 0; Idx < DEV_AlarmHead_ptr->Items.size(); Idx++)
                if ((DEV_AlarmHead_ptr->Items[Idx].Side == AutoconMain->DEV->GetSide()) &&
                    (DEV_AlarmHead_ptr->Items[Idx].Channel == AutoconMain->DEV->GetChannel()))
                {
                        if (DEV_AlarmHead_ptr->Items[Idx].State[AutoconMain->DEV->GetGateIndex()])
                        {
                            frm->GateSeriesMain->Color = clRed;
                            frm->GateSeriesLeft->Color = clRed;
                            frm->GateSeriesRight->Color = clRed;
                        }
                        else
                        {
                            frm->GateSeriesMain->Color = clBlue;
                            frm->GateSeriesLeft->Color = clBlue;
                            frm->GateSeriesRight->Color = clBlue;
                        }
                }

		 }


         // B-развертка
         int Crd = - 1;
         for (int idx__ = 0; idx__ < BIdx; idx__++)
         {
            idx = BScan2DataIdx[idx__];
            DEV_BScan2Head_ptr = (PtDEV_BScan2Head)AutoconMain->devt->Ptr_List_1[idx];
            PBScanData_ptr = (PtUMU_BScanData)AutoconMain->devt->Ptr_List_2[idx];


            for (unsigned int idx_ = 0; idx_ < DEV_BScan2Head_ptr->Items.size(); idx_++)
            {
             //   if (!DEV_BScan2Head_ptr)
             //   {
             //       break;
             //   }
             //   else
              if ((DEV_BScan2Head_ptr->Items[idx_].Side == AutoconMain->DEV->GetSide()) &&
                  (DEV_BScan2Head_ptr->Items[idx_].Channel == AutoconMain->DEV->GetChannel()))

                   //    DgbCnt++;

               /*   for (int idx = 0; idx < DEV_BScan2Head_ptr->Items[idx_].Count; idx++)
                  {
                      int Ampl;
                      int MaxAmpl = - 1;
                      for (int idx2 = 0; idx2 < 24; idx2++)
                      {
                          #ifdef BScanSize_12Byte
                             if (idx2&1) Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] >> 4)]; else
                                         Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] & 0x0F)];
                         //    Ampl = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2])];
                          #endif

                          #ifndef BScanSize_12Byte
                             Ampl = (*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2];
                          #endif
                          MaxAmpl = std::max(MaxAmpl, Ampl);
                      }

                      int Wth = 2;
                      int Dly = (*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Delay;
                      Crd = DEV_BScan2Head_ptr->XSysCrd[0];
                      frm->SetPoint(Crd, Dly - Wth, Dly + Wth, MaxAmpl); // точка на В-развертке
                  } */
           // }


            /*
              for (unsigned int idx_ = 0; idx_ < DEV_BScan2Head_ptr->Items.size(); idx_++)
		      {
                  if ((DEV_BScan2Head_ptr->Items[idx_].Channel == AutoconMain->DEV->GetChannel()) &&
                      (DEV_BScan2Head_ptr->Items[idx_].Side == AutoconMain->DEV->GetSide()))
                  {
                      frm->BScanSeries->Clear();
                      float y;
                      for (int idx = 0; idx < DEV_BScan2Head_ptr->Items[idx_].Count; idx++)
                      {
                        if (frm->SelEchoIdx->ItemIndex == idx)
                          for (int idx2 = 0; idx2 < 24; idx2++)
                          {
                              #ifdef BScanSize_12Byte
                                 if (idx2&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] >> 4)]; else
                                             y = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] & 0x0F)];
                             // y = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2])];
                              #endif

                              #ifndef BScanSize_12Byte
                                 float y = (*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2];
                              #endif
                              frm->BScanSeries->AddXY(idx2, y);
                          }
                      }
                  }
              }
              * /

              frm->BScanSeries->Clear(); // Вывод одного сигнала В-развертки
              for (int idx_ = 0; idx_ < DEV_BScan2Head_ptr->Items.size(); idx_++)
		      {
                  if ((DEV_BScan2Head_ptr->Items[idx_].Side == AutoconMain->DEV->GetSide()) &&
                      (DEV_BScan2Head_ptr->Items[idx_].Channel == AutoconMain->DEV->GetChannel()))
                      for (int idx = 0; idx < DEV_BScan2Head_ptr->Items[idx_].Count; idx++)
                      {
                          frm->BScanSeries->AddXY(frm->DelayToChartValue((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Delay), (*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[11]);
                      }
              }

            /*
               // Б-развертка - на А-развертке
              for (unsigned int idx_ = 0; idx_ < DEV_BScan2Head_ptr->Items.size(); idx_++)
		      {
                  if ((DEV_BScan2Head_ptr->Items[idx_].Channel == AutoconMain->DEV->GetChannel())  )
                  {

                      frm->BSSeries1->Clear();
                      frm->BSSeries2->Clear();
                      frm->BSSeries3->Clear();
                      frm->BSSeries4->Clear();
                      frm->BSSeries5->Clear();
                      frm->BSSeries6->Clear();
                      frm->BSSeries7->Clear();
                      frm->BSSeries8->Clear();
                      float y;
                      for (int idx = 0; idx < DEV_BScan2Head_ptr->Items[idx_].Count; idx++)
                      {

                          for (int idx2 = 0; idx2 < 24; idx2++)
                          {
                              #ifdef BScanSize_12Byte
                                 if (idx2&1) y = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] >> 4)]; else
                                             y = ThValList[((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2 / 2] & 0x0F)];
//                              y = ThValList[(*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2]];
                              #endif

                              #ifndef BScanSize_12Byte
                              float y = (*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Ampl[idx2];
                              #endif

                              float x = (float)frm->DelayToChartValue((*DEV_BScan2Head_ptr->Items[idx_].BScanDataPtr)[idx].Delay + 0.333 * (float)idx2 - (float)2);

                              switch (idx)
                              {
                                case 0: { frm->BSSeries1->AddXY(x, y); break; }
                                case 1: { frm->BSSeries2->AddXY(x, y); break; }
                                case 2: { frm->BSSeries3->AddXY(x, y); break; }
                                case 3: { frm->BSSeries4->AddXY(x, y); break; }
                                case 4: { frm->BSSeries5->AddXY(x, y); break; }
                                case 5: { frm->BSSeries6->AddXY(x, y); break; }
                                case 6: { frm->BSSeries7->AddXY(x, y); break; }
                                case 7: { frm->BSSeries8->AddXY(x, y); break; }
                              }
                          }
                      }
                  }
              }   * /
		}
        if (Crd != - 1) frm->SetViewCrdZone(Crd - 1500, Crd);

        tDEV_AScanMax tmp = AutoconMain->DEV->GetAScanMax();
        if ((AutoconMain->DEV->GetDeviceMode() == dmCalibration) && (tmp.Ampl >= AutoconMain->Config->EvaluationGateLevel))
        {
             frm->PeakSeries->Visible = true;
			 frm->PeakSeries->X0 = frm->DelayToChartValue(tmp.Delay) - frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
             frm->PeakSeries->X1 = frm->DelayToChartValue(tmp.Delay) + frm->DelayToChartValue(AutoconMain->DEV->GetAScanLen()) / 25.6 / 2;
             frm->PeakSeries->Y0 = tmp.Ampl - 5;
             frm->PeakSeries->Y1 = tmp.Ampl + 6;
        }
        else frm->PeakSeries->Visible = false;

        frm->AScanSeries->EndUpdate();
		frm->TVGSeries->EndUpdate();
        frm->GateSeriesMain->EndUpdate();
        frm->GateSeriesLeft->EndUpdate();
        frm->GateSeriesRight->EndUpdate();
		frm->BScanGateMainSeries->EndUpdate();
        frm->BScanGateLeftSeries->EndUpdate();
        frm->BScanGateRightSeries->EndUpdate();
        frm->PeakSeries->EndUpdate();

	}
	if (!frm->SkipFlag)
	{

		if ((frm->PageControl->ActivePageIndex == 13) || (frm->PageControl->ActivePageIndex == 15))
//		#endif
		{
			#ifndef SCANDRAW
			//Added by KirillB
			frm->UpdatePBoxData(true);
			frm->DrawPBoxData();
			#endif
		}
	}
	frm->SkipFlag = !frm->SkipFlag;

    if (StartIdx != EndIdx) // Удаление обработанных данных
    {
        int idx = StartIdx;
        while (true)
        {
            if(AutoconMain->devt->Ptr_List_1[idx])
            {
               delete AutoconMain->devt->Ptr_List_1[idx];
			   AutoconMain->devt->Ptr_List_1[idx] = NULL;
            }
            if(AutoconMain->devt->Ptr_List_2[idx])
            {
               delete AutoconMain->devt->Ptr_List_2[idx];
               AutoconMain->devt->Ptr_List_2[idx] = NULL;
            }

            AutoconMain->devt->Id_List[idx] = (EventDataType)0;

            idx++;
            if (idx == DEVTHREAD_LIST_SIZE) idx = 0;
            if (idx == EndIdx) break;
        }
    }
} */

// ---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
//	EnterModePanel = NULL;
//  EnterMode = false;
	DisCoord = 0;

	//Added by KirillB
	bLeftToRightBScanLayout=true;
	//memset(BScanLines,0,sizeof(cBScanLines*)*9);

	MMode_BScanGroupIdx = 3;
	m_MMode_LineSeries = new TLineSeries(this);
	m_MMode_BarSeries = new TBarSeries(this);
//	m_MMode_CustomChart->AddSeries(m_MMode_LineSeries);
//	m_MMode_CustomChart->AddSeries(m_MMode_BarSeries);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::FormShow(TObject *Sender)
{
    int MinGroupIdx;
    int MaxGroupIdx;

    AutoconMain->Config->GetChannelGroupCount(&MinGroupIdx, &MaxGroupIdx);
    GroupList->Items->Clear();
    for (int i = MinGroupIdx; i <= MaxGroupIdx; i++) {

       #ifndef FULLVERSION
       Label29->Caption = "Схема прозвучивания";
       if (i >= 3) break;
       if (AutoconMain->Config->GetScanChannelsCount_() != 0)
            GroupList->Items->AddObject("Схема №" + IntToStr(i), (TObject*)i);
       #endif

       #ifdef FULLVERSION
       if (AutoconMain->Config->GetScanChannelsCount_() != 0)
            GroupList->Items->AddObject("Группа №" + IntToStr(i), (TObject*)i);
       #endif
    }
    SetChannelList();
    GroupList->ItemIndex = 0;
//    Sleep(1200);
	GroupListClick(Sender);

	PageControl2->ActivePageIndex = ACConfig->ActivePage;
//    cbSwitchingChange(NULL);
    UMUEventTimer->Enabled = true;
}
// ---------------------------------------------------------------------------

void TMainForm::SetChannelList(void)
{

    sChannelDescription ChanDesc;
    sScanChannelDescription ScanChDesc;

    ChList2->Clear();
    ChList->Clear();
    CID id;
    CID saveid;
/*  int ChGroup = (int)GroupList->Items->Objects[GroupList->ItemIndex];
    AutoconMain->Config->SetChannelGroup(ChGroup);
    AutoconMain->DEV->SetChannelGroup()
    AutoconMain->DEV->SetChannelGroup(StrToInt(ComboBox1->Items->Strings[ComboBox1->ItemIndex]));
    AutoconMain->DEV->Update(true);
*/

    int chidx;
    UnicodeString str;

    ChList->Items->AddObject(" --- Сплошные --- ", (TObject*)123456);
    ChList2->Items->Add("");
//    AlarmList->Items->AddObject("", (TObject*)1000);


    eDeviceSide Side;
    eDeviceSide StartSide;
    eDeviceSide EndSide;

    if (AutoconMain->Config->getControlledRail() == crSingle) {
        StartSide = dsNone;
        EndSide = dsNone;
    } else {
        StartSide = dsLeft;
        EndSide = dsRight;
    };

    for (int ds = StartSide; ds <= EndSide; ds++)
        for (int i = 0; i < AutoconMain->Config->GetScanChannelsCount_(); i++) {

            chidx = AutoconMain->Config->GetScanChannelIndex_(i);
            id = AutoconMain->Config->GetScanChannel(chidx);

            AutoconMain->Config->getSChannelbyIdx(chidx, &ScanChDesc);


            if (ScanChDesc.DeviceSide == (eDeviceSide)ds) {

                AutoconMain->Table->ItemByCID(id, &ChanDesc);
                                                 // Сплошной
                if (ScanChDesc.DeviceSide == dsNone) { str = ""; saveid = id + 10000; }
                    else if (ScanChDesc.DeviceSide == dsLeft) { str = "Левая: "; saveid = id + 1; }
                      else { str = "Правая: "; saveid = -(id + 1); }

                if (cbShowCID->Checked)
                    ChList->Items->AddObject(Format("[- -] [CID: %2x] ", ARRAYOFCONST((abs(id)))) + str + ChanDesc.Title.c_str(), (TObject*)saveid);
                    else ChList->Items->AddObject(Format("[- -] ", ARRAYOFCONST((abs(id)))) + str + ChanDesc.Title.c_str(), (TObject*)saveid);
                ChList2->Items->Add("");

//                AlarmList->Items->AddObject("", (TObject*)saveid);
//                AlarmList->so
            }

            //AutoconMain->Calibration->GetState(dsLeft, F70E, 0);
            //AutoconMain->Calibration->GetTVG(dsLeft, F70E);

        }


    ChList->Items->AddObject(" --- Ручные --- ", (TObject*)123456);
    ChList2->Items->Add("");
//    AlarmList->Items->AddObject("", (TObject*)1000);

    sHandChannelDescription val;
    for (int i = 0; i < AutoconMain->Config->GetHandChannelsCount(); i++) {

        AutoconMain->Config->getHChannelbyIdx(i, &val);
        id =  val.Id;
        AutoconMain->Table->ItemByCID(id, &ChanDesc);
        saveid = id + 1;              // Ручной:
        if (cbShowCID->Checked) ChList->Items->AddObject(Format("[CID: %2x] ", ARRAYOFCONST((abs(id)))) + ChanDesc.Title.c_str(), (TObject*)saveid);
                                else ChList->Items->AddObject(Format("", ARRAYOFCONST((abs(id)))) + ChanDesc.Title.c_str(), (TObject*)saveid);
//        AlarmList->Items->AddObject("", (TObject*)saveid);
       ChList2->Items->Add("");
    }

//    ChList2->SelCount;

	if (AutoconMain->Config->Tag == -1) AutoconMain->Config->Tag == 1;

	ChList->ItemIndex = AutoconMain->Config->Tag;
	ChList->OnClick(this);

//    AlarmList->TopIndex = ChList->TopIndex;
}

void __fastcall TMainForm::InitializeLog(void)
{
	LANProtDebug::onAddLog = &AddLanLog;
	CANProtDebug::onAddLog = &AddCANLog;
}

void __fastcall TMainForm::Initialize(void)
{
	LANProtDebug::onAddLog = &AddLanLog;
	CANProtDebug::onAddLog = &AddCANLog;

	PageControl->ActivePageIndex = 6;
    PageControlChange(NULL);

	StartWork = true;
	EndWork = false;
/*
    AutoconMain->ac->SetMessagesProc(GetMessages);
    AutoconMain->ac->SetSearchMotion(SearchMotion_); // Режим "Поиск"
    AutoconMain->ac->SetTestMotion(TestMotion_);   // Режим "Тест"
	AutoconMain->ac->SetAdjustMotion(AdjustMotion_); // Режим "Юстировка"
	AutoconMain->ac->SetTuneMotion(TuneMotion_);   // Режим "Настройка"
	AutoconMain->ac->SetManualMotion(ManualMotion_);   // Режим "Ручной"
  */
	dt = new TDrawThread(AutoconMain->CS);
	dt->params = params;

//	Button_1->Enabled = false;
//	Button_2->Enabled = true;
//	Button_3->Enabled = true;
//	Button_4->Enabled = true;
//	Button_5->Enabled = true;

	ValueToControl();

//	AScanTh->Enabled = false;
//	BScanTh->Enabled = false;

//	BScanPBox->Width = AllChBScanTab->Width;
//	BScanPBox->Height = AllChBScanTab->Height;

//	ComboBox1->Items->Clear();
//	for (int i = 0; i < AutoconMain->Config->GetChannelGroupCount(); i++)
//	{
//		ComboBox1->Items->Add(IntToStr(AutoconMain->Config->GetChannelGroup(i)));
//	}
}

void TMainForm::CalculateBScanLayout(int BScanWidth,int BScanHeight, TRect* Rects, int* pRailWidth,int* pRailHeight)
{
	int W = BScanWidth;
	int H = BScanHeight;
	int S = 100;

	int midRegStart = H / 4;
	int midRegEnd = 3 * H / 4;
	int midRegHeight = midRegEnd - midRegStart;
	int midRegDelta = midRegHeight / 3;
	int midRegCorrection = midRegDelta / 3;

	Rects[1].Left = W / 2 - W / 4;
	Rects[1].Right = W / 2 + W / 4;
	Rects[1].Top = 0;
	//Modifyed by KirillB
	Rects[1].Bottom = H / 4;

	Rects[2].Left = W / 2 - W / 2;
	Rects[2].Right = W / 2 - S;
	//Modifyed by KirillB
	Rects[2].Top = midRegStart;
	Rects[2].Bottom = midRegStart + midRegDelta + midRegCorrection;

	Rects[3].Left = S + W / 2;
	Rects[3].Right = W / 2 + W / 2;
	//Modifyed by KirillB
	Rects[3].Top = midRegStart;
	Rects[3].Bottom = midRegStart + midRegDelta + midRegCorrection;

	Rects[4].Left = W / 2 - W / 2;
	Rects[4].Right = W / 2 - S;
	Rects[4].Top = midRegStart + midRegDelta;
	Rects[4].Bottom = midRegStart + midRegDelta*2 + midRegCorrection;

	Rects[5].Left = S + W / 2;
	Rects[5].Right = W / 2 + W / 2;
	Rects[5].Top = midRegStart + midRegDelta;
	Rects[5].Bottom = midRegStart + midRegDelta*2 + midRegCorrection;

	Rects[6].Left = W / 2 - W / 2;
	Rects[6].Right = W / 2 - S;
	Rects[6].Top = midRegStart + midRegDelta*2;
	Rects[6].Bottom = midRegStart + midRegDelta*3 + midRegCorrection;

	Rects[7].Left = S + W / 2;
	Rects[7].Right = W / 2 + W / 2;
	Rects[7].Top = midRegStart + midRegDelta*2;
	Rects[7].Bottom = midRegStart + midRegDelta*3 + midRegCorrection;

	Rects[8].Left = W / 2 - W / 4;
	Rects[8].Right = W / 2 + W / 4;
	//Modifyed by KirillB
	Rects[8].Top = 3 * H / 4;
	Rects[8].Bottom = H;

	*pRailWidth = S*2;
	*pRailHeight = S*3.2;
}
/*
void TMainForm::GetMessages(TMessagesType Type, UnicodeString Text, DWord Time) // Передача сообщения от класса
{
	//ScreenMessageForm->Add(Type, Text, Time);
	//ScreenMessageForm2->Add(Type, Text, Time,this);
	//this->SetEnabled(false);
}

void TMainForm::SearchMotion_(int WorkCycle) // Режим "Поиск"
{
	Search_or_Test(WorkCycle);
//		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TMainForm::TestMotion_(int WorkCycle)   // Режим "Тест"
{
	Search_or_Test(WorkCycle);
//		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TMainForm::AdjustMotion_(int WorkCycle) // Режим "Юстировка"
{

//		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TMainForm::TuneMotion_(int WorkCycle) // Режим "Настройка"
{
	//Added by KirillB
	TuneProc(WorkCycle);
//		Label2->Caption = "WorkCycle: " + IntToStr(ac->GetWorkCycle())  + " Processing: " + IntToStr((int)ac->Processing);
}

void TMainForm::ManualMotion_(int WorkCycle) // Режим "Ручной"
{
}
*/
// -------------------------------------------------------------------

int cnt = 0;
UnicodeString tmp;
UnicodeString tmp_Left;
UnicodeString tmp_Right;
UnicodeString tmp1;
/*
void __fastcall TMainForm::AddLog(int UMUIdx, unsigned char * msg, int way)
{
	if (EndWork) return;
	if (!LogForm) return;


    //if (!( ((SpeedButton2->Down) && (UMUIdx == 0)) ||
    //     ((SpeedButton5->Down) && (UMUIdx == 1)) ||
    //     ((SpeedButton6->Down) && (UMUIdx == 2)))) return;
	//
//     if (UMUIdx != 1) return;


	if (!LogForm->LogState) return;

	if (!LogForm->LogState->Down) return;

    if (way == 3)
    {
		LogForm->Memo1->Lines->Add("Ошибка приема данных");
        return;
    };

	if (LogForm->CheckBoxOneMessage->Checked) LogForm->Memo1->Lines->Clear();

    if (!msg) return;


    // -----------------------------------------
    // if ((*msg) == 0x7F)
    // {
    // Memo1->Lines->Add(Format("%d",  ARRAYOFCONST((GetTickCount(), tmp))));
    // }


    // -----------------------------------------

	if (((way == 1) && (!LogForm->LogIn->Down)) ||
		((way == 2) && (!LogForm->LogOut->Down)) ||
		(((*msg) == 0x3D) && (!LogForm->LogPathEncoder->Down)) ||
		(((*msg) == 0x82) && (!LogForm->LogAScanMeasure->Down)) ||
		(((*msg) == 0x7F) && (!LogForm->LogAScan->Down)) ||
		(((*msg) == 0x70) && (!LogForm->LogBScan->Down)) ||
		(((*msg) == 0x72) && (!LogForm->LogBScan2->Down)) ||
//        (((*msg) == 0x71) && (!LogMScan->Down)) ||
		(((*msg) == 0x83) && (!LogForm->LogASD->Down)) ||
		(((*msg) == 0xDF) && (!LogForm->LogFWver->Down)) ||
		(((*msg) == 0xDB) && (!LogForm->LogSerialNumber->Down))) return;

    switch(way)
    {

        case 1: {
                    tmp1 = "UMU " + IntToStr(UMUIdx) + " Recived";
                    break;
                };
		case 2: {
                    tmp1 = "UMU " + IntToStr(UMUIdx) + " Sended";
                    break;
                };
        case 3: {
					tmp = "ERROR";
					LogForm->Memo1->Lines->Add(tmp);
                    return;
                };
    }


	LogForm->Memo1->Lines->Add(" [" + GetUMUMessageName((msg[0])) + "]");
    tmp = "";
    for (int i = 0; i < 6; i++)
    {
		tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
	}
	LogForm->Memo1->Lines->Add(tmp1 + " HEAD:" + tmp);

	if  (!LogForm->LogNoBody->Down)
	{
		tmp = "";
		for (int i = 0; i < (msg[2] | (msg[3] << 8)); i++)
		{
            tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[6 + i])));
		}

		LogForm->Memo1->Lines->Add(tmp1 + " BODY:" + tmp);

        if ((*msg) == 0x83)
        {
            tmp = "Расшифровка: ";
		    LogForm->Memo1->Lines->Add(tmp);
            for (int i = 0; i < ((tLAN_Message*)msg)->Size / 2; i++)
            {
                tmp = "Такт: " + IntToStr(i);
		        LogForm->Memo1->Lines->Add(tmp);
                tmp = "Сторона: 0; Линия: 0;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[i + 0] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x08) != 0))));
		        LogForm->Memo1->Lines->Add(tmp);
                tmp = "Сторона: 0; Линия: 1;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[i + 0] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 0] & 0x80) != 0))));
		        LogForm->Memo1->Lines->Add(tmp);
                tmp = "Сторона: 1; Линия: 0;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[i + 1] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x08) != 0))));
		        LogForm->Memo1->Lines->Add(tmp);
                tmp = "Сторона: 1; Линия: 1;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[i + 1] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[i + 1] & 0x80) != 0))));
		        LogForm->Memo1->Lines->Add(tmp);
            }
        }

    }
}
*/

void __fastcall TMainForm::AddLanLog(int UMUIdx, unsigned char * msg, int way)
{


	if (EndWork) return;
	if (!LogForm) return;
	if (!LogForm->LogState) return;
	if (!LogForm->LogState->Down) return;

    AUTO_LOCK_WCS(*(AutoconMain->CS));


 /* if (way == 4)
    {
        tmp = "UMU " + IntToStr(UMUIdx) + " Recived";
        for (int i = 0; i < 6; i++)
        {
            tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
        }
        LogLines->Add(tmp);
        return;
    }; */

    if (way == 3)
    {
        LogLines->Add("Ошибка приема данных");
        for (int i = 0; i < 6; i++)
        {
            tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
        }
        return;
    };

	if (!msg) return;

     /* && (*msg) != 0x3D) && (*msg) != 0x3D)*/
	if (!LogForm->cbShowAll->Checked) {

		if (((way == 1) && (!LogForm->LogIn->Down)) ||
			((way == 2) && (!LogForm->LogOut->Down) && (!LogForm->LogOut2->Down)) ||
			(((*msg) == 0x3D) && (!LogForm->LogPathEncoder->Down)) ||
			(((*msg) == 0x3E) && (!LogForm->LogPathEncoder->Down)) ||
			(((*msg) == 0x80) && (!LogForm->LogAC_Summ->Down)) ||
			(((*msg) == 0x86) && (!LogForm->LogAC_->Down)) ||
			(((*msg) == 0x82) && (!LogForm->LogAScanMeasure->Down)) ||
			(((*msg) == 0x7F) && (!LogForm->LogAScan->Down)) ||
			(((*msg) == 0x70) && (!LogForm->LogBScan->Down)) ||
			(((*msg) == 0x72) && (!LogForm->LogBScan2->Down)) ||
			(((*msg) == dDeviceSpeed) && (!LogForm->LogSpeed->Down)) ||
	//        (((*msg) == 0x71) && (!LogMScan->Down)) ||
			(((*msg) == 0x83) && (!LogForm->LogASD->Down)) ||
			(((*msg) == 0xDF) && (!LogForm->LogFWver->Down)) ||

			(((*msg) == 0xDF) && (!LogForm->LogMetalSensor->Down)) ||

			(((*msg) == 0xDB) && (!LogForm->LogSerialNumber->Down)) ||
			(((*msg) == uEnable) && (((tLAN_Message*)msg)->Data[0] == 0xDE ) && (!LogForm->UMUManage->Down))) return;
	}

	if ((way == 2) && (LogForm->LogOut2->Down) && ((*msg) != uEnable) && ((*msg) != dPathEncoderSetup) && ((*msg) != uStrokeCount)) return;

	if (((*msg) == 0x3D) && (LogForm->PEMode->Checked) && (((tLAN_Message*)msg)->Data[1] == 0)) return;

    int fTakt;
    int fLine;
    int fSide;

    TryStrToInt(LogForm->TaktEdit->Text, fTakt);
    TryStrToInt(LogForm->LineEdit->Text, fLine);
    TryStrToInt(LogForm->SideEdit->Text, fSide);

    if ((LogForm->FilterCheckBox->Checked) &&
		(((*msg) == 0x82) || ((*msg) == 0x7F) || ((*msg) == 0x70) || ((*msg) == 0x72) || ((*msg) == 0x83))) {

//            LAN_message

            int cStroke = ((tLAN_Message*)msg)->Data[0] & 0x3F;
            int cSide;
            switch (((tLAN_Message*)msg)->Data[0] & 0x80)
            {
                case 0x00: { cSide = usRight; break; }
                case 0x80: { cSide = usLeft; break; }
            }
            int cLine = (((tLAN_Message*)msg)->Data[0] & 0x40) != 0;

            if ((cStroke != fTakt) || (cLine != fLine) || (cSide != fSide)) return;
        }


	if (LogForm->CheckBoxOneMessage->Checked) LogLines->Clear();


    switch(way)
    {

        case 1: {
                    tmp1 = "CDU < UMU " + IntToStr(UMUIdx) + " "; // Recived
                    break;
                };
		case 2: {
                    tmp1 = "CDU > UMU " + IntToStr(UMUIdx) + " "; // Sended
                    break;
                };
        case 3: {
					tmp = "ERROR";
					LogLines->Add(tmp);
                    return;
                };
    }

    if (LogForm->cbTime->Checked) tmp = Format("%3.1f", ARRAYOFCONST(((float)GetTickCount() / (float)1000))); else tmp = "";

    switch(way)
    {
        case 1: { //
                    LogLines->Add(tmp + " [" + GetUMUMessageName(msg[0], ((tLAN_Message*)msg)->Data[0]) + "]");
                    break;
                };
		case 2: {
                    LogLines->Add(tmp + " [" + GetCDUMessageName(msg[0], ((tLAN_Message*)msg)->Data[0]) + "]");
                    break;
                };
        case 3: {
                    return;
                };
    }

    tmp = "";
    for (int i = 0; i < 6; i++)
    {
		tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
	}
	LogLines->Add(tmp1 + " HEAD:" + tmp);

	if  (!LogForm->LogNoBody->Down)
	{
		tmp = "";

		for (int i = 0; i < (msg[2] | (msg[3] << 8)); i++)
		{
            tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[6 + i])));
		}

		LogLines->Add(tmp1 + " BODY:" + tmp);

        if (((*msg) == 0x83) && (LogForm->Decoding->Checked))
        {
            tmp = "Расшифровка: ";
		    LogLines->Add(tmp);
			int idx = 0;
            for (int i = 0; i < ((tLAN_Message*)msg)->Size / 2; i++)
            {
                tmp = "Такт: " + IntToStr(i);
		        LogLines->Add(tmp);
                tmp = "Сторона: 0; Линия: 0;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x08) != 0))));
		        LogLines->Add(tmp);
                tmp = "Сторона: 0; Линия: 1;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x80) != 0))));
                idx++;

		        LogLines->Add(tmp);
                tmp = "Сторона: 1; Линия: 0;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x01) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x02) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x04) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x08) != 0))));
		        LogLines->Add(tmp);
                tmp = "Сторона: 1; Линия: 1;";
                tmp = tmp + Format("Строб 0,1,2,3 - %d,%d,%d,%d ",
                                    ARRAYOFCONST(((int)((((tLAN_Message*)msg)->Data[idx] & 0x10) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x20) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x40) != 0),
                                                  (int)((((tLAN_Message*)msg)->Data[idx] & 0x80) != 0))));
                idx++;
		        LogLines->Add(tmp);
            }
        }

        if (((*msg) == dBScan2Data) && (LogForm->Decoding->Checked))
        {
            tmp = "Расшифровка: ";
		    LogLines->Add(tmp);

            int Idx3 = 0;
            int Stroke_ = ((tLAN_Message*)msg)->Data[Idx3] & 0x3F;
            int Line_ = ((((tLAN_Message*)msg)->Data[Idx3] & 0x40) != 0);
            Idx3++;
            int CountRight = (((tLAN_Message*)msg)->Data[Idx3]) & 0x0F;
            int CountLeft = (((tLAN_Message*)msg)->Data[Idx3] >> 4) & 0x0F;
            Idx3++;
            tmp = "Линия: " + IntToStr(Line_) + "; Такт: " + IntToStr(Stroke_) + "; Количество сигналов - Левая: " + IntToStr(CountLeft) + "; Правая: " + IntToStr(CountRight);
            LogLines->Add(tmp);
            for (int Idx = 0; Idx < std::max(         (((tLAN_Message*)msg)->Data[1]) & 0x0F, (((tLAN_Message*)msg)->Data[1] >> 4) & 0x0F           ); Idx++)
            {
                int RightDelay = ((tLAN_Message*)msg)->Data[Idx3];
                Idx3++;
                int LeftDelay = ((tLAN_Message*)msg)->Data[Idx3];
                Idx3++;

                tmp = "Индекс: " + IntToStr(Idx);
                tmp_Left = "Левая сторона - Задержка: " + IntToStr(LeftDelay) + " / " + FloatToStr(LeftDelay/3) + ", Амплитуды: ";
                tmp_Right = "Правая сторона - Задержка: " + IntToStr(RightDelay) + " / " + FloatToStr(RightDelay/3) + ", Амплитуды: ";

                #if defined(BScanSize_12Byte) && !defined(RestoreBScanDataUpTo24Byte)
                for (int Idx2 = 0; Idx2 < 12; Idx2++)
                {
                    int RightAmpl = ((tLAN_Message*)msg)->Data[Idx3];
                    Idx3++;
                    int LeftAmpl = ((tLAN_Message*)msg)->Data[Idx3];
                    Idx3++;
                    tmp_Left = tmp_Left + IntToStr(LeftAmpl) + ", ";
                    tmp_Right = tmp_Right + IntToStr(RightAmpl) + ", ";
                }
                #endif
                #ifndef BScanSize_12Byte
				for (int Idx2 = 0; Idx2 < 24; Idx2++)
                {
                    int RightAmpl = ((tLAN_Message*)msg)->Data[Idx3];
                    Idx3++;
                    int LeftAmpl = ((tLAN_Message*)msg)->Data[Idx3];
                    Idx3++;
                    tmp_Left = tmp_Left + " " + IntToStr(LeftAmpl);
                    tmp_Right = tmp_Right + " " + IntToStr(RightAmpl);
                }
                #endif
                LogLines->Add(tmp);
                LogLines->Add(tmp_Left);
                LogLines->Add(tmp_Right);
            }
        }
    }
}

void __fastcall TMainForm::AddCANLog(int UMUIdx, unsigned char * msg, int way)
{
	if (EndWork) return;
	if (!LogForm) return;
	if (!LogForm->LogState) return;
	if (!LogForm->LogState->Down) return;

	if (way == 3)
	{
		LogLines->Add("Ошибка приема данных");
		for (int i = 0; i < 6; i++)
		{
			tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i])));
		}
		return;
	};

	if (!msg) return;

	if (!LogForm->cbShowAll->Checked) {

		if (((way == 1) && (!LogForm->LogIn->Down)) ||
			((way == 2) && (!LogForm->LogOut->Down)) ||

			((way == 1) &&  ((((*msg) == 0x3F) || (*msg) == 0x3D))                      && (!LogForm->LogPathEncoder->Down)  ) ||
			((way == 1) &&  (((*msg) == 0x82) || ((*msg) == 0x84))                      && (!LogForm->LogAScanMeasure->Down) ) ||
			((way == 1) &&  (((*msg) == 0x7F) || ( (*msg) == 0x80) || ((*msg) == 0x81)) && (!LogForm->LogAScan->Down)        ) ||
			((way == 1) &&  ((*msg) == 0x70) && (!LogForm->LogBScan->Down)  ) ||
			((way == 1) &&  ((*msg) == 0x71) && (!LogForm->LogBScan2->Down) ) ||
			((way == 1) &&  ((*msg) == 0x83) && (!LogForm->LogASD->Down)    ) ||
			((way == 1) &&  ((*msg) == 0xDF) && (!LogForm->LogFWver->Down)  ) ||
	//		(((*msg) == 0xDF) && (!LogForm->LogMetalSensor->Down)) ||
			((way == 1) &&  ((*msg) == can_utils::acData) && (!LogForm->LogAC_->Down)    ) ||
			((way == 1) &&  ((*msg) == can_utils::acSumm) && (!LogForm->LogAC_Summ->Down)) ||
			((way == 1) &&  ((*msg) == 0xDB) && (!LogForm->LogSerialNumber->Down)) ||
			((way == 1) &&  ((*msg) == 0xDE) && (!LogForm->UMUManage->Down))  ) return;
	}

	if (LogForm->CheckBoxOneMessage->Checked) LogLines->Clear();


	switch(way)
	{

		case 1: {
					tmp1 = "UMU " + IntToStr(UMUIdx) + " Recived ";
					break;
				};
		case 2: {
					tmp1 = "UMU " + IntToStr(UMUIdx) + " Sended ";
					break;
				};
		case 3: {
					tmp = "ERROR ";
					LogLines->Add(tmp);
					return;
				};
	}

	if (LogForm->cbTime->Checked) tmp = IntToStr((__int64)GetTickCount()); else tmp = "";

//	LogLines->Add(tmp + " [" + GetUMUMessageName(msg[0], ((tLAN_Message*)msg)->Data[0]) + "]");
	tmp = "";
	tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[0])));
	tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[1])));
	for (int i = 0; i < (msg[1] & 0x0F); i++)
	{
		tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[i+2])));
	}
	LogLines->Add(tmp1 + tmp);

/*
	if  (!LogForm->LogNoBody->Down)
	{
		tmp = "";

		for (int i = 0; i < (msg[2] | (msg[3] << 8)); i++)
		{
			tmp = tmp + Format("%2x ", ARRAYOFCONST((msg[6 + i])));
		}

		LogLines->Add(tmp1 + " BODY:" + tmp);

	}
*/
}

// ---------------------------------------------------------------------------

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
//   LogCrd_->SaveToFile("crd.log");
   LogCrd_->SaveToFile("noise.log");

   ACConfig->ActivePage = PageControl2->ActivePageIndex;

  /*	if ((AutoconMain) && (AutoconMain->CtrlWork) && (AutoconMain->QryThreadFlag))
	{
		hThreadNeedEndFlag = true;
		while (!hThreadEndFlag) { Application->ProcessMessages(); }
	}     */

	EndWork = true;
	if (dt)
	{
		//dt->EndWorkFlag = true;
		//while (!dt->Finished) { Application->ProcessMessages(); };
		delete dt;
		dt = NULL;
    }

 //   TextLog->SaveToFile("log.txt");
    delete LogLines;
}


void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
//    ClearChartData();
//    delete BScanChartList;
    delete bsl;
    delete msSaveAscan;
    delete LogLines_;
    delete LogCrd_;
}

void __fastcall TMainForm::FormCreate(TObject *Sender)
{

    BSDebugBuffer = NULL;
    ACBuffer = NULL;
    BSBuffer = NULL;


    PathStepGis_LastVal = - 1;
    NeedUpdate = true;
    for (int i = 0; i < 1000; i++) {
        ACMinMas[i] = 66000;
        ACMaxMas[i] = 0;
    }

    StartACGate = 25;
    MainPathEncoderIndex = 0;
    MainPathEncoderType = 0;

    #ifdef UMUDataDebug
    BScanDebugPanel->Visible = true;
    BScanChart2->Visible = true;
    AmplDebugPanel->Visible = true;
//    ControlPanel->Visible = true;
//    BScanDataPanel->Height = BScanDataPanel->Height + 250;
    BScanDebugSeries->Visible = true;
    #endif
    BScanPanel->Top = 0;
//    HandChannelPanel->Top = this->Height + 100;

    bsl = new TBScanLine();

	ChangeModeFlag = false;
    //ViewMode = false;

	StartWork = false;
 //	hThreadEndFlag = false;
 //	hThreadNeedEndFlag = false;
	onLogoutChange(Sender);

//	EditMode = false;
	EditPanel = NULL;
	PeakSeries->Visible = false;

	Series1->Visible = false;
	Series2->Visible = false;


	LogLines = new (TStringList);

    this->Width = Screen->Width;
    this->Height = Screen->Height;
    this->Update();
    this->Refresh();

    #ifndef DEBUG
    DebugPanel->Visible = false;
    #endif

    LastSelectChannels = - 1;

    cbViewBScan->OnClick(NULL);
    cbBScanDebug->OnClick(NULL);

    WorkStatePanel->Visible = false;

    BScanPanel->Visible = cbViewBScan->Checked;

    msSaveAscan = new TMemoryStream();

    #ifdef BScanSize_12Byte
    BScanChart->LeftAxis->Maximum = 16;
    #endif
    #ifndef BScanSize_12Byte
    BScanChart->LeftAxis->Maximum = 255;
    #endif
//    BScanChartList = new TList();

    LogLines_ = new TStringList();
    LogCrd_ = new TStringList();

    for (int i = -50; i <= 50; i++) {

        AmpSeries_Pos_0->AddXY(i, 0);
        AmpSeries_Pos_P45->AddXY(i, 0);
        AmpSeries_Pos_M45->AddXY(i, 0);
    }

    sgGates->Cells[0][0] = "№";
    sgGates->Cells[1][0] = "Start";
    sgGates->Cells[2][0] = "End";
    sgGates->ColWidths[1] = 52;
    sgGates->ColWidths[2] = 52;

    sgGates->Cells[0][1] = "1";
    sgGates->Cells[0][2] = "2";
    sgGates->Cells[0][3] = "Mark";
    sgGates->Cells[0][4] = "Max";


    #ifndef FULLVERSION
    TabSheet23->TabVisible = false;
    tsAKTest->TabVisible = false;
    AC_TabSheet->TabVisible = false;
    Настройка->TabVisible = false;
    TabSheet2->TabVisible = false;
    TabSheet7->TabVisible = false;
    TabSheet21->TabVisible = false;
    Разное->TabVisible = false;
    tsGates->TabVisible = false;
    TabSheet22->TabVisible = false;
    TabSheet20->TabVisible = false;
    TabSheet19->TabVisible = false;
    TabSheet16->TabVisible = false;
    TabSheet13->TabVisible = false;
    TabSheet11->TabVisible = false;
    TabSheet12->TabVisible = false;
    TabSheet10->TabVisible = false;
    TabSheet9->TabVisible = false;
    TabSheet8->TabVisible = false;
    TabSheet15->TabVisible = false;
    Series7->Visible = false;
    ComboBox1->Visible = false;
    Button12->Visible = false;
    Button1811->Visible = false;
    PageControl1->Visible = false;
    RadioButton1->Visible = false;
    RadioButton2->Visible = false;
    SpinEdit5->Visible = false;
    UpDown2->Visible = false;
    cbPathEncoderGraph->Visible = false;
    AmpSeries_Pos_P_45->Visible = false;
    ComboBox2->Visible = false;
    ComboBox3->Visible = false;
    TabSheet17->TabVisible = false;
    HSDeviceConfig->Visible = false;
    Test->Visible = false;
    CheckBox7->Visible = false;
    cbBScanDebug->Visible = false;
    CheckBox8->Visible = false;
    BoltJntButton->Visible = false;
    AddKuSpinEdit->Visible = false;
    NotchPanel1->Visible = false;
    NotchPanel2->Visible = false;
    NotchPanel3->Visible = false;
    Memo3->Visible = false;
    BackBtnPanel->Align = alBottom;
    BackBtnPanel->Parent = Panel1;
    TabSheet14->Height = 150;
    Panel69->Height = 150;
    TabSheet6->Height = 150;
    PageControl2->Height = 160;
    PageControl2->ActivePage = TabSheet6;
//    Label31->Visible = false;
//    cbSwitching->Visible = false;
    #endif
    Button22Click(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::onLogoutChange(TObject *Sender) {
/*
	if (!LogForm->CheckBox1->Checked) {
		Panel2->Height = 35;
	}
	else {
		Panel2->Height = 300l;
	}
	LogForm->Panel48->Visible = CheckBox1->Checked;       */
}

// ---------------------------------------------------------------------------


void __fastcall TMainForm::Button13Click(TObject *Sender) {
    AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() + 1);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::Button14Click(TObject *Sender) {
    AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() - 1);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::sbSideLineClick(TObject *Sender) {

    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag) {
    case 1:
        { // Сторона / Нить
  /*            if ((AutoconMain->DEV->Rail == usLeft) && (AutoconMain->DEV->Line == 0)) {
                AutoconMain->DEV->Line = 1;
            }
            else if ((AutoconMain->DEV->Rail == usLeft) && (AutoconMain->DEV->Line == 1)) {
                AutoconMain->DEV->Line = 0;
                AutoconMain->DEV->Rail = usRight;
            }
            else if ((AutoconMain->DEV->Rail == usRight) && (AutoconMain->DEV->Line == 0)) {
                AutoconMain->DEV->Line = 1;
            }
            else if ((AutoconMain->DEV->Rail == usRight) && (AutoconMain->DEV->Line == 1)) {
                AutoconMain->DEV->Line = 0;
                AutoconMain->DEV->Rail = usLeft;
            };
            AutoconMain->DEV->TestInit(); */
            break;
        }
    case 13:
        { // Угол ввода [град]
        /*    if (AutoconMain->DEV->Angle == 0) {
                AutoconMain->DEV->Angle = 45;
            }
            else if (AutoconMain->DEV->Angle == 45) {
                AutoconMain->DEV->Angle = 50;
            }
            else if (AutoconMain->DEV->Angle == 50) {
                AutoconMain->DEV->Angle = 58;
            }
            else if (AutoconMain->DEV->Angle == 58) {
                AutoconMain->DEV->Angle = 65;
            }
            else if (AutoconMain->DEV->Angle == 65) {
                AutoconMain->DEV->Angle = 70;
            }
            else if (AutoconMain->DEV->Angle == 70) {
                AutoconMain->DEV->Angle = 0;
            } */
            break;
        }
    case 14: { // Настроить

			//	if(TuneChannel(AutoconMain->DEV->GetChannel(),AutoconMain->DEV->GetAScanMax().Ampl))
			//		 ValuePanel00->Caption = "Настроен";
			//	else
            //         ValuePanel00->Caption = "Ошибка";

			if (AutoconMain->DEV->ChannelSensCalibration()) ValuePanel00->Caption = "Настроен";
			  else ValuePanel00->Caption = "Ошибка";
			break;
        }
    case 18: { // Шумоподавление
                NoiseRed = !NoiseRed;
                break;
             }
    case 19: { // Шкала глубины
                RullerMode++;
                if (RullerMode == 3) // 3
                    RullerMode = 0;
                AutoconMain->DEV->RequestTVGCurve();
                break;
             }
    case 38: { // Отображение окна B-развертки
                BScanView = !BScanView;
                // BScanChart->Visible = BScanView;
                BScanDataPanel->Visible = BScanView;
                BScanPanel->Top = 0;
//                HandChannelPanel->Top = this->Height + 100;
                break;
             }
    case 40: { //Запись В-развертки
           /*     if(WriteBScanBtn->Down && !BScanHandTimer->Enabled) //Нажали
                {
                    WriteBScanLabel->Caption = "Запись В-развертки";
                    AutoconMain->Rep->ClearStoredHandSignals();
                    StartHandBScanTime = GetTickCount();

                    bsl->Clear(); //Clear b-scan
                    bsl->Draw(0,0,BScanPB->Canvas);

                    BScanHandTimer->Enabled = true; //Enable timer

                    SpeedButton2->Enabled = false; //Disable "Back" button
                    AutoconMain->Rep->EnableHandScanStore(true);
                }
                else
                {
                    BScanHandTimer->Enabled = false; // Disable timer
                    WriteBScanBtn->Down = false;

                    //WriteBScanLabel->Caption = "Запись В-развертки";
                    WriteBScanBtn->Caption = "";

                    CID curChannel = AutoconMain->DEV->GetChannel();
                    sChannelDescription chanDesc;
                    AutoconMain->Table->ItemByCID(curChannel, &chanDesc);

                    sHandScanParams HParams;
                    HParams.EnterAngle = chanDesc.cdEnterAngle;
                    HParams.Method = chanDesc.cdMethod[0];
                    HParams.ScanSurface = 0;
                    HParams.Ku = AutoconMain->DEV->GetSens();
                    HParams.Att =  AutoconMain->DEV->GetGain();
                    HParams.StGate = AutoconMain->DEV->GetStGate();
                    HParams.EdGate = AutoconMain->DEV->GetEdGate();
                    HParams.PrismDelay = AutoconMain->DEV->GetPrismDelay();
                    HParams.TVG = AutoconMain->DEV->GetTVG();
                    AutoconMain->Rep->AddStoredHandSignals(HParams);

                    SpeedButton2->Enabled = true; //Enable "Back" button
                    AutoconMain->Rep->EnableHandScanStore(false);
                } */
                break;
             }
        case 41: { // Настроить 2 - настроить

			AutoconMain->DEV->ChannelSensCalibration_Type2();
			break;
        }
        case 42: { // Настроить 2 - сброс

            AutoconMain->DEV->ChannelSensCalibration_ResetSens();
			break;
        }

    }
    ValueToControl();
}

void __fastcall TMainForm::BScanHandTimerTimer(TObject *Sender)
{
    DWORD CurrentTime = GetTickCount();
    DWORD ElapsedTime = CurrentTime - StartHandBScanTime;

    if( ElapsedTime < 40000)
        WriteBScanBtn->Caption = StringFormatU(L"%d%%",ElapsedTime*100 / 40000);
    else
    {
        WriteBScanBtn->Click();
        BScanHandTimer->Enabled = false;
    }
}

UnicodeString DBSampleToStr(int Value) {
    switch(Value) {
    case 0: {
            return "-12";
        }
    case 1: {
            return "-10";
        }
    case 2: {
            return "-8";
        }
    case 3: {
            return "-6";
        }
    case 4: {
            return "-4";
        }
    case 5: {
            return "-2";
        }
    case 6: {
            return "0";
        }
    case 7: {
            return "+2";
        }
    case 8: {
            return "+4";
        }
    case 9: {
            return "+6";
        }
    case 10: {
            return "+8";
        }
    case 11: {
            return "+10";
        }
    case 12: {
            return "+12";
        }
    case 13: {
            return "+14";
        }
    case 14: {
            return "+16";
        }
    case 15: {
            return "+18";
        }
	}
	return "";
}

// ---------------------------------------------------------------------------

void __fastcall TMainForm::ValueToControl(void) {

/*  if (AutoconMain->DEV->Rail == usLeft) ValuePanel08->Caption = "Правая ";
                        else ValuePanel08->Caption = "Левая ";

    if (AutoconMain->DEV->Line == 1) ValuePanel08->Caption = ValuePanel08->Caption + "/ 1";
                   else ValuePanel08->Caption = ValuePanel08->Caption + "/ 0";

    Stroke->Caption = IntToStr(AutoconMain->DEV->StrokeIdx);
//	GeneratorPanel->Caption = IntToStr(AutoconMain->DEV->Generator);
//    ReceiverPanel->Caption = IntToStr(AutoconMain->DEV->Receiver); // Приемник
    DurationPanel->Caption = IntToStr(AutoconMain->DEV->Duration); // Длительность развертки
    DelayScalePanel->Caption = IntToStr(AutoconMain->DEV->DelayScale); // Масштаб А разверки */


	StrokeCount->Caption = IntToStr(StartACGate);
    ZondAmplPanel->Caption = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
	SensPanel->Caption = IntToStr(AutoconMain->DEV->GetSens()); // Условная чувствительность [дБ]
	AttPanel->Caption = IntToStr(AutoconMain->DEV->GetGain());
	TVGPanel->Caption = IntToStr(AutoconMain->DEV->GetTVG()); // ВРЧ [мкс]
	double tmp = (double)((float)AutoconMain->DEV->GetPrismDelay() / (float)10);
    PrismDelayPanel->Caption = Format("%f", ARRAYOFCONST((tmp)));
    // Время в призме [10 * мкс]
/*  EvaluationGateLevelPanel->Caption = DBSampleToStr(AutoconMain->DEV->EvaluationGateLevel);
    BScanGatePanel->Caption = DBSampleToStr(AutoconMain->DEV->BScanGateLevel); */
	StGatePanel->Caption = IntToStr(AutoconMain->DEV->GetStGate()); // Начало строб АСД [мкс]
	EdGatePanel->Caption = IntToStr(AutoconMain->DEV->GetEdGate()); // Конец строба АСД [мкс]
    GateIdxTextPanel->Caption = IntToStr(AutoconMain->DEV->GetGateIndex()); // Номер строба
    GateIdxPanel->Enabled = AutoconMain->DEV->GetGateCount() != 1;

    NotchPanel1->Enabled = AutoconMain->DEV->GetNotchState();
    NotchPanel2->Enabled = AutoconMain->DEV->GetNotchState();
    NotchPanel3->Enabled = AutoconMain->DEV->GetNotchState();

    StNotchPanel->Caption = IntToStr(AutoconMain->DEV->GetStNotch());
    LenNotchPanel->Caption = IntToStr(AutoconMain->DEV->GetNotchLen());
    LevelNotchPanel->Caption = IntToStr(AutoconMain->DEV->GetNotchLevel());

/*  AnglePanel->Caption = IntToStr(AutoconMain->DEV->Angle);
    BScanMinDelayPanel->Caption = IntToStr(AutoconMain->DEV->BScanMinDelay);
    BScanMaxDelayPanel->Caption = IntToStr(AutoconMain->DEV->BScanMaxDelay);
    BScanGateLevelPanel->Caption = IntToStr(AutoconMain->DEV->BScanGateLevel);
  */
    switch(RullerMode) {
    case 0: {
            RullerModePanel->Caption = "мкс";
            break;
        }
    case 1: {
            RullerModePanel->Caption = "мм";
            break;
        }
    case 2: {
            RullerModePanel->Caption = "от-ты";
            break;
        }
    }

    if (BScanView)
        BScanViewPanel->Caption = "Вкл";
    else
        BScanViewPanel->Caption = "Выкл";
    // BScanChart->Visible = BScanView;

    if (NoiseRed) {
        NoiseRedPanel->Caption = "Вкл";
    }
    else
        NoiseRedPanel->Caption = "Выкл";

    if (AutoconMain->DEV->GetMark() >= 0)
        MarkPanel->Caption = IntToStr(AutoconMain->DEV->GetMark());
         else  MarkPanel->Caption = "Выкл";
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::SpinButton06DownClick(TObject *Sender)
{

    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag)
    {

    case -1: {
                StartACGate--;
                if (StartACGate < 0) StartACGate = 0;
             }

        /*
    case -1: { //
             AutoconMain->DEV->StrokeCount--;
            if (AutoconMain->DEV->StrokeCount < 0) {
                AutoconMain->DEV->StrokeCount = 0;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 2: { // [+]  Генератор
            AutoconMain->DEV->Generator--;
			if (AutoconMain->DEV->Generator < 0) {
				AutoconMain->DEV->Generator = 7;
			}
			AutoconMain->DEV->TestInit(); * /
            break;
        }
    case 3: { // [+]  Приемник
/*            AutoconMain->DEV->Receiver--;
			if (AutoconMain->DEV->Receiver < 0) {
				AutoconMain->DEV->Receiver = 7;
			}
			AutoconMain->DEV->TestInit(); * /
            break;
        }
    case 4: { // [+] Длительность развертки
            AutoconMain->DEV->Duration--;
            if (AutoconMain->DEV->Duration < 10) {
                AutoconMain->DEV->Duration = 10;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 5: { // [+] Масштаб А развертки
            AutoconMain->DEV->DelayScale--;
            if (AutoconMain->DEV->DelayScale < 1) {
                AutoconMain->DEV->DelayScale = 1;
            }
            AutoconMain->DEV->UMU3204->EnableAScan(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, 0, 0,
                AutoconMain->DEV->DelayScale, BIN8(00000000), 1);
            break;
        } */
    case 6: { // [+]  Амплитуда ЗИ
                int val = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
                val--;
                if (val < 0) { val = 0; }
                AutoconMain->Config->SetPulseAmplByCID(AutoconMain->DEV->GetChannel(), val);
                AutoconMain->DEV->Update(true);
                break;
            }
    case 7: { // [+]  Ку
			AutoconMain->DEV->SetSens(AutoconMain->DEV->GetSens() - 1);
            break;
            }
	case 8: { // [+]  ВРЧ
			if (AutoconMain->DEV->GetTVG() - 1 >= 0) {
				AutoconMain->DEV->SetTVG(AutoconMain->DEV->GetTVG() - 1);
            }
            break;
        }
    case 9: { // [+]  Время в призме
			if (AutoconMain->DEV->GetPrismDelay() - 1 > 0) {
				AutoconMain->DEV->SetPrismDelay(AutoconMain->DEV->GetPrismDelay() - 1);
            }
            break;
        }
    case 10: { // [+] Порог АСД
         /*   AutoconMain->DEV->EvaluationGateLevel--;
            if (AutoconMain->DEV->EvaluationGateLevel < 0) {
                AutoconMain->DEV->EvaluationGateLevel = 0;
            }
            AutoconMain->DEV->TestInit();                  */
            break;
        }
	case 11: { // [+] Начало строба АСД
			if (AutoconMain->DEV->GetStGate() - 1 >= 0)
				AutoconMain->DEV->SetStGate(AutoconMain->DEV->GetStGate() - 1);
			break;
		}
	case 12: { // [+] Конец строба АСД
			if ((AutoconMain->DEV->GetEdGate() - 1 >= 0) &&
				(AutoconMain->DEV->GetEdGate() - 1 >= AutoconMain->DEV->GetStGate()))
				AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() - 1);
			break;
		}
	case 13: { // [+] Начало полочки
//			if ((AutoconMain->DEV->GetStNotch() - 1 >= 0) &&
//				(AutoconMain->DEV->GetEdGate() - 1 >= AutoconMain->DEV->GetStGate()))
			AutoconMain->DEV->SetStNotch(AutoconMain->DEV->GetStNotch() - 1);
			break;
		}
	case 14: { // [+] Длина полочки
//			if ((AutoconMain->DEV->GetEdGate() - 1 >= 0) &&
//				(AutoconMain->DEV->GetEdGate() - 1 >= AutoconMain->DEV->GetStGate()))
			AutoconMain->DEV->SetNotchLen(AutoconMain->DEV->GetNotchLen() - 1);
			break;
		}
	case 15: { // [+] Уровень полочки
			//if ((AutoconMain->DEV->GetEdGate() - 1 >= 0) &&
			//	(AutoconMain->DEV->GetEdGate() - 1 >= AutoconMain->DEV->GetStGate()))
            AutoconMain->DEV->SetNotchLevel(AutoconMain->DEV->GetNotchLevel() - 1);
			break;
		}
	case 22: { // [+] Номер строба АСД
                if (AutoconMain->DEV->GetGateCount() != 1)
                {
                    if (AutoconMain->DEV->GetGateIndex() == 1) AutoconMain->DEV->SetGateIndex(2); else AutoconMain->DEV->SetGateIndex(1);
                    AutoconMain->DEV->Update(false);
                }
                break;
             }

/*    case 15: { // [+] Начало строба В-развертки
            AutoconMain->DEV->BScanMinDelay--;
            if (AutoconMain->DEV->BScanMinDelay < 1) {
                AutoconMain->DEV->BScanMinDelay = 1;
            }
//            AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
            break;
        }
    case 16: { // [+] Конец строба В-развертки
            AutoconMain->DEV->BScanMaxDelay--;
            if (AutoconMain->DEV->BScanMaxDelay < 1) {
                AutoconMain->DEV->BScanMaxDelay = 1;
            }
//            AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
            break;
        }
    case 17: { // [+]  BScan порог [дБ]
            AutoconMain->DEV->BScanGateLevel--;
            if (AutoconMain->DEV->BScanGateLevel < 1) {
                AutoconMain->DEV->BScanGateLevel = 1;
            }
            AutoconMain->DEV->TestInit();
            break;
        }                               */
    case 77: { // [+]  Атт
				AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() - 1);
                break;
             }
	case 99: { // [+] Метка
         //       if (AutoconMain->DEV->GetMark() >= 0)
         //           if (AutoconMain->DEV->GetMark() - 1 >= 0)
                        AutoconMain->DEV->SetMark(AutoconMain->DEV->GetMark() - 1);
                break;
            }
/*    case 78: { // [+] Порог B-Scan
            AutoconMain->DEV->BScanGateLevel--;
            if (AutoconMain->DEV->BScanGateLevel < 0) {
                AutoconMain->DEV->BScanGateLevel = 0;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 256: { // [+] Порог B-Scan
            AutoconMain->DEV->StrokeIdx--;
            if (AutoconMain->DEV->StrokeIdx < 0) {
                AutoconMain->DEV->StrokeIdx = 0;
            }
            AutoconMain->DEV->TestInit();
            break;
        } */
    }
    ValueToControl();

//    dt->Resume();
//    DevThr->Resume();
//    UMUThr->Resume();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::SpinButton06UpClick(TObject *Sender)
{

//    dt->Suspend();
//    DevThr->Suspend();
//    UMUThr->Suspend();

    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);
    switch(AComponent->Tag) {

    case -1: {
                StartACGate++;
                if (StartACGate > AutoconMain->DEV->GetAScanLen())
                    StartACGate = AutoconMain->DEV->GetAScanLen();
             }

/*    case -1: { // [+]  Генератор
            AutoconMain->DEV->StrokeCount++;
            if (AutoconMain->DEV->StrokeCount > 12) {
                AutoconMain->DEV->StrokeCount = 12;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 2: { // [+]  Генератор
            AutoconMain->DEV->Generator++;
			if (AutoconMain->DEV->Generator > 7) {
				AutoconMain->DEV->Generator = 0;
			}
			AutoconMain->DEV->TestInit(); * /
			break;
		}
	case 3: { // [+]  Приемник
/*            AutoconMain->DEV->Receiver++;
			if (AutoconMain->DEV->Receiver > 7) {
				AutoconMain->DEV->Receiver = 0;
			}
			AutoconMain->DEV->TestInit();
			break;
        }
    case 4: { // [+] Длительность развертки
            AutoconMain->DEV->Duration++;
            if (AutoconMain->DEV->Duration > 255) {
                AutoconMain->DEV->Duration = 255;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 5: { // [+] Масштаб А развертки
            AutoconMain->DEV->DelayScale++;
            if (AutoconMain->DEV->DelayScale > 10) {
                AutoconMain->DEV->DelayScale = 10;
            }
            // AutoconMain->DEV->UMU3204->EnableAScan(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, 0, 0, AutoconMain->DEV->DelayScale, BIN8(00000000), 1);
            break;
        } */
    case 6: { // [+]  Амплитуда ЗИ
                int val = AutoconMain->Config->GetPulseAmplByCID(AutoconMain->DEV->GetChannel());
                val++;
                if (val > 7) { val = 7; }
                AutoconMain->Config->SetPulseAmplByCID(AutoconMain->DEV->GetChannel(), val);
				AutoconMain->DEV->Update(true);
                break;
            }
    case 7: { // [+]  Ку
			AutoconMain->DEV->SetSens(AutoconMain->DEV->GetSens() + 1);
			break;
		}
	case 8: { // [+]  ВРЧ
			AutoconMain->DEV->SetTVG(AutoconMain->DEV->GetTVG() + 1);
			break;
		}
	case 9: { // [+]  Время в призме
			AutoconMain->DEV->SetPrismDelay(AutoconMain->DEV->GetPrismDelay() + 1);
			break;
		}
/*	case 10: { // [+] Порог АСД
			AutoconMain->DEV->EvaluationGateLevel++;
			if (AutoconMain->DEV->EvaluationGateLevel > 15) {
				AutoconMain->DEV->EvaluationGateLevel = 15;
			}
			AutoconMain->DEV->TestInit();
			break;
		}                    */
    case 11: { // [+] Начало строба АСД
			if ((AutoconMain->DEV->GetStGate() + 1 <= AutoconMain->DEV->GetAScanLen()) &&
				(AutoconMain->DEV->GetStGate() + 1 <= AutoconMain->DEV->GetEdGate()))
				AutoconMain->DEV->SetStGate(AutoconMain->DEV->GetStGate() + 1);
            break;
		}
    case 12: { // [+] Конец строба АСД
			if (AutoconMain->DEV->GetEdGate() + 1 <= AutoconMain->DEV->GetAScanLen())
				AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() + 1);
            break;
        }
    case 13: { // [+] Начало полочки
   		   AutoconMain->DEV->SetStNotch(AutoconMain->DEV->GetStNotch() + 1);
           break;
        }
    case 14: { // [+] Длина полочки
		   //	if (AutoconMain->DEV->GetEdGate() + 1 <= AutoconMain->DEV->GetAScanLen())
		   //		AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() + 1);
		   AutoconMain->DEV->SetNotchLen(AutoconMain->DEV->GetNotchLen() + 1);
           break;
        }
    case 15: { // [+] Уровень полочки
		  //	if (AutoconMain->DEV->GetEdGate() + 1 <= AutoconMain->DEV->GetAScanLen())
		  //		AutoconMain->DEV->SetEdGate(AutoconMain->DEV->GetEdGate() + 1);
            AutoconMain->DEV->SetNotchLevel(AutoconMain->DEV->GetNotchLevel() + 1);
            break;
        }
	case 22: { // [+] Номер строба АСД
                if (AutoconMain->DEV->GetGateCount() != 1)
                {
                    if (AutoconMain->DEV->GetGateIndex() == 1) AutoconMain->DEV->SetGateIndex(2); else AutoconMain->DEV->SetGateIndex(1);
                    AutoconMain->DEV->Update(false);
                }
                break;
             }

/*    case 15: { // [+] Начало строба В-развертки
            AutoconMain->DEV->BScanMinDelay++;
            if (AutoconMain->DEV->BScanMinDelay > 255) {
                AutoconMain->DEV->BScanMinDelay = 255;
            }
//            AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
            break;
        }
    case 16: { // [+] Конец строба В-развертки
            AutoconMain->DEV->BScanMaxDelay++;
            if (AutoconMain->DEV->BScanMaxDelay > 255) {
                AutoconMain->DEV->BScanMaxDelay = 255;
            }
//            AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
            break;
        }
    case 17: { // [+]  BScan порог [дБ]
            AutoconMain->DEV->BScanGateLevel++;
            if (AutoconMain->DEV->BScanGateLevel > 7) {
                AutoconMain->DEV->BScanGateLevel = 7;
            }
			AutoconMain->DEV->TestInit();
            break;
        } */
	case 77: { // [+]  Атт
				AutoconMain->DEV->SetGain(AutoconMain->DEV->GetGain() + 1);
                break;
            }

	case 99: { // [+] Метка
                if (AutoconMain->DEV->GetMark() >= 0)
                    if (AutoconMain->DEV->GetMark() + 1 <= AutoconMain->DEV->GetAScanLen())
                        AutoconMain->DEV->SetMark(AutoconMain->DEV->GetMark() + 1);
                break;
            }

/*    case 78: { // [+] Порог B-Scan
            AutoconMain->DEV->BScanGateLevel++;
            if (AutoconMain->DEV->BScanGateLevel > 15) {
                AutoconMain->DEV->BScanGateLevel = 15;
            }
            AutoconMain->DEV->TestInit();
            break;
        }
    case 256: { // [+] Порог B-Scan
            AutoconMain->DEV->StrokeIdx++;
            if (AutoconMain->DEV->StrokeIdx > 12) {
                AutoconMain->DEV->StrokeIdx = 12;
            }
            AutoconMain->DEV->TestInit();
            break;
        } */
    }

	ValueToControl();

//    dt->Resume();
//    DevThr->Resume();
//    UMUThr->Resume();
}

float TMainForm::DelayToChartValue(float Src)
{
    switch(RullerMode) {
        case 0: {
                    return Src;
                }
        case 1: {
                    //return AutoconMain->DEV->DelayToDepth(Src);
                    return AutoconMain->DEV->DelayToDepthByHeight(Src);
            }
        case 2: {
                    return AutoconMain->DEV->DelayToPixel(Src);
                    // return (232 * Src) / AutoconMain->DEV->GetAScanLen();
                    //return 10 * Src / 1; // !q! AutoconMain->DEV->DelayScale;
			    }
	}
	return 0;
}

float TMainForm::ChartValueToDelay(float Src)
{
    switch(RullerMode) {
		case 0: {
                return Src;
                }
        case 1: {
                return AutoconMain->DEV-> DepthToDelay( Src);
            }
        case 2: {
                return /*AutoconMain->DEV->DelayScale*/ 1 * Src / 10; // !q!
                }
	}
	return 0;
}


float TMainForm::PixelToChartValue(int Pixel) {
    switch(RullerMode) {
    case 0: {
            return AutoconMain->DEV->PixelToDelayFloat(Pixel);
        }
    case 1: {
  //          return AutoconMain->DEV->DelayToDepth(AutoconMain->DEV->PixelToDelayFloat(Pixel));
            return AutoconMain->DEV->DelayToDepthByHeight(AutoconMain->DEV->PixelToDelayFloat(Pixel));
        }
    case 2: {
            return Pixel;
        }
    }
    return 0;
//System::TDateTime
}

unsigned char TMainForm::Filter(unsigned char Value) {
    if (NoiseRed) {
        if (Value < 16) {
            return 0;
        }
        else if (Value < 32) {
            return(unsigned char)(32 * (Value - 15) / 16);
        }
        else {
            return Value;
        }
    }
    else
        return Value;
}

// ---------------------------------------------------------------------------

UnicodeString TMainForm::GetUMUMessageName(unsigned char id, unsigned char id2) {
    switch(id) {
    case dAScanDataHead: {
            return "А-Развертка";
        }
    case dAScanMeas: {
            return "Точные значения А-Развертки";
		}
    case dBScanData: {
            return "В-Развертка";
        }
    case dBScan2Data: {
            return "В-Развертка (тип 2)";
        }
    case dMScanData: {
            return "М-Развертка";
        }
    case dAlarmData: {
            return "АСД";
        }
    case dPathStep: {
            return "Датчик пути";
        }
    case dPathStepEx: {
            return "Датчик пути (расширенный + DisCrd)";
        }
    case dDeviceSpeed: {

            return "Скорость движения системы";
        }
    case dPathEncoderSetup: {
            return "Значение датчика пути УСТАНОВЛЕННО !!!";
        }
    case dVoltage: {
            return "U аккум";
        }
    case dWorkTime: {
            return "Время работы БУМ";
        }
//    case dEnvelopeMeas: {
//            return "Значение огибающей сигнала";
//		}
    case dTestAScan: {
            return "Подсчет А-разверток";
        }
    case uCalcACThreshold: { return "Расчитать пороги АК"; }
    case uSetACTresholds:  { return "Установка пороги АК"; }
    case uBSFilterStDelay: { return "Установка момента начала фильтрации сигналов B-развертки"; }

    case uStrokeCount   : { return "Установить количество тактов"; }
    case uParameter     : { return "Записать значение параметра в таблицу тактов"; }
    case uGainSeg       : { return "Записать параметры отрезка кривой ВРУ"; }
    case uGate          : { return "Записать параметры строба"; }
    case uPrismDelay    : { return "Записать задержку в призме"; }
    case uEnableAScan   : { return "Разрешить передачу А-развертки"; }
//    case uPathSimulator : {	return "Включение имитатора датчика пути"; }
//    case uPathSimulatorEx : {	return "Включение имитатора датчика пути (расширенный вариант)"; }
//    case uPathSimulatorEx2 : {	return "Включение имитатора датчика пути сканера"; }
    case uEnable        : {

                            switch(id2) {

                                 case wmDisableAScan      : { return "Управление работой БУМ - Запретить передачу А-развертки"; }
                                 case wmEnableBScan       : { return "Управление работой БУМ - Разрешить передачу В-развертки"; }
                                 case wmDisableBScan      : { return "Управление работой БУМ - Запретить передачу B-развертки"; }
                                 case wmEnableACSumm      : { return "Управление работой БУМ - Разрешить передачу сырых данных АК"; }
                                 case wmDisableACSumm     : { return "Управление работой БУМ - БУИ - Запретить передачу сырых данных АК"; }
                                 case wmEnableAC          : { return "Управление работой БУМ - Разрешить передачу данных АК"; }
                             //  case wmDisableAC         : { return "Управление работой БУМ - БУИ - Запретить передачу данных АК"; }
                                 case wmEnableAlarm       : { return "Управление работой БУМ - Разрешить передачу АСД"; }
                                 case wmDisableAlarm      : { return "Управление работой БУМ - Запретить передачу АСД"; }
                                 case wmEnable            : { return "Управление работой БУМ - Разрешить работу БУМ"; }
                                 case wmDisable           : { return "Управление работой БУМ - Запретить работу БУМ"; }
                                 case wmReboot            : { return "Управление работой БУМ - Перезагрузить БУМ"; }
                                 case wmGetWorkTime       : { return "Управление работой БУМ - Запрос на получение времени работы"; }
                                 case wmDeviceID          : { return "Управление работой БУМ - Запросить информацию о версии и самоидентификации устройства"; }
                                 case wmGetVoltage        : { return "Управление работой БУМ - Запрос на получение напряжения аккумулятора"; }
                            }

                          }
    case cDevice_       : { return "Сообщение о наличии устройства в сети"; }
    case cFWver         : { return "Версия и дата прошивки блока"; }
    case cSerialNumber  : {	return "Серийный номер устройства"; }
    case dConfirmStopBScan : {	return "Подтверждение выполнения команды «Запретить передачу В-развертки с идентификатором 0x72»"; }
    case dMetalSensor   : {	return "Срабатывание датчика маталла"; }
    case dAcousticContactSumm   : {	return "Сырые данные АК"; }
    case dAcousticContact_   : {	return "Данные АК"; }
    case dACTresholds        : {	return "Пороги АК"; }
    default: {
            return Format("%x - ", ARRAYOFCONST((id))) + "  <--------------------- ОШИБОЧНЫЙ ID --------------------->";
        }
    }
}

UnicodeString TMainForm::GetCDUMessageName(unsigned char id, unsigned char id2) {
    switch(id) {
    case dPathEncoderSetup: { if ((id2 >> 7) == 1) return "Выбор датчика пути"; else return "Установка значения датчика пути";}
    case uCalcACThreshold: { return "Расчитать пороги АК"; }
    case uSetACTresholds:  { return "Установка пороги АК"; }
    case uBSFilterStDelay: { return "Установка момента начала фильтрации сигналов B-развертки"; }
    case uStrokeCount   : { return "Установить количество тактов"; }
    case uParameter     : { return "Записать значение параметра в таблицу тактов"; }
    case uGainSeg       : { return "Записать параметры отрезка кривой ВРУ"; }
    case uGate          : { return "Записать параметры строба"; }
    case uPrismDelay    : { return "Записать задержку в призме"; }
    case uEnableAScan   : { return "Разрешить передачу А-развертки"; }
    case uPathSimulator : {	return "Включение имитатора датчика пути"; }
    case uPathSimulatorEx : {	return "Включение имитатора датчика пути (расширенный вариант)"; }
    case uPathSimulatorEx2 : {	return "Включение имитатора датчика пути сканера"; }
    case wmLeftSideSwitching : { return "Переключение линий БУМ с сплошного контроля на сканер"; }
    case uEnable        : {

                            switch(id2) {

                                 case wmDisableAScan      : { return "Управление работой БУМ - Запретить передачу А-развертки"; }
                                 case wmEnableBScan       : { return "Управление работой БУМ - Разрешить передачу В-развертки"; }
                                 case wmDisableBScan      : { return "Управление работой БУМ - Запретить передачу B-развертки"; }
                                 case wmEnableACSumm      : { return "Управление работой БУМ - Разрешить передачу сырых данных АК"; }
                                 case wmDisableACSumm     : { return "Управление работой БУМ - БУИ - Запретить передачу сырых данных АК"; }
                                 case wmEnableAC          : { return "Управление работой БУМ - Разрешить передачу данных АК"; }
                             //  case wmDisableAC         : { return "Управление работой БУМ - БУИ - Запретить передачу данных АК"; }
                                 case wmEnableAlarm       : { return "Управление работой БУМ - Разрешить передачу АСД"; }
                                 case wmDisableAlarm      : { return "Управление работой БУМ - Запретить передачу АСД"; }
                                 case wmEnable            : { return "Управление работой БУМ - Разрешить работу БУМ"; }
                                 case wmDisable           : { return "Управление работой БУМ - Запретить работу БУМ"; }
                                 case wmReboot            : { return "Управление работой БУМ - Перезагрузить БУМ"; }
                                 case wmGetWorkTime       : { return "Управление работой БУМ - Запрос на получение времени работы"; }
                                 case wmDeviceID          : { return "Управление работой БУМ - Запросить информацию о версии и самоидентификации устройства"; }
                                 case wmGetVoltage        : { return "Управление работой БУМ - Запрос на получение напряжения аккумулятора"; }
                                 case wmLeftSideSwitching : { return "Управление работой БУМ - Переключение линий БУМ с сплошного контроля на сканер"; }
                                 case wmEnableCalcDisCoord: { return "Управление работой БУМ - Включить дисплейную координату"; }
                                 case wmDisableCalcDisCoord: { return "Управление работой БУМ - Выключить дисплейную координату"; }
                            }

                          }
    case cDevice_       : { return "Сообщение о наличии устройства в сети"; }
    default: {
            return Format("%x - ", ARRAYOFCONST((id))) + "  <--------------------- ОШИБОЧНЫЙ ID --------------------->";
        }
    }
}


void __fastcall TMainForm::Button9Click(TObject *Sender)
{
//    AutoconMain->DEV->UMU3204->EnableAlarm();
    CheckListBox1->CheckAll(cbChecked, false, false);
    CheckListBox1Click(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::PrismDelayPanelClick(TObject *Sender)
{
/*
//    RaduisMode = !RaduisMode;
	// Device.OpenGate(FRaduisMode);
	if (RaduisMode)
	{
		PrismDelayPanel->Color = clLime;
	}
	else
	{
		PrismDelayPanel->Color = clBtnHighlight;
	}
*/
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::EditModePanelClick(TObject *Sender)
{
//	if ((EnterMode) && (EnterModePanel)) EnterModePanel->Color = clBtnHighlight;

	TPanel* EnterModePanel = dynamic_cast<TPanel*>(Sender);

    if (EnterModePanel->Tag == 14)
    {
        if (EditPanel)
        {
            EditPanel->Color = clBtnHighlight;
            EditMode = False;
            EditPanel = NULL;
            NumPadPanel->Visible = EditMode;
        };
        return;
    }

	TPanel* SavePanel;
	SavePanel = EditPanel;
	if (EditPanel)
	{
		EditPanel->Color = clBtnHighlight;
		EditMode = False;
		EditPanel = NULL;
	};
	if (SavePanel != EnterModePanel)
	{
		EditPanel = EnterModePanel;
		EditPanel->Color = clLime;
		EditMode = True;
	};

	NumPadPanel->Visible = EditMode;
/*
	if ((EnterModePanel->Tag == 13) || (EnterModePanel->Tag == 14) ||
		(EnterModePanel->Tag == 38) || (EnterModePanel->Tag == 18) ||
		(EnterModePanel->Tag == 19))
		return;

	if ((EnterMode) && (EnterModeIdx == EnterModePanel->Tag))
	{
		if (!EnterModeState)
		{
			EnterModePanel->Caption = EnterModeUndo;
		}
		EnterMode = false;
	}
	else
	{
		EnterMode = true;
		EnterModeIdx = EnterModePanel->Tag;
   //     EnterModePanel->Color = clYellow;
		EnterModeUndo = EnterModePanel->Caption;
   //     EnterModePanel->Caption = "";
		EnterModeState = false;
	}
	*/

	if ((EnterModePanel->Tag == 9) && (EditPanel != NULL))
	{
	   AutoconMain->DEV->SetCalibrationMode(cmPrismDelay);
	   AutoconMain->DEV->Update(false);
       ValueToControl();
	}
	if (((EnterModePanel->Tag == 9) && (EditPanel == NULL)) || (EnterModePanel->Tag != 9))
	{
	   AutoconMain->DEV->SetCalibrationMode(cmSens);
	   AutoconMain->DEV->Update(false);
       ValueToControl();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::FormShortCut(TWMKey &Msg, bool &Handled)
{
	if ((EditPanel != NULL) && ((int)Msg.Msg == 45078)) {
		String tmp;
		tmp = EditPanel->Caption;

        switch(Msg.CharCode) {
        case 48: {
                tmp = tmp + "0";
                break;
            };
        case 49: {
                tmp = tmp + "1";
                break;
            };
        case 50: {
                tmp = tmp + "2";
                break;
            };
        case 51: {
                tmp = tmp + "3";
                break;
            };
        case 52: {
                tmp = tmp + "4";
                break;
            };
        case 53: {
                tmp = tmp + "5";
                break;
            };
		case 54: {
                tmp = tmp + "6";
                break;
            };
        case 55: {
                tmp = tmp + "7";
                break;
            };
        case 56: {
                tmp = tmp + "8";
                break;
            };
        case 57: {
                tmp = tmp + "9";
                break;
            };

        case 109: {
                UnicodeString ttt = "-";
                if (tmp.Length() != 0) {
                    if (tmp[1] != ttt[1]) {
                        tmp = ttt + tmp;
                    }
                    else {
                        tmp.Delete(1, 1);
                    }
                }
				else
                    tmp = "-";
                break;
            };
        case 8: {
                tmp.Delete(tmp.Length(), 1);
                break;
			};
        case 32: {
                tmp = "";
                break;
            };
		case 27: {
				EditPanel->Caption = EnterModeUndo;
                tmp = EnterModeUndo;
				// EnterMode = false;
				EditPanel->Color = clBtnHighlight;
				ValueToControl();
				break;
			};
		case 13: {
				// EnterMode = false;
				EditPanel->Color = clBtnHighlight;
				if (tmp == "")
                    tmp = "0";
                if (tmp == "-")
                    tmp = "0";
				switch(EditPanel->Tag)
				{
				/*
				case 2: { // [+]  Генератор
						  / *AutoconMain->DEV->Generator = StrToInt(tmp);
						if (AutoconMain->DEV->Generator < 0) {
							AutoconMain->DEV->Generator = 0;
						}
						if (AutoconMain->DEV->Generator > 7) {
							AutoconMain->DEV->Generator = 7;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					}
				case 3: { // [+]  Приемник
					   / *	AutoconMain->DEV->Receiver = StrToInt(tmp);
						if (AutoconMain->DEV->Receiver < 0) {
							AutoconMain->DEV->Receiver = 0;
						}
						if (AutoconMain->DEV->Receiver > 7) {
							AutoconMain->DEV->Receiver = 7;
						}
						AutoconMain->DEV->TestInit();     * /
						break;
					}
				case 4: { // [+] Длительность развертки
					  / *  AutoconMain->DEV->Duration = StrToInt(tmp);
						if (AutoconMain->DEV->Duration < 0) {
							AutoconMain->DEV->Duration = 0;
						}
						if (AutoconMain->DEV->Duration > 255) {
							AutoconMain->DEV->Duration = 255;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					}
				case 5: { // [+] Масштаб А развертки
					   / * AutoconMain->DEV->DelayScale = StrToInt(tmp);
						if (AutoconMain->DEV->DelayScale < 0) {
							AutoconMain->DEV->DelayScale = 1;
						}
						if (AutoconMain->DEV->DelayScale > 10) {
							AutoconMain->DEV->DelayScale = 10;
						}
						AutoconMain->DEV->UMU3204->EnableAScan
							(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, 0, 0, AutoconMain->DEV->DelayScale, BIN8(00000000), 1);
							* /
						break;
					}
				case 6: { // [+]  Амплитуда ЗИ
					   / * AutoconMain->DEV->ZondAmpl = StrToInt(tmp);
						if (AutoconMain->DEV->ZondAmpl < 0) {
							AutoconMain->DEV->ZondAmpl = 1;
						}
						if (AutoconMain->DEV->ZondAmpl > 7) {
							AutoconMain->DEV->ZondAmpl = 7;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					} */
				case 77 : { // [+]  Ку - 7

						int Val;
						if (TryStrToInt(tmp, Val)) AutoconMain->DEV->SetSens(Val);
						break;
					}
				case 8: { // [+]  ВРЧ
						int Val;
						if (TryStrToInt(tmp, Val)) AutoconMain->DEV->SetTVG(Val);
						break;
					}
				case 9: { // [+]  Время в призме
						float Val;
						if (TryStrToFloat(tmp, Val)) AutoconMain->DEV->SetPrismDelay(Val * 10);
						break;
					}
			  /*  case 10: { // [+] Порог АСД
					  / *  AutoconMain->DEV->EvaluationGateLevel = StrToInt(tmp);
						if (AutoconMain->DEV->EvaluationGateLevel > 15) {
							AutoconMain->DEV->EvaluationGateLevel = 15;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					}     */
				case 11: { // [+] Начало строба АСД
						int Val;
						if (TryStrToInt(tmp, Val) && (Val <= AutoconMain->DEV->GetStrokeLen()))
                          AutoconMain->DEV->SetStGate(Val);
						break;
					}
				case 12: { // [+] Конец строба АСД
						int Val;
						if (TryStrToInt(tmp, Val) && (Val <= AutoconMain->DEV->GetStrokeLen())) AutoconMain->DEV->SetEdGate(Val);
						break;
					}       /*
				case 15: { // [+] Начало строба В-развертки
					  /*  AutoconMain->DEV->BScanMinDelay = StrToInt(tmp);
						if (AutoconMain->DEV->BScanMinDelay < 0) {
							AutoconMain->DEV->BScanMinDelay = 0;
						}
						if (AutoconMain->DEV->BScanMinDelay > 255) {
							AutoconMain->DEV->BScanMinDelay = 255;
						} * /
//                        AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
						break;
					}
				case 16: { // [+] Конец строба В-развертки
					/*    AutoconMain->DEV->BScanMaxDelay = StrToInt(tmp);
						if (AutoconMain->DEV->BScanMaxDelay < 0) {
							AutoconMain->DEV->BScanMaxDelay = 0;
						}
						if (AutoconMain->DEV->BScanMaxDelay > 255) {
							AutoconMain->DEV->BScanMaxDelay = 255;
						} * /
//                        AutoconMain->DEV->SetChannelParam(AutoconMain->DEV->Rail, AutoconMain->DEV->Line, false, false, false, true);
						break;
					}
				case 17: { // [+]  BScan порог [дБ]
					  /*  AutoconMain->DEV->BScanGateLevel = StrToInt(tmp);
						if (AutoconMain->DEV->BScanGateLevel < 0) {
							AutoconMain->DEV->BScanGateLevel = 0;
						}
						if (AutoconMain->DEV->BScanGateLevel > 7) {
							AutoconMain->DEV->BScanGateLevel = 7;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					}
				case 77: { // [+]  Атт
						if (StrToInt(tmp) < 128)
							AutoconMain->DEV->SetGain(StrToInt(tmp));
						break;
					}
				case 78: { // [+] Порог B-Scan
					   /* AutoconMain->DEV->BScanGateLevel = StrToInt(tmp);
						if (AutoconMain->DEV->BScanGateLevel < 0) {
							AutoconMain->DEV->BScanGateLevel = 0;
						}
						if (AutoconMain->DEV->BScanGateLevel > 15) {
							AutoconMain->DEV->BScanGateLevel = 15;
						}
						AutoconMain->DEV->TestInit(); * /
						break;
					} */
				}
	            Handled = true;
				ValueToControl();
				break;
			};
        }

		if (tmp != EditPanel->Caption)
		{
			EditPanel->Caption = tmp;
            Handled = true;
            EnterModeState = true;
        }
	}
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm::ApplicationEvents1Message(tagMSG &Msg, bool &Handled)
{
	if (Msg.message == WM_MOUSEWHEEL) {
		if ((EditPanel == NULL)) {
            ScrollBox1->VertScrollBar->Position =
                ScrollBox1->VertScrollBar->Position - (short)(Msg.wParam >> 16);
        }
        else {
            if (((short)(Msg.wParam >> 16) > 0)
                /* && (Msg.hwnd == EnterModePanel->Handle) */ ) {
				SpinButton06UpClick(EditPanel);
                EnterModeState = true;
            }

            if (((short)(Msg.wParam >> 16) < 0)
                /* && (Msg.hwnd == EnterModePanel->Handle) */ ) {
				SpinButton06DownClick(EditPanel);
                EnterModeState = true;
			}
        }
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::Button323Click(TObject *Sender)
{
  DisCoord = 0;

/*  for (unsigned int i = 0; i < AutoconMain->DEV->UMUList.size(); i++)
  {
      AutoconMain->DEV->UMUList[i]->SetPathEncoderData(0, false, 0, true);
  } */
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AScanChartClick(TObject *Sender)
{
    SpinButton06DownClick(SpinButton13);
/*    if (AutoconMain->DEV->GetGateCount() != 1) {

        if (SaveAScanChartMouseMoveX < AScanChart->Width / 2) {

            AutoconMain->DEV->GetGateIndex() SetGateIndex(1);
            AutoconMain->DEV->Update(false);
        }
        if (SaveAScanChartMouseMoveX >= AScanChart->Width / 2) {

            AutoconMain->DEV->SetGateIndex(2);
            AutoconMain->DEV->Update(false);
        }
    }
*/
//    if(SpeedButton2->Enabled)
//        SpeedButton2->Click();
//   Series1->X0 = AScanChart->BottomAxis->CalcPosPoint(AScanChart->ScreenToClient(Mouse->CursorPos).x);
//   Series1->X1 = AScanChart->BottomAxis->CalcPosPoint(AScanChart->ScreenToClient(Mouse->CursorPos).x);
//   Series1->Y0 = AScanChart->LeftAxis->Minimum;
//   Series1->Y1 = AScanChart->LeftAxis->Maximum;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AScanChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
/*
   if (Button == mbLeft)
   {
       Series1->X0 = AScanChart->BottomAxis->CalcPosPoint(X);
       Series1->X1 = Series1->X0;
       Series1->Y0 = AScanChart->LeftAxis->Minimum;
       Series1->Y1 = AScanChart->LeftAxis->Maximum;
       Series1->Visible = true;
       dt->params.Pixel1 = AutoconMain->DEV->DelayToPixel(ChartValueToDelay(AScanChart->BottomAxis->CalcPosPoint(X)));
   }
   else
   if (Button == mbRight)
   {
       Series2->X0 = AScanChart->BottomAxis->CalcPosPoint(X);
       Series2->X1 = Series2->X0;
       Series2->Y0 = AScanChart->LeftAxis->Minimum;
       Series2->Y1 = AScanChart->LeftAxis->Maximum;
       Series2->Visible = true;
       dt->params.Pixel2 = AutoconMain->DEV->DelayToPixel(ChartValueToDelay(AScanChart->BottomAxis->CalcPosPoint(X)));
   };
   */
}

// Att
// Ku
// нить канал - для В развертки
// Почистить исходники

// tbl->GetGateCount(
// Разделение блоков данных !!!
// UMU Index !!!!
// Strobe ->> Gate




      /*
        DWORD dwWaitResult;
        dwWaitResult = WaitForSingleObject(hDeviceEvent, 50 /* INFINITE * / ); // indefinite wait

        switch(dwWaitResult)
        {
            // Event object was signaled
        case WAIT_OBJECT_0:
        {
            const BUFSIZE = 1;
            CHAR chReadBuf[BUFSIZE];
            DWORD lpBytesRead;
            DWORD lpTotalBytesAvail;
            DWORD lpBytesLeftThisMessage;
            DWORD dwRead, dwWritten;

            PeekNamedPipe(hRead, chReadBuf, BUFSIZE, &lpBytesRead, &lpTotalBytesAvail, &lpBytesLeftThisMessage);
            if (lpTotalBytesAvail == 0) break;
            bool bSuccess = ReadFile(hRead, &DataID, 4, &dwRead, NULL);
            if (!bSuccess || dwRead == 0) break;
            switch(DataID)
            {
                case edAScanMeas: // А-развертка, точное значение амплитуды и задержки
					{
                        PeekNamedPipe(hRead, chReadBuf, BUFSIZE, &lpBytesRead, &lpTotalBytesAvail, &lpBytesLeftThisMessage);
                        if (lpTotalBytesAvail == 0) break;
                        bSuccess = ReadFile(hRead, &AScanMeasure_ptr, 4, &dwRead, NULL);
                        if (!bSuccess || dwRead == 0) break;
                        Synchronize(ValueToScreen);
                        delete AScanMeasure_ptr;
						AScanMeasure_ptr = NULL;
                        break;
                    }
                case edAScanData: // AScan
                    {
                        PeekNamedPipe(hRead, chReadBuf, BUFSIZE, &lpBytesRead, &lpTotalBytesAvail, &lpBytesLeftThisMessage);
                        if (lpTotalBytesAvail == 0) break;
                        bSuccess = ReadFile(hRead, &AScanData_ptr, 4, &dwRead, NULL);
                        if (!bSuccess || dwRead == 0) break;
                        Synchronize(ValueToScreen);
                        delete AScanData_ptr;
                        AScanData_ptr = NULL;
                        break;
                    }
                case edTVGData: // Отсчеты ВРЧ
                    {
                        PeekNamedPipe(hRead, chReadBuf, BUFSIZE, &lpBytesRead, &lpTotalBytesAvail, &lpBytesLeftThisMessage);
                        if (lpTotalBytesAvail == 0) break;
                        bSuccess = ReadFile(hRead, &TVGData_ptr, 4, &dwRead, NULL);
                        if (!bSuccess || dwRead == 0) break;
                        Synchronize(ValueToScreen);
                        delete TVGData_ptr;
                        TVGData_ptr = NULL;
                        break;
                    }
				case edBScan2Data: // BScan
                    {
						PeekNamedPipe(hRead, chReadBuf, BUFSIZE, &lpBytesRead, &lpTotalBytesAvail, &lpBytesLeftThisMessage);
                        if (lpTotalBytesAvail == 0) break;
                        bSuccess = ReadFile(hRead, &PBScanData_ptr, 4, &dwRead, NULL);
                        if (!bSuccess || dwRead == 0) break;

                        List_ptr[Count_ptr] = PBScanData_ptr;
                        Count_ptr++;

                        if (GetTickCount() - Time > 50 )
                        {
                            Synchronize(ValueToScreen);
                            Time = GetTickCount();
                            for (int idx = 0; idx < Count_ptr; idx++)
                            {
                               delete List_ptr[idx];
                            }
                            Count_ptr = 0;
                        }
                        break;
                    }
                }
                break;
            }

        default: ;
        }
       */


//       int ChartX = AScanChart->BottomAxis->CalcPosPoint(X);
//       int Delay = ChartValueToDelay(ChartX);
//       int Pix = AutoconMain->DEV->DelayToPixel(Delay);
//       params.Pixel1 = Pix;
//       Form2->Caption = IntToStr(params.Pixel1);

void __fastcall TMainForm::Button127Click(TObject *Sender)
{

	if (ChList->ItemIndex == - 1) return;
	AutoconMain->Config->Tag = ChList->ItemIndex;

    ValuePanel00->Caption = "-";
    TComponent *AComponent = dynamic_cast<TComponent*>(Sender);

//    if (LastSelectChannels != - 1)
//        AutoconMain->Rep->SetTimeInHandScanChannel(LastSelectChannels, AutoconMain->Rep->GetTimeInHandScanChannel(LastSelectChannels) + (GetTickCount() - LastSelectChannelsTickCount) / 1000);
    LastSelectChannelsTickCount = GetTickCount();

    int temp_id = (int)ChList->Items->Objects[ChList->ItemIndex];

    if (temp_id == 123456) return;

    eDeviceSide s;

    if (temp_id > 9999) {
                s = dsNone;
                temp_id = temp_id - 10000;
           }
      else {
                if (temp_id > 0) s = dsLeft; else s = dsRight;
                temp_id = abs(temp_id);
                temp_id = temp_id - 1;
           }

    AutoconMain->DEV->SetSide(s);
    AutoconMain->DEV->SetChannel(temp_id);

    // отображение строба В-развертки
    sChannelDescription ChanDesc;
    AutoconMain->Table->ItemByCID(temp_id, &ChanDesc);
    BScanGateLabel->Caption = Format("B Scan gate - min: %d, max: %d", ARRAYOFCONST((ChanDesc.cdBScanGate.gStart, ChanDesc.cdBScanGate.gEnd)));

    bool b1 = AutoconMain->DEV->IsCopySlaveChannel(s, temp_id);
    bool b2 = AutoconMain->DEV->GetMode() == dmCalibration;

    AScanChart->Visible = !(b1 && b2);

//    AScanChart->Visible = !(AutoconMain->DEV->IsCopyMasterChannelSetFaceData(s, temp_id) && (AutoconMain->DEV->GetMode() == dmCalibration));

//    if (RadioButton2->Checked) MainPathEncoderIndex = ComboBox2->ItemIndex;

//    if (!AutoconMain->Table->isHandScan(temp_id)) temp_id

    MainPathEncoderType = AutoconMain->Table->isHandScan(temp_id) || AutoconMain->DEV->UsePathEncoderSimulationinSearchMode;
//    ComboBox3->ItemIndex;


    /*

    switch(AComponent->Tag)
    {

        case 0:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1E); LastSelectChannels = 0x1E; break; }
        case 1:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1D); LastSelectChannels = 0x1D; break; }
        case 2:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x1F); LastSelectChannels = 0x1F; break; }
        case 3:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x20); LastSelectChannels = 0x20; break; }
        case 4:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x21); LastSelectChannels = 0x21; break; }
        case 5:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x22); LastSelectChannels = 0x22; break; }
        case 6:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x23); LastSelectChannels = 0x23; break; }

   / *
        case 0:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x49); break; }
        case 1:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x4A); break; }
        case 2:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x4B); break; }
        case 3:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x4D); break; }
        case 4:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x46); break; }
        case 5:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x3F); break; }
        case 6:  { AutoconMain->DEV->SetGateIndex(1); AutoconMain->DEV->SetChannel(0x43); break; }
     * /
        / *

        case 7:  { AutoconMain->DEV->SetChannel(0x01); break; }
        case 8:  { AutoconMain->DEV->SetChannel(0x03); break; }
        case 9:  { AutoconMain->DEV->SetChannel(0x04); break; }
        case 10: { AutoconMain->DEV->SetChannel(0x05); break; }
        case 11: { AutoconMain->DEV->SetChannel(0x06); break; }
        case 12: { AutoconMain->DEV->SetChannel(0x17); break; }
		case 13: { AutoconMain->DEV->SetChannel(0x19); break; }
          * /
    }
      */
//    bsl->Clear(); //Clear b-scan
//    bsl->Draw(0,0,BScanPB->Canvas);

//    AutoconMain->DEV->PathEncoderSim(true);
    AutoconMain->DEV->Update(false);
    if (AutoconMain->DEV->GetGateCount() == 1) {

        AutoconMain->DEV->SetGateIndex(1);
        AutoconMain->DEV->Update(false);
    }

    int tmp = DelayToChartValue(AutoconMain->DEV->GetAScanLen());
    if (tmp > AScanChart->BottomAxis->Minimum) AScanChart->BottomAxis->Maximum = tmp;

//    AScanChart->BottomAxis->Maximum = DelayToChartValue(AutoconMain->DEV->GetAScanLen());
    SaveMax1 = - 1;
    SaveMax2 = - 1;
    NeedUpdate = true;
    ValueToControl();
//	PageControl->ActivePageIndex = 1;
    bsl->SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());

    eGateAlarmMode tmp1 = AutoconMain->DEV->GetAlarmMode(s, temp_id, 1);
    switch (tmp1) {
        case amOneEcho : { sbFirstGateMode->Caption = "OneEcho"; break; }
        case amTwoEcho : { sbFirstGateMode->Caption = "TwoEcho"; break; }
        case amOff     : { sbFirstGateMode->Caption = "Off"; break; }
    }
    sbFirstGateMode->Tag = tmp1;

    tmp1 = AutoconMain->DEV->GetAlarmMode(s, temp_id, 2);
    switch (tmp1) {
        case amOneEcho : { sbSecondGateMode->Caption = "OneEcho"; break; }
        case amTwoEcho : { sbSecondGateMode->Caption = "TwoEcho"; break; }
        case amOff     : { sbSecondGateMode->Caption = "Off"; break; }
    }
    sbSecondGateMode->Tag = tmp1;

//    if (!AutoconMain->Table->isHandScan(temp_id))
//        AutoconMain->DEV->PathEncoderSim((bool)PathSimButton->Tag);

/*    if (PathSimButton->Tag == 1)
        PathSimButton->Caption = "[+] Имитатор ДП"; else
        PathSimButton->Caption = "[-] Имитатор ДП";
*/
    AScanChart->BottomAxis->Maximum = DelayToChartValue(AutoconMain->DEV->GetAScanLen());


    if (AutoconMain->DEV->GetMode() == dmCalibration) StrokeTableMemoChange(NULL);


//    Memo3->Lines->Add(sgPathCounters->Cells[0][0]);
}

//---------------------------------------------------------------------------



void __fastcall TMainForm::NumPadClick(TObject *Sender)
{
  UnicodeString Text;
  int Val;
  float Val2;

  TSpeedButton* Btm = dynamic_cast<TSpeedButton*>(Sender);

  if (Btm)
  {
	switch (Btm->Tag)
	{
	  case 0: { EditPanel->Caption = EditPanel->Caption + '0'; break; }
	  case 1: { EditPanel->Caption = EditPanel->Caption + '1'; break; }
	  case 2: { EditPanel->Caption = EditPanel->Caption + '2'; break; }
	  case 3: { EditPanel->Caption = EditPanel->Caption + '3'; break; }
	  case 4: { EditPanel->Caption = EditPanel->Caption + '4'; break; }
	  case 5: { EditPanel->Caption = EditPanel->Caption + '5'; break; }
	  case 6: { EditPanel->Caption = EditPanel->Caption + '6'; break; }
	  case 7: { EditPanel->Caption = EditPanel->Caption + '7'; break; }
	  case 8: { EditPanel->Caption = EditPanel->Caption + '8'; break; }
	  case 9: { EditPanel->Caption = EditPanel->Caption + '9'; break; }
	 case 10: {
				  Text = EditPanel->Caption;
				  if ((Text.Length() != 0) && (Text[1] == '-')) Text.Delete(1, 1);
														 else Text = "-" + Text;
				  EditPanel->Caption = Text;
				  break;
			  }
	 case 11: {
				  EditPanel->Caption = EditPanel->Caption + '.';
				  break;
			  }
	 case 12: {
					//Modifyed by KirillB
				  if (TryStrToInt(StGatePanel->Caption, Val))
					AutoconMain->DEV->SetStGate( CLAMP(Val, 0 , AutoconMain->DEV->GetEdGate()) );
				  if (TryStrToInt(EdGatePanel->Caption, Val))
					AutoconMain->DEV->SetEdGate( CLAMP(Val, AutoconMain->DEV->GetStGate(), 200) );

				  if (TryStrToInt(AttPanel->Caption, Val)) AutoconMain->DEV->SetGain(Val);
				  if (TryStrToInt(SensPanel->Caption, Val)) AutoconMain->DEV->SetSens(Val);
				  if (TryStrToInt(TVGPanel->Caption, Val)) AutoconMain->DEV->SetTVG(Val);
				  if (TryStrToFloat(PrismDelayPanel->Caption, Val2)) AutoconMain->DEV->SetPrismDelay(Val2 * 10);
				  ValueToControl();
				  break;
			  }
	 case 13: {
				  Text = EditPanel->Caption;
				  Text.Delete(Text.Length(), 1);
				  EditPanel->Caption = Text;
				  break;
			  }
	 case 14: {
				  EditPanel->Caption = "";
				  break;
			  }
  }
}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PathEncoderOnClick(TObject *Sender)
{
	AutoconMain->DEV->PathEncoderSim(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PathEncoderOffClick(TObject *Sender)
{
	AutoconMain->DEV->PathEncoderSim(false);
}
//---------------------------------------------------------------------------

/*
-12 dB
-10 dB
-8 dB
-6 dB
-4 dB
-2 dB
0 dB
2 dB
4 dB
6 dB
8 dB
10 dB
12 dB
14 dB
16 dB
18 dB
*/


void __fastcall TMainForm::Panel61Resize(TObject *Sender)
{
	//for (int idx = 1; idx < 9; idx++)
	for (int idx = 1; idx < 9; idx++)
	{
//		BScanPainter.setBScanPageRect(BScanTabSheet[idx]->ClientRect,idx);
		//BScanLines[idx]->BoundsRect = BScanTabSheet[idx]->ClientRect;
		//BScanLines[idx]->Refresh();
	}
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::Button42Click(TObject *Sender)
{

    AutoconMain->DEV->SetSide(dsNone);
	for (unsigned int index = 0; index < AutoconMain->Config->ChannelsList.size(); index++)
	{
		AutoconMain->Calibration->SetSens(dsNone, AutoconMain->Config->ChannelsList[index], 0, 72/*StrToInt(AttEdit->Text)*/);     // Условная чувствительность [дБ]
		AutoconMain->Calibration->SetGain(dsNone, AutoconMain->Config->ChannelsList[index], 0, 72/*StrToInt(AttEdit->Text)*/);     // Аттенюатор [дБ]
		AutoconMain->Calibration->SetStGate(dsNone, AutoconMain->Config->ChannelsList[index], 0, 10);   // Начало строб АСД [мкс]
		AutoconMain->Calibration->SetEdGate(dsNone, AutoconMain->Config->ChannelsList[index], 0, 75);   // Конец строба АСД [мкс]
        AutoconMain->Calibration->SetPrismDelay(dsNone, AutoconMain->Config->ChannelsList[index], 950); // Время в призме [10 * мкс]
        AutoconMain->Calibration->SetTVG(dsNone, AutoconMain->Config->ChannelsList[index], 10);          // ВРЧ [мкс]
        /*
		AutoconMain->DEV->SetSens(72);
		AutoconMain->DEV->SetGain(72);
		AutoconMain->DEV->SetTVG(8);
//		AutoconMain->DEV->SetPrismDelay(750);
		AutoconMain->DEV->SetPrismDelay(50);
		AutoconMain->DEV->SetStGate(10);
		AutoconMain->DEV->SetEdGate(75);
        */
    }
    AutoconMain->DEV->Update(false);
}
//---------------------------------------------------------------------------

//---------Added by KirillB------------------
//Отрисовка в буферы paint box'a
void TMainForm::UpdatePBoxData(bool NewItemFlag)
{
//	BScanPainter.updateBScan(NewItemFlag);
}
//Отрисовка буферов на экран
void TMainForm::DrawPBoxData()
{
	#ifndef BSCAN_ALL_IN_ONE
	BScanPaintBoxPaint(NULL);
	#endif
	#ifdef BSCAN_ALL_IN_ONE

//	BScanPainter.drawBScan(BScanPBox->Canvas);

	#endif


	if(this->PageControl->ActivePageIndex == 15)
	{
		MMode_UpdateInfo();
		MMode_DrawPBoxes();
    }
	//Отрисовка буфера на экран
	//Генерация события OnPaint
	//BScanPBox->Repaint();
}
//-------------------------------------------
//

void __fastcall TMainForm::BScanPaintBoxPaint(TObject *Sender)
{
	for (int idx = 1; idx < 9; idx++)
		if (PageControl->ActivePageIndex == 3 + idx - 1 )
		{
//			BScanPainter.drawBScanPage(idx, BScanPaintBox[idx]->Canvas);
			//BScanPaintBox[idx]->Canvas->Draw(0, 0, BScanLines[idx]->Buffer);
		}

}

//---------------------------------------------------------------------------
//Native OnPaint function!
void __fastcall TMainForm::BScanPBoxPaint(TObject *Sender)
{
	DrawPBoxData();
}

//---------------------------------------------------------------------------
void __fastcall TMainForm::OnAScanFrameLeftBtnClick(TObject *Sender)
{
}
void __fastcall TMainForm::OnAScanFrameRightBtnClick(TObject *Sender)
{
}

void __fastcall TMainForm::OnAScanFrameCloseBtnClick(TObject *Sender)
{
}

void __fastcall TMainForm::ReDrawAScan(void)
{

}


//---------------------------------------------------------------------------


void __fastcall TMainForm::KP2BScanPaintBoxPaint(TObject *Sender)
{
  //
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::KP4BScanPaintBoxPaint(TObject *Sender)
{
  //
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::AkkButtonClick(TObject *Sender)
{

	//dt->Suspend();
    DrawThreadTimer->Enabled = false;
//	AutoconMain->Rep->ClearData();
    DrawThreadTimer->Enabled = true;
    //dt->Resume();

	for (int idx = 1; idx < 9; idx++)
	{
//		BScanPainter.scanLines[idx]->PaintToBuffer();
	}

	//Added by KirillB
//	DrawPBoxData();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button11Click(TObject *Sender)
{
    BlockListMemo->Lines->Clear();
    for (int idx = 0; idx < AutoconMain->DEV->UnitsData.size(); idx++)
    {
        BlockListMemo->Lines->Add(Format("%d", ARRAYOFCONST((idx))));
        BlockListMemo->Lines->Add(Format("Тип блока %d", ARRAYOFCONST((AutoconMain->DEV->UnitsData[idx].Type))));
        BlockListMemo->Lines->Add(Format("SupplyVoltage %d", ARRAYOFCONST((AutoconMain->DEV->UnitsData[idx].SupplyVoltage))));
        BlockListMemo->Lines->Add(Format("RemainingTime %d", ARRAYOFCONST((AutoconMain->DEV->UnitsData[idx].RemainingTime))));
        BlockListMemo->Lines->Add("WorksNumber:");
        BlockListMemo->Lines->Add(AutoconMain->DEV->UnitsData[idx].WorksNumber);
		BlockListMemo->Lines->Add("FirmwareVer:");
        BlockListMemo->Lines->Add(AutoconMain->DEV->UnitsData[idx].FirmwareVer);
        BlockListMemo->Lines->Add("FirmwareDate:");
        BlockListMemo->Lines->Add(AutoconMain->DEV->UnitsData[idx].FirmwareDate);

        if (AutoconMain->DEV->GetUMUOnLineStatus(idx)) BlockListMemo->Lines->Add("Status - OnLine");
                                                  else BlockListMemo->Lines->Add("Status - OffLine");

//        BlockListMemo->Lines->Add(IntToStr(AutoconMain->DEV->GetUMUOnLineStatus_(idx)));
    }

//    int GetUMUCount(void);
//    bool GetUMUOnLineStatus(int Idx);

}

void __fastcall TMainForm::Button26Click(TObject *Sender)
{
//  AutoconMain->DEV->UMUList[1]->GetDeviceID();


}

int Tabl[256] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  1,  1,  1,  1,  1,  2,  2,  2,  2,  3,  3,  3,  3,  3,
  4,  4,  4,  4,  4,  4,  5,  5,  5,  5,  5,  5,  5,  6,  6,  6,
  6,  6,  6,  6,  7,  7,  7,  7,  7,  7,  7,  7,  8,  8,  8,  8,
  8,  8,  8,  8,  8,  8,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,
 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11,
 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12,
 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 13, 13, 13,
 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14,
 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15,
 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 16,
 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,
 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17, 17,
 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18
 };


void __fastcall TMainForm::Button27Click(TObject *Sender)
{

    LineSeries8->Clear();
    LineSeries9->Clear();

    for (int i = 1; i < 256; i++) {


        float Tmp = static_cast<float>(i) / 32.0f;
        LineSeries8->AddXY(20 * std::log10(Tmp), AmplId_to_DB[TableDB[i] & 0x0F]);


        Memo6->Lines->Add(Format("%3.3f - %d", ARRAYOFCONST((20 * std::log10(Tmp), AmplId_to_DB[TableDB[i] & 0x0F]))));


//        LineSeries8->AddXY(i, TableDB[i] & 0x0F);
//        LineSeries8->AddXY(i, AmplId_to_DB[TableDB[i] & 0x0F]);
//         LineSeries9->AddXY(i, Tabl[i]);

//        ThValList
//        AmplId_to_DB

    }
//    TableDB
//    AmplIdtoDB
//  AutoconMain->DEV->UMUList[2]->GetDeviceID();
}

void __fastcall TMainForm::PageControlChange(TObject *Sender)
{
/*	if ((PageControl->ActivePageIndex >= 3) && (PageControl->ActivePageIndex <= 10))
    {
		BScanPainter.scanLines[1]->Used = PageControl->ActivePageIndex == 3;
		BScanPainter.scanLines[2]->Used = PageControl->ActivePageIndex == 4;
		BScanPainter.scanLines[3]->Used = PageControl->ActivePageIndex == 5;
		BScanPainter.scanLines[4]->Used = PageControl->ActivePageIndex == 6;
		BScanPainter.scanLines[5]->Used = PageControl->ActivePageIndex == 7;
		BScanPainter.scanLines[6]->Used = PageControl->ActivePageIndex == 8;
		BScanPainter.scanLines[7]->Used = PageControl->ActivePageIndex == 9;
		BScanPainter.scanLines[8]->Used = PageControl->ActivePageIndex == 10;
    }

    if (PageControl->ActivePageIndex == 13)
    {
		BScanPainter.scanLines[1]->Used = true;
		BScanPainter.scanLines[2]->Used = true;
		BScanPainter.scanLines[3]->Used = true;
		BScanPainter.scanLines[4]->Used = true;
		BScanPainter.scanLines[5]->Used = true;
		BScanPainter.scanLines[6]->Used = true;
		BScanPainter.scanLines[7]->Used = true;
		BScanPainter.scanLines[8]->Used = true;
    }
  */

//    #ifndef BSCAN_ALL_IN_ONE
/*	if ((PageControl->ActivePageIndex >= 3) && (PageControl->ActivePageIndex <= 10))
	{
		BScanContolPanel->Parent = PageControl->ActivePage;
        BScanContolPanel->Align = alTop;
		PageControl->ActivePage->Refresh();
        BScanContolPanel->Visible = true;
    } */
//    #endif

	if (((PageControl->ActivePageIndex >= 3) && (PageControl->ActivePageIndex <= 10)) || (PageControl->ActivePageIndex == 13) || (PageControl->ActivePageIndex == 15))
	{
		//Added by KirillB
		UpdatePBoxData(false);
		DrawPBoxData();
    }
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//----- ЖЕЛЕЗО                                                            ---
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
		/*
void __fastcall TMainForm::Button_6Click(TObject *Sender)
{
	Memo_1->Lines->Clear();
	Memo_2->Lines->Clear();
	Label_26->Caption = "0";
	Label_27->Caption = "0";
	AutoconMain->dip12maxdiff = AutoconMain->dip34maxdiff = 0;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button_3Click(TObject *Sender)
{
	if (AutoconMain->CtrlWork && AutoconMain->ac->ctrl) {

		unsigned int res = AutoconMain->ac->ctrl->setmode(minit);
		if (res) {
			Memo_1->Lines->Add("Set mode init error");

		}
		else {
			Memo_1->Lines->Add("Set mode init OK");
			Button_2->Enabled = true;

		}
	}
}


void __fastcall TMainForm::Button_5Click(TObject *Sender)
{
	if (AutoconMain->CtrlWork && AutoconMain->ac->ctrl) {

		unsigned int res = AutoconMain->ac->ctrl->setmode(mrail);
		if (res) {
			Memo_1->Lines->Add("Set mode test error");
		}
		else {
			Memo_1->Lines->Add("Set mode test OK");
			Button_2->Enabled = false;
		}
	}
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::Button_7Click(TObject *Sender)
{
	if (AutoconMain->CtrlWork && AutoconMain->ac->ctrl) {

		unsigned int res = AutoconMain->ac->ctrl->setmode(madj1);
		if (res) {
			Memo_1->Lines->Add("Set mode adjust 1 error");
		}
		else {
			Memo_1->Lines->Add("Set mode adjust 1 OK");
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button_8Click(TObject *Sender)
{
	if (AutoconMain->CtrlWork && AutoconMain->ac->ctrl) {

		unsigned int res = AutoconMain->ac->ctrl->setmode(madj2);
		if (res) {
			Memo_1->Lines->Add("Set mode adjust 2 error");
		}
		else {
			Memo_1->Lines->Add("Set mode adjust 2 OK");
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button_9Click(TObject *Sender)
{
	AutoconMain->ac->ctrl->motionStart(&endProcedure);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button_10Click(TObject *Sender)
{
	AutoconMain->ac->ctrl->motionBreak(&breakEndProcedure);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::endProcedure(MOTIONRESULT r) {
	wchar_t msg[255];

	if (r.error.errorNumber != ERROR_NO) {
		swprintf(msg, L"onStartMotion returned with error = %d",
			r.error.errorNumber);
		Memo_1->Lines->Add(msg);
	}
	else {
		swprintf(msg, L"onStartMotion OK returned with %d", r.parameter);
		Memo_1->Lines->Add(msg);
	}

}

// ---------------------------------------------------------------------------
void __fastcall TMainForm::breakEndProcedure(MOTIONRESULT r) {
	wchar_t msg[255];

	if (r.error.errorNumber != ERROR_NO) {
		swprintf(msg, L"onBreakMotion returned with error = %d",
			r.error.errorNumber);
		Memo_1->Lines->Add(msg);
	}
	else {
		swprintf(msg, L"onBreakMotion OK returned with %d", r.parameter);
		Memo_1->Lines->Add(msg);
	}

}

// ---------------------------------------------------------------------------

void __fastcall TMainForm::MyendProcedure(MOTIONRESULT r)
{
	wchar_t msg[255];

	if (r.error.errorNumber != ERROR_NO) {
		swprintf(msg, L"onStartMotion returned with error = %d",
			r.error.errorNumber);
		Memo_1->Lines->Add(msg);
	}
	else {
		swprintf(msg, L"onStartMotion OK returned with %d", r.parameter);
		Memo_1->Lines->Add(msg);
	}
	ClickTimer->Enabled = True;
}
	 */
//---------------------------------------------------------------------------

void __fastcall TMainForm::ClickTimerTimer(TObject *Sender)
{
    if ((AutoconMain) && (AutoconMain->DEV)) {

        BScanSesionMemo->Lines->Clear();
        BScanSesionMemo->Lines->Add("Device -> ");
        BScanSesionMemo->Lines->Add("ctHandScan -> " + IntToStr((int)AutoconMain->DEV->GetBScanSessionID(ctHandScan)));
        BScanSesionMemo->Lines->Add("ctCompInsp -> " + IntToStr((int)AutoconMain->DEV->GetBScanSessionID(ctCompInsp)));

        for (int UMUIndex = 0; UMUIndex < AutoconMain->Config->GetUMUCount(); UMUIndex++) {
//            BScanSesionMemo->Lines->Add(" --- UMU #" + IntToStr(UMUIndex) + " ---");
//            BScanSesionMemo->Lines->Add("UMU -> [" + IntToStr((int)AutoconMain->DEV->UMUList[UMUIndex]->_common_state._bscan_session_id.size()) +"]");
            for (int idx = 0; idx < AutoconMain->DEV->UMUList[UMUIndex]->_common_state._bscan_session_id.size(); idx++)
              BScanSesionMemo->Lines->Add(IntToStr(idx) + " - " + IntToStr((int)AutoconMain->DEV->UMUList[UMUIndex]->_common_state._bscan_session_id[idx]));
//            BScanSesionMemo->Lines->Add("");
            BScanSesionMemo->Lines->Add("Last -> " + IntToStr((int)AutoconMain->DEV->UMUList[UMUIndex]->_common_state._bscan_last_session_id));
        }
    }


//	ClickTimer->Enabled = False;
//	Button18Click(NULL);
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::TuneProc(int WorkCycle)
{
}



void __fastcall TMainForm::Search_or_Test(int WorkCycle)
{
}

void __fastcall TMainForm::Button19Click(TObject *Sender)
{
    Memo2->Clear();
    LineSeries3->Clear();
//    CrdCount = 0;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button20Click(TObject *Sender)
{
    Memo2->Lines->SaveToFile("coord_list.txt");
}


//---------------------------------------------------------------------------

void __fastcall TMainForm::Button23Click(TObject *Sender)
{
    AutoconMain->DEV->EnableFiltration(true);
  /*
  if ((AutoconMain->Rep) && OpenDialog1->Execute())
    AutoconMain->Rep->ReadFromFile(OpenDialog1->FileName, False);

  AllChBScanTab->TabVisible = true;
  AllChBScanTab->Show();
*/

}

//---------------------------------------------------------------------------

AnsiString __fastcall GetWVersion()
{
  AnsiString temp;
  VS_FIXEDFILEINFO VerValue;
  DWORD size, n;
  n = GetFileVersionInfoSize(Application->ExeName.c_str(), &size);
  if (n > 0)
  {
    char *pBuf = new char[n];
    GetFileVersionInfo(Application->ExeName.c_str(), 0, n, pBuf);
    LPVOID pValue;
    UINT Len;
    if (VerQueryValueA(pBuf, "\\", &pValue, &Len))
    {
      memmove(&VerValue, pValue, Len);
      temp = IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFFFF);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFF);
    }
    delete pBuf;
  }
  return temp;
}



//---------------------------------------------------------------------------

#include "UMUTest_MainMenuUnit.h"







void __fastcall TMainForm::StartAlign(void)
{
}

//--------------------------MANUAL MODE-------------------------------------

//Начать движение каретки
//Сменить режим работы ( acReady, acSearch, acTest, acTuning, ... )

//Произвести настройку по накопленным максимумам (и очистить их)

//Очистить накопленные максимумы

//Разрешить/запретить ручное управление рабочим циклом

//Задать рабочий цикл

//Обновление текущей информации
void TMainForm::MMode_UpdateInfo()
{
}

//Отрисовка кареток и B-сканов

void TMainForm::MMode_DrawCaretPositionPBox(bool bForceRedraw)
{
}

void TMainForm::MMode_DrawBScanPBox()
{
}

void TMainForm::MMode_DrawPBoxes()
{
}

//Обновление табов (под в-сканом)
void TMainForm::MMode_UpdateBScanTabs()
{
}

//Вывести окно с а-сканом
//Смена КП



void __fastcall TMainForm::Button34Click(TObject *Sender)
{

    switch(RullerMode)
    {
        case 0:
        case 1: {
                    BScanChart2->BottomAxis->SetMinMax(0, DelayToChartValue(AutoconMain->DEV->GetAScanLen()));
                    break;
                }
        case 2: {
                    BScanChart2->BottomAxis->SetMinMax(0, 232);
                    break;
                }
    }
//    HandBScanChart->LeftAxis->SetMinMax(0, AutoconMain->DEV->GetAScanLen());
    bsl->SetViewDelayZone(0, AutoconMain->DEV->GetAScanLen());
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox1Click(TObject *Sender)
{
    BSSeries1->Pointer->Visible = CheckBox1->Checked;
    BSSeries2->Pointer->Visible = CheckBox1->Checked;
    BSSeries3->Pointer->Visible = CheckBox1->Checked;
    BSSeries4->Pointer->Visible = CheckBox1->Checked;
    BSSeries5->Pointer->Visible = CheckBox1->Checked;
    BSSeries6->Pointer->Visible = CheckBox1->Checked;
    BSSeries7->Pointer->Visible = CheckBox1->Checked;
    BSSeries8->Pointer->Visible = CheckBox1->Checked;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::m_MMode_ResetPathEncoderClick(TObject *Sender)
{
	AutoconMain->DEV->ResetPathEncoder();
	AutoconMain->DEV->Update(true);
}

//---------------------------------------------------------------------------





void __fastcall TMainForm::Timer2Timer(TObject *Sender)
{

  //  if (Timer2->Tag == 0)
  //  {
        if (EndWork) return;
        if (!LogForm) return;
        if (!LogForm->LogState) return;
     //   if (!LogForm->LogState->Down) return;
  //  }

    if (LastLogLinesCount == LogLines->Count) return;

    LastLogLinesCount = LogLines->Count;

    // -------------------- Определение количества строк в TMemo

    HGDIOBJ OldFont;
    HDC Hand;
    TTextMetric TM;
    TRect Rect;
    int tempint;
    Hand = GetDC(LogForm->Memo1->Handle);
    OldFont = SelectObject(Hand, LogForm->Memo1->Font->Handle);
    GetTextMetrics(Hand, &TM);
    LogForm->Memo1->Perform(EM_GETRECT, 0, (NativeInt)(&Rect));
    tempint = (Rect.Bottom - Rect.Top) / (TM.tmHeight + TM.tmExternalLeading);
    SelectObject(Hand, OldFont);
    ReleaseDC(LogForm->Memo1->Handle, Hand);

    // --------------------

    int TH = tempint;

    LogForm->ScrollBar1->Tag = 1;
    LogForm->ScrollBar1->Position = max(0, LogLines->Count - TH);
    LogForm->ScrollBar1->Max = max(0, LogLines->Count - TH);
    LogForm->ScrollBar1->Tag = 0;
    LogForm->Memo1->Lines->BeginUpdate();
    LogForm->Memo1->Clear();


//    int St = -1;
//    int Ed = -1;

    for (int i = 0; i < TH; i++)
      if (LogLines->Count > i + LogForm->ScrollBar1->Position)
      {
  //      if (St == - 1) { St = i + LogForm->ScrollBar1->Position; }
        LogForm->Memo1->Lines->Add(IntToStr(i + LogForm->ScrollBar1->Position) + ". " + LogLines->Strings[i + LogForm->ScrollBar1->Position]);
//        Ed  = i + LogForm->ScrollBar1->Position;
      }
    LogForm->Memo1->Lines->EndUpdate();

//    LogForm->Caption = IntToStr(TH) + " - St: " + IntToStr(St) + " - Ed: " + IntToStr(Ed);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::DrawThreadTimerTimer(TObject *Sender)
{
    if (dt)
    {
        dt->ValueToScreen();

        //Debug - outpud data lost counter to log
        static DWORD PrevUpdateTime = GetTickCount();
        DWORD CurrTime = GetTickCount();
        if((CurrTime - PrevUpdateTime) > 1000)
        {
            unsigned int lost_cnt = AutoconMain->devt->Ptr_List.data_lost_size(true);
            if(lost_cnt)
            {
//                LOGERROR("Lost data in last 1sec (RingBuffer): %u items",lost_cnt);
            }
            PrevUpdateTime = CurrTime;
        }


        RailTypeTuningInfo->Clear();
        UnicodeString str;
        for (int i = 0; i < AutoconMain->DEV->RailTypeTuningDataList.size(); i++)
        {

            sRailTypeTuningData tmp = AutoconMain->DEV->RailTypeTuningDataList[i];
//            str = Format("Time: %d; Side: %d; Ch: %x; H: %d; Delay: %d", ARRAYOFCONST((tmp.Time, tmp.Side, tmp.Channel, tmp.DelayHeight, tmp.DelayHeight)));
//            RailTypeTuningInfo->Lines->Add(str);
/*            str = Format("%d. Side: %d; Ch: %x", ARRAYOFCONST((i, tmp.Side, tmp.Channel)));
            RailTypeTuningInfo->Lines->Add(str);
            str = Format("   Time: %d; H: %d; Delay: %d", ARRAYOFCONST((tmp.Time, tmp.DelayHeight, tmp.DelayHeight)));
            RailTypeTuningInfo->Lines->Add(str); */

            //str = Format("S: %d; Ch: %x T: %d; H: %d; D: %d", ARRAYOFCONST((tmp.Side, tmp.Channel, tmp.Time, tmp.DelayHeight, tmp.DelayHeight, tmp.CurrMaxAmp )));

//            if (tmp.Channel == 0x3D) {

                str = Format("Side: %d; Channel: %x Time: %d", ARRAYOFCONST((tmp.Side, tmp.Channel, tmp.Time)));
                RailTypeTuningInfo->Lines->Add(str);
                str = Format("Height: %d; Delay: %d; Ampl: %d (%d)", ARRAYOFCONST((tmp.DelayHeight, tmp.DelayHeight, tmp.MaxAmp, tmp.SignalIdx)));
                RailTypeTuningInfo->Lines->Add(str);
                str = "-----";
                RailTypeTuningInfo->Lines->Add(str);

            //}
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Timer3Timer(TObject *Sender)
{
  if ((AutoconMain) && (AutoconMain->DEV) && (PageControl->ActivePageIndex == 1))
  {
    Panel36->Caption = "";
    int Summ = 0;
    float FAscan = 0;

    for (int UMUSide = 0; UMUSide < 2; UMUSide++)
      for (int UMULine = 0; UMULine < 2; UMULine++)
      {
//            Summ = 0; NEWDEFCORE
//            for (int i = 0; i < 20; i++)
//                Summ = Summ + AutoconMain->DEV->UMUList[0]->Lastd2[UMUSide][UMULine][i];

            FAscan = 0;
            if (Summ != 0)
                FAscan = (float)1 / ((float(Summ) / 20.f) / 1000.f);

//          NEWDEFCORE
//          Panel36->Caption = Panel36->Caption + " S:" + IntToStr(UMUSide) + " L:" + IntToStr(UMULine);
//          if (GetTickCount() - AutoconMain->DEV->UMUList[0]->AScanDebugArr[UMUSide][UMULine] < 250)
//            Panel36->Caption = Panel36->Caption + Format("[%3.1f]", ARRAYOFCONST((FAscan)));
//            else Panel36->Caption = Panel36->Caption + "[ X ]";
      }

//          NEWDEFCORE
//    Summ = 0;
//    for (int i = 0; i < 20; i++)
//        Summ = Summ + AutoconMain->DEV->UMUList[0]->Lastd[i];

    FAscan = 0;
    if (Summ != 0)
        FAscan = (float)1 / ((float(Summ) / 20.f) / 1000.f);


    if (DebugOut)
        DebugPanel->Caption = Format("Fascan: %3.1f Гц | devt: put-%d; get-%d; Device: %d; UMU: %d | Message Count - UMU:%d; DEV: %d | Error - Write: %d; Read: %d",
                                            ARRAYOFCONST((FAscan,
                                                          AutoconMain->devt->Ptr_List.head_idx(),
                                                          AutoconMain->devt->Ptr_List.tail_idx(),
                                                          AutoconMain->DeviceEventManage->EventDataSize(),
                                                          AutoconMain->UMUEventManage->EventDataSize(),
                                                          0 /* AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->PostCount */, // NEWDEFCORE
                                                          AutoconMain->devt->InTick,
                                                          0 /*AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->WriteErrorCount*/,
                                                          0  /*AutoconMain->DEV->UMUList[AutoconMain->DEV->GetUsedUMUIdx()]->ErrorMessageCount*/)));

  }

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::DebugPanel_DblClick(TObject *Sender)
{
    LogLines->SaveToFile("log.txt");
    DebugOut = !DebugOut;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BScanPanel_Resize(TObject *Sender)
{
    bsl->SetSize(TSize(BScanPB->Width, BScanPB->Height));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbSurfaceChange(TObject *Sender)
{
    bsl->Clear(); //Clear b-scan
    bsl->Draw(0,0,BScanPB->Canvas);
}
//---------------------------------------------------------------------------

void TMainForm::CallBackProcPtr(int GroupIndex, eDeviceSide Side, CID Channel, int GateIndex, eValueID ID, int Value)
{

 //   if ((Channel != 0x17) || (Side != dsLeft)) return;

    if (CBLogSwitch->Tag == 1) return;
    if (ID == vidChangeSensBySpeed) {

        ValueToControl();
//        return;

    };

    if (ID != vidDEBUG) {

        UnicodeString name;

        switch (ID) {
            case vidTVG       : { name = "ВРЧ    "; break; };
            case vidPrismDelay: { name = "2Тп    "; break; };
            case vidSens      : { name = "Ку     "; break; };
            case vidGain      : { name = "АТТ    "; break; };
            case vidStGate    : { name = "Нач Стр"; break; };
            case vidEdGate    : { name = "Кон Стр"; break; };
            case vidMark      : { name = "Отметка"; break; };
            case vidGateIdx   : { name = "Ном Стр"; break; };
            case vidAlarm     : { name = "АСД    "; break; }; // тип значения eGateAlarmMode
            case vidTestSens  : { name = "Ку OK ?"; break; }; // Соответствие условной чувствительности разрешенным пределам: 0 - соответствует; <> 0 - отклонение, не соответствует
            case vidStNotch   : { name = "StNotch"; break; }; // Начало полочки [мкс]
            case vidNotchLen  : { name = "NotchLn"; break; }; // Длина полочки [мкс]
            case vidNotchLevel: { name = "NotchLv"; break; }; // Уровень полочки [мкс]
            case vidDEBUG     : { name = " debug "; break; }; // для отладки
            case vidReturnSens: { name = "ВозврКу"; break; }; // для отладки
            case vidChangeSensBySpeed: { name = "Change Sens By Speed"; break; }; // для отладки
        }

        if (ID == vidReturnSens) ValueToControl();

        std::string title;
        AutoconMain->Table->GetChannelTitle(Channel, title);
        UnicodeString * StrTitle = new UnicodeString(title.c_str());
//        CBLog->Lines->Add(Format("GroupIndex: %d, Side: %d, Channel: %s (0x%x), GateIndex: %d, [%s] - Value: %d", ARRAYOFCONST((GroupIndex, Side, *StrTitle, Channel, GateIndex, name, Value))));
        if (ID != vidChangeSensBySpeed)
            LogLines_->Insert(0, Format("GroupIndex: %d, Side: %d, Channel: %s (0x%x), GateIndex: %d, [%s] - Value: %d", ARRAYOFCONST((GroupIndex, Side, *StrTitle, Channel, GateIndex, name, Value))));
            else LogLines_->Insert(0, Format("%s Delta: %d", ARRAYOFCONST((name, Value))));

        delete StrTitle;
    }

    if (ID == vidDEBUG)
//        CBLog->Lines->Add(Format("DEBUG: %d", ARRAYOFCONST((Value))));
//        LogLines_->Insert(0, Format("DEBUG: %d", ARRAYOFCONST((Value))));
        LogLines_->Insert(0, Format("MaxAmp: %d (%d дБ), Att: %d, New Att: %d", ARRAYOFCONST((GroupIndex, Value, Channel, GateIndex))));

}

void __fastcall TMainForm::GroupListClick(TObject *Sender)
{
    if (GroupList->ItemIndex != - 1) {

        int ChGroup = (int)GroupList->Items->Objects[GroupList->ItemIndex];
        AutoconMain->Config->SetChannelGroup(ChGroup);
        AutoconMain->DEV->SetChannelGroup(ChGroup);
        AutoconMain->DEV->Update(true);
        SetChannelList();
        StrokeTableMemoChange(NULL);
    }
}

//---------------------------------------------------------------------------


void __fastcall TMainForm::BoltJntButtonClick(TObject *Sender)
{
    if (AutoconMain->DEV->GetControlMode() == cmNormal)   {

        int Delta = AddKuSpinEdit->Value;
        LogLines_->Insert(0, "Режим 'Болтовой Стык' - Включён");
        AutoconMain->DEV->SetControlMode(cmTestBoltJoint, &Delta);
        BoltJntButton->Caption = "[+] Болтовой Стык";
        LogLines_->Insert(0, "-------------------------------");
    }
    else
    {
        LogLines_->Insert(0, "Режим 'Болтовой Стык' - Выключен");
        AutoconMain->DEV->SetControlMode(cmNormal, NULL);
        BoltJntButton->Caption = "[-] Болтовой Стык";
        LogLines_->Insert(0, "-------------------------------");
    }
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MarkSpeedButtonClick(TObject *Sender)
{
    if (MarkSpeedButton->Down)  {
            AutoconMain->DEV->StartUseMark();
            AutoconMain->DEV->SetMarkWidth(10);
            MarkShape->Visible = true;
            MarkSpeedButton->Caption = "ВКЛ";
        }
        else
        {
            AutoconMain->DEV->StopUseMark();
            MarkShape->Visible = false;
            MarkSpeedButton->Caption = "ВЫКЛ";
        }
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PathSimButtonClick(TObject *Sender)
{
    PathSimButton->Tag = (int)(!((bool)PathSimButton->Tag));

    if (PathSimButton->Tag == 1) {
        PathSimButton->Caption = "[+] Имитатор ДП";

        AutoconMain->DEV->PathEncoderSim(true);
        AutoconMain->DEV->SetPathEncoderData(0, true, 0, true);
        MainPathEncoderType = 1;

    } else {
        PathSimButton->Caption = "[-] Имитатор ДП";

        AutoconMain->DEV->PathEncoderSim(false);
        AutoconMain->DEV->SetPathEncoderData(0, false, 0, true);
        MainPathEncoderType = 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TuningButtonClick(TObject *Sender)
{
    TuningButton->Tag = (int)(!((bool)TuningButton->Tag));

    if (TuningButton->Tag)
    {
        TuningButton->Caption = "[+] Настройка Каналов";
//        TuningButton->Font->Style = TuningButton->Font->Style + Set<fsBold>;
        AutoconMain->DEV->InitTuningGateList();
        AutoconMain->DEV->SetCalibrationMode(cmSens);
        AutoconMain->DEV->SetMode(dmCalibration);
        AutoconMain->DEV->Update(false);
        StrokeTableMemoChange(NULL);
        TuningPanel->Visible = true;
        TuningPanel2->Visible = true;
        BackBtnPanel->Top = 1240;
    }
    else
    {
        TuningButton->Caption = "[-] Настройка Каналов";
        AutoconMain->DEV->SetMode(dmSearch);
        AutoconMain->DEV->Update(false);
        StrokeTableMemoChange(NULL);
        TuningPanel->Visible = false;
        TuningPanel2->Visible = false;
        BackBtnPanel->Top = 1240;
    }
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RailTypeTuningButtonClick(TObject *Sender)
{
    pCalibrationToRailTypeResult Result;

    UnicodeString str;
    LogLines_->Insert(0, "Настройка на тип рельса");

    Result = AutoconMain->DEV->CalibrationToRailType();


    if ((!Result) || (Result->size() == 0))
    {
        LogLines_->Insert(0, "Ошибка");
    }
    else
        for (int i = 0; i < Result->size(); i++) {

           int s = (*Result)[i].Side; // Сторона
           int ch = (*Result)[i].id; // Канал
           int r = (*Result)[i].Result; // Результат
           int h = (*Result)[i].RailHeight; // Высота рельса мм
           str = str + Format("Side: %d; Ch: %x; H: %d; Res: %d \n", ARRAYOFCONST((s, ch, h, h)));
        }
    LogLines_->Insert(0, str);
    LogLines_->Insert(0, "--------");
//    ShowMessage(str);
    ValueToControl();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ClearCBLogButtonClick(TObject *Sender)
{
    CBLog->Clear();
    LogLines_->Clear();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::cbSwitchingChange(TObject *Sender)
{
    if (cbSwitching->ItemIndex == 0) AutoconMain->DEV->SetLeftSideSwitching(toCompleteControl);
        else AutoconMain->DEV->SetLeftSideSwitching(toScaner);
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbBScanDebugClick(TObject *Sender)
{
    WorkStatePanel->Visible = cbBScanDebug->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbViewBScanClick(TObject *Sender)
{
    BScanPanel->Visible = cbViewBScan->Checked;
    BScanPanel_Resize(NULL);
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::sbFirstGateModeClick(TObject *Sender)
{
    sbFirstGateMode->Tag++;
    if (sbFirstGateMode->Tag == 3) sbFirstGateMode->Tag = 0;

    AutoconMain->DEV->SetAlarmMode(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 1, (eGateAlarmMode)sbFirstGateMode->Tag);

    switch (sbFirstGateMode->Tag) {
        case amOneEcho : { sbFirstGateMode->Caption = "OneEcho"; break; }
        case amTwoEcho : { sbFirstGateMode->Caption = "TwoEcho"; break; }
        case amOff     : { sbFirstGateMode->Caption = "Off"; break; }
    }
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::sbSecondGateModeClick(TObject *Sender)
{
    sbSecondGateMode->Tag++;
    if (sbSecondGateMode->Tag == 3) sbSecondGateMode->Tag = 0;

    AutoconMain->DEV->SetAlarmMode(AutoconMain->DEV->GetSide(), AutoconMain->DEV->GetChannel(), 2, (eGateAlarmMode)sbSecondGateMode->Tag);

    switch (sbSecondGateMode->Tag) {
        case amOneEcho : { sbSecondGateMode->Caption = "OneEcho"; break; }
        case amTwoEcho : { sbSecondGateMode->Caption = "TwoEcho"; break; }
        case amOff     : { sbSecondGateMode->Caption = "Off"; break; }
    }
}

void __fastcall TMainForm::AutoPrismDelayClick(TObject *Sender)
{
/*
    float Res;
    AutoconMain->DEV->PrismDelayAutoCalibration(&Res);
    AutoPrismDelay->Caption = Format("%3.3f", ARRAYOFCONST((Res))); */
/*
    if (AutoconMain->DEV->PrismDelayAutoCalibration()) AutoPrismDelay->Caption = "OK"; else AutoPrismDelay->Caption = "Ошибка";
    ValueToControl();
*/


    float Res;
    if (AutoconMain->DEV->PrismDelayAutoCalibration(&Res)) AutoPrismDelay->Caption = Format("%3.3f", ARRAYOFCONST((Res))); else AutoPrismDelay->Caption = "Ошибка";
    ValueToControl();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button2Click(TObject *Sender)
{

    for (int i = 0; i < ChList->Items->Count; i++) {

        int temp_id = (int)ChList->Items->Objects[i];
        eDeviceSide s;
        if (temp_id > 9999) {
                    s = dsNone;
                    temp_id = temp_id - 10000;
               }
          else {
                    if (temp_id > 0) s = dsLeft; else s = dsRight;
                    temp_id = abs(temp_id);
                    temp_id = temp_id - 1;
               }

        AutoconMain->DEV->SetGateIndex(1);
        AutoconMain->DEV->SetSide(s);
        AutoconMain->DEV->SetChannel(temp_id);
        AutoconMain->DEV->Update(false);
        AutoconMain->DEV->SetPrismDelay(10 * StrToInt(PrismDelayEdit->Text));

    }
    AutoconMain->DEV->Update(false);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox7Click(TObject *Sender)
{
    BScanDataPanel->Visible = CheckBox7->Checked;
    Button34Click(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox8Click(TObject *Sender)
{
    CBLogPanel->Visible = CheckBox8->Checked;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Timer5Timer(TObject *Sender)
{

  if ((AutoconMain) && (AutoconMain->DEV) && (PageControl2->ActivePageIndex == 17)) {

      PathEncoderDebug->Cells[0][0] = IntToStr((int)AutoconMain->DEV->PathEncoderDebug[0][0].Simulator);
      PathEncoderDebug->Cells[0][1] = IntToStr(AutoconMain->DEV->PathEncoderDebug[0][0].XSysCrd);
      PathEncoderDebug->Cells[0][2] = IntToStr(AutoconMain->DEV->PathEncoderDebug[0][0].XDisCrd);

      PathEncoderDebug->Cells[1][0] = IntToStr((int)AutoconMain->DEV->PathEncoderDebug[1][0].Simulator);
      PathEncoderDebug->Cells[1][1] = IntToStr(AutoconMain->DEV->PathEncoderDebug[1][0].XSysCrd);
      PathEncoderDebug->Cells[1][2] = IntToStr(AutoconMain->DEV->PathEncoderDebug[1][0].XDisCrd);
  }



//    AlarmList->TopIndex = ChList->TopIndex;
               /*
    UnicodeString tmp;
    if (AutoconMain)
        if (AutoconMain->DEV)
            if (AutoconMain->DEV->UMUList[0])
            {

                MetallSensorInfo->Clear();
                tmp = "";
                for (int s_idx = 0; s_idx < AutoconMain->DEV->UMUList[0]->_common_state._bscan_session_id.size(); s_idx++)
                {
                    int i = AutoconMain->DEV->UMUList[0]->_common_state._bscan_session_id[s_idx];
                    tmp = tmp + IntToStr(i) + ", ";
                }
                MetallSensorInfo->Lines->Add(tmp);
            }    */
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BScanPanelResize(TObject *Sender)
{
    BScanPanel_Resize(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::btnSaveAscanClick(TObject *Sender)
{
    btnSaveAscan->Tag = (int)(!((bool)btnSaveAscan->Tag));
    if (btnSaveAscan->Tag == 1)
        btnSaveAscan->Caption = "[+] Запись А-развертки"; else
        {
            btnSaveAscan->Caption = "[-] Запись А-развертки";
            if (msSaveAscan) msSaveAscan->SaveToFile("AScanSave_" + IntToStr((int)GetTickCount()) + ".dat");
        }

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button4Click(TObject *Sender)
{
    Panel1->Width = 10;
    Button6->Visible = True;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button5Click(TObject *Sender)
{
    ChPanel_->Width = 10;
    Button8->Visible = True;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button6Click(TObject *Sender)
{
    Panel1->Width = 202;
    Button6->Visible = False;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button8Click(TObject *Sender)
{
    ChPanel_->Width = 180;
    Button8->Visible = False;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PauseButtonClick(TObject *Sender)
{
    PauseButton->Tag = (int)(!((bool)PauseButton->Tag));

    if (PauseButton->Tag)
    {
        PauseButton->Caption = "[+] Пауза";
        DEVMode = AutoconMain->DEV->GetMode();
        AutoconMain->DEV->SetMode(dmPaused);
        AutoconMain->DEV->Update(false);
    }
    else
    {
        PauseButton->Caption = "[-] Пауза";
        AutoconMain->DEV->SetMode(DEVMode);
        AutoconMain->DEV->Update(false);
    }
    ValueToControl();

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button1323Click(TObject *Sender)
{
    CheckListBox1->CheckAll(cbUnchecked, false, false);
    CheckListBox1Click(Sender);

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckListBox1Click(TObject *Sender)
{
    BS1->Visible = CheckListBox1->Checked[0];
    BS2->Visible = CheckListBox1->Checked[1];
    BS3->Visible = CheckListBox1->Checked[2];
    BS4->Visible = CheckListBox1->Checked[3];
    BS5->Visible = CheckListBox1->Checked[4];
    BS6->Visible = CheckListBox1->Checked[5];
    BS7->Visible = CheckListBox1->Checked[6];
    BS8->Visible = CheckListBox1->Checked[7];
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PSOnClick(TObject *Sender)
{
//    this->Caption = IntToStr(PageControl1->ActivePageIndex);

    switch (PageControl1->ActivePageIndex)
    {
        case 0: AutoconMain->DEV->PathEncoderSimScaner(true, StrToInt(SpinEdit2->Text), StrToInt(SpinEdit3->Text), StrToInt(SpinEdit4->Text)); break;
        case 1: AutoconMain->DEV->PathEncoderSimEx(cbPathSimIndex1->ItemIndex, StrToInt(SpinEdit1->Text), true); break;
        case 2: AutoconMain->DEV->PathEncoderSim(cbPathSimIndex2->ItemIndex, true); break;
    }
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::PSOffClick(TObject *Sender)
{
    switch (PageControl1->ActivePageIndex)
    {
        case 0: AutoconMain->DEV->PathEncoderSimScaner(false, StrToInt(SpinEdit2->Text), StrToInt(SpinEdit3->Text), StrToInt(SpinEdit4->Text)); break;
        case 1: AutoconMain->DEV->PathEncoderSimEx(cbPathSimIndex1->ItemIndex, StrToInt(SpinEdit1->Text), false); break;
        case 2: AutoconMain->DEV->PathEncoderSim(cbPathSimIndex2->ItemIndex, false); break;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AmpSeries_Pos_P_45Click(TObject *Sender)
{
    AutoconMain->DEV->SetPathEncoderData(ComboBox2->ItemIndex, ComboBox3->ItemIndex == 1, StrToInt(SpinEdit5->Text), RadioButton2->Checked);
    if (RadioButton2->Checked) MainPathEncoderIndex = ComboBox2->ItemIndex;
    MainPathEncoderType = ComboBox3->ItemIndex;
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::AC_On_ButtonClick(TObject *Sender)
{
    AutoconMain->DEV->SetAcousticContact(1, true);
    cbViewAC->Tag = 1;
    Calibrate_AC->Enabled = True;
    cbViewACState->Checked = False;
    cbViewAC->Checked = True;
    cbViewACTh->Checked = True;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
    AutoconMain->DEV->SetAcousticContact(2, true);
    cbViewAC->Tag = 2;
//    cbViewAC->Enabled = False;
//    cbViewACState->Enabled = True;
//    cbViewACTh->Enabled = False;
    Calibrate_AC->Enabled = True;
    cbViewACState->Checked = True;
    cbViewAC->Checked = False;
    cbViewACTh->Checked = False;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AC_Off_ButtonClick(TObject *Sender)
{
    AutoconMain->DEV->SetAcousticContact(1, false);
    cbViewAC->Checked = False;
    cbViewACState->Checked = False;
    cbViewACTh->Checked = False;
//    Calibrate_AC->Enabled = False;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbShowCIDClick(TObject *Sender)
{
    SetChannelList();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbViewACClick(TObject *Sender)
{
    ChList2->Visible = cbViewAC->Checked || cbViewACState->Checked || cbViewACTh->Checked;

    ChList2->Width = 0;
    if (cbViewAC->Checked) ChList2->Width = ChList2->Width + 80;
    if (cbViewACState->Checked) ChList2->Width = ChList2->Width + 22;
    if (cbViewACTh->Checked) ChList2->Width = ChList2->Width + 50;
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::SetStartACDelayClick(TObject *Sender)
{
//    AutoconMain->Config->ACStartDelay = StrToInt(seACStartDelay->Text);
//    AutoconMain->DEV->Update(true);

    AutoconMain->DEV->SetACControlATT(StrToInt(seACStartDelay->Text));
    StrokeTableMemoChange(NULL);

//    AutoconMain->DEV->SetAcousticContact(true);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::SpeedButton2Click(TObject *Sender)
{
/*    AutoconMain->DEV->UMUList[0]->DisableAScan();
    AutoconMain->DEV->UMUList[0]->DisableBScan();
    AutoconMain->DEV->UMUList[0]->DisableAlarm();
    AutoconMain->DEV->UMUList[0]->DisableACSumm();
  */
    MainMenuForm->ExitButtonClick(NULL);
}
//---------------------------------------------------------------------------


/*
    XPathStep: Integer;   // Шаг ДП по горизонтали (мм * 100)
    YPathStep: Integer;   // Шаг ДП по вертикали (мм * 100)
    StartY: Integer;      // Начальное положение сканирования по вертикали (мм)
    ScanWidth: Integer;   // Ширина (по длине) зоны сконирования в (мм)
    ScanHeight: Integer;  // Высота зоны сконирования в отсчетах ДП

    YStart: Integer; // Начальное положение сканирования по вертикали (мм)
    XScanStep: Integer; // Шаг ДП по горизонтали (мм * 100)
    YScanStep: Integer; // Шаг ДП по вертикали (мм * 100)
    XAccuracy: Integer; // Точность по горизонтали (мм * 100)
    YAccuracy: Integer; // Точность по вертикали (мм * 100)
    HalfScanWidth_mm: Integer; // Половина ширины зоны сканирования (мм)
    ScanHeight_mm: Integer; // Высота зоны сканирования (мм)

    ScanWidth: Integer; // Размер буффера сконирования по длине
    ScanHeight: Integer; // Размер буффера сконирования по высоте


      FScan[Y, X].MaxAmpl[BP11_BP23]:= 255;
      FScan[Y, X].MaxAmpl[BP12_BP22]:= 255;
      FScan[Y, X].MaxAmpl[BP13_BP21]:= 255;

    XAccuracy

    property Scan[XIdx, YIdx: Integer]: TScanDataItem_2 read GetScanItem write SetScanItem;

    // mm - милиметры: [ - HalfScanWidth_mm ... 0 ... + HalfScanWidth_mm ]
    // Idx - индекс массива: [ 0 ... ScanWidth - 1 ]
    // Sys - системная координата, отсчеты датчика пути: [ - ScanWidth / 2 ... 0 ... ScanWidth / 2 ]



*/
void __fastcall TMainForm::Calibrate_ACClick(TObject *Sender)
{
/*    if (cbViewAC->Tag == 1)
    {
        for (unsigned int J = 0; J < ChList->Count; J++) {
            ACTreshold[J] = (int)ChList2->Items->Objects[J] * (seACThAddVal->Value + 100) / 100;
        }
    }
    else
    if (cbViewAC->Tag == 2)
    { */
    //    AutoconMain->DEV->SetAcousticContactThreshold(StrToInt(seACThAddVal->Text));
//    }
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::UpdateLogTimer(TObject *Sender)
{
    //

    if (CBLog->Lines->Count != LogLines_->Count) CBLog->Lines->Assign(LogLines_);

//    CBLog->Lines->Add(Format("GroupIndex: %d, Side: %d, Channel: %s (0x%x), GateIndex: %d, [%s] - Value: %d", ARRAYOFCONST((GroupIndex, Side, *StrTitle, Channel, GateIndex, name, Value))));
//    LogLines_->Add(Format("GroupIndex: %d, Side: %d, Channel: %s (0x%x), GateIndex: %d, [%s] - Value: %d", ARRAYOFCONST((GroupIndex, Side, *StrTitle, Channel, GateIndex, name, Value))));

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::CBLogSwitcClick(TObject *Sender)
{
    if (CBLogSwitch->Tag == 0) {

        CBLogSwitch->Caption = "On";
        CBLogSwitch->Tag = 1;

    } else {

        CBLogSwitch->Caption = "Off";
        CBLogSwitch->Tag = 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BScanFiltrOn_Click(TObject *Sender)
{
    AutoconMain->DEV->EnableFiltration(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BScanFiltrOff_Click(TObject *Sender)
{
    AutoconMain->DEV->EnableFiltration(false);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::A31DeviceConfigClick(TObject *Sender)
{
	cDeviceConfig *tmp;

	tmp = AutoconMain->Config;
	AutoconMain->Config = AutoconMain->Config2;
	AutoconMain->Config2 = tmp;
//    ((pHeadScanParams)AutoconMain->Config2->Params)->VerticalStartPos
//    ((pHeadScanParams)AutoconMain->Config2->Params)->ScanLength
/*    AutoconMain->DEV->UMUList[0]->DisableAScan();
	AutoconMain->DEV->UMUList[0]->DisableBScan();
	AutoconMain->DEV->UMUList[0]->DisableAlarm();
	AutoconMain->DEV->UMUList[0]->DisableACSumm(); */
	AutoconMain->DEV->ChangeDeviceConfig(AutoconMain->Config);

	String dn=AnsiString(AutoconMain->Config->GetDeviceName());

	if (dn.Compare("Avicon-31") == 0) {

		AutoconMain->DEV->SetLeftSideSwitching(toCompleteControl);
		AutoconMain->DEV->EnableAll();


//		Button127Click(NULL);
	}
	else
	{
		AutoconMain->DEV->SetLeftSideSwitching(toScaner);
		AutoconMain->DEV->SetPathEncoderData(static_cast<char>(AutoconMain->Config->getMainPathEncoderIndex()), false, 0, true);
	}

  /*
	MainPathEncoderIndex = AutoconMain->Config->MainPathEncoderIndex;
	MainPathEncoderType = 0;
	*/

	FormShow(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::HSDeviceConfigClick(TObject *Sender)
{
//    AutoconMain->DEV->ChangeDeviceConfig(AutoconMain->Config2);
//    FormShow(NULL);
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::TestClick(TObject *Sender)
{
    AutoconMain->DEV->MakeTestSensCallBackForAllChannel();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::BScan6DBClick(TObject *Sender)
{
    AutoconMain->DEV->SetBScanGateLevel(16);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::BScan12DBClick(TObject *Sender)
{
    AutoconMain->DEV->SetBScanGateLevel(8);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RrsButton7Click(TObject *Sender)
{
    for (int i = 0; i < 1000; i++) {
        ACMinMas[i] = 66000;
        ACMaxMas[i] = 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button7Click(TObject *Sender)
{
    CalMemo->Lines->Clear();
    CalMemo->Lines->Add(Format("StGate %d", ARRAYOFCONST((AutoconMain->DEV->GetStGate()))));
    CalMemo->Lines->Add(Format("EdGate %d", ARRAYOFCONST((AutoconMain->DEV->GetEdGate()))));
    CalMemo->Lines->Add(Format("Gain %d", ARRAYOFCONST((AutoconMain->DEV->GetGain()))));

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SmallerClick(TObject *Sender)
{
    Mouse->CursorPos.Y = Mouse->CursorPos.Y + (int)((float)CBLogPanel->Height * 0.1);
    CBLogPanel->Height = (int)((float)CBLogPanel->Height * 1.1);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BiggerClick(TObject *Sender)
{
    Mouse->CursorPos.Y = Mouse->CursorPos.Y - (int)((float)CBLogPanel->Height * 0.1);
    CBLogPanel->Height = (int)((float)CBLogPanel->Height * 1.1);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AkkButton_Click(TObject *Sender)
{
    AutoconMain->DEV->GetUAkkQuery();
//    StatusLabel->Caption = IntToStr( (int)AutoconMain->DEV->GetUMUOnLineStatus(0) );
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AutoCal_EnableClick(TObject *Sender)
{
    AutoconMain->DEV->EnableSMChSensAutoCalibration(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AutoCal_DisableClick(TObject *Sender)
{
    AutoconMain->DEV->EnableSMChSensAutoCalibration(false);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button10Click(TObject *Sender)
{
    Main_PE->Clear();
    HeadScaner_PE->Clear();
    HandScan_PE->Clear();
    LineSeries1->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StrokeTableMemoChange(TObject *Sender)
{
    StrokeTableMemo->Clear();
    /*
    StrokeTableMemo->Lines->Add("Т|Б|С|Л|П|Г|    |К     |Д");
    StrokeTableMemo->Lines->Add("а|У|т|и|р|е|    |а     |л");
    StrokeTableMemo->Lines->Add("к|М|о|н|и|н|    |н     |и");
    StrokeTableMemo->Lines->Add("т| |р|и|е|е|    |а     |т");
    StrokeTableMemo->Lines->Add(" | |о|я|м|р|    |л     |е");
    StrokeTableMemo->Lines->Add(" | |н| |н|.|    |      |л");
    StrokeTableMemo->Lines->Add(" | |.| |.| |    |      |ь");
*/
    StrokeTableMemo->Lines->Add("Т|Б|С|Л|П|Г|    |Д  |К     ");
    StrokeTableMemo->Lines->Add("а|У|т|и|р|е|    |л  |а     ");
    StrokeTableMemo->Lines->Add("к|М|о|н|и|н|    |и  |н     ");
    StrokeTableMemo->Lines->Add("т| |р|и|е|е|    |т  |а     ");
    StrokeTableMemo->Lines->Add(" | |о|я|м|р|    |е  |л     ");
    StrokeTableMemo->Lines->Add(" | |н| |н|.|    |л  |      ");
    StrokeTableMemo->Lines->Add(" | |.| |.| |    |ь  |      ");
//    if (!RadioButton4->Checked) {

        eDeviceSide DEV_Side;
        CID DEV_Channel;
        UnicodeString str;
        int SummLen = 0;

        for (int stroke = 0; stroke < AutoconMain->Config->GetScanStrokeCount(); stroke++)
        {
          int strokeLen = 0;
          for (int UMUIndex = 0; UMUIndex < AutoconMain->Config->GetUMUCount(); UMUIndex++)
            for (int side = 0; side < 2; side++)
              for (int line = 0; line < 2; line++)
                  {

                    str = "";
                    for (unsigned int i = 0; i < AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()].size(); i++)
                        if ((UMUIndex == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUIndex) &&
                            (side == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUSide) &&
                            (line == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMULine) &&
                            (stroke == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUStroke)) {

                        DEV_Side = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVSide;
                        DEV_Channel = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVChannel;
                        sChannelDescription ChanDesc;
                        AutoconMain->Table->ItemByCID(DEV_Channel, &ChanDesc);
                        str = "";
                        UnicodeString l = IntToStr(ChanDesc.cdStrokeDuration);
                        while (l.Length() < 3) l = " " + l;
                        str = str + l + " " + ChanDesc.Title.c_str();

                        if (ChanDesc.cdStrokeDuration > strokeLen) strokeLen = ChanDesc.cdStrokeDuration;
                        break;
                    }

                    unsigned char tmp = AutoconMain->DEV->UMUGenResList[UMUIndex].List[side][line][stroke];
                    UnicodeString Lstr;
                    if (line == 0) { Lstr = " L"; }
                              else { Lstr = " R"; }
                    UnicodeString GENstr = "I" + IntToStr(((tmp >> 4) & 0x0F) + 1) + Lstr;
                    if (((tmp >> 4) & 0x0F) == 0x0F) { GENstr = "ВЫКЛ"; }

                    StrokeTableMemo->Lines->Add(Format("%d %d %d %d %x %x %s %s", ARRAYOFCONST((stroke + (int)RadioButton4->Checked, UMUIndex + (int)RadioButton4->Checked, side + (int)RadioButton4->Checked, line + (int)RadioButton4->Checked, (tmp & 0x0F) + (int)RadioButton4->Checked, ((tmp >> 4) & 0x0F) + (int)RadioButton4->Checked, GENstr, str))));
                  }
          SummLen = SummLen + strokeLen;
        }
        StrokeTableMemo->Lines->Add(Format("Длительность цикла: %d", ARRAYOFCONST((SummLen))));
 /*   }
    else
{
//        StrokeTableMemo->Clear();
//        StrokeTableMemo->Lines->Add("Такт;Сторона;Линия;Приемник;Генератор;Канал;Длительность;В строб");

//        int UMUIndex = 0;
        eDeviceSide DEV_Side;
        CID DEV_Channel;
        int SummLen = 0;

        for (int UMUIndex = 0; UMUIndex < AutoconMain->Config->GetUMUCount() - 1; UMUIndex++)
            for (int stroke = 0; stroke < AutoconMain->DEV->UMUGenResList[UMUIndex].StrokeCount; stroke++)
              for (int side_ = 0; side_ < 2; side_++)
                for (int line = 0; line < 2; line++)
                  {

                    eUMUSide side;
                    if (side_ == 0) side = usLeft; else side = usRight;

                    UnicodeString CHstr;
                    for (unsigned int i = 0; i < AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()].size(); i++)
                        if (//(UMUIndex == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUIndex) &&
                            (side == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUSide) &&
                            (line == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMULine) &&
                            (stroke == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUStroke)) {

                        DEV_Side = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVSide;
                        DEV_Channel = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVChannel;
                        sChannelDescription ChanDesc;
                        AutoconMain->Table->ItemByCID(DEV_Channel, &ChanDesc);

                        CHstr = ChanDesc.Title;
                        if ((side == 0) && (line == 0)) SummLen = SummLen + ChanDesc.cdStrokeDuration;
                        break;
                    }

                    UnicodeString Rstr;
                    if (side == usLeft) { Rstr = "Левая "; }
                                   else { Rstr = "Правая"; }
                    UnicodeString Lstr;
                    if (line == 0) { Lstr = "L"; }
                              else { Lstr = "R"; }
                    unsigned char tmp = AutoconMain->DEV->UMUGenResList[UMUIndex].List[side][line][stroke];
                    UnicodeString GENstr = "I" + IntToStr(((tmp >> 4) & 0x0F) + 1) + Lstr;
                    if (((tmp >> 4) & 0x0F) == 0x0F) { GENstr = "ВЫКЛ"; }
                    StrokeTableMemo->Lines->Add(Format("%d; %d; %s; %s; %s; P%x%s;", ARRAYOFCONST((UMUIndex, stroke + 1, Rstr, CHstr, GENstr, (tmp & 0x0F) + 1, Lstr))));
                  }

        StrokeTableMemo->Lines->Add(Format("Длительность цикла: %d", ARRAYOFCONST((SummLen))));
    }
    */
/*
    StrokeTableMemo->Clear();
    StrokeTableMemo->Lines->Add("Такт");
    StrokeTableMemo->Lines->Add("   Сторона");
    StrokeTableMemo->Lines->Add("      Линия");
    StrokeTableMemo->Lines->Add("         Приемник");
    StrokeTableMemo->Lines->Add("            Генератор");

    int UMUIndex = 0;
    eDeviceSide DEV_Side;
    CID DEV_Channel;
    UnicodeString str;
    int SummLen = 0;

    for (int stroke = 0; stroke < AutoconMain->DEV->UMUStrokeCount[UMUIndex]; stroke++)
      for (int side = 0; side < 2; side++)
        for (int line = 0; line < 2; line++)
          {

            str = "";
            for (unsigned int i = 0; i < AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()].size(); i++)
                if ((side == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUSide) &&
                    (line == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMULine) &&
                    (stroke == AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].UMUStroke)) {

                DEV_Side = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVSide;
                DEV_Channel = AutoconMain->DEV->UMUtoDEVList[AutoconMain->DEV->GetChannelType()][i].DEVChannel;
                sChannelDescription ChanDesc;
                AutoconMain->Table->ItemByCID(DEV_Channel, &ChanDesc);

                if (DEV_Side == dsLeft) { str = "Левая: "; }
                                   else { str = "Правая: "; }

                str = str + ChanDesc.Title + " " + IntToStr(ChanDesc.cdStrokeDuration) + " мкс";

                if ((side == 0) && (line == 0)) SummLen = SummLen + ChanDesc.cdStrokeDuration;
                break;
            }

            unsigned char tmp = AutoconMain->DEV->UMUGenResList[UMUIndex][side][line][stroke];
            StrokeTableMemo->Lines->Add(Format("%d %d %d %x %x - %s", ARRAYOFCONST((stroke, side + (int)RadioButton4->Checked, line + (int)RadioButton4->Checked, (tmp & 0x0F) + (int)RadioButton4->Checked, ((tmp >> 4) & 0x0F) + (int)RadioButton4->Checked, str))));
          }

    StrokeTableMemo->Lines->Add(Format("Длительность цикла: %d", ARRAYOFCONST((SummLen))));
 */
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::RadioButton3Click(TObject *Sender)
{
    StrokeTableMemoChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TabSheet19Show(TObject *Sender)
{
    StrokeTableMemoChange(NULL);
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown8ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction)
{
    AutoconMain->Config->SetGenerator(usLeft, ulRU1, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown10ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetGenerator(usLeft, ulRU2, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown12ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetGenerator(usRight, ulRU1, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown14ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetGenerator(usRight, ulRU2, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown9ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction)
{
    AutoconMain->Config->SetReceiver(usLeft, ulRU1, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown11ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetReceiver(usLeft, ulRU2, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown13ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetReceiver(usRight, ulRU1, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UpDown15ChangingEx(TObject *Sender, bool &AllowChange,
          int NewValue, TUpDownDirection Direction)
{
    AutoconMain->Config->SetReceiver(usRight, ulRU2, NewValue);
    AutoconMain->DEV->Update(true);
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::Button13__Click(TObject *Sender)
{
	AutoconMain->DEV->EnableAll();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TestButtonClick(TObject *Sender)
{
	AutoconMain->DEV->DisableAll();
    while (!AutoconMain->DEV->TestDisableAll()) Application->ProcessMessages();
    while (AutoconMain->devt->Ptr_List.size() != 0) Application->ProcessMessages();

    Series7->X0 = Main_PE->MaxXValue();
    Series7->X1 = Main_PE->MaxXValue();
    Series7->Y0 = - 100;
    Series7->Y1 = Main_PE->MaxYValue() + 100;

    AutoconMain->DEV->SetPathEncoderData(0, 0, 0, False);
/*	AutoconMain->DEV->EnableAll(); */
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::TestButton3Click(TObject *Sender)
{
    TestButton3->Caption = IntToStr( AutoconMain->DEV->GetScanZeroProbeAmpl() );
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::WorkButtonClick(TObject *Sender)
{
    AutoconMain->DEV->SetFaceData(LeftSideTrolley_WorkSide);
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::UnWorkButtonClick(TObject *Sender)
{
    AutoconMain->DEV->SetFaceData(LeftSideTrolley_UnWorkSide);
    ValueToControl();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button12Click(TObject *Sender)
{
/*    AutoconMain->DEV->DisableAll();
    while (!AutoconMain->DEV->TestDisableAll()) { Sleep(1); }
    AutoconMain->DEV->ResetPathEncoder();

    while (AutoconMain->devt->Ptr_List.size() != 0) { DrawThreadTimerTimer(0); Sleep(1); }
    Button10Click(0);
//    AutoconMain->DEV->EnableAll(); */
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button1811Click(TObject *Sender)
{
//    AutoconMain->DEV->EnableAll();
}
//---------------------------------------------------------------------------

bool getFirstScanChannelDescriptionInScheme(sScanChannelDescription & scanChanDesc, int schemeNumber)
{


    unsigned int scanChannelsCount = AutoconMain->Config->GetAllScanChannelsCount();
    if (AutoconMain->Config->getControlledRail() == crBoth) {
        scanChannelsCount *= 2;
    }

    int strokeGroupIdx = AutoconMain->Config->GetChannelGroup(schemeNumber);
    unsigned int idx;
    for (idx = 0; idx < scanChannelsCount; ++idx) {
        if (AutoconMain->Config->getSChannelbyIdx(static_cast<int>(idx), &scanChanDesc) == false) {
            continue;
        }

        if (strokeGroupIdx == scanChanDesc.StrokeGroupIdx) {
            break;
        }
    }
    if (idx < scanChannelsCount) {
        return true;
    }
    return false;
}

void __fastcall TMainForm::Button_17Click(TObject *Sender)
{

    sScanChannelDescription scanChanDesc;

    Panel78->Caption = IntToStr(static_cast<int>(getFirstScanChannelDescriptionInScheme(scanChanDesc, ComboBox4->ItemIndex)));

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbNoiseLogClick(TObject *Sender)
{
    NoiseTick = - 1;
    NoiseStartTick = GetTickCount();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ACChartClearButtonClick(TObject *Sender)
{
   FastLineSeries1->Clear();
   FastLineSeries2->Clear();
   FastLineSeries3->Clear();
   FastLineSeries4->Clear();
   FastLineSeries5->Clear();
   FastLineSeries6->Clear();
   FastLineSeries7->Clear();
   FastLineSeries8->Clear();
   FastLineSeries9->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::GenOffButtonClick(TObject *Sender)
{
    AutoconMain->Config->GeneratorOff = true;
    AutoconMain->DEV->Update(true, true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::GenOnButtonClick(TObject *Sender)
{
    AutoconMain->Config->GeneratorOff = false;
    AutoconMain->DEV->Update(true, true);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::AC_TabSheetResize(TObject *Sender)
{
    Chart4->Height = AC_TabSheet->Height / 9;
    Chart5->Height = AC_TabSheet->Height / 9;
    Chart6->Height = AC_TabSheet->Height / 9;
    Chart7->Height = AC_TabSheet->Height / 9;
    Chart8->Height = AC_TabSheet->Height / 9;
    Chart9->Height = AC_TabSheet->Height / 9;
    Chart10->Height = AC_TabSheet->Height / 9;
    Chart11->Height = AC_TabSheet->Height / 9;
    Chart12->Height = AC_TabSheet->Height / 9;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox2Click(TObject *Sender)
{
    AutoconMain->DEV->SetTimeToFixBadSens(SpinEdit6->Value);
    AutoconMain->DEV->SetFixBadSensState(CheckBox2->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SpinEdit6Change(TObject *Sender)
{
    AutoconMain->DEV->SetTimeToFixBadSens(SpinEdit6->Value);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::tsAKTestResize(TObject *Sender)
{
    if (ACBuffer != NULL) delete ACBuffer;

    ACBuffer = new TBitmap;
    ACBuffer->SetSize(PaintBox1->Width, PaintBox1->Height);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button18Click(TObject *Sender)
{
    AutoconMain->DEV->SetAcousticContact(1, true);
    cbViewAC->Tag = 1;
    Calibrate_AC->Enabled = True;
    cbViewACState->Checked = False;
    cbViewAC->Checked = True;
    cbViewACTh->Checked = True;

    Button117->Click();
    Button21->Click();
    Button5->Click();
    Button4->Click();
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button117Click(TObject *Sender)
{
    AutoconMain->DEV->SetACControlATT(StrToInt(Edit9->Text));
    StrokeTableMemoChange(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button21Click(TObject *Sender)
{
//   AutoconMain->DEV->SetAcousticContactThreshold(StrToInt(Edit10->Text));
   AutoconMain->DEV->SetAcousticContactThreshold_(StrToInt(Edit10->Text), StrToInt(Edit11->Text));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button137Click(TObject *Sender)
{
    Button8->Click();
    Button6->Click();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Add(unsigned char id, UnicodeString Text)
{
    unsigned int Count = AutoconMain->DEV->UMUList[0]->_common_state._event_count[id];
    Memo4->Lines->Add(Format(Text +  " - кол-во: %d; частота %d [ед./сек]", ARRAYOFCONST((Count, Count - EventCounts_[id]))));
    EventCounts_[id] = Count;
}

void __fastcall TMainForm::UMUEventTimerTimer(TObject *Sender)
{
    Memo4->Lines->Clear();

    unsigned int Count;
    Add(dAScanDataHead, "Данные А-развертки");
    Add(dAScanMeas, "А-развертка, точное значение амплитуды и задержки");
    Add(dBScan2Data, "Данные B-развертки");
    Add(dAlarmData, "Данные АСД-развертки");
    Add(dPathStep, "Данные датчика пути ");
/*
    Memo4->Lines->Add(Format("А-развертка, точное значение амплитуды и задержки %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dAScanMeas]))));
    Memo4->Lines->Add(Format("Данные В-развертки %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dBScan2Data]))));
    Memo4->Lines->Add(Format("Данные АСД %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dAlarmData]))));
    Memo4->Lines->Add(Format("Данные датчика пути %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dPathStep]))));
    Memo4->Lines->Add(Format("Напряжение аккумулятора НА БУМ %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dVoltage]))));
    Memo4->Lines->Add(Format("Время работы БУМ %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dWorkTime]))));
    Memo4->Lines->Add(Format("Данные акустического контакта %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._event_count[dAcousticContact_]))));
    Memo4->Lines->Add("");
    Memo4->Lines->Add(Format("Количество ОШИБОК %d", ARRAYOFCONST((AutoconMain->DEV->UMUList[0]->_common_state._error_message_count))));
*/
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::SpinEdit7Change(TObject *Sender)
{
  bsl->MaxACSumm = SpinEdit7->Value;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button17Click(TObject *Sender)
{
    for (int Gain = 0; Gain <= 80; Gain++)
    {
        GainArrMemo->Lines->Add(IntToStr(Gain) + " - " + IntToStr(AutoconMain->Config->dBGain_to_UMUGain(Gain)));
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::TabSheet25Resize(TObject *Sender)
{
    if (BSBuffer != NULL) delete BSBuffer;

    BSBuffer = new TBitmap;
    BSBuffer->SetSize(PaintBox2->Width, PaintBox2->Height);
}

//---------------------------------------------------------------------------

void __fastcall TMainForm::TabSheet25Show(TObject *Sender)
{
  TabSheet25Resize(0);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button22Click(TObject *Sender)
{
    memset(&PathStepGis[0], 0, 80);
    Series8->Clear();
//    for (int Gain = 0; Gain < 20; Gain++) Series8->Add(0);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::MScanerResetButtonClick(TObject *Sender)
{
    Series12->Clear();
    StartScanerCursorPos = Mouse->CursorPos;
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::Panel48Resize(TObject *Sender)
{
    if (BSDebugBuffer != NULL) delete BSDebugBuffer;

    BSDebugBuffer = new TBitmap;
    BSDebugBuffer->SetSize(Panel48->Width, Panel48->Height);


}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox3Click(TObject *Sender)
{
    AutoconMain->DEV->SkipSetStrokeTableByChannelSelect = CheckBox3->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PathSimButton2Click(TObject *Sender)
{
    PathSimButton2->Tag = (int)(!((bool)PathSimButton2->Tag));

    if (PathSimButton2->Tag == 1) {
        PathSimButton2->Caption = "[+] Им. ДП";

        AutoconMain->DEV->setUsePathEncoderSimulationinSearchMode(true);
//        MainPathEncoderType = 1;

    } else {
        PathSimButton2->Caption = "[-] Им. ДП";

        AutoconMain->DEV->PathEncoderSim(false);
        AutoconMain->DEV->setUsePathEncoderSimulationinSearchMode(false);
//        MainPathEncoderType = 0;
    }

}
//---------------------------------------------------------------------------


void __fastcall TMainForm::Button24Click(TObject *Sender)
{
    LineSeries5->Clear();
    LineSeries6->Clear();
    LineSeries7->Clear();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::CheckBox5Click(TObject *Sender)
{
    if (CheckBox5->Checked) {
      CheckBox9->Checked = false;
      CheckBox10->Checked = false;
      CheckBox11->Checked = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox9Click(TObject *Sender)
{
    if (CheckBox9->Checked) {
        CheckBox5->Checked = false;
        CheckBox10->Checked = false;
        CheckBox11->Checked = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox10Click(TObject *Sender)
{
    if (CheckBox10->Checked) {

        CheckBox5->Checked = false;
        CheckBox9->Checked = false;
        CheckBox11->Checked = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CheckBox11Click(TObject *Sender)
{
    if (CheckBox11->Checked) {

        CheckBox5->Checked = false;
        CheckBox9->Checked = false;
        CheckBox10->Checked = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button28Click(TObject *Sender)
{
    AutoconMain->DEV->ManageSensBySpeed(true, StrToFloat(PathStepEdit->Text));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::Button29Click(TObject *Sender)
{
    AutoconMain->DEV->ManageSensBySpeed(false, StrToFloat(PathStepEdit->Text));
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::cbTrackingRailTypeClick(TObject *Sender)
{
	AutoconMain->DEV->SetRailTypeTrackingMode(cbTrackingRailType->Checked);
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::PageControl2Change(TObject *Sender)
{
	if (ACConfig) {

		ACConfig->ActivePage = PageControl2->ActivePageIndex;
	}
}
//---------------------------------------------------------------------------

