//----------------------------------------------------------------
#include <stddef.h>
//----------------------------------------------------------------
#ifndef UMUTest_DataStorageH
#define UMUTest_DataStorageH

#include <jpeg.hpp>
#include "Definitions.h"
#include "System.ZLib.hpp"
#include "vector"
#include "prot_umu_lan.h"
//Added by KirillB
#include "UMUTest_Utils.h"

//Modifyed by KirillB (fix?)
//const ScanChannelCount = 84;
const ScanChannelCount = 90;
const OneCrdOneChannelSignalsCount = 8;
const NewItemListSize = 5000;

struct sScanChannelParams       // ��������� ������ ��������
{
	int Sens;                 // �������� ���������������� [��]
	int Gain;                 // ���������� [��]
	int StGate;               // ������ ����� ��� [���]
	int EdGate;               // ����� ������ ��� [���]
	int PrismDelay;           // ����� � ������ [10 * ���]
	int TVG;                  // ��� [���]
};

struct sJointTestingReportHeader_Version_01   // ��������� ��������� �������� �������� �����
{
						 // ------------- ��������


    int FileType;               // ��� ������: 1 - ��������; 2 - ����
	TDateTime DateTime;     	// ���� / ����� ��������
	int	GangNumber;				// ����� �����
	int	ReportNumber;	 	    // ����� ��������� ��������
	int PletNumber;        	    // ����� �����
	int JointNumber;    	    // ����� �����
	float TopStraightness; 		// ��������������� ������ [��]
	float SideStraightness;		// ��������������� ����� [��]
	int	Hardness; 				// ��������
	int	HardnessUnit; 			// ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

						 // ------------- ��������� ��������

	int CentrSysCoord; 			// �����, ��������� �����
	int MaxSysCoord;   			// ������������ ����������
	int MaxChannel;    			// ���������� ������� ������������

	sScanChannelParams ChannelParams[ScanChannelCount]; // ��������� ������� ��������

	int END_;    		 // ������ ��������� ������

						 // ------------- ��������� ����

	UnicodeString Plaint;   	  // �����������
	UnicodeString PlantNumber;    // ��������� ����� ���������
	UnicodeString VerSoft;		  // ������ ��
	UnicodeString Operator; 	  // �������� ���
	UnicodeString DefectCode;     // ��� �������
	UnicodeString ValidityGroup;  // ��. ��������

};

enum HardnessPoint
{
    hpCentrePoint1 = 0,
    hpCentrePoint2 = 1,
    hpCentrePoint3 = 2,
    hpLeftPoint1 = 3,
    hpLeftPoint2 = 4,
    hpLeftPoint3 = 5,
    hpRightPoint1 = 6,
    hpRightPoint2 = 7,
    hpRightPoint3 = 8
};

struct sJointTestingReportHeader_Version_02   // ��������� ��������� �������� �������� �����
{
						 // ------------- ��������

    int FileType;               // ��� ������: 1 - ��������; 2 - ����
	TDateTime DateTime;     	// ���� / ����� ��������
	int	GangNumber;				// ����� �����
	int	ReportNumber;	 	    // ����� ��������� ��������
	int PletNumber;        	    // ����� �����
	int JointNumber;    	    // ����� �����
	float TopStraightness; 		// ��������������� ������ [��]
	float SideStraightness;		// ��������������� ����� [��]
	int	HardnessState;          // ��������: 0 - �� ����������, 1 - ����������
	int	HardnessUnit_Old; 		// ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

						 // ------------- ��������� ��������

	int CentrSysCoord; 			// �����, ��������� �����
	int MaxSysCoord;   			// ������������ ����������
	int MaxChannel;    			// ���������� ������� ������������

	sScanChannelParams ChannelParams[ScanChannelCount]; // ��������� ������� ��������

    int	Hardness[9];            // ��������

	int END_;    		 // ������ ��������� ������

						 // ------------- ��������� ����

	UnicodeString Plaint;   	  // �����������
	UnicodeString PlantNumber;    // ��������� ����� ���������
	UnicodeString VerSoft;		  // ������ ��
	UnicodeString Operator; 	  // �������� ���
	UnicodeString DefectCode;     // ��� �������
	UnicodeString ValidityGroup;  // ��. ��������
	UnicodeString �onclusion;     // ����������

};

struct sJointTestingReportHeader_Version_03   // ��������� ��������� �������� �������� �����
{
						 // ------------- ��������

    int FileType;               // ��� ������: 1 - ��������; 2 - ����; 3 - ���������; 4 - �������� ������ �����;
	TDateTime DateTime;     	// ���� / ����� ��������
	int	GangNumber;				// ����� �����
	int	ReportNumber;	 	    // ����� ��������� ��������
	int PletNumber;        	    // ����� �����
	int JointNumber;    	    // ����� �����
	float TopStraightness; 		// ��������������� ������ [��]
	float SideStraightness;		// ��������������� ����� [��]
	int	HardnessState;          // ��������: 0 - �� ����������, 1 - ����������
	int	HardnessUnit_Old; 		// ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

						 // ------------- ��������� ��������

	int CentrSysCoord; 			// �����, ��������� �����
	int MaxSysCoord;   			// ������������ ����������
	int MaxChannel;    			// ���������� ������� ������������

	sScanChannelParams ChannelParams[ScanChannelCount]; // ��������� ������� ��������

    int	Hardness[9];            // ��������
    int ScanChannelCount;       // ���������� ������� �������� (������������)

	int END_;    		        // ������ ��������� ������

						 // ------------- ��������� ����

	UnicodeString Plaint;   	  // �����������
	UnicodeString PlantNumber;    // ��������� ����� ���������
	UnicodeString VerSoft;		  // ������ ��
	UnicodeString Operator; 	  // �������� ���
	UnicodeString DefectCode;     // ��� �������
	UnicodeString ValidityGroup;  // ��. ��������
	UnicodeString �onclusion;     // ����������

};

struct sJointTestingReportHeader_Version_04   // ��������� ��������� �������� �������� �����
{
						 // ------------- ��������

    int FileType;               // ��� ������: 1 - ��������; 2 - ����; 3 - ���������; 4 - �������� ������ �����;
	TDateTime DateTime;     	// ���� / ����� ��������
	int	GangNumber;				// ����� �����
	int	ReportNumber;	 	    // ����� ��������� ��������
	int PletNumber;        	    // ����� �����
	int JointNumber;    	    // ����� �����
	float TopStraightness; 		// ��������������� ������ [��]
	float SideStraightness;		// ��������������� ����� [��]
	int	HardnessState;          // ��������: 0 - �� ����������, 1 - ����������
	int	HardnessUnit_Old; 		// ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

						 // ------------- ��������� ��������

	int CentrSysCoord; 			// �����, ��������� �����
	int MaxSysCoord;   			// ������������ ����������
	int MaxChannel;    			// ���������� ������� ������������

	sScanChannelParams ChannelParams[ScanChannelCount]; // ��������� ������� ��������

    int	Hardness[9];            // ��������
    int NotUse;                 // ���� - ���������� ������� �������� (������������) ????
    float TimeInHandScanChannel[10]; // ����� ������ � ������ ������ ��������

	int END_;    		        // ������ ��������� ������

						 // ------------- ��������� ����

	UnicodeString Plaint;   	  // �����������
	UnicodeString PlantNumber;    // ��������� ����� ���������
	UnicodeString VerSoft;		  // ������ ��
	UnicodeString Operator; 	  // �������� ���
	UnicodeString DefectCode;     // ��� �������
	UnicodeString ValidityGroup;  // ��. ��������
	UnicodeString �onclusion;     // ����������

};

struct sSystemCheckReportHeader   // ��������� ��������� ������������ ����������������� ���������
{
	UnicodeString Operator; // �������� ���
	TDateTime DateTime;     // ���� ����� ��������
	// .....
};

struct sScanSignalsOneCrdOneChannel // ������� ������ ������ ��������������� �� ����� ���������� ��� ������������
{
	char Count;                                              // ���������� ��������
	tUMU_BScanSignal Signals[OneCrdOneChannelSignalsCount];  // �������
    unsigned short WorkCycle;
};

struct sScanSignalsOneCrdAllChannel
{
	sScanSignalsOneCrdOneChannel Channel[ScanChannelCount];
};

typedef sScanSignalsOneCrdAllChannel* PsScanSignalsOneCrdAllChannel;

typedef sScanSignalsOneCrdOneChannel* PsScanSignalsOneCrdOneChannel;


struct sHandSignalsOneCrd // ������� ������ ������ ��������������� �� ����� ���������� ��� ������ ��������
{
	int SysCoord;           // ����������
	char Count;             // ���������� ��������
	tUMU_BScanSignal Signals[OneCrdOneChannelSignalsCount];  // �������
};

typedef sHandSignalsOneCrd* PsHandSignalsOneCrd;

struct sHandScanParams          // ��������� ������ ������� ��������
{
	int EnterAngle;           // ���� �����
	eInspectionMethod Method; // ����� ��������
	int ScanSurface;          // �������������� �����������
	int Ku;                   // �������� ���������������� [��]
	int Att;                  // ���������� [��]
	int StGate;               // ������ ����� ��� [���]
	int EdGate;               // ����� ������ ��� [���]
	int PrismDelay;           // ����� � ������ [10 * ���]
	int TVG;                  // ��� [���]
};

struct sHandSignalsRecord
{
	sHandScanParams Params;
	std::vector < sHandSignalsOneCrd > BScanData;
};

struct sAlarmRecord
{
	sAlarmRecord()
	{
		Side = dsNone;
		Channel = 0;
        memset(State, false, 4 * sizeof( bool ));
	};

	eDeviceSide Side; // ������� ������������
	CID Channel;      // �����
	bool State[4];    // [����� ������]
};

// ����

// # .pragma pack (1)
#pragma pack(push,1)

struct sFileHeaderVersion_02  // ��������� ���������� �����
{
	char FileID[16];          // �������������
	int Version;              // ������ ������� �����

	int HeaderDataOffset;     // ��������� �����
	int HeaderDataSize;
	int ScanDataOffset;       // ������ ������������
    int ScanDataSize;
	int HandDataOffset[32];   // ������ ������� ������������
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       // ���� �����
	int FotoDataSize;
};

struct sFileHeaderVersion_03  // ��������� ��������� ���������� �����
{
	char FileID[16];          // �������������
	int Version;              // ������ ������� �����

	int HeaderDataOffset;     // ��������� �����
	int HeaderDataSize;
	int ScanDataOffset;       // ������ ������������
    int ScanDataSize;
	int HandDataOffset[32];   // ������ ������� ������������
	int HandDataSize;
	int HandDataItemsCount;

	int FotoDataOffset;       // ���� �����
	int FotoDataSize;

	int ScreenShotDataOffset;         // �������� ������ �������� �������
	int ScreenShotDataSize;

	int AlarmRecordsDataOffset;       // ������ ��� ��� ������� ������
	int AlarmRecordsDataSize;
    int AlarmRecordsItemsCount;

	int HandScanScreenShotOffset[32]; // ��������� ������ ������� ������������
	int HandScanScreenShotSize[32];
	int HandScanScreenShotCount;
};

#pragma pack(pop)

//# pragma pack ()


typedef struct { int Crd; int Ch; } TNewItemStruct;

class cChannelsTable;
class cDeviceCalibration;
// ������ ��������� �������� �������� �����
class cJointTestingReport
{
	private:
		int FSearchSysCoord;
		int FSearchChannel;

		// ������ ������������
		std::vector < int > FCoordPtrList;
		std::vector < sScanSignalsOneCrdAllChannel > FSignalList;

		// ���������� �������� ���
		std::vector < sAlarmRecord > FAlarmList;
		bool bAlarmsUpdated; //��������������� ��� ��������� ��������

		//------------Added by KirillB---------------------
		//��������� ��� ���������
		std::vector <int> FTuneMaximumsList;
		//������ ��� ���������
		bool FKPSurfaceStates[9]; //false - no alarm, true - alarm
		sChannelDescription FKPCheckChannels[9];
		std::vector<float> FChannelsDelta;
		std::vector<int> FChannelsOldValue;
		//std::vector<bool> FChannelStates;
        bool bHandScanStoreEnabled;
		//-------------------------------------------------

		// ������ �������
		std::vector < sHandSignalsRecord > FHandScanList;
        sHandSignalsRecord storedHandSignals;

	public:

        bool FExitFlag;

		sJointTestingReportHeader_Version_04 Header;

		cJointTestingReport(int MaxSysCoord);
		~cJointTestingReport();

		TJPEGImage * Photo;               // ���� �����
		TJPEGImage * PhoneScreenShot;     // �������� ������ �������� �������
		std::vector < TJPEGImage* > ScreenShotHandScanList; // ��������� ������ ������� ������������

			// ������� ������������
		void ClearData(void);
        void Release();

		//-------------Added by KirillB-------------------
		//If no maximum data, or out of bound - return -1;
        void ClearScreenShotHandScanList();
		int GetChannelMaximum(unsigned int index);
		void AddChannelMaximum(unsigned int index, int chMax);

		unsigned int GetChannelMaximumCount();
		unsigned int GetChannelMaximumAliveCount();

        bool UpdateSurfChannelsState(cDeviceCalibration* Calib,int workCycle); //�������� �� �������

		void ClearMaximumsData();
		bool GetKPSurfaceState(unsigned int index);
        int GetKPSurfaceChannelId(unsigned int kp_index);

        void UpdateKPSurfaceState(int kpIndex, int fromCoord, int toCoord);
		void InitChannelIndicationData(cChannelsTable* Tbl);
		//-----------------------------------------------

		void AddAlarmSignals(int Channel, sAlarmRecord& item);
		sAlarmRecord GetAlarmSignals(int Channel);
		bool CheckAlarmUpdated(bool bResetUpdateToFalse = false);
		int GetAlarmSignalsCount();

		void PutScanSignals(int SysCoord, int Channel, PsScanSignalsOneCrdOneChannel pData);
        PsScanSignalsOneCrdOneChannel GetScanSignals(int SysCoord, int Channel);
		PsScanSignalsOneCrdOneChannel GetFirstScanSignals(int SysCoord, int Channel, bool* Result);
		PsScanSignalsOneCrdOneChannel GetNextScanSignals(int* SysCoord, int* Channel, bool* Result);
				// ������� ������� ��������
					// ��� CrdCount � ���������� ������� sHandOneCrdSignals, pData � ��������� �� ������ ������
        void EnableHandScanStore(bool bVal);
        void ClearStoredHandSignals();
        void StoreScanSignal(sHandSignalsOneCrd& signal);
        int AddStoredHandSignals(sHandScanParams Params);
		int AddHandSignals(sHandScanParams Params, PsHandSignalsOneCrd pData, int CrdCount);
					// ��������� ������ ������� ��������
		PsHandSignalsOneCrd GetHandSignals(int RecIndex, sHandScanParams* Params, bool* Result, int * CrdCount);
					// ��������� ���������� ��������� �������:
		int  GetHandScanCount(void);
					// �������� ������
		bool DeleteHandScan(int Index);

		bool WriteToFile(UnicodeString filename);
		bool ReadFromFile(UnicodeString filename, bool OnlyHeader);

		void ClearNewItemList(int Idx);
        float GetTimeInHandScanChannel(CID cid);
        void SetTimeInHandScanChannel(CID cid, float NewValue);

        TNewItemStruct NewItemList[NewItemListSize];
        int NewItemListIdx;
};

typedef cJointTestingReport* PcJointTestingReport;

class cJointTestingBase // ��������� ���������� ��������
{
		bool PutTestingReport(cJointTestingReport * NewReport);
		bool GetScanSignals(int Params1, int Params2, PcJointTestingReport * Report);
};
//---------------------------------------------------------------------------
#endif



/*
char Data = 65;

typedef char* mytype;

mytype Func1(void)
{
    return &Data;
}

void Func2(mytype* data)
{
//    data = &Data;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
   mytype test;
   test = Func1();
   Form1->Caption = *test;
}
//---------------------------------------------------------------------------
*/
