//---------------------------------------------------------------------------
#ifndef _DataStorage
#define _DataStorage

#include "ChannelsTable.h"
#include "DeviceCalibration.h"
#include "UMUTest_DataStorage.h"
#include <stdio.h>	// For in/out - FILE*

#pragma hdrstop
//---------------------------------------------------------------------------
//#pragma package(smart_init)

//---------------------------------------------------------------------------

cJointTestingReport::cJointTestingReport(int MaxSysCoord)
{
	Photo = NULL;
	PhoneScreenShot = NULL;     // �������� ������ �������� �������
	ScreenShotHandScanList.resize(0); // ��������� ������ ������� ������������

	Header.MaxSysCoord = MaxSysCoord; // ������������ ����������
	Header.MaxChannel = ScanChannelCount; // ���������� ������� ������������
    FExitFlag = false;
    NewItemListIdx = 0;

	FCoordPtrList.resize(MaxSysCoord + 1);
	for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
	{
		FCoordPtrList[idx] = - 1;
	}

	bAlarmsUpdated = true;
    bHandScanStoreEnabled = false;
}

//---------------------------------------------------------------------------

cJointTestingReport::~cJointTestingReport()
{
	 Release();
}

//---------------------------------------------------------------------------

void cJointTestingReport::Release()
{
    //Clear scan items
    ClearNewItemList(NewItemListIdx);
    FHandScanList.clear();
    FSignalList.resize(0);
    //FCoordPtrList.resize(Header.MaxSysCoord, -1);
    // ^^ In old variant:
      FCoordPtrList.resize(Header.MaxSysCoord + 1);
	  for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
	  {
		    FCoordPtrList[idx] = - 1;
	  }
    // But now FCoordPtrList.resize called in read report function

    ClearStoredHandSignals();

    //Clear photos
    ClearScreenShotHandScanList();
    if(PhoneScreenShot)
    {
        delete PhoneScreenShot;
        PhoneScreenShot = NULL;
    }
    if (Photo)
    {
        delete(Photo);
	    Photo = NULL;
    }

    //Clear alarm and maximum items
	ClearMaximumsData();
    FAlarmList.clear();
    bAlarmsUpdated = true;
    memset(FKPSurfaceStates,0,sizeof(FKPSurfaceStates));

    // Other stuff
    FChannelsDelta.clear();
    FChannelsOldValue.clear();

    //Don't know
    FExitFlag = true;
	for (unsigned int idx = 0; idx <= 10; idx++) Header.TimeInHandScanChannel[idx] = 0;

}

void cJointTestingReport::ClearData(void)
{
    Release();
}

//---------------------------------------------------------------------------

void cJointTestingReport::PutScanSignals(int SysCoord, int Channel, PsScanSignalsOneCrdOneChannel pData)
{
	//Added by KirillB
	if(SysCoord>Header.MaxSysCoord)
	{
//		LOGERROREX(LOGCHANNEL_CHECK,"Wrong sys coord - %d (channel-%d)",SysCoord, Channel);
		return;
	}


	if ((SysCoord >= 0) && (SysCoord <= Header.MaxSysCoord) && (pData->Count != 0))
	{
		if (FCoordPtrList[SysCoord] == - 1)
		{
			FSignalList.resize(FSignalList.size() + 1);
			for (int Ch = 0; Ch < ScanChannelCount; Ch++)
				FSignalList[FSignalList.size() - 1].Channel[Ch].Count = 0;
			FCoordPtrList[SysCoord] = FSignalList.size() - 1;
		}

		//Added by KirillB (bug)
		if(Channel>=ScanChannelCount)
		{
//			LOGERROREX(LOGCHANNEL_CHECK,"Wrong channel: %d (max-%d, fullIndex-%#04x)!",Channel,ScanChannelCount,Channel  + 63);
			return;
		}

		FSignalList[FCoordPtrList[SysCoord]].Channel[Channel] = *pData;

		//---------------Added by KirillB-------------------------
		int AbsChannelId = Channel + 0x3F;
		//���������� ����������
		float currAmplMax = -1;

		bool bGoodChValue = false;
		for(int nSig = 0; nSig < pData->Count; nSig++)
		{
		  //currAmplMax = std::max(currAmplMax, (float)pData->Signals[nSig].Ampl[maxAmplPos]);
			/*if(currKpCtlIndex!=-1)
			{
				if( (pData->Signals[nSig].Delay >= FKPCheckChannels[currKpCtlIndex].cdBScanGate.gStart) ||
					(pData->Signals[nSig].Delay <= FKPCheckChannels[currKpCtlIndex].cdBScanGate.gEnd))
				{
					bGoodChValue = true;
                }
            }*/

			/*for(int i = 0; i < 24; i++)
			{
				//���������� ������������ ���������
				if(currAmplMax < (int)(pData->Signals[nSig].Ampl[i]))
				{
					currAmplMax = (int)pData->Signals[nSig].Ampl[i];
				}
				//currAmplMax = std::max(currAmplMax, (int)pData->Signals[nSig].Ampl[i]);
			}*/
			currAmplMax = std::max((int)currAmplMax, (int)pData->Signals[nSig].Ampl[11]);
		}

		AddChannelMaximum(AbsChannelId,currAmplMax);
		//--------------------------------------------------------

		if (NewItemListIdx < NewItemListSize)
		{
		  NewItemList[NewItemListIdx].Crd = SysCoord;
		  NewItemList[NewItemListIdx].Ch = Channel;
		  NewItemListIdx++;
		}
	}
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetScanSignals(int SysCoord, int Channel)
{
	bool res;
	return GetFirstScanSignals(SysCoord, Channel, &res);
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetFirstScanSignals(int SysCoord, int Channel, bool* Result)
{
     FSearchSysCoord = SysCoord;
     FSearchChannel = Channel;

	 if ((FSearchSysCoord >= 0) && (FSearchSysCoord <= Header.MaxSysCoord))
	 {
		  if (FCoordPtrList[FSearchSysCoord] == - 1)
		  {
              FSearchSysCoord++;
              FSearchChannel = 0;
              *Result = false;
			  return NULL;
		  }
          else
          {
              if((FSearchChannel >= ScanChannelCount) || (FSearchChannel < 0))
              {
//                  LOGERROR("Wrong channel number! FSearchChannel = %d (must be below %d)",FSearchChannel,ScanChannelCount);
                  *Result = false;
                  return NULL;
              }

              PsScanSignalsOneCrdOneChannel tmp = &FSignalList[FCoordPtrList[FSearchSysCoord]].Channel[FSearchChannel];
              if (FSearchChannel != (ScanChannelCount - 1))
              {
                 FSearchChannel++;
              }
              else
              {
                 FSearchChannel = 0;
                 FSearchSysCoord++;
              }
              *Result = true;
              return tmp;
          }
	 }
     else
     {
        *Result = false;
        return NULL;
     }
}

//---------------------------------------------------------------------------

PsScanSignalsOneCrdOneChannel cJointTestingReport::GetNextScanSignals(int * SysCoord, int * Channel, bool * Result)
{
	 while (TRUE)
	 {
          if (FExitFlag)
          {
                FExitFlag = false;
                break;
          }
	      if ((FSearchSysCoord < 0) || (FSearchSysCoord >= FCoordPtrList.size()))
          {
			  *Result = false;
			  return NULL;
          }

		  if (FCoordPtrList[FSearchSysCoord] == - 1)
		  {
              FSearchSysCoord++;
              FSearchChannel = 0;
		      continue;
		  }
          else
          {
              int FCoord = FCoordPtrList[FSearchSysCoord];
              if( FCoord >= FSignalList.size() )
              {
                    *Result = false;
			        return NULL;
              };

              if((FSearchChannel >= ScanChannelCount) || (FSearchChannel < 0))
              {
//                  LOGERROR("Wrong channel number! FSearchChannel = %d (must be below %d)",FSearchChannel,ScanChannelCount);
                  *Result = false;
                  return NULL;
              }

              PsScanSignalsOneCrdOneChannel tmp = &FSignalList[FCoord].Channel[FSearchChannel];

              if (FSearchChannel < (ScanChannelCount - 1))
              {
                 FSearchChannel++;
              }
              else
              {
                 FSearchChannel = 0;
                 FSearchSysCoord++;
              }

              if (tmp->Count == 0) continue;

              *SysCoord = FSearchSysCoord;
              *Channel = FSearchChannel - 1;
              *Result = true;
		      return tmp;
          }
	 }
     *Result = false;
	 return NULL;
}

//---------------------------------------------------------------------------

void cJointTestingReport::EnableHandScanStore(bool bVal)
{
    bHandScanStoreEnabled = bVal;
}

void cJointTestingReport::StoreScanSignal(sHandSignalsOneCrd& signal)
{
    if(bHandScanStoreEnabled)
        storedHandSignals.BScanData.push_back(signal);
}

void cJointTestingReport::ClearStoredHandSignals()
{
    storedHandSignals.BScanData.clear();
}

int cJointTestingReport::AddStoredHandSignals(sHandScanParams Params)
{
    int iRet = AddHandSignals(Params, &(storedHandSignals.BScanData[0]), storedHandSignals.BScanData.size());
    ClearStoredHandSignals();
    return iRet;
}

int cJointTestingReport::AddHandSignals(sHandScanParams Params, PsHandSignalsOneCrd pData, int CrdCount)
{
	int NewIdx = FHandScanList.size();
	if (NewIdx > 31) return - 1;
	FHandScanList.resize(NewIdx + 1);
	FHandScanList[NewIdx].Params = Params;
	FHandScanList[NewIdx].BScanData.resize(CrdCount);
	memcpy(&FHandScanList[NewIdx].BScanData[0], pData, CrdCount * sizeof(sHandSignalsOneCrd));
	return NewIdx;
}

//---------------------------------------------------------------------------

PsHandSignalsOneCrd cJointTestingReport::GetHandSignals(int RecIndex, sHandScanParams* Params, bool* Result, int* CrdCount)
{
    if((RecIndex < 0) || (RecIndex >= FHandScanList.size()))
    {
        *Result = false;
        return NULL;
    };

    if(FHandScanList[RecIndex].BScanData.empty())
    {
        *Result = false;
        return NULL;
    }

	*Params = FHandScanList[RecIndex].Params;
	*CrdCount  = FHandScanList[RecIndex].BScanData.size();
    *Result = true;
	return &FHandScanList[RecIndex].BScanData[0];
}

//---------------------------------------------------------------------------

int cJointTestingReport::GetHandScanCount(void)
{
	return FHandScanList.size();
}

//---------------------------------------------------------------------------

bool cJointTestingReport::DeleteHandScan(int Index)
{
	FHandScanList.erase(FHandScanList.begin() + Index);
	return true;
}

//---------------------------------------------------------------------------

bool cJointTestingReport::WriteToFile(UnicodeString FileName)
{
	TFileStream * out;

    // --- ������ ���������� ��������� ---

	sFileHeaderVersion_03 head;

	head.FileID[0] = 0x52; // ������������� ����� [RSPBRAFD]
	head.FileID[1] = 0x53;
	head.FileID[2] = 0x50;
	head.FileID[3] = 0x42;
	head.FileID[4] = 0x52;
	head.FileID[5] = 0x41;
	head.FileID[6] = 0x46;
	head.FileID[7] = 0x44;
	head.FileID[8] = 0x41; // ������������� ����� [AUTOCONS]
	head.FileID[9] = 0x55;
	head.FileID[10] = 0x54;
	head.FileID[11] = 0x4F;
	head.FileID[12] = 0x43;
	head.FileID[13] = 0x4F;
	head.FileID[14] = 0x4E;
	head.FileID[15] = 0x53;

	head.Version = 3; // ������ 3
	head.HeaderDataOffset = 0;
	head.HeaderDataSize = 0;
	head.FotoDataOffset = 0;
	head.FotoDataSize = 0;
	head.ScanDataOffset = 0;
    head.ScanDataSize = 0;
	for (int i = 0; i < 32; i++) { head.HandDataOffset[i] = 0; }
	head.HandDataSize = 0;
	head.HandDataItemsCount = 0;

	head.ScreenShotDataOffset = 0;      // �������� ������ �������� �������
	head.ScreenShotDataSize = 0;
	head.AlarmRecordsDataOffset = 0;    // ������ ��� ��� ������� ������
	head.AlarmRecordsDataSize = 0;
    head.AlarmRecordsItemsCount = 0;
	for (int i = 0; i < 32; i++)        // �������� ������ ������� ������������
    {
        head.HandScanScreenShotOffset[i] = 0;
        head.HandScanScreenShotSize[i] = 0;
    }
	head.HandScanScreenShotCount = 0;

	out = new TFileStream("tmp.data", fmCreate /*fmOpenReadWrite*/);
	out->WriteBuffer(&head, sizeof(sFileHeaderVersion_03)); // ������ ���������

    // --- ������ ��������������� ��������� ---

    // ��������� ����

    TStringList * TextData = new TStringList;
    TextData->Add(Header.Plaint);   	 // �����������
    TextData->Add(Header.PlantNumber);   // ��������� ����� ���������
    TextData->Add(Header.VerSoft);	     // ������ ��
    TextData->Add(Header.Operator); 	 // �������� ���
    TextData->Add(Header.DefectCode);    // ��� �������
    TextData->Add(Header.ValidityGroup); // ��. ��������
    TextData->Add(Header.�onclusion);    // ����������

    TMemoryStream * tmp = new TMemoryStream;
    TextData->SaveToStream(tmp);

    int HeaderDataSize = (int)(&(Header.END_)) - (int)(&(Header)); // ������ ��������� ��� ��������� ������

	head.HeaderDataOffset = out->Position;
	head.HeaderDataSize = HeaderDataSize + tmp->Size;
    out->WriteBuffer(&(Header.FileType), HeaderDataSize);
    out->WriteBuffer(tmp->Memory, tmp->Size); // ��������� ����

    delete tmp;
    delete TextData;

	// --- ������ �������� ������������ ---

	head.ScanDataOffset = out->Position;
	int cnt;
	for (int SysCoord = 0; SysCoord <= Header.MaxSysCoord; SysCoord++)
	{
		if (FCoordPtrList[SysCoord] == -1) continue;

		// ������� ���������� �������� �� ���������� SysCoord
		cnt = 0;
		for (int Channel = 0; Channel < ScanChannelCount; Channel++)
		{
		   cnt = cnt + (FSignalList[FCoordPtrList[SysCoord]].Channel[Channel].Count != 0);
		}

		// ������ �������� ���������� SysCoord
		if (cnt != 0)
		{
			out->WriteBuffer(&SysCoord, 4);
			out->WriteBuffer(&cnt, 4);
			for (int Channel = 0; Channel < ScanChannelCount; Channel++)
			{
			   cnt = FSignalList[FCoordPtrList[SysCoord]].Channel[Channel].Count;
			   if (cnt != 0)
			   {
				   out->WriteBuffer(&Channel, 1);
				   out->WriteBuffer(&cnt, 1);
				   out->WriteBuffer(&FSignalList[FCoordPtrList[SysCoord]].Channel[Channel].Signals[0], cnt * sizeof(tUMU_BScanSignal));
			   }
			}
		}
	}
	head.ScanDataSize = out->Position - head.ScanDataOffset - 1;

	// --- ������ �������� ������� ������������ ---

	int RecCount;
	head.HandDataItemsCount = FHandScanList.size();
	for (int recitem = 0; recitem < head.HandDataItemsCount; recitem++)
	{
		head.HandDataOffset[recitem] = out->Position;
		// ������ ��������� ������� ������������
		out->WriteBuffer(&FHandScanList[recitem].Params, sizeof(sHandScanParams));
		// ������� ���������� ������� �-���������
        RecCount = 0;
		for (unsigned int item = 0; item < FHandScanList[recitem].BScanData.size(); item++)
			if (FHandScanList[recitem].BScanData[item].Count != 0) { RecCount++; }
		// ������ ���������� ������� �-���������
		out->WriteBuffer(&RecCount, 4);
		// ������ �������� �-���������
		for (unsigned int item = 0; item < FHandScanList[recitem].BScanData.size(); item++)
		{
			cnt = FHandScanList[recitem].BScanData[item].Count;
            if (cnt != 0)
            {
                out->WriteBuffer(&FHandScanList[recitem].BScanData[item].SysCoord, 4);
                out->WriteBuffer(&cnt, 1);
                out->WriteBuffer(&FHandScanList[recitem].BScanData[item].Signals[0], cnt * sizeof(tUMU_BScanSignal));
            }
		}
	}
	head.HandDataSize = out->Position - head.HandDataOffset[0] - 1;

	// --- ������ ���������� ---

	if (Photo)
	{
		head.FotoDataOffset = out->Position;
		Photo->SaveToStream(out);
		head.FotoDataSize = out->Position - head.FotoDataOffset - 1;
	}

	// --- ������ ��������� ������ �������� ������� ---

	if (PhoneScreenShot)
	{
		head.ScreenShotDataOffset = out->Position;
		PhoneScreenShot->SaveToStream(out);
		head.ScreenShotDataSize = out->Position - head.ScreenShotDataOffset - 1;
	}

	// --- ������ ���������� ������� �������� ---

	for (unsigned int idx = 0; idx < ScreenShotHandScanList.size(); idx++)
        if ((ScreenShotHandScanList[idx]) && (idx < 32))
        {
            head.HandScanScreenShotOffset[idx] = out->Position;
            ScreenShotHandScanList[idx]->SaveToStream(out);
            head.HandScanScreenShotSize[idx] = out->Position - head.HandScanScreenShotOffset[idx] - 1;
            head.HandScanScreenShotCount++;
        }
    ClearScreenShotHandScanList();
    // --- ������ ������ ��� ��� ������� ������ ---

	head.AlarmRecordsDataOffset = out->Position;
    out->WriteBuffer(&FAlarmList[0], sizeof(sAlarmRecord) * FAlarmList.size());
	head.AlarmRecordsDataSize = out->Position - head.AlarmRecordsDataOffset - 1;
    head.AlarmRecordsItemsCount = FAlarmList.size();

    // --- ���������� ��������� � �������������� ���������� ---

	out->Position = 0;
	out->WriteBuffer(&head, sizeof(sFileHeaderVersion_03));

	out->Position = 0;
	// Create the Output and Compressed streams.
	TFileStream *output = new TFileStream(FileName, fmCreate);
	TZCompressionStream *zip = new TZCompressionStream(System::Zlib::clMax, (TStream*)output);

	// Compress data.
	zip->CopyFrom(out, out->Size);

	// Free the streams.
	zip->Free();
	out->Free();
	output->Free();
	return true;
}

//---------------------------------------------------------------------------

bool cJointTestingReport::ReadFromFile(UnicodeString filename, bool OnlyHeader)
{
    Release(); //������� ���������
    FExitFlag = false;

	/* Create the Input and Decompressed streams. */
	TFileStream *input = new TFileStream(filename, fmOpenRead);
	TZDecompressionStream *unzip_ = new TZDecompressionStream(input);
    TMemoryStream * src = new TMemoryStream;
    src->LoadFromStream(unzip_);

    // --- �������� ���������� ��������� ---

	sFileHeaderVersion_03 head;
    memset(&head, 0, sizeof(sFileHeaderVersion_03));
    src->ReadBuffer(&head, 20);
    switch (head.Version) {
        case 1: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_02) - 20); break;
        case 2: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_02) - 20); break;
        case 3: src->ReadBuffer(&(head.HeaderDataOffset), sizeof(sFileHeaderVersion_03) - 20); break;
    }

	if (((head.FileID[0] == 0x52) && // ������������� ����� [RSPBRAFD]
		 (head.FileID[1] == 0x53) &&
		 (head.FileID[2] == 0x50) &&
		 (head.FileID[3] == 0x42) &&
		 (head.FileID[4] == 0x52) &&
		 (head.FileID[5] == 0x41) &&
		 (head.FileID[6] == 0x46) &&
		 (head.FileID[7] == 0x44) &&
		 (head.FileID[8] == 0x41) && // ������������� ����� [AUTOCONS]
		 (head.FileID[9] == 0x55) &&
		 (head.FileID[10] == 0x54) &&
		 (head.FileID[11] == 0x4F) &&
		 (head.FileID[12] == 0x43) &&
		 (head.FileID[13] == 0x4F) &&
		 (head.FileID[14] == 0x4E) &&
		 (head.FileID[15] == 0x53)) &&
		 ((head.Version == 1) || (head.Version == 2) || (head.Version == 3) || (head.Version == 4)))
	{

        // --- �������� ��������������� ��������� ---

        // ��������� ����

        TMemoryStream * tmp = new TMemoryStream; // ����� ��� ��������� ������

        int HeaderDataSize = (int)&(Header.END_) - (int)&(Header); // ������ ��������� ��� ��������� ������
        if (head.HeaderDataSize - HeaderDataSize > 0)
        {
            tmp->SetSize(head.HeaderDataSize - HeaderDataSize);
        }

        //Check for wrong file size
        if(src->Size < (head.HeaderDataOffset + HeaderDataSize))
        {
            return false;
        }

        src->Position = head.HeaderDataOffset;
        src->ReadBuffer(&(Header.FileType), HeaderDataSize);
        src->ReadBuffer(tmp->Memory, tmp->Size);

        TStringList * TextData = new TStringList;
        TextData->LoadFromStream(tmp);
//        if (((TextData->Count == 6) && (head.Version == 1)) ||
//            ((TextData->Count == 7) && (head.Version == 2)))
        if ((TextData->Count == 6) || (TextData->Count == 7))
        {
            Header.Plaint        = TextData->Strings[0]; // �����������
            Header.PlantNumber   = TextData->Strings[1]; // ��������� ����� ���������
            Header.VerSoft       = TextData->Strings[2]; // ������ ��
            Header.Operator      = TextData->Strings[3]; // �������� ���
            Header.DefectCode    = TextData->Strings[4]; // ��� �������
            Header.ValidityGroup = TextData->Strings[5]; // ��. ��������
            Header.�onclusion    = "";
        }

        if /*(*/ (TextData->Count == 7) /* && (head.Version == 2)) */
        {
            Header.�onclusion = TextData->Strings[6]; // ����������
        }

        delete tmp;
        delete TextData;

        // --- ���������� ������ � �������� ---

		Photo = NULL;
        ClearScreenShotHandScanList();
		FSignalList.resize(0);
		FCoordPtrList.resize(Header.MaxSysCoord + 1);
		for (unsigned int idx = 0; idx < FCoordPtrList.size(); idx++)
		{
			FCoordPtrList[idx] = - 1;
		}

        if (!OnlyHeader)
        {
            // --- �������� �������� ������������ ---

            src->Position = head.ScanDataOffset;

            int SysCoord;
            int Channel = 0;
            int SignalCount;
            sScanSignalsOneCrdOneChannel Data;

            while (src->Position < head.ScanDataOffset + head.ScanDataSize)
            {
                src->ReadBuffer(&SysCoord, 4);
                src->ReadBuffer(&SignalCount, 4);
                for (int idx = 0; idx < SignalCount; idx++)
                {
                    src->ReadBuffer(&Channel, 1);
                    src->ReadBuffer(&Data.Count, 1);
                    src->ReadBuffer(&Data.Signals[0], Data.Count * sizeof(tUMU_BScanSignal));
                    PutScanSignals(SysCoord, Channel, &Data);
                }
            }

            // --- �������� �������� ������� ������������ ---

            int RecCount;
            FHandScanList.resize(head.HandDataItemsCount);
            for (int recitem = 0; recitem < head.HandDataItemsCount; recitem++)
            {
                src->Position = head.HandDataOffset[recitem];
                // �������� ��������� ������� ������������
                src->ReadBuffer(&FHandScanList[recitem].Params, sizeof(sHandScanParams));
                // �������� ���������� ������� �-���������
                src->ReadBuffer(&RecCount, 4);
                // �������� �������� �-���������
                FHandScanList[recitem].BScanData.resize(RecCount);
                for (int item = 0; item < RecCount; item++)
                {
                    src->ReadBuffer(&FHandScanList[recitem].BScanData[item].SysCoord, 4);
                    src->ReadBuffer(&FHandScanList[recitem].BScanData[item].Count, 1);
                    src->ReadBuffer(&FHandScanList[recitem].BScanData[item].Signals[0], FHandScanList[recitem].BScanData[item].Count * sizeof(tUMU_BScanSignal));
                }
            }

            // --- �������� ���������� ����� ---

            if (head.FotoDataSize != 0)
            {
                src->Position = head.FotoDataOffset;
                TMemoryStream * tmp = new TMemoryStream;
                tmp->SetSize(head.FotoDataSize);
                src->ReadBuffer(tmp->Memory, tmp->Size);
                if (!Photo) Photo = new TJPEGImage;
                Photo->LoadFromStream(tmp);
                delete tmp;
            }
            else
            {
                head.FotoDataOffset = 0;
                head.FotoDataSize = 0;
            };

            // --- �������� ��������� ������ �������� ������� ---

            if (head.ScreenShotDataSize > 0) {

                src->Position = head.ScreenShotDataOffset;
                TMemoryStream * tmp = new TMemoryStream;
                tmp->SetSize(head.ScreenShotDataSize);
                src->ReadBuffer(tmp->Memory, tmp->Size);
                if (!PhoneScreenShot) PhoneScreenShot = new TJPEGImage;
                PhoneScreenShot->LoadFromStream(tmp);
                delete tmp;
            }

            // --- �������� ���������� ������� �������� ---

            for (int idx = 0; idx < head.HandScanScreenShotCount; idx++)
                {
                    src->Position = head.HandScanScreenShotOffset[idx];
                    TMemoryStream * tmp = new TMemoryStream;
                    tmp->SetSize(head.HandScanScreenShotSize[idx]);
                    src->ReadBuffer(tmp->Memory, tmp->Size);
                    TJPEGImage * ss = new TJPEGImage;
                    ss->LoadFromStream(tmp);
                    ScreenShotHandScanList.push_back(ss);
                    delete tmp;
                }

            // --- �������� ������ ��� ��� ������� ������ ---

            if (head.AlarmRecordsDataSize != 0) {
                FAlarmList.resize(head.AlarmRecordsItemsCount);
                src->Position = head.AlarmRecordsDataOffset;
                src->ReadBuffer(&FAlarmList[0], head.AlarmRecordsDataSize);
            }
        }
	}

	/* Free the streams. */
	unzip_->Free();
	input->Free();
    src->Free();
	return true;
}

//---------------------------------------------------------------------------

void cJointTestingReport::ClearScreenShotHandScanList()
{
    for(int i = 0; i < ScreenShotHandScanList.size(); i++)
    {
        if(ScreenShotHandScanList[i])
            delete ScreenShotHandScanList[i];
    };
    ScreenShotHandScanList.clear();
};

void cJointTestingReport::ClearNewItemList(int Idx)
{
    NewItemListIdx = 0;

    memcpy(&NewItemList[0], &NewItemList[Idx], (NewItemListIdx - Idx) * sizeof(TNewItemStruct));
}

//--------------------------------------------------------------------------

//����� ������� ������� �������
bool cJointTestingReport::UpdateSurfChannelsState(cDeviceCalibration* Calib, int workCycle)
{
    bool bGapsFound = false;

    int LastKnownPositions[9]; //��������� ����������, �� ������� ������ ���
    memset(LastKnownPositions,-1,sizeof(LastKnownPositions));

    int SurfaceChannelsGates[9][2]; //[����� �������][������ ������, ����� ������]
    for(int j = 1; j < 9; j ++) //���������� ������� �������
    {
        int chId = FKPCheckChannels[j].id;
        SurfaceChannelsGates[j][0] =
                Calib->GetStGate(dsNone, chId, 0); //������ ������
        SurfaceChannelsGates[j][1] =
                Calib->GetEdGate(dsNone, chId, 0); //����� ������
    }

    for(unsigned int crdX = 0; crdX < FCoordPtrList.size(); crdX++) //������ �� ���� �����������
    {
        int nSig = FCoordPtrList[crdX];
        if( nSig == -1 )
            continue;

        for(int j = 1; j < 9; j ++)
        {
            int chIdRel = FKPCheckChannels[j].id - 0x3F;
            sScanSignalsOneCrdOneChannel& sig = FSignalList[nSig].Channel[chIdRel];

            if((sig.Count == 0) ||            //���� ��� ��������
                (sig.WorkCycle != workCycle))  //��� ������� �� �� ���� �������� �����
                continue;                      //�� ����������

            for (int idx = 0; idx < sig.Count; idx++)
            {
                //���� ������ �� ������ � ����� ������� ������� - ����������
                if ((sig.Signals[idx].Delay < SurfaceChannelsGates[j][0]) ||
                        (sig.Signals[idx].Delay > SurfaceChannelsGates[j][1]))
                    continue;

                //���� ������ �� ������ � ����� b-��������� - ����������
                if ((sig.Signals[idx].Delay < FKPCheckChannels[j].cdBScanGate.gStart) ||
                        (sig.Signals[idx].Delay > FKPCheckChannels[j].cdBScanGate.gEnd))
                    continue;

                unsigned int coordDistance = 0;
                if(LastKnownPositions[j] != -1)
                   coordDistance = abs(int(crdX) - LastKnownPositions[j]);

                LastKnownPositions[j] = crdX;

                const unsigned int MaxCoordGap = 10; //(��?)

                if(coordDistance > MaxCoordGap)
                {
                    bGapsFound = true;
//                    LOGWARNING("(KP%d, ch-0x%X) Gaps found: crd-%dmm, wc-%d, dist-%d",
//                                    j,FKPCheckChannels[j].id,crdX,workCycle,coordDistance);
                    FKPSurfaceStates[j] |= true;
                }

            }

        }
    }
    return bGapsFound;
}

int cJointTestingReport::GetChannelMaximum(unsigned int index)
{
	if(index >= FTuneMaximumsList.size())
		return -1;

	return FTuneMaximumsList[index];
}

void cJointTestingReport::AddChannelMaximum(unsigned int index, int chMax)
{
	if(index>200)
	{
//		LOGERROR("Wrong channel index: %d",index);
		return;
    }
	if(FTuneMaximumsList.size() <= index)
		FTuneMaximumsList.resize(index+1,-1);

	FTuneMaximumsList[index] = std::max(FTuneMaximumsList[index], chMax);
}

unsigned int cJointTestingReport::GetChannelMaximumCount()
{
	return FTuneMaximumsList.size();
}

unsigned int cJointTestingReport::GetChannelMaximumAliveCount()
{
	unsigned int count = 0;
	for( unsigned int i = 0; i < FTuneMaximumsList.size(); i++)
	{
		if(FTuneMaximumsList[i] > 0)
			count++;
    }
	return count;
}

void cJointTestingReport::ClearMaximumsData()
{
	FTuneMaximumsList.clear();
}

bool cJointTestingReport::GetKPSurfaceState(unsigned int index)
{
	if(index>=9)
		return false;
	return FKPSurfaceStates[index];
}

void cJointTestingReport::InitChannelIndicationData(cChannelsTable* Tbl)
{
	const int RailCheckChannels[] = {0,0x46,0x52,0x5A,0x63,0x6E,0x79,0x84,0x90};

	for(int i=1;i<9;i++)
	{
		//FKPStates[i]=true;
		Tbl->ItemByCID(RailCheckChannels[i], &(FKPCheckChannels[i]));
    }
}

int cJointTestingReport::GetKPSurfaceChannelId(unsigned int kp_index)
{
    return FKPCheckChannels[kp_index].id;
}

void cJointTestingReport::AddAlarmSignals(int Channel, sAlarmRecord& item)
{
	if( (Channel >= ScanChannelCount ) || (Channel < 0))
	{
//		LOGERROREX(LOGCHANNEL_CHECK,"Wrong channel: %d (max-%d, fullIndex-%#04x)!",Channel,ScanChannelCount,Channel  + 63);
		return;
	}

	if( Channel >= FAlarmList.size() )
	{
		FAlarmList.resize( Channel + 1 );
		bAlarmsUpdated = true;
	}

	//Check surface control channels
	/*bool bSurfCtlChannel = false;
	int kp_index = 0;
	for(int i=1;i<9;i++)
	{
		int RelID = FKPCheckChannels[i].id - 0x3F;
		if(Channel != RelID)
			continue;

		bSurfCtlChannel = true;
		kp_index = i;
		break;
	}*/

	bool bNewVal;
	for(unsigned int i = 0; i < 4; i++ )
	{
		bNewVal = FAlarmList[Channel].State[i] | item.State[i];
		if(bNewVal != FAlarmList[Channel].State[i])
			bAlarmsUpdated = true;
		FAlarmList[Channel].State[i] = bNewVal;
	}

	//if(bSurfCtlChannel)
	  //	FKPSurfaceStates[kp_index] |=  !item.State[1];
}

sAlarmRecord cJointTestingReport::GetAlarmSignals(int Channel)
{
	if( Channel >= FAlarmList.size() )
	{
     	return sAlarmRecord();
	}
	return FAlarmList[Channel];
}

bool cJointTestingReport::CheckAlarmUpdated(bool bResetUpdateToFalse)
{
	bool bRet = bAlarmsUpdated;
	if(bResetUpdateToFalse)
		bAlarmsUpdated = false;
	return bRet;
}

int cJointTestingReport::GetAlarmSignalsCount()
{
	return FAlarmList.size();
}

float cJointTestingReport::GetTimeInHandScanChannel(CID cid)
{
    switch (cid)
    {
        case 0x1E: return Header.TimeInHandScanChannel[0]; break;
        case 0x1D: return Header.TimeInHandScanChannel[1]; break;
        case 0x1F: return Header.TimeInHandScanChannel[2]; break;
        case 0x20: return Header.TimeInHandScanChannel[3]; break;
        case 0x21: return Header.TimeInHandScanChannel[4]; break;
        case 0x22: return Header.TimeInHandScanChannel[5]; break;
        case 0x23: return Header.TimeInHandScanChannel[6]; break;
    }
}

void cJointTestingReport::SetTimeInHandScanChannel(CID cid, float NewValue)
{
    switch (cid)
    {
        case 0x1E: Header.TimeInHandScanChannel[0] = NewValue; break;
        case 0x1D: Header.TimeInHandScanChannel[1] = NewValue; break;
        case 0x1F: Header.TimeInHandScanChannel[2] = NewValue; break;
        case 0x20: Header.TimeInHandScanChannel[3] = NewValue; break;
        case 0x21: Header.TimeInHandScanChannel[4] = NewValue; break;
        case 0x22: Header.TimeInHandScanChannel[5] = NewValue; break;
        case 0x23: Header.TimeInHandScanChannel[6] = NewValue; break;
    }
}


#endif

