//---------------------------------------------------------------------------

#ifndef UMUTest_BScanLineH
#define UMUTest_BScanLineH

#include <Vcl.Graphics.hpp>

class TBScanLine
{

    public:

    int CrdToPixel(int Crd);
    int DelayToPixel(float Delay);

    TBScanLine(void);
    ~TBScanLine(void);

    TBitmap* Buffer;
    int MinCrd;
    int MaxCrd;
    int MinDelay;
    int MaxDelay;

    int ViewWidth;
    int PointWidth;
    int MaxACSumm;

    void Clear(void);
    void SetSize(TSize Sz);
    void SetViewCrdZone(int MinCrd_, int MaxCrd_);
    void SetViewDelayZone(int MinDelay_, int MaxDelay_);
    void SetPoint(int Crd, float minDelay, float maxDelay, int Ampl); // ����� �� �-���������
    void SetPoint2(int Crd, int Delay, int Ampl); // ����� �� �-���������
    void SetPoint3(int Crd, int Val, TColor Color); // ����� �� �-���������
    void SetMark(int Crd, TColor Color); // �������

    void Draw(int X, int Y, TCanvas *Canvas);
};

#endif
