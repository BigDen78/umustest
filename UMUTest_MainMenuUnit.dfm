object MainMenuForm: TMainMenuForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1041#1059#1052' '#1058#1077#1089#1090
  ClientHeight = 591
  ClientWidth = 1121
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel7: TPanel
    Left = 656
    Top = 0
    Width = 465
    Height = 591
    Align = alRight
    BorderWidth = 25
    TabOrder = 0
    object Memo1: TMemo
      Left = 26
      Top = 26
      Width = 413
      Height = 539
      Align = alClient
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 22
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        #1056#1091#1082#1086#1074#1086#1076#1089#1090#1074#1086' '#1087#1086' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1102' '#1041#1059#1052' '#1082' '#1055#1050
        ''
        '1. '#1053#1072#1089#1090#1088#1086#1080#1090#1100' '#1089#1074#1086#1081#1089#1090#1074#1072' '#1089#1077#1090#1077#1074#1086#1075#1086' '#1072#1076#1072#1087#1090#1077#1088#1072': '
        ''
        '    '#1055#1086#1083#1077'                  '#1047#1085#1072#1095#1077#1085#1080#1077#9
        '    IP-'#1072#1076#1088#1077#1089'             192.168.100.1'#9
        '    '#1052#1072#1089#1082#1072' '#1087#1086#1076#1089#1077#1090#1080'     255.255.255.0'#9
        ''
        '2. '#1055#1086#1076#1082#1083#1102#1095#1080#1090#1100' '#1041#1059#1052' '#1082' '#1055#1050
        '3. '#1055#1086#1076#1072#1090#1100' '#1087#1080#1090#1072#1085#1080#1077' '#1085#1072' '#1041#1059#1052' ('#1079#1072#1075#1086#1088#1080#1090#1089#1103' PWR)'
        '4. '#1044#1086#1078#1076#1072#1090#1100#1089#1103' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1041#1059#1052' ('#1087#1077#1088#1077#1089#1090#1072#1085#1077#1090' '#1084#1080#1075#1072#1090#1100' '
        'Status)'
        '5. '#1053#1072#1078#1072#1090#1100' '#1082#1085#1086#1087#1082#1091' '#8220#1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077#8221)
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 656
    Height = 591
    Align = alClient
    TabOrder = 1
    object Panel6: TPanel
      Left = 1
      Top = 497
      Width = 654
      Height = 38
      Align = alBottom
      BevelOuter = bvNone
      Caption = ' '#1057#1072#1085#1082#1090'-'#1055#1077#1090#1077#1088#1073#1091#1088#1075' 2017'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel4: TPanel
      Left = 1
      Top = 535
      Width = 654
      Height = 38
      Align = alBottom
      BevelOuter = bvNone
      Caption = ' '#1054#1040#1054' "'#1056#1072#1076#1080#1086#1072#1074#1080#1086#1085#1080#1082#1072'"'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Panel1: TPanel
      Left = 1
      Top = 573
      Width = 654
      Height = 17
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 654
      Height = 94
      Align = alTop
      BevelOuter = bvNone
      Caption = #1041#1059#1052' 3204 - Te'#1089#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -53
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object Panel2: TPanel
      Left = 1
      Top = 95
      Width = 654
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      Caption = #1042#1077#1088#1089#1080#1103': 2.0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object MenuPanel: TPanel
      Left = 89
      Top = 122
      Width = 465
      Height = 356
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      object Bevel1: TBevel
        Left = 0
        Top = 0
        Width = 465
        Height = 185
      end
      object OpenConnectionButton: TButton
        Left = 92
        Top = 109
        Width = 281
        Height = 61
        Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        WordWrap = True
        OnClick = OpenConnectionButtonClick
      end
      object ExitButton: TButton
        AlignWithMargins = True
        Left = 91
        Top = 277
        Width = 281
        Height = 61
        Margins.Left = 10
        Margins.Top = 10
        Margins.Right = 10
        Margins.Bottom = 10
        Caption = #1042#1099#1093#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = ExitButtonClick
      end
      object ShowHideDebugMenus: TButton
        Left = 92
        Top = 203
        Width = 280
        Height = 61
        Caption = #1057#1082#1088#1099#1090#1100' '#1084#1077#1085#1102
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        WordWrap = True
        OnClick = ShowHideDebugMenusClick
      end
      object Panel10: TPanel
        Left = 3
        Top = 30
        Width = 124
        Height = 25
        BevelOuter = bvNone
        Caption = 'IP:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object IPComboBox: TComboBox
        Left = 124
        Top = 29
        Width = 331
        Height = 26
        Style = csDropDownList
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnChange = IPComboBoxChange
        Items.Strings = (
          '192.168.100.100'
          '192.168.100.101'
          '192.168.100.102'
          '192.168.100.103'
          '3 '#1073#1083#1086#1082#1072' (101,102,103)'
          '3 '#1073#1083#1086#1082#1072' (100,102,103)'
          '2 '#1073#1083#1086#1082#1072' (100,102)')
      end
      object Panel11: TPanel
        Left = 3
        Top = 62
        Width = 124
        Height = 25
        BevelOuter = bvNone
        Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Myriad Pro'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object ComboBox1: TComboBox
        Left = 124
        Top = 61
        Width = 331
        Height = 26
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnChange = ComboBox1Change
        Items.Strings = (
          #1040#1074#1080#1082#1086#1085'-31'
          #1040#1074#1090#1086#1082#1086#1085'-'#1057
          #1041#1059#1052' '#1058#1077#1089#1090
          'Deutsche Bahn ('#1084#1085#1086#1075#1086#1082#1072#1085#1072#1083#1100#1085#1099#1081')'
          #1057#1082#1072#1085#1077#1088' '#1075#1086#1083#1086#1074#1082#1080' '#1088#1077#1083#1100#1089#1072
          #1057#1082#1072#1085#1077#1088' '#1075#1086#1083#1086#1074#1082#1080' '#1088#1077#1083#1100#1089#1072' ('#1087#1088#1086#1074#1077#1088#1082#1072' '#1055#1069#1055')'
          #1040#1074#1080#1082#1086#1085'-15 USB-CAN Win'
          #1041#1059#1052' '#1058#1077#1089#1090' 2'
          'Deutsche Bahn ('#1086#1076#1085#1086#1082#1072#1085#1072#1083#1100#1085#1099#1081')'
          #1040#1074#1080#1082#1086#1085'-31 ('#1069#1089#1090#1086#1085#1080#1103')')
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 824
    Top = 384
  end
  object OpenConnectionTimer: TTimer
    Enabled = False
    OnTimer = OpenConnectionTimerTimer
    Left = 824
    Top = 456
  end
  object ChangeModeTimer: TTimer
    Interval = 300
    Left = 928
    Top = 384
  end
  object OpenDialog: TOpenDialog
    Left = 928
    Top = 456
  end
end
