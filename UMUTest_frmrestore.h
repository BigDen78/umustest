//  �������������� ����� �������� �� �������� ���������� � ���������  tIncomeData:
//  x0,y0 - ����� � ���������   ������ ��������
//  x1,y1 - ����� � ���������   ������������� �������� ��������
//  x2,y2 - ����� � ���������   �����, ��������������� ��������� ��������
//
//
#include "math.h"

typedef struct _incomeData
{
// ������ ���� x0 <= x1 < x2 ��� x0 < x1 <=x2
	int y0; // ��������� ������ ��������
	double x0;       // ����� ������ ��������, � ���. ����� ���
	int y1; // ��������� ������������� �������� ��������
	double x1;       // ����� ������������� �������� ��������, � ���. ����� ���
	int y2; // ��������� ��������� ��������
	double x2;       // ����� ��������� ��������, � ���. ����� ���
} tIncomeData;

typedef struct _POINT
{
  double x;
  double y;
} tPOINT;

typedef struct _PARPARAMS
{
	double a; // ������������
	double b; // ��������
	double c;
}tPARPARAMS;
typedef enum _eMODE
{
   mLCut = 2, // ����� x0 == x1 ��. tIncomeData
   mRCut = 3, // ����� x1 == x2
   mRightToLeft = 0,
   mLeftToRight = 1,
   mLCut2 = 4, // ����� x0 == x1, �� y1 = y2
   mRCut2 = 5  // ����� x1 == x2, �� y0 = y1
}eMODE;
//------------------------------------------------------------------------------
class frmrestore
{
private:
//	tIncomeData data;
	tPARPARAMS pL;  // ������������ �����
	tPARPARAMS pR;  // � ������ �������

//
	tPOINT crossPoint; // ����� �������� � ����� �������� �� ������
	double sqrOfSubstract(double lPar,double rPar)
	{
		double substr = lPar-rPar;
		return substr * substr;
	}

	double fractionCoef;
	eMODE mode;
    double Abs(double n);
	double getSqParValue(tPARPARAMS *pP,double x);
	double getSqParDiffValue(tPARPARAMS *pP,double x);
	double getXCoordBySqParDiffValue(tPARPARAMS *pP,double diffValue);
    double getXCoordOfLineByValue(tPARPARAMS *pP,double value);
	double getLineValue(tPARPARAMS *pP,double x);
	void   gefCurve(tIncomeData *pData);
	double gefEqRoot(bool lParFlag);
	void getLParCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP,double diffValue);
	void getRParCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP,double diffValue);
	void getLineCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP);
    void getHLineCoef(tPARPARAMS *pP,double y);

public:
   frmrestore();
//
// *pY0XLCoord - ����� ������� "�����������" ������� � ���� x
   void   gefCurve(tIncomeData *pData, double *pY0XLCoord,double *pY0XRCoord);
   int getValue(double p);
};


