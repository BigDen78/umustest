//---------------------------------------------------------------------------

#ifndef UMUTest_BScanDrawH
#define UMUTest_BScanDrawH

#include "UMUTest_DataStorage.h"
#include "DeviceConfig.h"
#include "DeviceConfig_Autocon.h"
#include "DeviceCalibration.h"
#include "UMUTest_BScanLines.h"
//Added by KirillB
#include "UMUTest_Utils.h"

const
  MaxLinkData = 100;

typedef
  CID Shift_CID_List[6];

typedef
  TColor BScan_Color_List[6];

//Modifyed by KirillB
struct LinkDataItem
{
	TRect Rt;
	bool Enabled;
	sChannelDescription ChDesc;
	TBitmap* Buffer;
	TColor Color;
	eBScanLineDrawType drawType;
	bool Used;
};

class cBScanDraw
{
	private:

       int x;
       int y;
       cChannelsTable* Tbl;
       cDeviceConfig* Cfg;
       cJointTestingReport* Rep;
       cDeviceCalibration* Calib;
       std::vector < cBScanLines* > BScanLinesList;
	   void DrawSignals(int SysCoord, CID Channel, PsScanSignalsOneCrdOneChannel Sgl);

	   //Modifyed By KirillB
	   LinkDataItem LinkData[MaxLinkData];

       int channels_gates_temp[9][3];//[kp number][chId, stGate, edGate]

       int CursorSysCrd;
       int CursorBScanLinesIdx;
       int CursorBScanLinesSubIdx;
       bool bCursorEnabled;

	public:

        int Th;
        int GroupShift[15];

        cBScanDraw(cChannelsTable* Table, cDeviceConfig* Config, cJointTestingReport* Report,cDeviceCalibration* Calibration);
        ~cBScanDraw(void);

        //Sets cursor with xy screen coordinates (for example, with mouse click)
        void SetCursorXY(bool bEnable, int X = 0, int Y = 0, TColor col = clBlack);
        //Sets cursor with system b-scan coordinates
        void SetCursorSC(bool bEnable, int SysCoord = 0, int BScanLines_idx = -1,int BScanLines_sub_idx = -1, TColor col = clBlack);

        int SysCoordToScreen(int SysCoord,CID Channel);

		void ClearBScanLines();
        void AddBScanLine(cBScanLines* BScanLines);
        void Prepare(void);
        void Draw(bool NewItemFlag);
        bool GetSysCoord(int X, int Y, int* SysCoord, Shift_CID_List* Shift_CID, int* Shift_CID_Cnt, BScan_Color_List* BScanColor);
};

#endif
