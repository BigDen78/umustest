
#ifndef _INC_
#define _INC_


#define FULLVERSION                         // ������ ������ ��� ��� / ��������� ��� ������

// -----------------------------------------------------------------------------
//#define UMUDebug
#define DEBUG                         // ����� ���������� ����: ����� IP � ��������� �����, ���������� IP ������, LOG ���� + ���������� � �������
//#define SIMULATION                      // ��������� ���-��? ��� ����� ���������� ������ ������ REBUILD !!!
#define SIMULATION_CONTROLER_AND_MOTORS // ��������� ����������� �������� � �������
//#define SIMULATE_PATH_ENCODER_IN_REAL_UMU

#define AC_DEBUG_PRINT                  // ���������� ���������� �� �����������

#define FILTER_UMU_COUNT_TO_ONE         // ����������� ������ - ������� ���� ��� (����� �������� � 1-� ������������ �����, ��� ������ IP �������� DEBUG)
#define HANDSCAN_UMU_0                  // ����������� ������� � 2-�� ���� �� 1-�
#define HANDSCAN_LINE_NEW_UMU           // ����������� ������� � ����� 1 �� ����� 2
//#define FILTER_UMU_COUNT_TO_TWO         // ����������� ������ - ������� ��� ���� (����� �������� � 2-� ������������� ������, ��� ������ IP �������� DEBUG)

//#define CONTROLER_Kv1Kv2FailIgnore - ������������� �������� ��1, ��2
#define NO_MOTORS
//#define NO_CONTROLER_AND_MOTORS
//#define BScanSize_12Byte     //      - ����� ������ ������ �-��������� (��������� ������� 4 ����)

#define BSCAN_ALL_IN_ONE // - �� ������������ ????????
//#define SCANDRAW

// -----------------------------------------------------------------------------

const ScanLen = 1300;

#endif

// 1.
// GetChannelData - Line - ?

// 2.
// GetChannelBySLS(eChannelType ChType, int UMUIdx, UMUSide Side, int ReceiverLine, int StrokeIndex, eDeviceSide* Side_, CID* Channel)
//               (HandChannels[i].Line == ReceiverLine

// 3.
// void cDeviceConfig::GetSLSByChannel(eChannelType ChType, eDeviceSide Side_, CID Channel, int* UMUIdx, UMUSide* Side, int* Line, int* StrokeIndex)
//    *Line = ScanChannels[i].ReceiverLine;

