//---------------------------------------------------------------------------

#ifndef UMUTest_MainMenuUnitH
#define UMUTest_MainMenuUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.StdCtrls.hpp>;
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Buttons.hpp>;

#include "UMUTest_DebugStateUnit.h"
#include "UMUTest_AutoconMain.h"
#include "UMUTest_ConfigUnit.h";
//#include "IPCameraUnit.h"
//#include "ScreenMessageUnit.h";
//Added by KirillB
//#include "ScreenMessageUnit2.h";
//#include "EnterParamUnit.h";
//#include "HeaderTestUnit.h";
//#include "AutoconClases.h"
//#include "ArchiveDefinitions.h"

//---------------------------------------------------------------------------
class TMainMenuForm : public TForm
{
__published:	// IDE-managed Components
    TTimer *Timer1;
	TTimer *OpenConnectionTimer;
	TTimer *ChangeModeTimer;
    TOpenDialog *OpenDialog;
    TPanel *Panel7;
    TMemo *Memo1;
    TPanel *Panel3;
    TPanel *Panel6;
    TPanel *Panel4;
    TPanel *Panel1;
    TPanel *Panel5;
    TPanel *Panel2;
    TPanel *MenuPanel;
    TBevel *Bevel1;
    TButton *OpenConnectionButton;
    TButton *ExitButton;
    TButton *ShowHideDebugMenus;
    TPanel *Panel10;
    TComboBox *IPComboBox;
    TPanel *Panel11;
    TComboBox *ComboBox1;
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall ExitButtonClick(TObject *Sender);
    void __fastcall Timer1Timer(TObject *Sender);
    void __fastcall OpenConnectionButtonClick(TObject *Sender);
    void __fastcall Button8Click(TObject *Sender);
	void __fastcall OpenConnectionTimerTimer(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall Button6Click(TObject *Sender);
    void __fastcall ShowHideDebugMenusClick(TObject *Sender);
    void __fastcall FormResize(TObject *Sender);
    void __fastcall ComboBox1Change(TObject *Sender);
    void __fastcall IPComboBoxChange(TObject *Sender);
private:	// User declarations
	bool ChangeModeFlag;
    bool bDebugMenusAreHidden;
    void UpdateInfo();
public:		// User declarations
    __fastcall TMainMenuForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainMenuForm *MainMenuForm;
//---------------------------------------------------------------------------

extern cAutoconMain *AutoconMain;
extern cACConfig * ACConfig;
extern TStringList* TextLog;

#endif
