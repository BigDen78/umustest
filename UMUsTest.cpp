//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
#include "UMUsTest.inc"

//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("UMUTest_DebugStateUnit.cpp", DebugStateForm);
USEFORM("UMUTest_ConfigUnit.cpp", ConfigForm);
USEFORM("UMUTest_MainUnit.cpp", MainForm);
USEFORM("UMUTest_LogUnit.cpp", LogForm);
USEFORM("UMUTest_MainMenuUnit.cpp", MainMenuForm);
//---------------------------------------------------------------------------
#ifdef DEBUG
USEFORM("LogUnit.cpp", LogForm);
#endif
USEFORM("MainUnit.cpp", MainForm);
USEFORM("ConfigUnit.cpp", ConfigForm);
#ifdef DEBUG
USEFORM("DebugStateUnit.cpp", DebugStateForm);
#endif
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		 Application->Initialize();
		 Application->MainFormOnTaskBar = true;
		 Application->CreateForm(__classid(TMainMenuForm), &MainMenuForm);
         Application->CreateForm(__classid(TMainForm), &MainForm);
         Application->CreateForm(__classid(TLogForm), &LogForm);
         Application->CreateForm(__classid(TConfigForm), &ConfigForm);
         Application->CreateForm(__classid(TDebugStateForm), &DebugStateForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
