#pragma once


//Place filepath, function name and codeline in format string (windows)
#define LOGGER_OUT_FUNC_LINE

#include "Logger.h"

#define LOGCHANNEL_MAIN 		Logger::CHANNEL_ID1
#define LOGCHANNEL_AC 			Logger::CHANNEL_ID3
#define LOGCHANNEL_UMU 			Logger::CHANNEL_ID4
#define LOGCHANNEL_DEVICE		Logger::CHANNEL_ID5
#define LOGCHANNEL_ASCAN 		Logger::CHANNEL_ID6
#define LOGCHANNEL_BSCAN 		Logger::CHANNEL_ID7
#define LOGCHANNEL_CHECK 		Logger::CHANNEL_ID8

