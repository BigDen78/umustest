//---------------------------------------------------------------------------

#pragma hdrstop

#include "UMUTest_AutoconMain.h"
//---------------------------------------------------------------------------

void TestLoggerFunc();

cAutoconMain::cAutoconMain(int ConfigIndex)
{
    UMU_Connection_OK = false;
	CtrlWork = false;
	QryThreadFlag = false;

//	DT = NULL;
	DEV = NULL;
	Table = NULL;
	Config = NULL;
	Config2 = NULL;
	Calibration = NULL;
	DeviceEventManage = NULL;
	UMUEventManage = NULL;
	threadList = NULL;
//	Rep = NULL;
	CS = NULL;
	devt = NULL;
//	ac = NULL;
//	cavtk_test = NULL;

    Create(ConfigIndex);
}

cAutoconMain::~cAutoconMain(void)
{
    Release();
}


void cAutoconMain::Create(int ConfigIndex)
{
    Release();
    UMU_Connection_OK = false;
	CtrlWork = false;
	QryThreadFlag = false;

//	DT = NULL;
	DEV = NULL;
	Table = NULL;
	Config = NULL;
	Config2 = NULL;
	Calibration = NULL;
	DeviceEventManage = NULL;
	UMUEventManage = NULL;
	threadList = NULL;
//	Rep = NULL;
	CS = NULL;
	devt = NULL;
///	ac = NULL;
//	cavtk_test = NULL;

  // ���� ��������� �����������

	RegCrdParamsList[0].Used = false;
	RegCrdParamsList[1].Used = false;
	RegCrdParamsList[2].Used = false;

	RegCrdParamsList[3].StartCoord = -465;
	RegCrdParamsList[3].RegLength = 860;
	RegCrdParamsList[3].Used = true;

    RegCrdParamsList[4].Used = false;

	RegCrdParamsList[5].StartCoord = -435;
    RegCrdParamsList[5].RegLength = 830;
    RegCrdParamsList[5].Used = true;

	//Modifyed by KirillB (fix)
	RegCrdParamsList[6].StartCoord = 420;
	//RegCrdParamsList[6].StartCoord = -315;
	RegCrdParamsList[6].RegLength = 840;
	RegCrdParamsList[6].Used = true;

	RegCrdParamsList[7].Used = false;

    RegCrdParamsList[8].StartCoord = 25;
    RegCrdParamsList[8].RegLength = 180;
	RegCrdParamsList[8].Used = true;

	RegCrdParamsList[9].Used = false;

	RegCrdParamsList[10].StartCoord = -155;
    RegCrdParamsList[10].RegLength = 180;
    RegCrdParamsList[10].Used = true;

	RegCrdParamsList[11].Used = false;

    RegCrdParamsList[12].StartCoord = 266;
    RegCrdParamsList[12].RegLength = 161;
	RegCrdParamsList[12].Used = true;

    RegCrdParamsList[13].Used = false;

	RegCrdParamsList[14].StartCoord = 105;
    RegCrdParamsList[14].RegLength = 161;
	RegCrdParamsList[14].Used = true;

    //Initialize
    Table = new cChannelsTable;

    switch (ConfigIndex) {

       case 0: { Config = new cDeviceConfig_Avk31(Table, 32, 8);
                 Config2 = new cDeviceConfig_HeadScan(Table, 32, 16);
                 break; }
       case 1: { Config = new cDeviceConfig_Autocon(Table, 32, 16); break; }
       case 2: { Config = new cDeviceConfig_BUMTest(Table, 32, 16); break; }
       case 3: { Config = new cDeviceConfig_DB_lan(Table, 32, 16); break; }
       case 4: { Config = new cDeviceConfig_HeadScan(Table, 32, 16); break; }
       case 5: { Config = new cDeviceConfig_HeadScan_Test(Table, 32, 16); break; }
       case 6: { Config = new cDeviceConfig_Av15_usbcan(Table, 32, 16); break; }
       case 7: { Config = new cDeviceConfig_ManualSet(Table, 32, 16); break; }
       case 8: { Config = new cDeviceConfig_DB_can(Table, 32, 16); break; }
       case 9: { Config = new cDeviceConfig_Avk31E(Table, 32, 8); break; }
    }


    CS = new cCriticalSectionWin();

    Calibration = new cDeviceCalibration("calibration.dat", Table, CS);

    Calibration->SetGateMode(gmSearch);

    if (Calibration->Count() == 0)
    {
        Calibration->CreateNew();
        Calibration->SetCurrent(0);
    }
    else
    {
        Calibration->SetCurrent(0);
    }


//    Rep = new cJointTestingReport(ScanLen);
    //Added by KirillB
//    Rep->InitChannelIndicationData(this->Table);
}

void cAutoconMain::Release()
{
//	TextLog->Add("~cAutoconMain"); TextLog->SaveToFile("log.txt");

//	TextLog->Add("delete cavtk_test"); TextLog->SaveToFile("log.txt");

	UMU_Connection_OK = false;


  //	TextLog->Add("delete devt"); TextLog->SaveToFile("log.txt");
	if ((devt) && (devt->Started) && (!devt->Finished))
	{
		devt->EndWorkFlag = true;
		while (!devt->Finished) { Application->ProcessMessages(); };
		delete devt;
	}

   //	TextLog->Add("delete DEV"); TextLog->SaveToFile("log.txt");
//    DEV->EndWork();
	if (DEV) delete DEV;
	DEV = NULL;

    if(CS && DeviceEventManage)
        CleanupEventMgr();

	if (threadList) delete threadList;
	if (DeviceEventManage) delete DeviceEventManage;
	if (UMUEventManage) delete UMUEventManage;
	if (CS) delete CS;

	if (Calibration) delete Calibration;
	if (Table) delete Table;
	if (Config) delete Config;
	if (Config2) delete Config2;
//	if (Rep) delete Rep;


//	TextLog->Add("delete ac"); TextLog->SaveToFile("log.txt");
//	if (ac) delete ac;
//	ac = NULL;
	CtrlWork = false;

//	if (DT) delete DT;
//	DT = NULL;
}

typedef int TDataChannelIdList[3];

bool cAutoconMain::OpenConnection(int ModeIdx, CallBackProcPtr cbf)
{

	// -----------------------------------------------------------------------------

	int res = - 1;

    threadList = new cThreadClassList_Win;

    DeviceEventManage = new cEventManagerWin();
    UMUEventManage = new cEventManagerWin();
//    DataCnt = new cDataContainer();

    DEV = new cDevice(threadList,
                      Config,
                      Calibration,
                      Table,
                      (cEventManager*)DeviceEventManage,
                      (cEventManager*)UMUEventManage,
                      (cCriticalSection*)CS, (cCriticalSection*)CS);

    if (DEV->connectToDevice() < 0) return false;
    DEV->ManagePathEncoderSimulation = true;
//  DEV->ManagePathEncoderSimulation = false;

//    Table->SkipChangeGatePermission = true;

    devt = new cDeviceThread(this);
    devt->Resume();

    DEV->StartWork();

 	// -----------------------------------------------------------------------------

    DEV->SetCallBack(cbf);

	return 1;
}
// ---------------------------------------------------------------------------

void cAutoconMain::SetMode(int Mode_)
{
    ControlMode = Mode_;
/*    if (Mode_ == 1)
    {
        Panel3->Caption = "����� �������� ��������";
    }
    else
    {
        Panel3->Caption = "����� �������� ������������";
    }
    */
}
/*
void cAutoconMain::SetReportFileType(int Type)
{
    ReportFileType = Type;
}
*/
int cAutoconMain::GetMode(void)
{
    return ControlMode;
}

void cAutoconMain::CleanupEventMgr()
{
    DWORD DataID;
    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType DataType;

    CS->Enter();
    while (DeviceEventManage->EventDataSize() >= 12)
    {
        if (DeviceEventManage->ReadEventData(&DataID, 4, NULL))
        {
            switch(DataID)
            {
                case edAScanMeas: // �-���������, ������ �������� ��������� � ��������
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                   //     DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        Ptr2.pVoid = NULL;
                        DataType = edAScanMeas;
                        delete Ptr1.pAScanMeasure;
                        break;
                    }
                case edAScanData: // AScan
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edAScanData;
                        delete Ptr1.pAScanHead;
                        delete Ptr2.pAScanData;
                        break;
                    }
                case edTVGData: // TVG
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edTVGData;
                        delete Ptr1.pAScanHead;
                        delete Ptr2.pAScanData;
                        break;
                    }
                case edAlarmData:  // ������ ���
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edAlarmData;
                        delete Ptr1.pAlarmHead;
                        delete Ptr2.pAlarmItem;
                        break;
                    }
                case edBScan2Data: // ������ �-��������� (��� 2)
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edBScan2Data;
                        delete Ptr1.pBScan2Head;
                        delete Ptr2.pBScanData;
                        break;
                    }
                case edMScan2Data:  // ������ �-��������� (��� 2)
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                        DataType = edMScan2Data;
                        delete Ptr1.pBScan2Head;
                        delete Ptr2.pBScanData;
                        break;
                    }
                case edMSensor:     // ������ ������� �������
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edMSensor;
                        delete Ptr1.pMetalSensorData;
                        break;
                    }
                case edPathStepData:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edPathStepData;
                        delete Ptr1.pPathStepData;
                        break;
                    }

                case edA15ScanerPathStepData:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edA15ScanerPathStepData;
                        delete Ptr1.pPathStepData;
                        break;
                    }

                case edDefect53_1:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                   //     DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edDefect53_1;
                        delete Ptr1.pDefect53_1;
                        break;
                    }
                case edSignalSpacing:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                   //     DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edSignalSpacing;
                        delete Ptr1.pSignalSpacing;
                        break;
                    }
                case edSignalPacket:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                   //     DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edSignalPacket;
                        delete Ptr1.pBScanPacket;
                        break;
                    }
                case edBottomSignalAmpl:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                    //    DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edBottomSignalAmpl;
                        delete Ptr1.pBottomSignalAmpl;
                        break;
                    }
                case edDeviceSpeed:
                    {
                        DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                        DataType = edDeviceSpeed;
                        break;
                    }
            }
        }
    }
    CS->Release();
}

// --- DeviceThread -----------------------------------------------------------

__fastcall cDeviceThread::cDeviceThread(cAutoconMain *main_) :
    TThread(false),
    Ptr_List(DEVTHREAD_LIST_SIZE)
{
	main = main_;
	EndWorkFlag = False;
    //Ptr_List_Put_Idx = 0;
    //Ptr_List_Get_Idx = 0;
    OutTick = 0;
    InTick = 0;

    #ifdef TextLog_def
	TextLog = new (TStringList);
    #endif
}

__fastcall cDeviceThread::~cDeviceThread(void)
{
    #ifdef TextLog_def
    TextLog->SaveToFile("Device Thread log.txt");
    delete TextLog;
    #endif
}



void __fastcall cDeviceThread::Execute(void)
{
    uPtrListPointer Ptr1;
    uPtrListPointer Ptr2;
    EventDataType DataType;

	while (!EndWorkFlag)
    {
		unsigned long EventId;

		if (main->DeviceEventManage->WaitForEvent())
		{
            #ifdef TextLog_def
            main->DeviceEventManage->SaveIndex();
            while (main->DeviceEventManage->ReadEventData(&DataID, 4, NULL))
            {
                TextLog->Add(Format("%x", ARRAYOFCONST((DataID))));
            }
            main->DeviceEventManage->RestoreIndex();
            #endif

			while (main->DeviceEventManage->EventDataSize() >= 12)
			{
                main->CS->Enter();
				if (main->DeviceEventManage->ReadEventData(&DataID, 4, NULL))
				{
					//Lock (write to PtrList)
				  //	PtrListRWLock.Enter();
				  /*	if(Count_ptr>=DEVTHREAD_LIST_SIZE)
					{
						TRACE("Count_ptr>=DEVTHREAD_LIST_SIZE - %d",Count_ptr);
					} */
					switch(DataID)
					{
                        case edPathStepData:
                            {
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
							//	main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								Ptr2.pVoid = NULL;
								DataType = edPathStepData;
                                sPtrListItem item(Ptr1,Ptr2,DataType);
                                Ptr_List.push_back(item);
								break;
                            }
                        case edA15ScanerPathStepData:
                            {
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								Ptr2.pVoid = NULL;
								DataType = edA15ScanerPathStepData;
                                sPtrListItem item(Ptr1,Ptr2,DataType);
                                Ptr_List.push_back(item);
								break;
                            }

						case edAScanMeas: // �-���������, ������ �������� ��������� � ��������
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
							//	main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								Ptr2.pVoid = NULL;
								DataType = edAScanMeas;
                                //delete Ptr1.pAScanMeasure;
                                //break;

								//Added by KirillB
								//-------------Add channel maximum (tune)-----------
								/*if (main->ac->GetWorkCycle() != - 1)
								{
                                    if(Ptr_List_1[Ptr_List_Put_Idx])
                                    {
                                        main->Rep->AddChannelMaximum(((PtDEV_AScanMeasure)Ptr_List_1[Ptr_List_Put_Idx])->Channel,
                                                ((PtDEV_AScanMeasure)Ptr_List_1[Ptr_List_Put_Idx])->ParamA);
                                    }
								}*/

								//--------------------------------------------------
                                sPtrListItem item(Ptr1,Ptr2,DataType);
                                Ptr_List.push_back(item);
                                //Inc_Ptr_List_Put_Idx();
								break;
							}
						case edAScanData: // AScan
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
								DataType = edAScanData;
                                //delete Ptr1.pAScanHead;
                                //delete Ptr2.pAScanData;
                                //break;

                                sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
								break;
							}
						case edTVGData: // TVG
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
								DataType = edTVGData;
                                //delete Ptr1.pAScanHead;
                                //delete Ptr2.pAScanData;
                                //break;

								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
								break;
							}
						case edAlarmData:  // ������ ���
							{
								//PtDEV_AlarmHead    Head;
								//PtUMU_AlarmItem    Item;

								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
						 //		main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edAlarmData;
                                //delete Ptr1.pAlarmHead;
                                //delete Ptr2.pAlarmItem;
                                //break;
								//Ptr_List_1[Ptr_List_Put_Idx] = Head;
								//Ptr_List_2[Ptr_List_Put_Idx] = Item;
								//Id_List[Ptr_List_Put_Idx] = edAlarmData;
								//Inc_Ptr_List_Put_Idx();

                                sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);

								// ���������� ��������
							  /*	if (main->ac->GetWorkCycle() != - 1)
								{
									sAlarmRecord tmp;
                                    sChannelDescription chanDesc;
                                	for (unsigned int idx_ = 0; idx_ < Ptr1.pAlarmHead->Items.size(); idx_++)
									{
										int curChannelGroup = main->DEV->GetChannelGroup();
										tmp.Side = Ptr1.pAlarmHead->Items[idx_].Side;
										tmp.Channel = Ptr1.pAlarmHead->Items[idx_].Channel;

                                        main->Table->ItemByCID(tmp.Channel, &chanDesc); //���������� ������ �������� � ��������� ������ ���
										memcpy(tmp.State, Ptr1.pAlarmHead->Items[idx_].State, 4 * sizeof( bool ));

										if (main->RegCrdParamsList[curChannelGroup].Used)
											main->Rep->AddAlarmSignals( Ptr1.pAlarmHead->Items[idx_].Channel - AutoconStartCID, tmp);
                                    }
                                } */

								break;
							}
						case edBScan2Data: // ������ �-��������� (��� 2)
							{
								main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
						  //		main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edBScan2Data;
                                //delete Ptr1.pVoid;
                                //delete Ptr2.pVoid;
                                //break;

								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);

								//DEV_BScan2Head_ptr = (PtDEV_BScan2Head)dt->Ptr_List_1[idx];
								//PBScanData_ptr = (PtUMU_BScanData)dt->Ptr_List_2[idx];
								//frm->BScanDebugSeries->Clear();
								//DisCoord = DisCoord + abs(Data->Dir[0]);
								//frm->Caption = "������ ����: ��� - Dir: " + IntToStr(Data->Dir[0]) + "; XSysCrd: " + IntToStr(Data->XSysCrd[0]);
								//frm->Label27->Caption = "ChannelGroup: " + IntToStr(dev->GetChannelGroup());
								//frm->Label26->Caption = "State: " + IntToStr((int)frm->RegCrdParamsList[frm->WorkCycle].Used);
								//frm->Label28->Caption = "ResCrd: " + IntToStr(frm->RegCrdParamsList[frm->WorkCycle].StartCoord + Data->XSysCrd[0]);
								//frm->Label29->Caption = "RegCrd: " + IntToStr(frm->RegCrdParamsList[frm->WorkCycle].StartCoord + Data->XSysCrd[0] + ScanLen / 2);
								// frm->TextLog->Add("ResCrd: " + IntToStr(frm->RegCrdParamsList[frm->WorkCycle].StartCoord + Data->XSysCrd[0]) + " RegCrd: " + IntToStr(frm->RegCrdParamsList[frm->WorkCycle].StartCoord + Data->XSysCrd[0] + ScanLen / 2));

								// ���������� ��������
                               /*
								//���������� ���������� (��� �������)
								main->XSysCrd_Stored[0] = Ptr2.pBScanData->XSysCrd[0];
								main->XSysCrd_Stored[1] = Ptr2.pBScanData->XSysCrd[1];
								main->XSysDir_Stored[0] = Ptr2.pBScanData->Dir[0];
								main->XSysDir_Stored[1] = Ptr2.pBScanData->Dir[1];

                                bool bIsHandBScanEnabled = (main->DEV->GetChannelType() == ctHandScan);

								if ((main->ac->GetWorkCycle() != - 1) ||
                                        (main->ac->GetMode() == acAdjusting1) ||
                                        (main->ac->GetMode() == acAdjusting2) ||
                                        bIsHandBScanEnabled)
								{
									sScanSignalsOneCrdOneChannel tmp;
                                    sHandSignalsOneCrd handSig;
									for (unsigned int idx_ = 0; idx_ < Ptr1.pBScan2Head->Items.size(); idx_++)
									{
										PtUMU_BScanSignalList ptr = Ptr1.pBScan2Head->Items[idx_].BScanDataPtr;

                                        if(bIsHandBScanEnabled)
                                        {
                                            handSig.Count = Ptr1.pBScan2Head->Items[idx_].Count;
                                            for(int i = 0; i < handSig.Count; i++)
                                                handSig.Signals[i] = (*ptr)[i];
                                        }
                                        else
                                        {
                                            tmp.Count = Ptr1.pBScan2Head->Items[idx_].Count;
                                            for(int i = 0; i < tmp.Count; i++)
                                                tmp.Signals[i] = (*ptr)[i];
                                            tmp.WorkCycle = main->ac->GetWorkCycle();
                                        }

										int curChannelGroup = main->DEV->GetChannelGroup();

										if (main->RegCrdParamsList[curChannelGroup].Used || bIsHandBScanEnabled)
											//if (abs(Data->XSysCrd[0]) < abs(main->RegCrdParamsList[curChannelGroup].RegLength))
												if (Ptr2.pBScanData->Dir[0] != 0)
												{
													if(curChannelGroup<15)
													{
														main->XSysCrd_Transformed[0] = main->RegCrdParamsList[curChannelGroup].StartCoord + Ptr2.pBScanData->XSysCrd[0] + ScanLen / 2;
														main->XSysCrd_Transformed[1] = main->RegCrdParamsList[curChannelGroup].StartCoord + Ptr2.pBScanData->XSysCrd[1] + ScanLen / 2;
														int transformedCrd = 0;
														if(main->ac->GetMode() == acTuning) // if tuning mode - no start coord for carets
															transformedCrd = Ptr2.pBScanData->XSysCrd[0];
														else
															transformedCrd = main->RegCrdParamsList[curChannelGroup].StartCoord + Ptr2.pBScanData->XSysCrd[0] + ScanLen / 2;

                                                        if(bIsHandBScanEnabled)
                                                        {
                                                            main->Rep->StoreScanSignal(handSig);
                                                        }
                                                        else
														    main->Rep->PutScanSignals(transformedCrd, Ptr1.pBScan2Head->Items[idx_].Channel - AutoconStartCID, &tmp);
													}
												}
									}
								}
                                */
								break;
							}
						case edMScan2Data:  // ������ �-��������� (��� 2)
							{
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
						  //		main->DeviceEventManage->ReadEventData(&Ptr2, 4, NULL);
                                DataType = edMScan2Data;
                                //delete Ptr1.pBScan2Head;
                                //delete Ptr2.pBScanData;
                                //break;

								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);

							  /*
								dev->DEV_Event->ReadEventData(&DEV_BScan2Head_ptr, 4, NULL);
								dev->DEV_Event->ReadEventData(&PBScanData_ptr, 4, NULL);

								List_ptr[Count_ptr] = PBScanData_ptr;
								Count_ptr++;

								if (GetTickCount() - Time > 50 )
								{
									frm->DataIDFlag[edMScan2Data] = true;
									Synchronize(ValueToScreen);
									Time = GetTickCount();
									for (int idx = 0; idx < Count_ptr; idx++)
									{
									   delete List_ptr[idx];
									}
									Count_ptr = 0;
								}

								delete DEV_BScan2Head_ptr;
								DEV_BScan2Head_ptr = NULL;
							 */
								break;

							 //   delete PBScanData_ptr;
							 //   PBScanData_ptr = NULL;

							}
						case edMSensor:  // ������ �������
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
								Ptr2.pVoid = NULL;
                                DataType = edMSensor;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
								break;
                            }
                        case edDefect53_1:
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                           //     main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                DataType = edDefect53_1;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
                                break;
                            }
                        case edSignalSpacing:
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                           //     main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                DataType = edSignalSpacing;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
                                break;
                            }
                        case edSignalPacket:
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                            //    main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                DataType = edSignalPacket;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
                                break;
                            }
                        case edBottomSignalAmpl:
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                          //      main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                DataType = edBottomSignalAmpl;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
                                break;
                            }
                        case edDeviceSpeed:
                            {
                                main->DeviceEventManage->ReadEventData(&Ptr1, 4, NULL);
                                DataType = edDeviceSpeed;
								sPtrListItem item(Ptr1,Ptr2,DataType);
								Ptr_List.push_back(item);
                                break;
                            }

					}
					//Unlock (write to PtrList)
					//PtrListRWLock.Release();
                }
				main->CS->Release();
			}
		}
		//Added by KirillB
		//Sleep(1);
    }
}
