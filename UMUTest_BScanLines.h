
#ifndef UMUTest_BScanLinesH
#define UMUTest_BScanLinesH

#include <Vcl.Graphics.hpp>
#include <System.Types.hpp>

//Added by KirillB
#include "UMUTest_Utils.h"

enum eBScanLineDrawType
{
	LINEDRAW_STD = 0,
	LINEDRAW_SMALL_LINE,
};

enum eBScanHighlightType
{
	HIGHLIGHT_NONE = 0,
	HIGHLIGHT_WARNING,
	HIGHLIGHT_ERROR,
	HIGHLIGHT_OK,
};


class cBScanLines
{
	private:

	   int LineHeight;
	   //Added by KirillB
	   int SmallLineHeight;
       int BtnWidth;
       int LineWidth;

       TRect BtnBigTextRect[6][6];
       TRect BtnSmallTextRect[6][6];
       TRect BtnBtnRect[6][6];
	   TRect BtnRect[6][6];

	public:

        cBScanLines(void);
		~cBScanLines(void);

        cBScanLines& operator=(cBScanLines& other);

        TRect BoundsRect;
        int BScanLineCount;
		int BtnColumnCount;
        bool bOnlyOneCol;
		bool BtnVisible[6][6];
        TColor BtnColor[6][6];
        TColor BtnChColor[6][6];
        TColor BackGroundColor;
        TColor BtnBtnColor;
        TColor BtnSmallColor;
        UnicodeString BtnBigText[6][6];
		UnicodeString BtnSmallText[6][6];
        int BtnId[6][6];
		bool BtnBtnState[6][6];
		TRect LineRect[6];
		//Added by KirillB
		eBScanHighlightType BtnHighlight[6][6];
		eBScanLineDrawType LineDrawType[6];
		bool BtnChannelBehind[6][6];     //������� �����������/������������ ������ (��� ���������� ���������)
        bool BtnChannelIsActive[6][6];          //������� ���������� ������. ������������ ��� ���������.
		TBitmap* Buffer;
        bool Used;

		eBScanHighlightType LineHilightType; //���� ��������� ��� ����� �� ����������� ��

        void Refresh(void);
		void PaintToBuffer(void);
		int Click(int X, int Y);
		void ApplyBtnHighlightLevel(const char level, unsigned int LineIdx, unsigned int BtnIdx);
        bool GetSysCoord(int X, int Y, int MaxSysCoord, int* SysCoord, int* BScanLine);

        int SysCoordToScreen(int SysCoord, int MaxSysCoord);

       int LastGetCoord_BScanLines_idx;
       int LastGetCoord_X;
       bool DrawCursor;

};

#endif

