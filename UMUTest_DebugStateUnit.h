//---------------------------------------------------------------------------

#ifndef UMUTest_DebugStateUnitH
#define UMUTest_DebugStateUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <Vcl.Buttons.hpp>

#include "UMUTest_AutoconMain.h"

//---------------------------------------------------------------------------
class TDebugStateForm : public TForm
{
__published:	// IDE-managed Components
    TPanel *Panel2;
    TPanel *Panel1;
    TShape *Green1;
    TShape *Red1;
    TPanel *PanelUMU1;
    TShape *Green2;
    TShape *Red2;
    TLabel *m_DownloadSpeedLabel1;
    TLabel *m_UploadSpeedLabel1;
    TLabel *m_ErrorsLabel1;
    TPanel *PanelUMU2;
    TShape *Green3;
    TShape *Red3;
    TLabel *m_DownloadSpeedLabel2;
    TLabel *m_UploadSpeedLabel2;
    TLabel *m_ErrorsLabel2;
    TPanel *PanelUMU3;
    TShape *Green4;
    TShape *Red4;
    TLabel *m_DownloadSpeedLabel3;
    TLabel *m_UploadSpeedLabel3;
    TLabel *m_ErrorsLabel3;
    TCheckBox *m_ShowConsole;
    TCheckBox *m_ManualModeCheck;
    TCheckBox *PECheck;
    TPanel *Panel3;
    TPanel *Panel4;
    TChart *Chart1;
    TBarSeries *Series1;
    TButton *ClearButton;
    TButton *StartStopButton;
    TShape *GreenShape;
    TShape *RedShape;
    TBitBtn *ScreenshotBtn;
    TTimer *ScreenshotTimer;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);
    void __fastcall PECheckClick(TObject *Sender);
    void __fastcall ClearButtonClick(TObject *Sender);
    void __fastcall StartStopButtonClick(TObject *Sender);
    void __fastcall ScreenshotBtnClick(TObject *Sender);
    void __fastcall ScreenshotTimerTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TDebugStateForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TDebugStateForm *DebugStateForm;
//---------------------------------------------------------------------------
#endif
