//---------------------------------------------------------------------------

#include <vcl.h>
//#include "ConsoleDlgUnit.h"
//#include "ManualCtrlPanelUnit.h"
#include "UMUTest_LogUnit.h"
#include "UMUTest_Utils.h"
#pragma hdrstop

#include "UMUTest_DebugStateUnit.h"
//#include "TuneTableFormUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TDebugStateForm *DebugStateForm;
//---------------------------------------------------------------------------
__fastcall TDebugStateForm::TDebugStateForm(TComponent* Owner)
    : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TDebugStateForm::FormShow(TObject *Sender)
{
//    Top = 0;
//    Left = 0;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::FormCreate(TObject *Sender)
{
    Left = Screen->Width - Width - 125;
    Top = 05;
}
//---------------------------------------------------------------------------




void __fastcall TDebugStateForm::PECheckClick(TObject *Sender)
{
	if(PECheck->Checked)
		Width = 416;
	else
		Width = 139;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ClearButtonClick(TObject *Sender)
{
    AutoconMain->DEV->ResetPathEncoderGis();
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::StartStopButtonClick(TObject *Sender)
{
    if (StartStopButton->Tag == 0) {

        StartStopButton->Tag = 1;
        AutoconMain->DEV->SetPathEncoderGisState(true);
        GreenShape->BringToFront();

    } else {

        StartStopButton->Tag = 0;
        AutoconMain->DEV->SetPathEncoderGisState(false);
        RedShape->BringToFront();
    }
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ScreenshotBtnClick(TObject *Sender)
{
    this->Visible = false;
    LogForm->Visible = false;
    ScreenshotTimer->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TDebugStateForm::ScreenshotTimerTimer(TObject *Sender)
{
    HDC ScreenDC = GetDC(0);
    TBitmap* ScreenBmp = new TBitmap();

    __try
    {
        int W = Screen->Width;
        int H = Screen->Height;
        ScreenBmp->Width = W;
        ScreenBmp->Height = H;
        BitBlt(ScreenBmp->Canvas->Handle, 0, 0, W, H, ScreenDC, 0, 0, SRCCOPY);

        TJPEGImage* jpg = new TJPEGImage();
        __try
        {
            PathMgr::PushPath(L"$screenshots", L"resources\\screenshots");

            UnicodeString screenshot_path =
                        PathMgr::GetPath(StringFormatU(L"($screenshots)\\%s.jpg",FormatDateTime(L"ddmmyyyy_hh_mm_ss",Now()).c_str()).c_str());

            jpg->Assign(ScreenBmp);
            jpg->CompressionQuality = 90;
            jpg->SaveToFile(screenshot_path);
        }
        __finally
        {
            delete jpg;
        };
    }
    __finally
    {
        delete ScreenBmp;
    };

    this->Visible = true;
    LogForm->Visible = true;

    ScreenshotTimer->Enabled = false;
}
//---------------------------------------------------------------------------

