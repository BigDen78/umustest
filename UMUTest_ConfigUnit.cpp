//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <DateUtils.hpp>

#include "UMUTest_ConfigUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TConfigForm *ConfigForm;

//---------------------------------------------------------------------------
__fastcall TConfigForm::TConfigForm(TComponent* Owner)
    : TForm(Owner)
{
}

cACConfig::cACConfig(void)
{
    Operators = new TStringList();
    Ini = new TIniFile(ExtractFilePath(Application->ExeName) + "config.ini");
    BScan_ThTrackBarPos = 0;
}

cACConfig::~cACConfig(void)
{
    delete Operators;
    delete Ini;
}

void cACConfig::LoadData(void)
{
    Last_DateTime         = (TDateTime)Ini->ReadFloat("EnterParams", "Last_DateTime", Now()); // ���� / ����� ��������
	Last_TopStraightness  =            Ini->ReadFloat("EnterParams", "Last_TopStraightness", 0); // ��������������� ������ [��]
	Last_SideStraightness =            Ini->ReadFloat("EnterParams", "Last_SideStraightness", 0); // ��������������� ����� [��]

	Last_GangNumber	  = Ini->ReadInteger("EnterParams", "Last_GangNumber", 1); // ����� �����
	Last_ReportNumber = Ini->ReadInteger("EnterParams", "Last_ReportNumber", 1); // ����� ��������� ��������
    Last_TestReportNumber = Ini->ReadInteger("EnterParams", "Last_TestReportNumber", 1); // ����� ��������� ������������
	Last_PletNumber   = Ini->ReadInteger("EnterParams", "Last_PletNumber", 1); // ����� �����
	Last_JointNumber  = Ini->ReadInteger("EnterParams", "Last_JointNumber", 1); // ����� �����
 	ActivePage   	  = Ini->ReadInteger("EnterParams", "Last_Hardness", 0); // ��������
//	Last_HardnessUnit = Ini->ReadInteger("EnterParams", "Last_HardnessUnit", 1); // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

    Last_HardnessState = Ini->ReadBool("EnterParams", "HardnessState", false); // �������� - ���������� ��� ���
    Last_Hardness[0] = Ini->ReadInteger("EnterParams", "Last_Hardness0", 0); // ��������
    Last_Hardness[1] = Ini->ReadInteger("EnterParams", "Last_Hardness1", 0); // ��������
    Last_Hardness[2] = Ini->ReadInteger("EnterParams", "Last_Hardness2", 0); // ��������
    Last_Hardness[3] = Ini->ReadInteger("EnterParams", "Last_Hardness3", 0); // ��������
    Last_Hardness[4] = Ini->ReadInteger("EnterParams", "Last_Hardness4", 0); // ��������
    Last_Hardness[5] = Ini->ReadInteger("EnterParams", "Last_Hardness5", 0); // ��������
    Last_Hardness[6] = Ini->ReadInteger("EnterParams", "Last_Hardness6", 0); // ��������
    Last_Hardness[7] = Ini->ReadInteger("EnterParams", "Last_Hardness7", 0); // ��������
    Last_Hardness[8] = Ini->ReadInteger("EnterParams", "Last_Hardness8", 0); // ��������

	Last_VerSoft	   = Ini->ReadString("EnterParams", "Last_VerSoft", "-"); // ������ ��
	Last_Operator 	   = Ini->ReadString("EnterParams", "Last_Operator", "-"); // �������� ���
	Last_DefectCode    = Ini->ReadString("EnterParams", "Last_DefectCode", "-"); // ��� �������
	Last_ValidityGroup = Ini->ReadString("EnterParams", "Last_ValidityGroup", "-"); // ��. ��������
	Last_�onclusion    = Ini->ReadString("EnterParams", "Last_�onclusion", "-"); // ����������

    FotoSide = Ini->ReadBool("Common", "FotoSide", true); // ������� ��������� ���������� (true - ������� ���������)

    Last_WorkTimeSec = Ini->ReadInteger("Common", "Last_WorkTimeSec", 0);
    Last_WorkStartTime = Now();

    BScan_ThTrackBarPos = Ini->ReadInteger("Common", "BScan_ThTrackBarPos", 0);

    Current_Plaint   	   = Ini->ReadString("Common", "Current_Plaint", "-"); // �����������
	Current_PlantNumber   = Ini->ReadString("Common", "Current_PlantNumber", "-"); // ��������� ����� ���������

    Operators->Clear();
    Ini->ReadSection("OperatorsList", Operators);
    int Count = Operators->Count;
    Operators->Clear();
    for (int i = 0; i < Count; i++)
        Operators->Add(Ini->ReadString("OperatorsList", IntToStr(i), ""));

}

void cACConfig::SaveData(void)
{
    Ini->WriteFloat("EnterParams", "Last_DateTime",         Last_DateTime        	 	); // ���� / ����� ��������
    Ini->WriteFloat("EnterParams", "Last_TopStraightness",  Last_TopStraightness 	 	); // ��������������� ������ [��]
    Ini->WriteFloat("EnterParams", "Last_SideStraightness", Last_SideStraightness	 	); // ��������������� ����� [��]

    Ini->WriteInteger("EnterParams", "Last_GangNumber",     Last_GangNumber	     	 	); // ����� �����
    Ini->WriteInteger("EnterParams", "Last_ReportNumber",   Last_ReportNumber    	 	); // ����� ��������� ��������
    Ini->WriteInteger("EnterParams", "Last_TestReportNumber",   Last_TestReportNumber 	 	); // ����� ��������� ������������
    Ini->WriteInteger("EnterParams", "Last_PletNumber",     Last_PletNumber      	 	); // ����� �����
    Ini->WriteInteger("EnterParams", "Last_JointNumber",    Last_JointNumber     	 	); // ����� �����
    Ini->WriteInteger("EnterParams", "Last_Hardness",       ActivePage   	     	 	); // ��������
//    Ini->WriteInteger("EnterParams", "Last_HardnessUnit",   Last_HardnessUnit    	 	); // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC

    Ini->WriteBool("EnterParams", "HardnessState", Last_HardnessState); // �������� - ���������� ��� ���
    Ini->WriteInteger("EnterParams", "Last_Hardness0",   Last_Hardness[0]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness1",   Last_Hardness[1]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness2",   Last_Hardness[2]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness3",   Last_Hardness[3]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness4",   Last_Hardness[4]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness5",   Last_Hardness[5]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness6",   Last_Hardness[6]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness7",   Last_Hardness[7]); // ��������
    Ini->WriteInteger("EnterParams", "Last_Hardness8",   Last_Hardness[8]); // ��������

    Ini->WriteString("EnterParams", "Last_VerSoft",         Last_VerSoft	     	     	); // ������ ��
    Ini->WriteString("EnterParams", "Last_Operator",        Last_Operator 	     	 	); // �������� ���
    Ini->WriteString("EnterParams", "Last_DefectCode",      Last_DefectCode      	 	); // ��� �������
    Ini->WriteString("EnterParams", "Last_ValidityGroup",   Last_ValidityGroup   	 	); // ��. ��������
    Ini->WriteString("EnterParams", "Last_�onclusion",      Last_�onclusion  	 	    ); // ����������

    for (int i = 0; i < Operators->Count; i++)
        Ini->WriteString("OperatorsList", IntToStr(i), Operators->Strings[i]);

    Ini->WriteBool("Common", "FotoSide", FotoSide); // ������� ��������� ���������� (true - ������� ���������)

    Last_WorkTimeSec += SecondsBetween(Last_WorkStartTime, Now());
    Last_WorkStartTime = Now();
    Ini->WriteInteger("Common", "Last_WorkTimeSec", Last_WorkTimeSec);

    Ini->WriteInteger("Common", "BScan_ThTrackBarPos", BScan_ThTrackBarPos);

    //Do not overwrite Current_Plaint and Current_PlantNumber in config
    //Ini->WriteString("Common", "Current_Plaint", Current_Plaint); // �����������
    //Ini->WriteString("Common", "Current_PlantNumber", Current_PlantNumber); // ��������� ����� ���������
};



