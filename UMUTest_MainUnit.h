//---------------------------------------------------------------------------

#ifndef UMUTest_MainUnitH
#define UMUTest_MainUnitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>

#include <Vcl.AppEvnts.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.Series.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.TeeShape.hpp>
#include <System.Classes.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Graphics.hpp>
#include <Bde.DBTables.hpp>
#include <Data.DB.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.CheckLst.hpp>
#include <Vcl.Grids.hpp>
#include <VCL.Forms.hpp>
#include <VclTee.TeeGDIPlus.hpp>


#include <sstream>
//#include <IniFiles.hpp>
#include "cspin.h"
#include "Spin.hpp"

//#include "AScanViewUnit.h"
#include "Device.h"
#include "prot_umu_lan.h"
#include "prot_umu_rawcan.h"
#include "EventManager.h"
#include "EventManager_Win.h"
#include "DeviceConfig_Autocon.h"
#include "UMUTest_DataStorage.h"
#include "threadClassList_Win.h"
#include "DeviceCalibration.h"
#include "CriticalSection_Win.h"
#include "Platforms.h"
#include "prot_umu_lan_constants.h"
#include "TickCount.h"
#include "idatatransfer.h"
#include "UMUTest_BScanLines.h"
#include "UMUTest_BScanDraw.h"
//#include "cavtk2.h"
#include "UMUTest_AutoconMain.h"
#include "UMUTest_LogUnit.h"
#include "UMUTest_ConfigUnit.h";
#include "UMUTest_BScanLine.h"
#include "prot_umu_lan_constants.h"
#include "UMUTest_frmrestore.h"

#define MF_FLAG_BSCAN_HANDSCAN_BTN  0x01
#define MF_FLAG_BSCAN_CHANNEL_BTN   0x02
#define MF_FLAG_ASCAN_PHOTO_BTN     0x04
#define MF_FLAG_ASCAN_BSCAN_BTN     0x08
#define MF_FLAG_ASCAN_TUNE_BTN      0x10
#define MF_FLAG_BSCAN_CAPTION_CLOSE_PROTO   0x20
#define MF_FLAG_BSCAN_CAPTION_CLOSE         0x40
#define MF_FLAG_ASCAN_KU_PANEL      0x80
#define MF_FLAG_ASCAN_ATT_PANEL     0x100
#define MF_FLAG_ASCAN_SURFACE_COMBO 0x200


#define MF_SEARCH_FLAGS (MF_FLAG_BSCAN_HANDSCAN_BTN | MF_FLAG_BSCAN_CHANNEL_BTN | MF_FLAG_ASCAN_KU_PANEL | MF_FLAG_ASCAN_SURFACE_COMBO)
#define MF_TEST_FLAGS (MF_FLAG_BSCAN_CHANNEL_BTN | MF_FLAG_ASCAN_KU_PANEL)
#define MF_TUNE_FLAGS (MF_FLAG_BSCAN_CHANNEL_BTN | MF_FLAG_ASCAN_KU_PANEL)
#define MF_HAND_FLAGS (MF_FLAG_ASCAN_PHOTO_BTN | MF_FLAG_ASCAN_BSCAN_BTN | MF_FLAG_ASCAN_KU_PANEL | MF_FLAG_ASCAN_SURFACE_COMBO)
#define MF_HAND_TUNE_FLAGS (MF_FLAG_ASCAN_TUNE_BTN | MF_FLAG_ASCAN_KU_PANEL | MF_FLAG_ASCAN_ATT_PANEL)

/*
    const unsigned char TableDB[256] =
    {
     0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x11,0x11,0x11,0x22,0x22,0x22,
     0x33,0x33,0x33,0x33,0x44,0x44,0x44,0x44,0x44,0x44,0x55,0x55,0x55,0x55,0x55,0x55,
     0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x66,0x77,0x77,0x77,0x77,0x77,0x77,0x77,
     0x77,0x77,0x77,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,0x88,
     0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,0x99,
     0x99,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,
     0xAA,0xAA,0xAA,0xAA,0xAA,0xAA,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,0xBB,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,0xCC,
     0xCC,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,
     0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xDD,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,
     0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xEE,0xFF
    };
  */

//---------------------------------------------------------------------------
/*    static const int StartDelay_int = 0;
    static const int StartDelay_frac = 1;
    static const int StartAmpl = 2;
    static const int MaxDelay_int = 3;
    static const int AmplDBIdx_int = 4;
    static const int MaxDelay_frac = 5;
    static const int MaxAmpl = 6;
    static const int EndDelay_int = 7;
    static const int EndDelay_frac = 8;
*/
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TApplicationEvents *ApplicationEvents1;
    TPageControl *PageControl;
    TTabSheet *AScanSheet;
    TPanel *ControlsPanel;
    TPanel *NumPadPanel;
    TSpeedButton *SpeedButton10;
    TSpeedButton *SpeedButton11;
    TSpeedButton *SpeedButton20;
    TSpeedButton *SpeedButton21;
    TSpeedButton *SpeedButton26;
    TSpeedButton *SpeedButton27;
    TSpeedButton *SpeedButton28;
    TSpeedButton *SpeedButton30;
    TSpeedButton *SpeedButton31;
    TSpeedButton *SpeedButton32;
    TSpeedButton *SpeedButton33;
    TSpeedButton *SpeedButton34;
    TSpeedButton *SpeedButton35;
    TSpeedButton *SpeedButton36;
    TSpeedButton *SpeedButton29;
    TScrollBox *ScrollBox1;
	TPanel *Panel7;
    TPanel *PrismDelayPanel;
    TSpinButton *SpinButton05;
    TPanel *Panel8;
    TPanel *Panel9;
    TPanel *TVGPanel;
    TSpinButton *SpinButton04;
    TPanel *Panel10;
    TPanel *AScanKuPanel;
    TPanel *SensPanel;
    TSpinButton *SpinButton03;
    TPanel *Panel12;
    TPanel *Panel13;
    TPanel *EdGatePanel;
    TSpinButton *SpinButton02;
    TPanel *Panel15;
    TPanel *Panel16;
    TPanel *StGatePanel;
    TSpinButton *SpinButton01;
    TPanel *Panel18;
    TPanel *LabelPanel;
    TPanel *GeneratorPanel;
	TSpinButton *SpinButton06;
    TPanel *Panel21;
    TPanel *BScanRecPanel;
    TPanel *ReceiverPanel;
    TPanel *Panel14;
    TSpinButton *SpinButton3;
    TPanel *EndPanel;
    TPanel *Panel24;
    TSpeedButton *SpeedButton1;
    TPanel *RullerModePanel;
    TPanel *TuningPanel;
    TSpeedButton *CallibrationButton;
    TPanel *ValuePanel00;
    TPanel *Panel29;
    TPanel *Panel17;
    TButton *Button15;
    TPanel *Panel19;
    TSpeedButton *sbSideLine;
    TPanel *ValuePanel08;
    TPanel *Panel20;
    TPanel *Panel25;
    TPanel *BScanGateLevelPanel;
	TPanel *Panel28;
    TSpinButton *SpinButton5;
    TPanel *Panel32;
    TSpeedButton *SpeedButton8;
    TPanel *NoiseRedPanel;
    TPanel *Panel34;
    TPanel *Panel35;
    TSpeedButton *SpeedButton9;
    TPanel *AnglePanel;
    TPanel *Panel37;
    TPanel *Panel38;
    TPanel *DurationPanel;
    TPanel *Panel40;
    TSpinButton *SpinButton9;
    TPanel *Panel41;
    TPanel *DelayScalePanel;
    TPanel *Panel43;
    TSpinButton *SpinButton10;
    TPanel *Panel44;
    TPanel *ZondAmplPanel;
    TPanel *Panel46;
    TSpinButton *SpinButton11;
	TPanel *Panel33;
    TPanel *BScanMaxDelayPanel;
    TSpinButton *SpinButton1;
    TPanel *Panel39;
    TPanel *Panel42;
    TPanel *BScanMinDelayPanel;
    TSpinButton *SpinButton2;
    TPanel *Panel47;
    TPanel *Panel22;
    TPanel *EvaluationGateLevelPanel;
    TPanel *Panel30;
    TSpinButton *SpinButton4;
    TPanel *AScanAttPanel;
    TPanel *AttPanel;
    TSpinButton *SpinButton6;
    TPanel *Panel31;
    TPanel *Panel3;
    TPanel *BScanGatePanel;
    TPanel *Panel5;
    TSpinButton *SpinButton7;
    TPanel *Panel4;
    TSpeedButton *BScanViewBtn;
	TPanel *BScanViewPanel;
    TPanel *Panel52;
    TPanel *Panel55;
    TPanel *Stroke;
    TPanel *Panel57;
    TSpinButton *SpinButton8;
    TPanel *Panel56;
    TPanel *StrokeCount;
    TPanel *Panel59;
    TSpinButton *SpinButton12;
    TPanel *AScanDataPanel;
    TSplitter *Splitter1;
    TPanel *ParamPanel;
    TPanel *HPanel;
    TChart *AScanChart;
    TAreaSeries *AScanSeries_;
    TChartShape *MaxPosSeries;
    TAreaSeries *TVGSeries_;
    TChartShape *PeakSeries;
    TPointSeries *BScanDebugSeries;
    TChartShape *BScanGateMainSeries;
	TChartShape *BScanGateLeftSeries;
    TChartShape *BScanGateRightSeries;
    TChartShape *GateSeriesLeft;
    TChartShape *GateSeriesRight;
    TChartShape *Series1;
    TChartShape *Series2;
    TPanel *BScanDataPanel;
    TPanel *Panel64;
    TPanel *ChannelTitlePanel_;
    TPanel *Panel66;
    TPanel *LPanel;
    TPanel *NPanel;
	TPanel *Panel65;
    TPanel *InfoPanel;
    TTabSheet *TabSheet2;
    TTabSheet *TabSheet5;
    TChart *Chart2;
    TLineSeries *LineSeries3;
    TMemo *Memo2;
    TPanel *Panel67;
	TButton *Button19;
    TButton *Button20;
    TTimer *ClickTimer;
    TPanel *BackBtnPanel;
    TSpeedButton *SpeedButton2;
    TPanel *Panel26;
    TPanel *CBLogPanel;
    TTimer *UpdateLog;
    TTimer *ChangeModeTimer_;
    TLineSeries *AScanSeries;
    TLineSeries *TVGSeries;
    TPanel *ControlPanel;
    TButton *Button34;
    TCheckBox *CheckBox1;
    TTimer *Timer2;
    TTimer *DrawThreadTimer;
    TTimer *Timer3;
    TPanel *WorkStatePanel;
    TPanel *DebugPanel_;
    TPanel *Panel36;
    TPanel *GateIdxPanel;
    TPanel *GateIdxTextPanel;
    TSpinButton *SpinButton13;
    TPanel *Panel49;
    TCheckBox *CheckBox6;
    TPanel *BScanDebugPanel;
    TPanel *AmplDebugPanel;
    TChart *BScanChart;
    TLineSeries *BScanSeries;
    TPanel *Panel54;
    TLabel *Label1;
    TComboBox *SelEchoIdx;
    TChart *BScanChart2;
    TLineSeries *BSSeries1;
    TLineSeries *BSSeries2;
    TLineSeries *BSSeries3;
    TLineSeries *BSSeries4;
    TLineSeries *BSSeries5;
    TLineSeries *BSSeries6;
    TLineSeries *BSSeries7;
    TLineSeries *BSSeries8;
    TTimer *HandScanTimeTimer;
    TPanel *WriteBScanPanel;
    TSpeedButton *WriteBScanBtn;
    TPanel *WriteBScanLabel;
    TTimer *BScanHandTimer;
    TPanel *Panel6;
    TPanel *MarkPanel;
    TPanel *Panel23;
    TSpinButton *SpinButton14;
    TChartShape *MarkShape;
    TPanel *Panel11;
    TPanel *ChPanel_;
    TPanel *ChannelsPanel;
    TListBox *ChList;
    TPanel *Panel71;
    TPanel *Panel72;
    TButton *ClearCBLogButton;
    TPanel *Panel73;
    TPanel *Panel75;
    TSpeedButton *AutoPrismDelay;
    TPanel *Panel74;
    TSpeedButton *MarkSpeedButton;
    TPanel *Panel76;
    TMemo *CBLog;
    TPanel *BScanPanel;
    TPaintBox *BScanPB;
    TTimer *Timer5;
    TPanel *Panel1;
    TPanel *Panel50;
    TButton *RailTypeTuningButton;
    TButton *TuningButton;
    TButton *PathSimButton;
    TButton *BoltJntButton;
    TCheckBox *cbBScanDebug;
    TCheckBox *cbViewBScan;
    TCheckBox *CheckBox7;
    TCheckBox *CheckBox8;
    TPanel *Panel45;
    TLabel *Label29;
    TComboBox *GroupList;
    TTrackBar *TrackBar1;
    TButton *btnSaveAscan;
    TMemo *DebugMemo;
    TPanel *Panel60;
    TButton *Button4;
    TPanel *Panel68;
    TButton *Button5;
    TButton *Button6;
    TButton *Button8;
    TButton *PauseButton;
    TLineSeries *BS1;
    TLineSeries *BS2;
    TLineSeries *BS3;
    TLineSeries *BS5;
    TLineSeries *BS6;
    TLineSeries *BS7;
    TLineSeries *BS8;
    TChartShape *Series5;
    TMemo *BScanDebugMemo2;
    TLineSeries *BS4;
    TPointSeries *Series3;
    TPointSeries *Series4;
    TListBox *ChList2;
    TCheckBox *cbShowCID;
    TPanel *Panel2;
    TChart *AmpChart_0;
    TChartShape *AmpShape_0;
    TChart *AmpChart_P45;
    TChartShape *AmpShape_P45;
    TChart *AmpChart_M45;
    TChartShape *AmpShape_M45;
    TButton *CBLogSwitch;
    TFastLineSeries *AmpSeries_Pos_0;
    TFastLineSeries *AmpSeries_Pos_P45;
    TFastLineSeries *AmpSeries_Pos_M45;
    TTabSheet *TabSheet15;
    TChart *Chart1;
    TLineSeries *Series6;
    TPanel *ChannelTitlePanel;
    TChartShape *GateSeriesMain;
    TChartShape *GateSeriesMain_;
    TPanel *Panel51;
    TPanel *AlarmAlg;
    TPanel *BScanAlg;
    TLabel *DebugPanel;
    TTabSheet *���������;
    TPanel *Panel53;
    TMemo *CalMemo;
    TButton *Button7;
    TButton *Bigger;
    TButton *Smaller;
    TTabSheet *TabSheet18;
    TChart *Chart3;
    TLineSeries *Main_PE;
    TLineSeries *HandScan_PE;
    TLineSeries *HeadScaner_PE;
    TPanel *Panel58;
    TButton *Button10;
    TComboBox *ComboBox1;
    TCheckBox *CoordGraphCheckBox;
    TChartShape *Series7;
    TPanel *NotchPanel3;
    TPanel *LevelNotchPanel;
    TPanel *Panel79;
    TSpinButton *SpinButton15;
    TPanel *NotchPanel2;
    TPanel *LenNotchPanel;
    TPanel *Panel82;
    TSpinButton *SpinButton16;
    TPanel *NotchPanel1;
    TPanel *StNotchPanel;
    TPanel *Panel85;
    TSpinButton *SpinButton17;
    TButton *Button12;
    TButton *Button1811;
    TSpinEdit *AddKuSpinEdit;
    TScrollBox *ScrollBox2;
    TPageControl *PageControl2;
    TTabSheet *TabSheet6;
    TPanel *Panel69;
    TLabel *Label32;
    TLabel *Label33;
    TLabel *Label38;
    TLabel *Label39;
    TRadioButton *RadioButton1;
    TStringGrid *sgPathCounters;
    TPageControl *PageControl1;
    TTabSheet *TabSheet1;
    TLabel *Label35;
    TLabel *Label36;
    TLabel *Label37;
    TButton *PSOn;
    TButton *PSOff;
    TUpDown *UpDown4;
    TEdit *SpinEdit2;
    TUpDown *UpDown5;
    TEdit *SpinEdit3;
    TUpDown *UpDown6;
    TEdit *SpinEdit4;
    TTabSheet *TabSheet3;
    TLabel *Label34;
    TButton *Button3;
    TButton *Button13;
    TComboBox *cbPathSimIndex1;
    TUpDown *UpDown3;
    TEdit *SpinEdit1;
    TTabSheet *TabSheet4;
    TButton *Button14;
    TButton *Button16;
    TComboBox *cbPathSimIndex2;
    TComboBox *ComboBox2;
    TComboBox *ComboBox3;
    TRadioButton *RadioButton2;
    TButton *AmpSeries_Pos_P_45;
    TUpDown *UpDown2;
    TEdit *SpinEdit5;
    TCheckBox *cbPathEncoderGraph;
    TMemo *Memo3;
    TTabSheet *TabSheet7;
    TLabel *Label27;
    TLabel *Label28;
    TEdit *PrismDelayEdit;
    TButton *Button2;
    TTabSheet *TabSheet8;
    TMemo *MetallSensorInfo;
    TTabSheet *TabSheet9;
    TPanel *Panel27;
    TCheckListBox *CheckListBox1;
    TButton *Button1323;
    TButton *Button9;
    TButton *BScan12DB;
    TButton *BScan6DB;
    TMemo *BScanDebugMemo;
    TStringGrid *BScanData;
    TTabSheet *TabSheet10;
    TMemo *RailTypeTuningInfo;
    TTabSheet *TabSheet11;
    TSpeedButton *sbFirstGateMode;
    TSpeedButton *sbSecondGateMode;
    TLabel *Label2;
    TLabel *Label26;
    TTabSheet *TabSheet12;
    TBevel *Bevel1;
    TBevel *Bevel2;
    TLabel *Label40;
    TLabel *Label41;
    TButton *AC_On_Button;
    TButton *AC_Off_Button;
    TButton *Calibrate_AC;
    TCheckBox *cbViewAC;
    TButton *SetStartACDelay;
    TCheckBox *cbViewACState;
    TCheckBox *cbViewACTh;
    TButton *Button1;
    TButton *RrsButton7;
    TEdit *seACStartDelay;
    TUpDown *UpDown1;
    TEdit *seACThAddVal;
    TUpDown *UpDown7;
    TCheckBox *cbNoiseLog;
    TTabSheet *TabSheet13;
    TButton *BScanFiltrOff_;
    TButton *BScanFiltrOn_;
    TMemo *Memo1;
    TTabSheet *TabSheet14;
    TLabel *Label31;
    TButton *HSDeviceConfig;
    TButton *A31DeviceConfig;
    TButton *Test;
    TComboBox *cbSwitching;
    TTabSheet *TabSheet16;
    TLabel *StatusLabel;
    TButton *AkkButton_;
    TTabSheet *TabSheet17;
    TButton *AutoCal_Enable;
    TButton *AutoCal_Disable;
    TTabSheet *TabSheet19;
    TMemo *StrokeTableMemo;
    TPanel *Panel61;
    TRadioButton *RadioButton3;
    TRadioButton *RadioButton4;
    TTabSheet *TabSheet20;
    TLabel *Label3;
    TLabel *Label4;
    TEdit *Edit1;
    TUpDown *UpDown8;
    TEdit *Edit2;
    TUpDown *UpDown9;
    TEdit *Edit3;
    TUpDown *UpDown10;
    TEdit *Edit4;
    TUpDown *UpDown11;
    TEdit *Edit5;
    TUpDown *UpDown12;
    TEdit *Edit6;
    TUpDown *UpDown13;
    TEdit *Edit7;
    TUpDown *UpDown14;
    TEdit *Edit8;
    TUpDown *UpDown15;
    TPanel *Panel62;
    TLabel *Label5;
    TTabSheet *TabSheet21;
    TMemo *BlockListMemo;
    TPanel *Panel63;
    TButton *Button11;
    TPanel *Panel70;
    TTabSheet *������;
    TScrollBox *ScrollBox3;
    TLabel *Label6;
    TButton *TestButton3;
    TMemo *BScanSesionMemo;
    TButton *Button12_;
    TButton *Button13__;
    TPanel *Panel77;
    TButton *WorkButton;
    TButton *UnWorkButton;
    TButton *Button_17;
    TComboBox *ComboBox4;
    TPanel *Panel78;
    TTabSheet *tsGates;
    TStringGrid *sgGates;
    TTabSheet *AC_TabSheet;
    TPanel *Panel80;
    TButton *ACChartClearButton;
    TCheckBox *ACCheckBox;
    TButton *GenOffButton;
    TButton *GenOnButton;
    TChart *Chart5;
    TFastLineSeries *FastLineSeries1;
    TChart *Chart4;
    TFastLineSeries *FastLineSeries2;
    TChart *Chart6;
    TFastLineSeries *FastLineSeries3;
    TChart *Chart7;
    TFastLineSeries *FastLineSeries4;
    TChart *Chart8;
    TFastLineSeries *FastLineSeries5;
    TChart *Chart9;
    TFastLineSeries *FastLineSeries6;
    TChart *Chart10;
    TFastLineSeries *FastLineSeries7;
    TChart *Chart11;
    TFastLineSeries *FastLineSeries8;
    TChart *Chart12;
    TFastLineSeries *FastLineSeries9;
    TTabSheet *TabSheet22;
    TCheckBox *CheckBox2;
    TLabel *Label8;
    TSpinEdit *SpinEdit6;
    TTabSheet *tsAKTest;
    TPanel *Panel81;
    TPaintBox *PaintBox1;
    TButton *Button117;
    TEdit *Edit9;
    TLabel *Label7;
    TButton *Button18;
    TButton *Button21;
    TUpDown *UpDown16;
    TEdit *Edit10;
    TLabel *Label9;
    TPanel *Panel83;
    TButton *Button137;
    TPanel *Panel87;
    TPanel *Panel84;
    TLabel *Label10;
    TEdit *Edit11;
    TUpDown *UpDown17;
    TTabSheet *TabSheet23;
    TMemo *Memo4;
    TTimer *UMUEventTimer;
    TSpinEdit *SpinEdit7;
    TTabSheet *TabSheet24;
    TMemo *GainArrMemo;
    TPanel *Panel86;
    TButton *Button17;
    TLabel *BScanGateLabel;
    TLabel *BadBScanDelay;
    TPanel *TuningPanel2;
    TSpeedButton *SpeedButton3;
    TPanel *Panel90;
    TSpeedButton *SpeedButton4;
    TTabSheet *TabSheet25;
    TPaintBox *PaintBox2;
    TPanel *Panel88;
    TLabel *Label11;
    TEdit *Edit12;
    TLabel *Label12;
    TEdit *Edit13;
    TButton *Button22;
    TChart *Chart13;
    TBarSeries *Series8;
    TTabSheet *TabSheet26;
    TMemo *ShiftMemo;
    TPanel *Panel89;
    TChart *Chart17;
    TPointSeries *Series10;
    TChartShape *Series11;
    TChart *Chart16;
    TFastLineSeries *FastLineSeries11;
    TChart *Chart15;
    TFastLineSeries *FastLineSeries10;
    TChart *Chart14;
    TFastLineSeries *Series9;
    TTabSheet *TabSheet27;
    TChart *Chart18;
    TPointSeries *Series12;
    TPanel *Panel91;
    TButton *MScanerResetButton;
    TChart *Chart19;
    TFastLineSeries *MousScanerAScan;
    TTabSheet *TabSheet28;
    TMemo *Memo5;
    TPanel *Panel48;
    TPaintBox *BScanTestData;
    TCheckBox *CheckBox3;
    TTabSheet *FiltrationTabSheet;
    TMemo *FiltrationTabMemo;
    TPanel *Panel92;
    TLabel *Label13;
    TLabel *FiltrSizeLabel;
    TButton *Button23;
    TButton *PathSimButton2;
    TChart *Chart20;
    TLineSeries *LineSeries1;
    TLineSeries *LineSeries2;
    TLineSeries *LineSeries4;
    TChartShape *ChartShape1;
    TSplitter *Splitter3;
    TTabSheet *PE_Test;
    TPanel *Panel93;
    TButton *Button24;
    TComboBox *ComboBox5;
    TCheckBox *CheckBox4;
    TButton *Button25;
    TButton *Button26;
    TChart *Chart21;
    TLineSeries *LineSeries5;
    TLineSeries *LineSeries6;
    TChart *Chart22;
    TLineSeries *LineSeries7;
    TLabel *Label14;
    TLabel *UMU0_PEValue;
    TLabel *Label16;
    TLabel *UMU1_PEValue;
    TLabel *PE_Delta;
    TLabel *Label17;
    TTabSheet *TabSheet29;
    TPanel *Panel94;
    TButton *Button27;
    TCheckBox *CheckBox5;
    TCheckBox *CheckBox9;
    TCheckBox *CheckBox10;
    TPanel *Panel95;
    TChart *Chart23;
    TLineSeries *LineSeries8;
    TLineSeries *LineSeries9;
    TMemo *Memo6;
    TTabSheet *TabSheet30;
    TMemo *UMU0_Memo;
    TMemo *UMU1_Memo;
    TPanel *Panel96;
    TStringGrid *PathEncoderDebug;
    TCheckBox *CheckBox11;
    TTabSheet *TabSheet31;
    TButton *Button28;
    TButton *Button29;
    TLabel *SpeedState;
    TEdit *PathStepEdit;
    TTabSheet *TabSheet32;
    TLabel *Label15;
    TLabel *Label18;
    TLabel *Label19;
    TPanel *Panel97;
    TCheckBox *cbTrackingRailType;
    TChart *Chart24;
    TChartShape *Series13;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Initialize(void);
    void __fastcall InitializeLog(void);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall onLogoutChange(TObject *Sender);
    void __fastcall Button13Click(TObject *Sender);
    void __fastcall Button14Click(TObject *Sender);
    void __fastcall sbSideLineClick(TObject *Sender);
    void __fastcall ValueToControl(void);
    void __fastcall SpinButton06DownClick(TObject *Sender);
    void __fastcall SpinButton06UpClick(TObject *Sender);
    void __fastcall Button9Click(TObject *Sender);
    void __fastcall PrismDelayPanelClick(TObject *Sender);
    void __fastcall EditModePanelClick(TObject *Sender);
	void __fastcall FormShortCut(TWMKey &Msg, bool &Handled);
	void __fastcall ApplicationEvents1Message(tagMSG &Msg, bool &Handled);
	void __fastcall Button323Click(TObject *Sender);
	void __fastcall AScanChartClick(TObject *Sender);
	void __fastcall AScanChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
	void __fastcall Button127Click(TObject *Sender);
	void __fastcall NumPadClick(TObject *Sender);
	void __fastcall PathEncoderOnClick(TObject *Sender);
	void __fastcall PathEncoderOffClick(TObject *Sender);
    void __fastcall Panel61Resize(TObject *Sender);
    void __fastcall Button42Click(TObject *Sender);
	void __fastcall BScanPaintBoxPaint(TObject *Sender);
    void __fastcall KP2BScanPaintBoxPaint(TObject *Sender);
    void __fastcall KP4BScanPaintBoxPaint(TObject *Sender);
    void __fastcall AkkButtonClick(TObject *Sender);
    void __fastcall Button26Click(TObject *Sender);
    void __fastcall Button27Click(TObject *Sender);
    void __fastcall PageControlChange(TObject *Sender);
    void __fastcall Button19Click(TObject *Sender);
    void __fastcall Button20Click(TObject *Sender);
    void __fastcall ClickTimerTimer(TObject *Sender);
	void __fastcall Button23Click(TObject *Sender);
    void __fastcall Button1Click(TObject *Sender);
    void __fastcall sbFirstGateModeClick(TObject *Sender);
	void __fastcall BScanPBoxPaint(TObject *Sender);
    void __fastcall Button34Click(TObject *Sender);
    void __fastcall CheckBox1Click(TObject *Sender);
	void __fastcall m_MMode_ResetPathEncoderClick(TObject *Sender);
    void __fastcall Timer2Timer(TObject *Sender);
    void __fastcall DrawThreadTimerTimer(TObject *Sender);
    void __fastcall Timer3Timer(TObject *Sender);
    void __fastcall DebugPanel_DblClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall BScanPanel_Resize(TObject *Sender);
    void __fastcall BScanHandTimerTimer(TObject *Sender);
    void __fastcall cbSurfaceChange(TObject *Sender);
    void __fastcall GroupListClick(TObject *Sender);
    void __fastcall BoltJntButtonClick(TObject *Sender);
    void __fastcall MarkSpeedButtonClick(TObject *Sender);
    void __fastcall PathSimButtonClick(TObject *Sender);
    void __fastcall TuningButtonClick(TObject *Sender);
    void __fastcall RailTypeTuningButtonClick(TObject *Sender);
    void __fastcall ClearCBLogButtonClick(TObject *Sender);
    void __fastcall PSOnClick(TObject *Sender);
    void __fastcall cbBScanDebugClick(TObject *Sender);
    void __fastcall cbViewBScanClick(TObject *Sender);
    void __fastcall sbSecondGateModeClick(TObject *Sender);
    void __fastcall AutoPrismDelayClick(TObject *Sender);
    void __fastcall Button2Click(TObject *Sender);
    void __fastcall CheckBox7Click(TObject *Sender);
    void __fastcall CheckBox8Click(TObject *Sender);
    void __fastcall Timer5Timer(TObject *Sender);
    void __fastcall BScanPanelResize(TObject *Sender);
    void __fastcall btnSaveAscanClick(TObject *Sender);
    void __fastcall Button4Click(TObject *Sender);
    void __fastcall Button5Click(TObject *Sender);
    void __fastcall Button6Click(TObject *Sender);
    void __fastcall Button8Click(TObject *Sender);
    void __fastcall cbSwitchingChange(TObject *Sender);
    void __fastcall PauseButtonClick(TObject *Sender);
    void __fastcall Button1323Click(TObject *Sender);
    void __fastcall CheckListBox1Click(TObject *Sender);
    void __fastcall PSOffClick(TObject *Sender);
    void __fastcall AmpSeries_Pos_P_45Click(TObject *Sender);
    void __fastcall AC_On_ButtonClick(TObject *Sender);
    void __fastcall AC_Off_ButtonClick(TObject *Sender);
    void __fastcall cbShowCIDClick(TObject *Sender);
    void __fastcall cbViewACClick(TObject *Sender);
    void __fastcall SetStartACDelayClick(TObject *Sender);
    void __fastcall SpeedButton2Click(TObject *Sender);
    void __fastcall Calibrate_ACClick(TObject *Sender);
    void __fastcall UpdateLogTimer(TObject *Sender);
    void __fastcall CBLogSwitcClick(TObject *Sender);
    void __fastcall BScanFiltrOn_Click(TObject *Sender);
    void __fastcall BScanFiltrOff_Click(TObject *Sender);
    void __fastcall A31DeviceConfigClick(TObject *Sender);
    void __fastcall HSDeviceConfigClick(TObject *Sender);
    void __fastcall TestClick(TObject *Sender);
    void __fastcall BScan6DBClick(TObject *Sender);
    void __fastcall BScan12DBClick(TObject *Sender);
    void __fastcall RrsButton7Click(TObject *Sender);
    void __fastcall Button7Click(TObject *Sender);
    void __fastcall SmallerClick(TObject *Sender);
    void __fastcall BiggerClick(TObject *Sender);
    void __fastcall AkkButton_Click(TObject *Sender);
    void __fastcall AutoCal_EnableClick(TObject *Sender);
    void __fastcall AutoCal_DisableClick(TObject *Sender);
    void __fastcall Button10Click(TObject *Sender);
    void __fastcall StrokeTableMemoChange(TObject *Sender);
    void __fastcall RadioButton3Click(TObject *Sender);
    void __fastcall TabSheet19Show(TObject *Sender);
    void __fastcall UpDown8ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown10ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown12ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown14ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown9ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown11ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown13ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall UpDown15ChangingEx(TObject *Sender, bool &AllowChange, int NewValue,
          TUpDownDirection Direction);
    void __fastcall Button13__Click(TObject *Sender);
    void __fastcall TestButtonClick(TObject *Sender);
    void __fastcall TestButton3Click(TObject *Sender);
    void __fastcall WorkButtonClick(TObject *Sender);
    void __fastcall UnWorkButtonClick(TObject *Sender);
    void __fastcall Button12Click(TObject *Sender);
    void __fastcall Button1811Click(TObject *Sender);
    void __fastcall Button_17Click(TObject *Sender);
    void __fastcall cbNoiseLogClick(TObject *Sender);
    void __fastcall ACChartClearButtonClick(TObject *Sender);
    void __fastcall GenOffButtonClick(TObject *Sender);
    void __fastcall GenOnButtonClick(TObject *Sender);
    void __fastcall AC_TabSheetResize(TObject *Sender);
    void __fastcall CheckBox2Click(TObject *Sender);
    void __fastcall SpinEdit6Change(TObject *Sender);
    void __fastcall tsAKTestResize(TObject *Sender);
    void __fastcall Button18Click(TObject *Sender);
    void __fastcall Button117Click(TObject *Sender);
    void __fastcall Button21Click(TObject *Sender);
    void __fastcall Button137Click(TObject *Sender);
    void __fastcall UMUEventTimerTimer(TObject *Sender);
    void __fastcall Add(unsigned char id, UnicodeString Text);
    void __fastcall SpinEdit7Change(TObject *Sender);
    void __fastcall Button17Click(TObject *Sender);
    void __fastcall TabSheet25Resize(TObject *Sender);
    void __fastcall TabSheet25Show(TObject *Sender);
    void __fastcall Button22Click(TObject *Sender);
    void __fastcall MScanerResetButtonClick(TObject *Sender);
    void __fastcall Panel48Resize(TObject *Sender);
    void __fastcall CheckBox3Click(TObject *Sender);
    void __fastcall PathSimButton2Click(TObject *Sender);
    void __fastcall Button24Click(TObject *Sender);
    void __fastcall CheckBox5Click(TObject *Sender);
    void __fastcall CheckBox9Click(TObject *Sender);
    void __fastcall CheckBox10Click(TObject *Sender);
    void __fastcall CheckBox11Click(TObject *Sender);
    void __fastcall Button28Click(TObject *Sender);
    void __fastcall Button29Click(TObject *Sender);
    void __fastcall cbTrackingRailTypeClick(TObject *Sender);
	void __fastcall PageControl2Change(TObject *Sender);


private:	// User declarations

    void __fastcall OnAScanFrameLeftBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameRightBtnClick(TObject *Sender);
    void __fastcall OnAScanFrameCloseBtnClick(TObject *Sender);

	bool State;
	bool renewParam;
	int MouseX;
	int MouseY;
	bool ChangeModeFlag;
    eDeviceMode DEVMode;
   // TList * BScanChartList;

	void __fastcall Search_or_Test(int WorkCycle);

	//-------------Added by KirillB-----------------
	void __fastcall TuneProc(int WorkCycle);
	bool bLeftToRightBScanLayout;
	void MMode_UpdateInfo();
	void MMode_UpdateBScanTabs();
	void MMode_DrawPBoxes();
	void MMode_DrawCaretPositionPBox(bool bForceRedraw= false);
	void MMode_DrawBScanPBox();
	UINT MMode_BScanGroupIdx;
	BYTE MMode_MaximumsAtPos[1000];
	int KPErrorValue[9];
//	cScanScriptPrg tunePrg;
//	std::ostringstream scriptOutput;
	TLineSeries* m_MMode_LineSeries;
	TBarSeries* m_MMode_BarSeries;
	std::vector<UINT> MMode_SelChartChannels;
	std::vector<int> MMode_chSortedByKP[9];
	int prevUpdateWorkCycle;
	//cDrawRailUnit RailDraw;
	//cScanScriptCmdBuffer tuneCmdBuffer;
	//-----------------------------------------------

    int LastLogLinesCount;

      // ����� �-��������� �� ������ �-���������
    CID Shift_CID[6];
    int Shift_CID_Cnt;
    TColor BSColor[6];
    int LastSelectChannels;
    DWORD LastSelectChannelsTickCount;
    unsigned int EventCounts_[255];



public:		// User declarations

    int UMU1_last_XSysCrd;
    int UMU0_last_XSysCrd;
    int PathStepGis[100];
    int PathStepGis_LastVal;
    unsigned long PathStepGis_LastUpdate;
    unsigned long PathStepGis_LastUpdate2;
    unsigned long PathStepGis_LastUpdate3;
    int PathStepGis_LastVal2;
    AScanData SaveAData;
    TPoint StartScanerCursorPos;

    TStringList* LogLines_;
    int LastXSysCrd;
    UnicodeString LastChType;

    DWORD NoiseTick;
    DWORD NoiseStartTick;
    TStringList* LogCrd_;

    TBitmap * ACBuffer;
    TBitmap * BSBuffer;
    TBitmap * BSDebugBuffer;

    int ACMinMas[2014];
    int ACMaxMas[2014];
    bool NeedUpdate;
    float LastParamM;

    TStringList* LogLines;
    int AScanSysCoord;
    bool DebugOut;
    TBScanLine *bsl;
    TMemoryStream *msSaveAscan;
    int MainPathEncoderIndex;
    int MainPathEncoderType; // 0 - ��������; 1 - ��������

    int StartACGate;
    int ACTreshold[64];

    //--------Added by KirillB------------
//    DWORD mf_flags;
//    void SetFlags(DWORD flags);


//	void setVisualMode(eBScanVisualMode mode);

	//void SetBScanLayout(bool bLeftToRight);

	void CalculateBScanLayout(int BScanWidth,int BScanHeight,
						TRect* rects /*Must be 9 rects*/, int* pRailWidth,int* pRailHeight);
	//��������� � ������ paint box'a
	void UpdatePBoxData(bool NewItemFlag);
	//��������� ������� �� �����
	void DrawPBoxData();
	//cDrawRailUnit* GetRailDraw() {return &RailDraw;};
    DWORD StartHandBScanTime;
	//-------------------------------

	//cBScanLines* BScanLines[9];
//    TPaintBox *BScanPaintBox[9];
//	TTabSheet *BScanTabSheet[9];
//	cBScanPainter BScanPainter;
	//cBScanDraw* Draw;
	//bool HandScanFlag;
    //bool ViewMode; // ����� ��������� ���������

	__fastcall TMainForm(TComponent* Owner);

	bool EditMode;
	double SaveMax1;
	double SaveMax2;
	bool ShowLog;
	bool StartWork;
	UnicodeString GetUMUMessageName(unsigned char id, unsigned char id2);
	UnicodeString GetCDUMessageName(unsigned char id, unsigned char id2);

	UnicodeString EnterModeUndo;
	bool EnterModeState;
	TPanel* EditPanel;
	int Counter;

	void __fastcall AddLanLog(int UMUIdx, unsigned char * msg, int way);
  	void __fastcall AddCANLog(int UMUIdx, unsigned char * msg, int way);

	void  prn(char* s);

	float DelayToChartValue(float Src);
	float ChartValueToDelay(float Src);
	float PixelToChartValue(int Pixel);
	unsigned char Filter(unsigned char Value);

//    void eventSignProc(BOOL start);
//	void __fastcall endProcedure(MOTIONRESULT r);
//	void __fastcall breakEndProcedure(MOTIONRESULT r);
//	void __fastcall MyendProcedure(MOTIONRESULT r);
//	void __fastcall MyendProcedure2(MOTIONRESULT r);

	TRect Rects[9];
	bool SkipFlag;
    unsigned long Tag;

//	HANDLE hThread;
//	bool hThreadNeedEndFlag;
//	bool hThreadEndFlag;

	void __fastcall StartAlign(void);

/*    void GetMessages(TMessagesType Type, UnicodeString Text, DWord Time); // �������� ��������� �� ������
    void SearchMotion_(int WorkCycle); // ����� "�����"
    void TestMotion_(int WorkCycle);   // ����� "����"
	void AdjustMotion_(int WorkCycle); // ����� "���������"
	void TuneMotion_(int WorkCycle); // ����� "���������"
	void ManualMotion_(int WorkCycle); // ����� "������"
	void __fastcall Clear_Data(void);
*/
    void __fastcall ReDrawAScan(void);
//    void __fastcall SetPoint(int Crd, int minDelay, int maxDelay, int Ampl);
//    void __fastcall SetViewCrdZone(int MinCrd, int MaxCrd);
//    void __fastcall ClearChartData(void);
    void CallBackProcPtr(int GroupIndex, eDeviceSide Side, CID Channel, int GateIndex, eValueID ID, int Value);
    void SetChannelList(void);
};

// --------------------------------------------------------------------------

typedef struct
{
	int Pixel1;
	int Pixel2;
} thparams;

thparams params;

class TDrawThread : public TThread
{
  DWORD Time;

  cCriticalSectionWin * cs;
  int MetalSensorState[3][2]; // ��������� ������� �������
  int XSysCrd_;

public:
  thparams params;
  int SaveParamH;
  int SaveParamKd;
  bool EndWorkFlag;

  void __fastcall Execute(void);
  void __fastcall ValueToScreen(void);

  __fastcall TDrawThread(cCriticalSectionWin * cs);
  __fastcall ~TDrawThread(void);

};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
extern cAutoconMain *AutoconMain;
extern cACConfig * ACConfig;
extern TStringList* TextLog;

#endif
