//---------------------------------------------------------------------------

#ifndef UMUTest_TestThreadUnitH
#define UMUTest_TestThreadUnitH

#include <Classes.hpp>
#include "UMUTest_AutoconMain.h"
#include "UMUsTest.inc"

//---------------------------------------------------------------------------


class cAutoconMain; // ����������� ��. AutoconMain.h

class cTestThread : public TThread
{
    cAutoconMain *Main;
    void __fastcall Execute(void);

public:
    bool EndWorkFlag;
    bool StateOK;

    __fastcall cTestThread(cAutoconMain *Main_);
    __fastcall ~cTestThread(void);
};

#endif
