#include "UMUTest_frmrestore.h"
#include "assert.h"


//----------------------------------------------------------
frmrestore::frmrestore()
{
   pL.a = pL.b = pL.c = 0;
   pR.a = pR.b = pR.c = 0;
   mode = mRightToLeft;
//   fractionCoef = 7.0 / 8.0; //
}
//----------------------------------------------------------
double frmrestore::Abs(double n)
{
 return n*((n>0)+(-n<0)-1);
}
//---------------------------------------------------------------------------
double frmrestore::getSqParValue(tPARPARAMS *pP,double x)
{
return (pP->a * x + pP->b) * x + pP->c;
}
//---------------------------------------------------------------------------
double frmrestore::getLineValue(tPARPARAMS *pP,double x)
{
return pP->b * x + pP->c;
}
//---------------------------------------------------------------------------
double frmrestore::getXCoordOfLineByValue(tPARPARAMS *pP,double value)
{
return (value - pP->c) / pP->b;
}
//---------------------------------------------------------------------------
double frmrestore::getSqParDiffValue(tPARPARAMS *pP,double x)
{
return pP->a * 2.0 * x + pP->b;
}
//---------------------------------------------------------------------------
// ���������� ���������� x, � ������� ����������� ����� �������� ��������
double frmrestore::getXCoordBySqParDiffValue(tPARPARAMS *pP,double diffValue)
{
return (diffValue - pP->b)/ (pP->a * 2.0);
}

//---------------------------------------------------------------------------
int frmrestore::getValue(double p)
{
double res;
 switch (mode)
 {
   case mLCut:
//	   res = getSqParValue(&pR,p);
	   res = getLineValue(&pR,p);
	   break;
   case mRCut:
//	   res = getSqParValue(&pL,p);
	   res = getLineValue(&pL,p);
	   break;
   case mLCut2:
	   res = pL.c;
	   break;
   case mRCut2:
       res = pR.c;
	   break;
   default:
	   if (p <= crossPoint.x)  res = getSqParValue(&pL,p);
		  else res = getSqParValue(&pR,p);
 }
 if (res >= 0)  return floor(res);
 return ceil(res);
}
//---------------------------------------------------------------------------
 void frmrestore::gefCurve(tIncomeData *pData)
{
tPOINT startP;
tPOINT endP;
double kLLine; // ���� ������� �����, ����������� y1,y0
double kRLine; // ���� ������� �����, ����������� y2,y1

   if (pData->x0 == pData->x1)
   {
	  if (pData->y1 != pData->y2)  mode = mLCut;
		  else
		  {
			  getHLineCoef(&pL,pData->y2);
			  mode = mLCut2;
		  }
   }
	  else if (pData->x1 == pData->x2)
		   {
			  if (pData->y0 != pData->y1)  mode = mRCut;
				  else
				  {
					  getHLineCoef(&pR,pData->y0);
                      mode = mRCut2;
                  }
		   }
			   else
			   {
				   kLLine = ((double)(pData->y1 - pData->y0)) / (pData->x1 - pData->x0);
				   kRLine = ((double)(pData->y2 - pData->y1)) / (pData->x2 - pData->x1);
				   if (Abs(kLLine) <= Abs(kRLine))  mode = mRightToLeft; // ������ �������� ����� ��������
					   else mode = mLeftToRight;
			   }
   switch(mode)
   {
   case mLCut:
	   startP.x = pData->x1;
	   startP.y = (double)pData->y1;
	   endP.x = pData->x2;
	   endP.y = (double) pData->y2;
//	   getRParCoefs(&pR,startP,endP,kRLine);
	   getLineCoefs(&pR,startP,endP);
	   break;
   case mRCut:
	   startP.x = pData->x0;
	   startP.y = (double)pData->y0;
	   endP.x = pData->x1;
	   endP.y = (double) pData->y1;
//	   getLParCoefs(&pL,startP,endP,kLLine);
	   getLineCoefs(&pL,startP,endP);

	   break;
   case mLeftToRight:
	   startP.x = pData->x0;
	   startP.y = (double)pData->y0;
	   endP.x = pData->x1;
	   endP.y = (double) pData->y1;
	   getLParCoefs(&pL,startP,endP,0.0);
	   crossPoint.x = getXCoordBySqParDiffValue(&pL,kRLine);
	   crossPoint.y = getSqParValue(&pL,crossPoint.x);
	   endP.x = pData->x2;
	   endP.y = (double)pData->y2;
	   getRParCoefs(&pR,crossPoint,endP,kRLine);
	   break;
//
   case mLCut2:
   case mRCut2:
       break;
   default:
	   startP.x = pData->x1;
	   startP.y = (double)pData->y1;
	   endP.x = pData->x2;
	   endP.y = (double) pData->y2;
	   getRParCoefs(&pR,startP,endP,0.0);
	   crossPoint.x = getXCoordBySqParDiffValue(&pR,kLLine);
	   crossPoint.y = getSqParValue(&pR,crossPoint.x);
	   startP.x = pData->x0;
	   startP.y = (double)pData->y0;
	   getLParCoefs(&pL,startP,crossPoint,kLLine);
   }
}
//----------------------------------------------------------
void frmrestore::gefCurve(tIncomeData *pData, double *pY0XLCoord,double *pY0XRCoord)
{

   assert((pY0XLCoord != NULL) && (pY0XRCoord != NULL));
   gefCurve(pData);
   switch(mode)
   {
   case mLCut:

	   *pY0XLCoord =   pData->x0;
//	   *pY0XRCoord =   gefEqRoot(false);
//	   if (*pY0XRCoord < 0) *pY0XRCoord = pData->x2; //  ���� � *pY0XRCoord �� ������������
	   *pY0XRCoord = getXCoordOfLineByValue(&pR,0.0);
	   break;
   case mRCut:
	   *pY0XRCoord =   pData->x2;
//	   *pY0XLCoord =   gefEqRoot(true);
//	   if (*pY0XLCoord < 0) *pY0XLCoord = pData->x0;
	   *pY0XLCoord = getXCoordOfLineByValue(&pL,0.0);
	   break;
   case mLCut2:
   case mRCut2:
	   *pY0XLCoord =   pData->x0;
	   *pY0XRCoord =   pData->x2;
	   break;
   default:
	   *pY0XLCoord =   gefEqRoot(true);
	   *pY0XRCoord =   gefEqRoot(false);
	   if (*pY0XLCoord < 0) *pY0XLCoord = pData->x0;
	   if (*pY0XRCoord < 0) *pY0XRCoord = pData->x2;
   }
}
//----------------------------------------------------------
// lParFlag - ���� true, ���� ����� ����� ����������� � ���� X ����� ��������,
// ����� - ������ ��� ������ � ���������� ��� ���������
// ��������������, ��� ��������, ��������������� �������������, ������� ������ ���
// ���������� � A!=0, ������ ����� ����� ����������� � ���� x
// -- ���� A==0, ���������� -2.0 ��� ��������� ������������� ����������� ����������
//  ���� ������������� �������� ������������ ��������
double frmrestore::gefEqRoot(bool lParFlag)
 {
tPARPARAMS *pParams;
double D;
	if (lParFlag) pParams = &pL;
		else pParams = &pR;
//#ifdef DEBUG
	if (pParams->a == 0)
	{
		return (double)-2;
	}
//#endif
	D = pParams->b*pParams->b - 4.0 * pParams->a * pParams->c;
//#ifdef DEBUG
	if (D<0) return (double)-1;
//#endif
	if (lParFlag)
	{
		return (sqrt(D)-pParams->b) / (2.0 * pParams->a);
	}
	return (-pParams->b - sqrt(D)) / (2.0 * pParams->a);
 }
//----------------------------------------------------------
// diffValue - �������� ����������� � �������� �����
void frmrestore::getLParCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP,double diffValue)
{
   pP->a = (startP.y - endP.y  + diffValue * (endP.x - startP.x) )/ sqrOfSubstract(endP.x,startP.x);
   pP->c = startP.y -(pP->a*(startP.x - 2.0 * endP.x) + diffValue)*startP.x;
   pP->b = diffValue- 2.0 * pP->a * endP.x;
}
//----------------------------------------------------------
// diffValue - �������� ����������� � ��������� �����
void frmrestore::getRParCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP,double diffValue)
{
   pP->a = (endP.y - startP.y - diffValue * (endP.x - startP.x) )/ sqrOfSubstract(endP.x,startP.x);
   pP->c = startP.y + (pP->a * startP.x - diffValue)*startP.x;
   pP->b = diffValue- 2.0 * pP->a * startP.x;
}
//----------------------------------------------------------
void frmrestore::getLineCoefs(tPARPARAMS *pP,tPOINT startP, tPOINT endP)
{
   pP->a = 0.0;
   pP->b = (endP.y - startP.y) / (endP.x - startP.x);
   pP->c =  startP.y - pP->b * startP.x;
}
//----------------------------------------------------------
void frmrestore::getHLineCoef(tPARPARAMS *pP,double y)
{
   pP->a = pP->b = 0.0;
   pP->c = y;
}
