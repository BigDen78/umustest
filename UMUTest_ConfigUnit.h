//---------------------------------------------------------------------------

#ifndef UMUTest_ConfigUnitH
#define UMUTest_ConfigUnitH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.IniFiles.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <TChar.h>
#include <jpeg.hpp>
#include <ExtCtrls.hpp>
//#include "IPCameraUnit.h";

//#pragma pack(push)
//#pragma pack(8)

class cACConfig
{
    private:


    public:

    TIniFile * Ini;

    cACConfig::cACConfig(void);
    ~cACConfig(void);

    void LoadData(void);
    void SaveData(void);

	TDateTime Last_DateTime;     	  // ���� / ����� ��������
	int	Last_GangNumber;			  // ����� �����
	int	Last_ReportNumber;	 	      // ����� ��������� ��������
	int	Last_TestReportNumber;	 	  // ����� ��������� ������������
	int Last_PletNumber;        	  // ����� �����
	int Last_JointNumber;    	      // ����� �����
	float Last_TopStraightness; 	  // ��������������� ������ [��]
	float Last_SideStraightness;	  // ��������������� ����� [��]
	int	Last_Hardness_; 			  // ��������
	int	Last_HardnessUnit; 			  // ��. ���. ��������: 1 � HB, 2 - HV, 3 - HRC
	UnicodeString Last_VerSoft;		  // ������ ��
	UnicodeString Last_Operator; 	  // �������� ���
	UnicodeString Last_DefectCode;    // ��� �������
	UnicodeString Last_ValidityGroup; // ��. ��������
	UnicodeString Last_�onclusion;    // ����������
    int	Last_Hardness[9];             // �������� [9 ��������]
    bool Last_HardnessState;          // �������� - ���������� ��� ���

    int BScan_ThTrackBarPos;

    UnicodeString Current_Plaint;   	  // �����������
	UnicodeString Current_PlantNumber;   // ��������� ����� ���������
	int ActivePage;                      // �������� �������


    int Buffer[1023];


    bool FotoSide;                    // ������� ��������� ���������� (true - ������� ���������)
    int Buffer1[1023];
    TStringList *Operators;
    int Buffer2[1023];

    unsigned int Last_WorkTimeSec;
    TDateTime Last_WorkStartTime;
};

//#pragma pack(pop)
//#pragma pack()

//---------------------------------------------------------------------------
class TConfigForm : public TForm
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
    __fastcall TConfigForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TConfigForm *ConfigForm;
//---------------------------------------------------------------------------
#endif
