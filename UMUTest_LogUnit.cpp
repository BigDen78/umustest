//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UMUTest_LogUnit.h"
//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TLogForm *LogForm;
//---------------------------------------------------------------------------
__fastcall TLogForm::TLogForm(TComponent* Owner)
	: TForm(Owner)
{
	logger.setChannelMask(Logger::CHANNEL_ID_ALL);
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::Button12Click(TObject *Sender)
{
    Memo1->Lines->Clear();
    MainForm->LogLines->Clear();
}

//---------------------------------------------------------------------------

void __fastcall TLogForm::CheckBox1Click(TObject *Sender)
{
	if (CheckBox1->Checked)
	{
	    Tag  = 1;
		Width = SaveWidth;
		Height = SaveHeight;
        Tag = 0;
	}
	else
	{
		SaveWidth = Width;
		SaveHeight = Height;
		Tag  = 1;
		Width = 98;
		Height = 70;
		Tag = 0;
	};
/*
    MainForm->Timer2->Tag = 1;
    MainForm->Timer2Timer(NULL);
    MainForm->Timer2->Tag = 0;
*/
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::FormCreate(TObject *Sender)
{
		Width = 94;
		Height = 71;

        Tag  = 1;
		SaveWidth = 1050 + 88;
		SaveHeight = 190;
        Left = Screen->Width - Width - 25;
        Top = 05;
/*
    Left = 0;
    SaveHeight = 871;
    SaveWidth = 588;
    CheckBox1->Checked = true;
    CheckBox1Click(NULL);
*/
}
//---------------------------------------------------------------------------
void __fastcall TLogForm::FormResize(TObject *Sender)
{
    if (Tag == 1) { return;

    }
    SaveWidth = Width;
    SaveHeight = Height;

    ScrollBar1Change(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TLogForm::UpdateLogTimerTimer(TObject *Sender)
{

//    this->BringToFront();
    if(!LoggerMsgBtn->Down)
        return;

	Logger::sLoggerMessage msg;
	while(logger.pop(msg))
	{
		std::wstring str = msg.toString().c_str();
		//str.TrimRight();
		if(str[str.size()-1]==L'\n')
			str[str.size()-1]=L' ';
//		Memo1->Lines->Add(str.c_str());
        MainForm->LogLines->Add(str.c_str());

	}

	if(logger.isNewPersistendData())
	{
		Logger::sLoggerMessage* pMessages;
		UINT MsgSize = 0;

		if(logger.readPersistentData(&pMessages,&MsgSize,true))
		{
			while(PersistDataListBox->GetCount() != MsgSize)
			{
				if( PersistDataListBox->GetCount() <= MsgSize)
					PersistDataListBox->AddItem("",NULL);
			}

			for(unsigned int i=0;i<MsgSize;i++)
			{
				while(PersistDataListBox->GetCount() <= i)
					PersistDataListBox->AddItem("",NULL);

				PersistDataListBox->Items->Strings[i] = pMessages[i].data.c_str();
				//PersistDataListBox->AddItem(pMessages[i].data.c_str(),NULL);
			}


			PersistDataListBox->Update();
            delete [] pMessages;
        }
	}
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------


void __fastcall TLogForm::ScrollBar1Change(TObject *Sender)
{
    HGDIOBJ OldFont;
    HDC Hand;
    TTextMetric TM;
    TRect Rect;
    int tempint;
    Hand = GetDC(Memo1->Handle);
    OldFont = SelectObject(Hand, Memo1->Font->Handle);
    GetTextMetrics(Hand, &TM);
    Memo1->Perform(EM_GETRECT, 0, (NativeInt)(&Rect));
    tempint = (Rect.Bottom - Rect.Top) / (TM.tmHeight + TM.tmExternalLeading);
    SelectObject(Hand, OldFont);
    ReleaseDC(Memo1->Handle, Hand);

    // --------------------

    if (ScrollBar1->Tag == 1) return;
    Memo1->Clear();
    Memo1->Lines->BeginUpdate();
     for (int i = 0; i < tempint; i++)
       if (MainForm->LogLines->Count > i + LogForm->ScrollBar1->Position)
         Memo1->Lines->Add(IntToStr(i + ScrollBar1->Position) + ". " + MainForm->LogLines->Strings[i + ScrollBar1->Position]);
     Memo1->Lines->EndUpdate();
}

//---------------------------------------------------------------------------

void __fastcall TLogForm::AddText(UnicodeString Str)
{
	if (LogState->Down)
        MainForm->LogLines->Add(Str);
}


void __fastcall TLogForm::Button1Click(TObject *Sender)
{
//LogLines
    Clipboard()->AsText = MainForm->LogLines->Text;
}
//---------------------------------------------------------------------------


