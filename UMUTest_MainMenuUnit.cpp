//---------------------------------------------------------------------------

#include <vcl.h>
#include <DateUtils.hpp>

#include "UMUTest_MainMenuUnit.h"
#include "UMUTest_MainUnit.h"
#include "UMUsTest.inc"

//---------------------------------------------------------------------------
//#pragma package(smart_init)
#pragma resource "*.dfm"
TMainMenuForm *MainMenuForm;
//---------------------------------------------------------------------------
__fastcall TMainMenuForm::TMainMenuForm(TComponent* Owner)
    : TForm(Owner)
{
//	ChangeModeFlag = false;
//    AutoconMain = new cAutoconMain(ComboBox1->ItemIndex);
}
//---------------------------------------------------------------------------//---------------------------------------------------------------------------
void __fastcall TMainMenuForm::FormCreate(TObject *Sender)
{

//	this->BorderWidth = 100;
	//AutoconMain = NULL;

	#ifndef DEBUG
	IPComboBox->ItemIndex = 0;
	OpenConnectionTimer->Enabled = true;
	#endif

    #ifdef FILTER_UMU_COUNT_TO_ONE         // ����������� ������ - ������� ���� ��� (����� �������� � 1-� ������������ �����, ��� ������ IP �������� DEBUG)
	IPComboBox->ItemIndex = 0;
	#endif

	#ifdef DEBUG
//	DebugMenuPanel->Visible = true;
	#endif

	TextLog = new(TStringList);


    #ifdef DEBUG
        bDebugMenusAreHidden = false;
    #else
        bDebugMenusAreHidden = true;
    #endif

 //   InfoPanel->Caption = DateTimeToStr(Now());
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::ExitButtonClick(TObject *Sender)
{
    Timer1->Enabled = false;
	ExitButton->Enabled = false;
	MainForm->Close();

    this->Close();
    Application->Terminate();

	if (AutoconMain)
	{
		delete AutoconMain;
		AutoconMain = NULL;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Timer1Timer(TObject *Sender)
{
/*
	int UMUOKCount = 0;
    bool cavtk_test_ = false;

    UnicodeString Text;

	if ((AutoconMain))
	{
//        if (AutoconMain->cavtk_test->StateOK)
  /*      {
            if (DebugStateForm)
                DebugStateForm->Green1->BringToFront();
            Text = "���������� �����������";
            cavtk_test_ = true;
        }
        else * /
        {
            if (DebugStateForm)
                DebugStateForm->Red1->BringToFront();
            Text = "��� ����������";
		}
    } else Text = "��� ����������";

    if ((AutoconMain) && (AutoconMain->DEV) && (DebugStateForm)) {
        DebugStateForm->Series1->Clear();
        for (int i = -5; i < 5; i++)
            DebugStateForm->Series1->AddXY(i, AutoconMain->DEV->PathEncoderGis[i + 5]);
    }

	if ((AutoconMain) && (AutoconMain->DEV))
	{
		for (int i = 0; i < AutoconMain->DEV->GetUMUCount(); i++)
		{
			if (AutoconMain->DEV->GetUMUOnLineStatus(i))
			{
                if (DebugStateForm)
                    switch (i)
                    {
                        case 0: {
                                    DebugStateForm->Red2->Visible = false;
                                    DebugStateForm->Green2->Visible = true;
                                    break;
                                }
                        case 1: {
                                    DebugStateForm->Red3->Visible = false;
                                    DebugStateForm->Green3->Visible = true;
                                    break;
                                }
                        case 2: {
                                    DebugStateForm->Red4->Visible = false;
                                    DebugStateForm->Green4->Visible = true;
                                    break;
                                }
                    }

				Text = "���������� �����������";
				UMUOKCount++;

                UpdateInfo();
			}
			else
			{
                if (DebugStateForm)
                    switch (i)
                    {
                        case 0: { DebugStateForm->Red2->Visible = true;
                                  DebugStateForm->Green2->Visible = false ;
                                  break; }
                        case 1: { DebugStateForm->Red3->Visible =true ;
								  DebugStateForm->Green3->Visible = false;
								  break; }
						case 2: { DebugStateForm->Red4->Visible =true ;
								  DebugStateForm->Green4->Visible =false ;
									break; }
					}
				Text = "��� ����������";
			}

			if (DebugStateForm)
			{
                if (AutoconMain->DEV->GetUMUCount() >= 1) DebugStateForm->m_ErrorsLabel1->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(0)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(0));
                if (AutoconMain->DEV->GetUMUCount() >= 2) DebugStateForm->m_ErrorsLabel2->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(1)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(1));
                if (AutoconMain->DEV->GetUMUCount() >= 3) DebugStateForm->m_ErrorsLabel3->Caption = "E: " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(2)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(2));
				//if (AutoconMain->DEV->GetUMUCount() >= 1) DebugStateForm->PanelUMU1->Caption = "  ��� �1       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(0)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(0));
				//if (AutoconMain->DEV->GetUMUCount() >= 2) DebugStateForm->PanelUMU2->Caption = "  ��� �2       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(1)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(1));
				//if (AutoconMain->DEV->GetUMUCount() >= 3) DebugStateForm->PanelUMU3->Caption = "  ��� �3       " + IntToStr(AutoconMain->DEV->GetUMUErrorMessageCount(2)) + "/" + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(2));

                DebugStateForm->m_DownloadSpeedLabel1->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(0)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel1->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(0)) / 1024.f);

                DebugStateForm->m_DownloadSpeedLabel2->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(1)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel2->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(1)) / 1024.f);

                DebugStateForm->m_DownloadSpeedLabel3->Caption = StringFormatU(L"D: %.2f KB/s", float(AutoconMain->DEV->GetUMUDownloadSpeed(2)) / 1024.f);
                DebugStateForm->m_UploadSpeedLabel3->Caption = StringFormatU(L"U: %.2f KB/s", float(AutoconMain->DEV->GetUMUUploadSpeed(2)) / 1024.f);

                DebugStateForm->Refresh();
            }
            #ifdef DEBUG
			if (AutoconMain->DEV->GetUMUSkipMessageCount(i) != 0) Text = "������ ����: " + IntToStr(AutoconMain->DEV->GetUMUSkipMessageCount(i));
            #endif
		}
	}
	else
	{
	}
*/
}

//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::OpenConnectionButtonClick(TObject *Sender)
{
//    ShowHideDebugMenus->Click();

	ChangeModeFlag = false;
    MainForm->InitializeLog();
    AutoconMain = new cAutoconMain((int)ComboBox1->Items->Objects[ComboBox1->ItemIndex]);

	Timer1->Enabled = true;
    OpenConnectionButton->Enabled = false;

    Timer1Timer(NULL);
	if (!AutoconMain->OpenConnection(IPComboBox->ItemIndex, MainForm->CallBackProcPtr))
	{
		OpenConnectionTimer->Interval = 7500;
		OpenConnectionTimer->Enabled = true;
        AutoconMain->Release();
        AutoconMain->Create((int)ComboBox1->Items->Objects[ComboBox1->ItemIndex]);
		return;
	}
    MainForm->Initialize();

//    if (ComboBox1->ItemIndex == 1) {
//        MainForm->SensToRailTypePanel->Visible = false;
//        MainForm->AlarmControlPanel->Visible = false;
//        MainForm->ChannelsPanel->Height = MainForm->ChannelsPanel->Height + MainForm->AlarmControlPanel->Height + MainForm->SensToRailTypePanel->Height;
//    }

//    AdjustingForm->Initialize();

    UpdateInfo();

//        AutoconMain->DEV->DisableAll();
//        AutoconMain->DEV->ResetPathEncoder();
//        AutoconMain->DEV->EnableAll();
        AutoconMain->DEV->SetMode(dmSearch);

//        AutoconMain->SetReportFileType(4); // ��� ������: 1 - ��������; 2 - ����; 3 - ���������; 4 - �������� ������ �����;
//        AutoconMain->DEV->Update(false);

        MainForm->Visible = true;
        MainForm->PageControl->ActivePage = MainForm->AScanSheet;
        MainForm->ValueToControl();


}

AnsiString __fastcall GetWVersion()
{
  AnsiString temp;
  VS_FIXEDFILEINFO VerValue;
  DWORD size, n;
  n = GetFileVersionInfoSize(Application->ExeName.c_str(), &size);
  if (n > 0)
  {
    char *pBuf = new char[n];
    GetFileVersionInfo(Application->ExeName.c_str(), 0, n, pBuf);
    LPVOID pValue;
    UINT Len;
    if (VerQueryValueA(pBuf, "\\", &pValue, &Len))
    {
      memmove(&VerValue, pValue, Len);
      temp = IntToStr((__int64)VerValue.dwFileVersionMS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionMS & 0xFFFF);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionLS >> 16);
      temp += "." + IntToStr((__int64)VerValue.dwFileVersionLS & 0xFF);
    }
    delete pBuf;
  }
  return temp;
}

void TMainMenuForm::UpdateInfo()
{

}

//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button8Click(TObject *Sender)
{
    ConfigForm->Visible = true;
    ConfigForm->BringToFront();
}

void __fastcall TMainMenuForm::OpenConnectionTimerTimer(TObject *Sender)
{
	OpenConnectionTimer->Enabled = false;
	OpenConnectionButtonClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TMainMenuForm::FormShow(TObject *Sender)
{
    if (Tag == 0)
    {
        Tag = 1;
        ACConfig = new cACConfig;
        ACConfig->LoadData();
    }

    ComboBox1->Clear();

    #ifdef FULLVERSION
    ComboBox1->Items->AddObject("������-31", (TObject*)0);
    ComboBox1->Items->AddObject("�������-�", (TObject*)1);
    ComboBox1->Items->AddObject("��� ����", (TObject*)2);
    ComboBox1->Items->AddObject("Deutsche Bahn (��������������)", (TObject*)3);
    ComboBox1->Items->AddObject("������ ������� ������", (TObject*)4);
    ComboBox1->Items->AddObject("������ ������� ������ (�������� ���)", (TObject*)5);
    ComboBox1->Items->AddObject("������-15 USB-CAN Win",  (TObject*)6);
    ComboBox1->Items->AddObject("��� ���� 2",  (TObject*)7);
    ComboBox1->Items->AddObject("Deutsche Bahn (�������������)",  (TObject*)8);
    ComboBox1->Items->AddObject("������-31 (�������)",  (TObject*)9);
    #endif

    #ifndef FULLVERSION
    ComboBox1->Items->AddObject("������-31", (TObject*)0);
    ComboBox1->Items->AddObject("��� ����", (TObject*)2);
    ShowHideDebugMenus->Enabled = false;
    DebugStateForm->Visible = false;
    LogForm->Visible = false;
    #endif

    ComboBox1->ItemIndex = 0;
    if (ACConfig->Last_PletNumber < ComboBox1->Items->Count)
        ComboBox1->ItemIndex = ACConfig->Last_PletNumber;

    IPComboBox->ItemIndex = ACConfig->Last_JointNumber;
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::FormDestroy(TObject *Sender)
{
	ACConfig->SaveData();
	delete ACConfig;
   //	TextLog->SaveToFile("log.txt");
	delete TextLog;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::Button6Click(TObject *Sender)
{
    AutoconMain->DEV->InitTuningGateList();
    AutoconMain->DEV->SetMode(dmCalibration);
    AutoconMain->DEV->Update(false);

    AutoconMain->SkipRep = True;
	MainForm->Visible = true;
//    MainForm->HandChannelPanel->Visible = true;
    MainForm->PageControl->ActivePage = MainForm->AScanSheet;
//    MainForm->Button16->Click();
    MainForm->ValueToControl();
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::ShowHideDebugMenusClick(TObject *Sender)
{
    if(bDebugMenusAreHidden)
    {
        LogForm->Visible = true;
        //DebugStateForm->Visible = true;
        DebugStateForm->Visible = false;
        ShowHideDebugMenus->Caption = "������ ����";
    }
    else
    {
        LogForm->Visible = false;
        DebugStateForm->Visible = false;
        ShowHideDebugMenus->Caption = "�������� ����";
    }

    bDebugMenusAreHidden = !bDebugMenusAreHidden;
}
//---------------------------------------------------------------------------


void __fastcall TMainMenuForm::FormResize(TObject *Sender)
{
    MenuPanel->Left = (Width - MenuPanel->Width) / 2;
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::ComboBox1Change(TObject *Sender)
{
    ACConfig->Last_PletNumber = (int)ComboBox1->Items->Objects[ComboBox1->ItemIndex];
}
//---------------------------------------------------------------------------

void __fastcall TMainMenuForm::IPComboBoxChange(TObject *Sender)
{
    ACConfig->Last_JointNumber = IPComboBox->ItemIndex;
}
//---------------------------------------------------------------------------


