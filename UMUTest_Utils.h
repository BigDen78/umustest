#ifndef UMUTest_UtilsH
#define UMUTest_UtilsH



 #include "UMUTest_AutoconLogging.h"
 #include "UMUTest_HTMLLogger.h"

#define CLAMP(x, min, max) ((x<min) ? min : ((x>max) ? max : x))
#define LERP(a,b,c)     (((b) - (a)) * (c) + (a))
#define SMOOTH(a,b,c)     (a*c + b*(1-c))//(((b) - (a)) * (c) + (a))

#define COLORVALUE(x,ind) (((BYTE*)(&x))[ind])

//���������� ��������� ��� �����
template<typename StringType, typename CharType>
inline StringType StringFormatTpl(const CharType* Str, ...)
{
	va_list args;
	va_start(args, Str);

	//����������� ������� (���)


	int len = 0;
	if(sizeof(CharType) == 1)//ANSI
		len=vsnprintf_s(" ",1,(char*)Str, args)+ 1;
	else                     //UTF8
		len=vsnwprintf_s(L" ",1,(wchar_t*)Str, args)+ 1;


	CharType* buffer = (CharType*)malloc( len * sizeof(CharType) );

	if(sizeof(CharType) == 1)//ANSI
		vsprintf_s((char*)buffer, len, (char*)Str, args);
	else                     //UTF8
		vswprintf_s((wchar_t*)buffer, len, (wchar_t*)Str, args);

	va_end(args);

	StringType RetStr;
	RetStr=buffer;
	free(buffer);
	return RetStr;
}

#define StringFormatU StringFormatTpl<UnicodeString,wchar_t>
#define StringFormatA StringFormatTpl<AnsiString,char>

#ifdef _UNICODE
	#define StringFormatStd StringFormatTpl<std::wstring, TCHAR>
#else
	#define StringFormatStd StringFormatTpl<std::string, TCHAR>
#endif

#define StringFormatStdA StringFormatTpl<std::string, char>
#define StringFormatStdW StringFormatTpl<std::wstring, wchar_t>


inline void TRACE(const char* Str, ...)
{
	va_list args;
	va_start(args, Str);

	//����������� ������� (���)
	int len=vsnprintf_s(" ",1,Str, args)+ 1;
	char* buffer = (char*)malloc( len * sizeof(char) );
	vsprintf_s(buffer, len, Str, args);   //���������� ��������������
	va_end(args);

    for(int i = 0; i < len; i+=1024)
	{
        const char* ptr = buffer + i;
		OutputDebugStringA(ptr);
	}

	free(buffer);
}

std::wstring CharToWCharStr(const char* str);
std::string  WCharToCharStr(const wchar_t* str);

void TraceCallStack();

const char* GetAliveFilePath(const char* paths[], const int pathCount);
const char* GetAliveFolderPath(const char* paths[], const int pathCount);


#include <System.Types.hpp>

struct sPathData
{
	UnicodeString name;
	std::vector< UnicodeString > paths;
};

class PathMgr
{
public:
	PathMgr() {};
	~PathMgr() {};

	static void PushPath(const wchar_t* name, const wchar_t* path, bool bTransform = true);
	static void RemovePath(const wchar_t* name);
	static UnicodeString GetPathFromName(const wchar_t* name);

	static UnicodeString GetPath(const wchar_t* path);

protected:
	static std::map<UINT, sPathData> pathItems;
	static bool isNameCharacter(const wchar_t c);
	static bool isName(const wchar_t* name);
	static UnicodeString transformPath(const wchar_t* path);
};

template<typename T>
class ringbuf
{
public:
    typedef T          value_type;
    typedef T         *pointer;
    typedef const T   *const_pointer;
    typedef T         &reference;
    typedef const T   &const_reference;
    typedef size_t     size_type;
    typedef ptrdiff_t  difference_type;

public:
    explicit ringbuf(unsigned int buf_size = 128) :
        _array(new T[buf_size]), _array_size(buf_size),
        _head(0),_tail(0),_used_space(0), _data_lost_cnt(0)
    {
    };
    ~ringbuf()
    {
        delete [] _array;
    };

    reference front() { return _array[_tail]; }
    reference back()  { return _array[_head]; }
    const_reference front() const { return _array[_tail]; }
    const_reference back() const  { return _array[_head]; }

    size_type size() const {return _used_space;};//{return (_head >= _tail) ? (_head - _tail) + 1 : (_array_size - _tail) + _head + 1;};
    size_type capacity() const { return _array_size; };
    bool empty() const {return _used_space == 0;};//{ return _head == _tail; };
    size_type max_size() const {
      return size_type(-1) /
                     sizeof(value_type);
    }

    reference       &operator[](size_t idx)
    {
        if( idx >= size())
        {
            assert(false);
        }
        return _array[(_tail + idx) % _array_size];
    }
    const_reference &operator[](size_t idx) const
    {
        if( idx >= size())
        {
            assert(false);
        }
        return _array[(_tail + idx) % _array_size];
    }

    void push_back(value_type &item) {
      if (!_used_space) {
        _array[_head] = item;
        _tail = _head;
        ++_used_space;
      }
      else if (_used_space != _array_size)
      {
        inc_head();
        _array[_head] = item;
      }
      else {
        // We always accept data when full
        // and lose the front()
        inc_tail();
        inc_head();
        _array[_head] = item;
        _data_lost_cnt++;
      }
    }
    void pop_front() { inc_tail(); }

    unsigned int head_idx() const {return _head;};
    unsigned int tail_idx() const {return _tail;};
    unsigned int data_lost_size(bool bResetLostCounter = false)
    {
        unsigned int tmp_cnt = _data_lost_cnt;
        if(bResetLostCounter) _data_lost_cnt = 0;
        return tmp_cnt;
    };

private:
    void inc_head()
    {
        ++ _head;
        ++ _used_space;
        if(_head >= _array_size)
            _head = 0;
    }
    void inc_tail()
    {
        ++ _tail;
        -- _used_space;
        if(_tail >= _array_size)
            _tail = 0;
    }
private:
    T* _array;
    unsigned int _array_size;
    unsigned int _head;
    unsigned int _tail;
    unsigned int _used_space;
    volatile unsigned int _data_lost_cnt;
};

#endif
